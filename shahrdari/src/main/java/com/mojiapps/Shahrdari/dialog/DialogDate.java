package com.mojiapps.Shahrdari.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.ahmadian.ruhallah.commons.utils.text.span.CustomSpannableStringConstructor;
import com.mohamadamin.persianmaterialdatetimepicker.date.DatePickerDialog;
import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.enums.Fonts;
import com.mojiapps.Shahrdari.utils.*;
import com.mojiapps.Shahrdari.webservice.Params;
import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.ArrayWheelAdapter;
import kankan.wheel.widget.adapters.NumericWheelAdapter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by mojtaba on 9/26/14.
 */
public class DialogDate extends AppCompatActivity {

    @Bind(R.id.txtToDate)
    TextView txtToDate;
    @Bind(R.id.txtFromDate)
    TextView txtFromDate;

    String[] years;
    CheckBox chkUpdateLicenses;

    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_select_date);

        chkUpdateLicenses = (CheckBox) findViewById(R.id.chkGetOnline);

        SpannableString spannableString = new CustomSpannableStringConstructor(this)
                .setText(getString(R.string.DialogDate))
                .build();
        setTitle(spannableString);

        ButterKnife.bind(this, this);
        configDate();

        final WheelView monthFrom = (WheelView) findViewById(R.id.monthFrom);
        final WheelView yearFrom = (WheelView) findViewById(R.id.yearFrom);
        final WheelView dayFrom = (WheelView) findViewById(R.id.dayFrom);

        final WheelView monthTo = (WheelView) findViewById(R.id.monthTo);
        final WheelView yearTo = (WheelView) findViewById(R.id.yearTo);
        final WheelView dayTo = (WheelView) findViewById(R.id.dayTo);

        String months[] = this.getResources().getStringArray(R.array.persian_months);

        Button btnOK = (Button) findViewById(R.id.btnOK);

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                /*String dayy;
                if (dayFrom.getCurrentItem() + 1 > 9)
                    dayy = Integer.toString(dayFrom.getCurrentItem() + 1);
                else
                    dayy = "0" + Integer.toString(dayFrom.getCurrentItem() + 1);

                String mont;
                if (monthFrom.getCurrentItem() + 1 > 9)
                    mont = Integer.toString(monthFrom.getCurrentItem() + 1);
                else
                    mont = "0" + Integer.toString(monthFrom.getCurrentItem() + 1);

                String dateFrom = years[yearFrom.getCurrentItem()] + "/" + mont + "/" + dayy;

                if (dayTo.getCurrentItem() + 1 > 9)
                    dayy = Integer.toString(dayTo.getCurrentItem() + 1);
                else
                    dayy = "0" + Integer.toString(dayTo.getCurrentItem() + 1);

                if (monthTo.getCurrentItem() + 1 > 9)
                    mont = Integer.toString(monthTo.getCurrentItem() + 1);
                else
                    mont = "0" + Integer.toString(monthTo.getCurrentItem() + 1);

                String dateTo = years[yearTo.getCurrentItem()] + "/" + mont + "/" + dayy;*/

                intent.putExtra("dateFrom", txtFromDate.getText().toString());
                intent.putExtra("dateTo", txtToDate.getText().toString());
                intent.putExtra("editLicenseConfirmNoViolation", chkUpdateLicenses.isChecked());
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        OnWheelChangedListener listenerFrom = new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                updateDays(yearFrom, monthFrom, dayFrom);
            }
        };

        OnWheelChangedListener listenerTo = new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                updateDays(yearTo, monthTo, dayTo);
            }
        };

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String date = simpleDateFormat.format(Calendar.getInstance().getTime());
        date = CalendarTool.SiminehCalendar.getIranianDate(Integer.parseInt(date.substring(0, 4)), Integer.parseInt(date.substring(5, 7)), Integer.parseInt(date.substring(8, 10))).toString();

        // month
        int curMonth = Integer.parseInt(date.length() < 10 ? "0" + date.substring(5, 6) : date.substring(5, 7)) - 1;
        monthFrom.setViewAdapter(new DateArrayAdapter(this, months, curMonth));
        monthFrom.setCurrentItem(curMonth);
        monthFrom.addChangingListener(listenerFrom);

        monthTo.setViewAdapter(new DateArrayAdapter(this, months, curMonth));
        monthTo.setCurrentItem(curMonth);
        monthTo.addChangingListener(listenerTo);


        // year
        int curYear = Integer.parseInt(date.substring(0, 4));

        years = new String[21];
        for (int i = curYear - 10, j = 0; i <= curYear + 10 && j < years.length; i++, j++) {
            years[j] = Integer.toString(i);
        }
        yearFrom.setViewAdapter(new DateArrayAdapter(this, years, curYear));
        yearFrom.setCurrentItem(10);
        yearFrom.addChangingListener(listenerFrom);

        yearTo.setViewAdapter(new DateArrayAdapter(this, years, curYear));
        yearTo.setCurrentItem(10);
        yearTo.addChangingListener(listenerFrom);

        //day

        updateDays(yearFrom, monthFrom, dayFrom);
        updateDays(yearTo, monthTo, dayTo);
        dayFrom.setCurrentItem(Integer.parseInt(date.split("/")[2]) - 1);
        dayTo.setCurrentItem(Integer.parseInt(date.split("/")[2]) - 1);
    }

    private void configDate() {
        txtFromDate.setText(DateUtils.MiladiToPersianDate(Calendar.getInstance().getTime()));
        txtToDate.setText(DateUtils.MiladiToPersianDate(Calendar.getInstance().getTime()));

        txtFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JalaliCalendar jalaliCalendar = new JalaliCalendar();
                try {
                    Date date = dateFormat.parse(txtFromDate.getText().toString());
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    DatePickerDialog.newInstance(
                            mFromDateSetListener,
                            calendar.get(Calendar.YEAR),
                            calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH)
                    ).show(getFragmentManager(), "Datepickerdialog");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        txtToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JalaliCalendar jalaliCalendar = new JalaliCalendar();
                try {
                    Date date = dateFormat.parse(txtToDate.getText().toString());
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    DatePickerDialog.newInstance(
                            mToDateSetListener,
                            calendar.get(Calendar.YEAR),
                            calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH)
                    ).show(getFragmentManager(), "Datepickerdialog");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private DatePickerDialog.OnDateSetListener mFromDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                    monthOfYear++;
                    txtFromDate.setText(year + "/" + monthOfYear + "/" + dayOfMonth);
                    Log.e("txtFromDate", year + "/" + monthOfYear + "/" + dayOfMonth);
                }
            };

    private DatePickerDialog.OnDateSetListener mToDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                    monthOfYear++;
                    txtToDate.setText(year + "/" + monthOfYear + "/" + dayOfMonth);
                    Log.e("txtToDate", year + "/" + monthOfYear + "/" + dayOfMonth);
                }
            };

    void updateDays(WheelView year, WheelView month, WheelView day) {
        if (month.getCurrentItem() >= 0 && month.getCurrentItem() <= 5) {
            day.setViewAdapter(new DateNumericAdapter(this, 1, 31, day.getCurrentItem()));
        } else if (month.getCurrentItem() >= 6 && month.getCurrentItem() <= 10) {
            day.setViewAdapter(new DateNumericAdapter(this, 1, 30, day.getCurrentItem()));
            if (day.getCurrentItem() == 30)
                day.setCurrentItem(29);
        } else {
            List<Integer> leapYears = new ArrayList<Integer>();
            leapYears.add(1383);
            leapYears.add(1387);
            leapYears.add(1391);
            leapYears.add(1395);
            leapYears.add(1399);
            leapYears.add(1403);
            if (leapYears.contains(Integer.parseInt(years[year.getCurrentItem()]))) {
                if (day.getCurrentItem() == 30)
                    day.setCurrentItem(29);
                day.setViewAdapter(new DateNumericAdapter(this, 1, 30, day.getCurrentItem()));
            } else {
                if (day.getCurrentItem() == 29 || day.getCurrentItem() == 30)
                    day.setCurrentItem(28);
                day.setViewAdapter(new DateNumericAdapter(this, 1, 29, day.getCurrentItem()));
            }
        }
    }

    private class DateArrayAdapter extends ArrayWheelAdapter<String> {
        int currentItem;
        int currentValue;

        public DateArrayAdapter(Context context, String[] items, int current) {
            super(context, items);
            this.currentValue = current;
            setTextSize(20);
        }

        @Override
        protected void configureTextView(TextView view) {
            super.configureTextView(view);
            if (currentItem == currentValue) {
                view.setTextColor(0xFF0000F0);
            }
            view.setText(Texts.ChangeToUnicode(view.getText().toString()));
            //view.setTypeface(Typeface.SANS_SERIF);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            currentItem = index;
            return super.getItem(index, cachedView, parent);
        }
    }

    private class DateNumericAdapter extends NumericWheelAdapter {
        int currentItem;
        int currentValue;

        public DateNumericAdapter(Context context, int minValue, int maxValue, int current) {
            super(context, minValue, maxValue);
            this.currentValue = current;
            setTextSize(20);
        }

        @Override
        protected void configureTextView(TextView view) {
            super.configureTextView(view);
            if (currentItem == currentValue) {
                view.setTextColor(0xFF0000F0);
            }
            view.setText(Texts.ChangeToUnicode(view.getText().toString()));
            //view.setTypeface(Typeface.SANS_SERIF);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            currentItem = index;
            return super.getItem(index, cachedView, parent);
        }
    }
}