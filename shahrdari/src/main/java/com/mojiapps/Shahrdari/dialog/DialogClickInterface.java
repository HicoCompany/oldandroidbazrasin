package com.mojiapps.Shahrdari.dialog;

/**
 * Created by Neda-PC on 9/18/2017.
 */

import android.content.DialogInterface;

/**
 * Created by sayosoft on 26/7/16.
 */
public interface DialogClickInterface {

    public void onClickPositiveButton(DialogInterface pDialog, int pDialogIntefier);

    public void onClickNegativeButton(DialogInterface pDialog, int pDialogIntefier);
}
