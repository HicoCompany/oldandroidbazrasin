package com.mojiapps.Shahrdari.dialog;

/**
 * Created by Neda-PC on 9/18/2017.
 */
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.mojiapps.Shahrdari.R;

public class CustomAlertDialogOk implements DialogClickInterfaceOk, DialogInterface.OnClickListener {

    public static CustomAlertDialogOk mDialog;
    public DialogClickInterfaceOk mDialogClickInterface;
    private int mDialogIdentifier;
    private Context mContext;

    public static CustomAlertDialogOk getInstance() {

        if (mDialog == null)
            mDialog = new CustomAlertDialogOk();

        return mDialog;

    }

    /**
     * Show confirmation dialog with two buttons
     *
     * @param pMessage
     * @param pPositiveButton
     * @param pContext
     * @param pDialogIdentifier
     */
    public void showConfirmDialog(String pTitle,String pMessage,
                                  String pPositiveButton,
                                  Context pContext, final int pDialogIdentifier) {

        mDialogClickInterface = (DialogClickInterfaceOk) pContext;
        mDialogIdentifier = pDialogIdentifier;
        mContext = pContext;

        final Dialog dialog = new Dialog(pContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_ok);

        if(!pTitle.equals(""))
        {
            TextView title = (TextView) dialog.findViewById(R.id.textTitle);
            title.setText(pTitle);
            title.setVisibility(View.VISIBLE);
        }

        TextView text = (TextView) dialog.findViewById(R.id.textDialog);
        text.setText(pMessage);
        Button button = (Button) dialog.findViewById(R.id.button);
        button.setText(pPositiveButton);

        dialog.setCancelable(false);
        dialog.show();      // if decline button is clicked, close the custom dialog
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                mDialogClickInterface.onClickOkButton(dialog,pDialogIdentifier);
            }
        });


    }



    @Override
    public void onClick(DialogInterface pDialog, int pWhich) {

        switch (pWhich) {
            case DialogInterface.BUTTON_POSITIVE:
                mDialogClickInterface.onClickOkButton(pDialog, mDialogIdentifier);

                break;

        }

    }



    @Override
    public void onClickOkButton(DialogInterface pDialog, int pDialogIntefier) {

    }
}