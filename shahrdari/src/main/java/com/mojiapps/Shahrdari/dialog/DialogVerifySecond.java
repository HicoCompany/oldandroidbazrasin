package com.mojiapps.Shahrdari.dialog;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.ahmadian.ruhallah.commons.utils.text.Typefaces;
import com.ahmadian.ruhallah.commons.utils.text.span.CustomSpannableStringConstructor;
import com.mojiapps.Shahrdari.Configuration;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.database.DatabaseService;
import com.mojiapps.Shahrdari.enums.SharedPrefs;
import com.mojiapps.Shahrdari.models.License;
import com.mojiapps.Shahrdari.models.ResultAdd;
import com.mojiapps.Shahrdari.webservice.Params;
import com.mojiapps.Shahrdari.webservice.WebServiceSyncImp;

/**
 * Created by mojtaba on 9/25/14.
 */
public class DialogVerifySecond extends AppCompatActivity implements View.OnClickListener{
    EditText txtNote;
    Button btnOk;
    int licenseId;
    RadioGroup confirmField;
    RadioButton confirm;
    RadioButton dontConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_verify_second);

        if (getIntent() != null)
            if (getIntent().getStringExtra(Params.TITLE) != null) {
                SpannableString spannableString = new CustomSpannableStringConstructor(this)
                        .setText(getIntent().getStringExtra(Params.TITLE))
                        .build();
                setTitle(spannableString);
            }

        licenseId = getIntent().getExtras().getInt(Params.LICENSE_ID);
        txtNote = (EditText) findViewById(R.id.txtNote);
        btnOk = (Button) findViewById(R.id.cmdOk);
        confirmField = (RadioGroup) findViewById(R.id.confirm_field);
        confirm = (RadioButton) findViewById(R.id.confirm);
        dontConfirm = (RadioButton) findViewById(R.id.dont_confirm);
        confirm.setTypeface(Typefaces.get(getApplicationContext(), null));
        confirm.setTag(true);
        dontConfirm.setTypeface(Typefaces.get(getApplicationContext(), null));
        dontConfirm.setTag(false);

        License license = DatabaseService.getNotSendLicenseById(licenseId).get(0);
        txtNote.setText(license.getConfirmDescription2()!=null ? license.getConfirmDescription2():"");
        if (license.getConfirm2() == null) {
        }else if (license.getConfirm2()) {
            confirm.setChecked(true);
        }else {
            dontConfirm.setChecked(true);
        }

        btnOk.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.cmdOk){
            if (confirmField.getCheckedRadioButtonId() == -1 && txtNote.getText().toString().length() == 0)
                return;
            RadioButton radioButton = (RadioButton) findViewById(confirmField.getCheckedRadioButtonId());
            new DatabaseUpdater(licenseId, (Boolean) radioButton.getTag(), txtNote.getText().toString()).execute();
        }
    }

    private void done(boolean synced) {
        RadioButton radioButton = (RadioButton) findViewById(confirmField.getCheckedRadioButtonId());
        Intent intent = new Intent();
        intent.putExtra(Params.LICENSE_ID, licenseId);
        intent.putExtra("position", getIntent().getExtras().getInt("position"));
        intent.putExtra("answer", (Boolean) radioButton.getTag());
        intent.putExtra("description", txtNote.getText().toString());
        intent.putExtra("synced", synced);
        setResult(RESULT_OK, intent);
        finish();
    }

    private class DatabaseUpdater extends AsyncTask<Void, Void, ResultAdd> {
        ProgressDialog progressDialog;
        int licenseId;
        boolean isConfirmNoViolation;
        String confirmNoViolationDescription;

        DatabaseUpdater(int licenseId, boolean isConfirmNoViolation, String confirmNoViolationDescription) {
            this.licenseId = licenseId;
            this.isConfirmNoViolation = isConfirmNoViolation;
            this.confirmNoViolationDescription = confirmNoViolationDescription;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(DialogVerifySecond.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
                progressDialog.setProgressNumberFormat("");
                progressDialog.setProgressPercentFormat(null);
            }
            progressDialog.setMessage(getResources().getString(R.string.fetching_data_from_server));
            progressDialog.setCanceledOnTouchOutside(false);

            progressDialog.show();
        }

        @Override
        protected ResultAdd doInBackground(Void... voids) {

            int id = Configuration.getInstance().getInt(SharedPrefs.ID);
            return WebServiceSyncImp.editLicenseConfirm(licenseId, isConfirmNoViolation, confirmNoViolationDescription, id);
        }

        @Override
        protected void onPostExecute(ResultAdd result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result!= null && result.getState() != null && Integer.valueOf(result.getState()) == 1) {
                RadioButton radioButton = (RadioButton) findViewById(confirmField.getCheckedRadioButtonId());
                DatabaseService.verifyLicense2(licenseId, (Boolean) radioButton.getTag(), txtNote.getText().toString());
                done(true);
            }else {
                Toast.makeText(getApplicationContext(), result.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }
    }
}
