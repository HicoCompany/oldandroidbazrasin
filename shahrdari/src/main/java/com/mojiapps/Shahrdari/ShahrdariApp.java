package com.mojiapps.Shahrdari;

import android.app.Application;
import android.content.*;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import com.ahmadian.ruhallah.commons.utils.text.Typefaces;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.mojiapps.Shahrdari.database.ShahrdariDatabaseHelper;
import com.mojiapps.Shahrdari.enums.Fonts;
import com.mojiapps.Shahrdari.webservice.ShahrdariWebService;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mojtaba on 3/21/14.
 */
public class ShahrdariApp extends Application {

     public static final String BASE_URL ="http://waste.qom.ir";//"http://beta.motagh.in:8485";//"http://88.99.79.99:8099";//"http://waste.qom.ir";
   // public static final String BASE_URL = "http://94.183.179.17:9000";

    static Context shahrdariContext;
    static GPSService gpsService;
    static boolean gpsServiceIsConnecting;
    static ShahrdariWebService shahrdariWebService;

    private static ShahrdariDatabaseHelper mShahrdariDatabaseHelper = null;

    private static final OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build();

    public ShahrdariApp(){
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        shahrdariContext = getApplicationContext();

        initOkHttp();

        createShahrdariService();

        /*
        set default font for smart widgets
         */
        Typefaces.setDefaultFont(getApplicationContext(), Fonts.IRAN_SANS.toString());
    }

    private void initOkHttp() {
        okHttpClient.newBuilder().networkInterceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder()
                        .build();
                return chain.proceed(request);
            }
        });
    }

    public static void createShahrdariService() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(GsonSettings.getGson()))
                .build();

        shahrdariWebService = retrofit.create(ShahrdariWebService.class);
    }

    public static Context getContext(){
        return shahrdariContext;
    }

    public static ShahrdariWebService getShahrdariWebService(){
        return shahrdariWebService;
    }

    public static ShahrdariDatabaseHelper getShahrdariDatabaseHelper(){
        if (mShahrdariDatabaseHelper == null) {
            mShahrdariDatabaseHelper = ShahrdariDatabaseHelper.getHelper(ShahrdariApp.getContext());// OpenHelperManager.getHelper(BoomrangApplication.getContext(), MessagesDatabaseHelper.class);
        }
        return mShahrdariDatabaseHelper;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if(mShahrdariDatabaseHelper != null ){
            OpenHelperManager.releaseHelper();
            mShahrdariDatabaseHelper = null;
        }
    }

    public static void sendLocalBroadcast(Intent intent){
        LocalBroadcastManager.getInstance(shahrdariContext).sendBroadcast(intent);
    }

    public static void registerLocalReceiver(Context context,BroadcastReceiver broadcastReceiver, IntentFilter intentFilter){
        LocalBroadcastManager.getInstance(context).registerReceiver(broadcastReceiver, intentFilter);
    }

    public static void unregisterLocalReceiver(Context context, BroadcastReceiver broadcastReceiver){
        LocalBroadcastManager.getInstance(context).unregisterReceiver(broadcastReceiver);
    }

    public static GPSService getGpsService() {
        if (gpsService != null)
            return gpsService;

        if (!gpsServiceIsConnecting)
            startGpsService();

        while (gpsService == null){
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return gpsService;
    }

    private static void startGpsService() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                gpsServiceIsConnecting = true;

                ServiceConnection serviceConnection = new ServiceConnection() {
                    @Override
                    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                        gpsService = ((GPSService.GPSServiceBinder) iBinder).getService();
                        gpsServiceIsConnecting = false;
                    }

                    @Override
                    public void onServiceDisconnected(ComponentName componentName) {
                        gpsService = null;
                        gpsServiceIsConnecting = false;
                    }
                };
                shahrdariContext.bindService(new Intent(shahrdariContext, GPSService.class), serviceConnection, BIND_AUTO_CREATE);
            }
        }).start();
    }

    public static String replaceNumbers(String str) {
        str=str.replaceAll("۱", "1");
        str=str.replaceAll("۲", "2");
        str=str.replaceAll("۳", "3");
        str=str.replaceAll("۴", "4");
        str=str.replaceAll("۵", "5");
        str=str.replaceAll("۶", "6");
        str=str.replaceAll("۷", "7");
        str=str.replaceAll("۸", "8");
        str=str.replaceAll("۹", "9");
        str=str.replaceAll("۰", "0");
        return str;
    }
}