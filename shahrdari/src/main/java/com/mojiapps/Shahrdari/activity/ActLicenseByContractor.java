package com.mojiapps.Shahrdari.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.webservice.Params;

/**
 * Created by mojtaba on 22/10/2014.
 */
public class ActLicenseByContractor extends ActivityConfigAware {

    ActionBar actionBar;
    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_license_by_contractor);

        int contractorId = getIntent().getExtras().getInt("contractorId");
        String contractorName = getIntent().getExtras().getString("contractorName");

        /*FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Bundle bundle = new Bundle();
        bundle.putInt("contractorId", contractorId);
        FragmentLicenseList fragLicense = new FragmentLicenseList();
        fragLicense.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragLicenseByContractor, fragLicense);
        fragmentTransaction.commit();*/

        Fragment fragment = new FragmentLicenseList();
        Bundle bundle = new Bundle();
        Log.e("1",getIntent().getExtras().getInt("contractorId")+"");
        bundle.putInt(Params.CONTRACTOR_ID, contractorId);
        fragment.setArguments(bundle);
        FragmentTransaction tran = getSupportFragmentManager().beginTransaction();
        tran.replace(R.id.fragLicenseByContractor, fragment);
        tran.commit();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setIcon(R.drawable.hico_logo_last);//ic_launcher_light

        actionBar.setTitle("لیست مجوزهای");
        actionBar.setSubtitle(contractorName);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
