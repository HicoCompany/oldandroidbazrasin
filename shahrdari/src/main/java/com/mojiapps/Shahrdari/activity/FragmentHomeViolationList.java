package com.mojiapps.Shahrdari.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ahmadian.ruhallah.commons.fragment.list.FactorySmartItemView;
import com.ahmadian.ruhallah.commons.fragment.list.SmartFragment;
import com.ahmadian.ruhallah.commons.fragment.list.SmartItemView;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.database.DatabaseService;
import com.mojiapps.Shahrdari.items.ItemViewHomeViolation;
import com.mojiapps.Shahrdari.models.HomeViolation;

import java.util.List;

/**
 * Created by ruhallah-PC on 12/16/2016.
 */

public class FragmentHomeViolationList extends SmartFragment<HomeViolation> {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setType(Type.STATIC);
        View view =  super.onCreateView(inflater, container, savedInstanceState);
        recyclerView.setBackgroundColor(getResources().getColor(R.color.fragment_backcolor));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRefreshing(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        list.clear();
        loadData();
    }

    @Override
    protected FactorySmartItemView getViewFactory() {
        return new FactorySmartItemView() {
            @Override
            public SmartItemView makeView(String searchPhrase) {
                ItemViewHomeViolation itemView = new ItemViewHomeViolation(getContext());
                itemView.setSearchPhrase(searchPhrase);
                itemView.setOnItemClickListener(new SmartItemView.OnItemClickListener<HomeViolation>() {
                    @Override
                    public void click(HomeViolation item, View view, int position) {
                    }
                });
                itemView.setOnImageClickListener(new SmartItemView.OnItemClickListener<HomeViolation>() {
                    @Override
                    public void click(HomeViolation item, View view, int position) {
                        Intent intent = new Intent(getActivity(), PicturesActivity.class);
                        intent.putExtra("id", item.getHomeViolationId());
                        intent.putExtra("type", PicturesActivity.ViolationType.HOME);
                        startActivity(intent);
                    }
                });
                return itemView;
            }
        };
    }

    @Override
    protected List<HomeViolation> fetchMoreDataAsync(int startPage, int count) {
        return null;
    }

    @Override
    protected List<HomeViolation> fetchDataAsync() {
        return DatabaseService.getViolationEstate();
    }

    @Override
    protected void onOperationFailed() {

    }

    /*@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfi
        gurationChanged(newConfig);
        int mSpanCount = getActivity().getResources().getInteger(R.integer.NumberOfColumn);
        setColumnCount(mSpanCount);
    }*/

}
