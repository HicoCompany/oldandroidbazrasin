package com.mojiapps.Shahrdari.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.mojiapps.Shahrdari.Configuration;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.ShahrdariApp;
import com.mojiapps.Shahrdari.database.CarType;
import com.mojiapps.Shahrdari.database.DatabaseService;
import com.mojiapps.Shahrdari.database.Officer;
import com.mojiapps.Shahrdari.enums.Fonts;
import com.mojiapps.Shahrdari.enums.SharedPrefs;
import com.mojiapps.Shahrdari.models.Car;
import com.mojiapps.Shahrdari.models.CarViolation;
import com.mojiapps.Shahrdari.models.Certificate;
import com.mojiapps.Shahrdari.models.Contractor;
import com.mojiapps.Shahrdari.models.HomeViolation;
import com.mojiapps.Shahrdari.models.License;
import com.mojiapps.Shahrdari.models.Personnel;
import com.mojiapps.Shahrdari.models.StaffCarViolation;
import com.mojiapps.Shahrdari.models.Violation;
import com.mojiapps.Shahrdari.utils.ConnectionDetector;
import com.mojiapps.Shahrdari.utils.Texts;
import com.mojiapps.Shahrdari.utils.Utils;
import com.mojiapps.Shahrdari.webservice.CarOwnerViolations;
import com.mojiapps.Shahrdari.webservice.ConfirmLicense;
import com.mojiapps.Shahrdari.webservice.ConfirmLicenses;
import com.mojiapps.Shahrdari.webservice.HomeViolations;
import com.mojiapps.Shahrdari.webservice.ShahrdariWebService;
import com.mojiapps.Shahrdari.webservice.WebResult;
import com.mojiapps.Shahrdari.webservice.WebServiceSyncImp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mojtaba on 9/21/14.
 */
public class FragUpdate extends Fragment implements View.OnClickListener {

    Button btnUpdateAll;
    Button btnUpdateCarType;
    Button btnUpdateViolationType;
    Button btnUpdateCertificateType;
    Button btnUpdatePersonnel;
    Button btnUpdateContractor;
    Button btnUpdateCar;
    Button btnUpdateLicense;
    Button btnUpdateViolationMember;
    Button btnUpdateViolationNotMember;
    Button btnUpdateViolationEstate;
    Button btnUpdateConfirmLicense;

    ShahrdariWebService shahrdariWebService;
    ProgressDialog progressDialog;
    ConnectionDetector cd;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_update, container, false);

        init(view);

        shahrdariWebService = ShahrdariApp.getShahrdariWebService();

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            progressDialog.setProgressNumberFormat("");
            progressDialog.setProgressPercentFormat(null);
        }
        progressDialog.setMessage(getResources().getString(R.string.fetching_data_from_server));
        progressDialog.setCanceledOnTouchOutside(false);

        cd = new ConnectionDetector(getActivity());
//        shahrdariWebService.confirmLicense(new ConfirmLicense(1, 5, true, "تست"), new Callback<ConfirmLicense>() {
//            @Override
//            public void success(ConfirmLicense confirmLicense, Response response) {
//               Toast.makeText(getActivity(), response.getStatus() + " " + response.getBody() + " " + response.getReason(), Toast.LENGTH_LONG).show();
//            }
//
//            @Override
//            public void failure(RetrofitError retrofitError) {
//                Toast.makeText(getActivity(), retrofitError.getMessage(), Toast.LENGTH_LONG).show();
//            }
//        });

        //shahrdariWebService.updateHomeViolation();
        return view;
    }

    private void init(View v) {
        btnUpdateAll = (Button) v.findViewById(R.id.btnUpdateAll);
        btnUpdateCarType = (Button) v.findViewById(R.id.btnUpdateCarType);
        btnUpdateViolationType = (Button) v.findViewById(R.id.btnUpdateViolationType);
        btnUpdateCertificateType = (Button) v.findViewById(R.id.btnUpdateCertificateType);
        btnUpdatePersonnel = (Button) v.findViewById(R.id.btnUpdatePersonnel);
        btnUpdateContractor = (Button) v.findViewById(R.id.btnUpdateContractor);
        btnUpdateCar = (Button) v.findViewById(R.id.btnUpdateCar);
        btnUpdateLicense = (Button) v.findViewById(R.id.btnUpdateLicense);
        btnUpdateViolationMember = (Button) v.findViewById(R.id.btnUpdateViolationMember);
        btnUpdateViolationNotMember = (Button) v.findViewById(R.id.btnUpdateViolationNotMember);
        btnUpdateViolationEstate = (Button) v.findViewById(R.id.btnUpdateViolationEstate);
        btnUpdateConfirmLicense = (Button) v.findViewById(R.id.btnUpdateConfirmLicense);

        btnUpdateAll.setTypeface(Texts.getFont(Fonts.DEFAULT));
        btnUpdateCarType.setTypeface(Texts.getFont(Fonts.DEFAULT));
        btnUpdateViolationType.setTypeface(Texts.getFont(Fonts.DEFAULT));
        btnUpdateCertificateType.setTypeface(Texts.getFont(Fonts.DEFAULT));
        btnUpdatePersonnel.setTypeface(Texts.getFont(Fonts.DEFAULT));
        btnUpdateContractor.setTypeface(Texts.getFont(Fonts.DEFAULT));
        btnUpdateCar.setTypeface(Texts.getFont(Fonts.DEFAULT));
        btnUpdateLicense.setTypeface(Texts.getFont(Fonts.DEFAULT));
        btnUpdateViolationMember.setTypeface(Texts.getFont(Fonts.DEFAULT));
        btnUpdateViolationNotMember.setTypeface(Texts.getFont(Fonts.DEFAULT));
        btnUpdateViolationEstate.setTypeface(Texts.getFont(Fonts.DEFAULT));
        btnUpdateConfirmLicense.setTypeface(Texts.getFont(Fonts.DEFAULT));

        btnUpdateAll.setOnClickListener(this);
        btnUpdateCarType.setOnClickListener(this);
        btnUpdateViolationType.setOnClickListener(this);
        btnUpdateCertificateType.setOnClickListener(this);
        btnUpdatePersonnel.setOnClickListener(this);
        btnUpdateContractor.setOnClickListener(this);
        btnUpdateCar.setOnClickListener(this);
        btnUpdateLicense.setOnClickListener(this);
        btnUpdateViolationMember.setOnClickListener(this);
        btnUpdateViolationNotMember.setOnClickListener(this);
        btnUpdateViolationEstate.setOnClickListener(this);
        btnUpdateConfirmLicense.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getActivity(), getString(R.string.cant_connect_to_server), Toast.LENGTH_SHORT).show();
            return;
        }
        Calendar calendar = Calendar.getInstance();
        progressDialog.show();
        switch (v.getId()) {
            case R.id.btnUpdateAll:
                new DatabaseUpdater().execute();
                break;
            case R.id.btnUpdateCarType:
                Call<List<Car>> cars = shahrdariWebService.getCarTypes(Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS));
                cars.enqueue(new Callback<List<Car>>() {
                    @Override
                    public void onResponse(Call<List<Car>> call, Response<List<Car>> response) {
                        for (Car car : response.body()) {
                            DatabaseService.addCarType(new CarType(car.getCarId(), car.getTitle()));
                        }
                        Toast.makeText(getActivity(), "موفقیت آمیز بود", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<List<Car>> call, Throwable t) {
                        Toast.makeText(getActivity(), "دریافت اطلاعات انواع خودروها موفقیت آمیز نبود", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
                break;
            case R.id.btnUpdateViolationType:
                String date = Utils.GetDateTime1(SharedPrefs.DATE_VIOLATIONS);
                Call<List<Violation>> violations = shahrdariWebService.getViolationsByTime(Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS), date);
                violations.enqueue(new Callback<List<Violation>>() {
                    @Override
                    public void onResponse(Call<List<Violation>> call, Response<List<Violation>> response) {

                        Configuration.getInstance().setString(SharedPrefs.DATE_VIOLATIONS, Utils.getCurrentGaregoryDate1().replace("/","-"));

                        for (Violation violation : response.body()) {

                            if (DatabaseService.getViolationById(violation.getViolationId()) == null) {
                                DatabaseService.addViolation(new Violation(violation.getViolationId(), violation.getTitle()));
                            } else {
                                DatabaseService.UpdateViolation(violation);
                            }
                        }
                        Toast.makeText(getActivity(), "موفقیت آمیز بود", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<List<Violation>> call, Throwable t) {
                        Toast.makeText(getActivity(), "دریافت اطلاعات انواع تخلفات موفقیت آمیز نبود", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
                break;
            case R.id.btnUpdateCertificateType:
                date = Utils.GetDateTime1(SharedPrefs.DATE_CERTIFICATES);
                Call<List<Certificate>> issuedCertificates = shahrdariWebService.getCertificateTypesByTime(Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS), date);
                issuedCertificates.enqueue(new Callback<List<Certificate>>() {
                    @Override
                    public void onResponse(Call<List<Certificate>> call, Response<List<Certificate>> response) {

                        Configuration.getInstance().setString(SharedPrefs.DATE_CERTIFICATES, Utils.getCurrentGaregoryDate1().replace("/","-"));

                        for (Certificate issuedCertificate : response.body()) {
                            if (DatabaseService.getCertificateById(issuedCertificate.getCertificateId()) == null) {
                                DatabaseService.addCertificateType(issuedCertificate);
                            } else {
                                DatabaseService.UpdateCertificate(issuedCertificate);
                            }
                        }

                        Toast.makeText(getActivity(), "موفقیت آمیز بود", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<List<Certificate>> call, Throwable t) {
                        Toast.makeText(getActivity(), "دریافت اطلاعات انواع مدارک موفقیت آمیز نبود", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
                break;
            case R.id.btnUpdatePersonnel:

                date = Utils.GetDateTime1(SharedPrefs.DATE_PERSONNELS);

                Call<List<Personnel>> personnels = shahrdariWebService.getPersonnelsByTime(Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS), date);
                personnels.enqueue(new Callback<List<Personnel>>() {
                    @Override
                    public void onResponse(Call<List<Personnel>> call, Response<List<Personnel>> response) {

                        Configuration.getInstance().setString(SharedPrefs.DATE_PERSONNELS, Utils.getCurrentGaregoryDate1().replace("/","-"));

                        for (Personnel personnel : response.body()) {

                            if (DatabaseService.getOfficerById(personnel.getPersonnelId()) == null) {
                                DatabaseService.addOfficer(new Officer(personnel.getUserName(), personnel.getPassword(), personnel.getPersonnelId(),
                                        personnel.getName(), null, false));
                            } else {
                                DatabaseService.UpdateOfficer(new Officer(personnel.getUserName(), personnel.getPassword(), personnel.getPersonnelId(),
                                        personnel.getName(), null, false));
                            }
                        }
                        Toast.makeText(getActivity(), "موفقیت آمیز بود", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<List<Personnel>> call, Throwable t) {
                        Toast.makeText(getActivity(), "دریافت اطلاعات پرسنل موفقیت آمیز نبود", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
                break;
            case R.id.btnUpdateContractor:
                date = Utils.GetDateTime1(SharedPrefs.DATE_CONTRACTORS);
                Call<List<Contractor>> contractors = shahrdariWebService.getContractorsByTime(Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS), date);
                contractors.enqueue(new Callback<List<Contractor>>() {
                    @Override
                    public void onResponse(Call<List<Contractor>> call, Response<List<Contractor>> response) {
                        Configuration.getInstance().setString(SharedPrefs.DATE_CONTRACTORS, Utils.getCurrentGaregoryDate1().replace("/","-"));

                        for (Contractor contractor : response.body()) {
                            /*Calendar calendar = Calendar.getInstance();
                            calendar.setTime(new Date(contractor.getActivityDate().substring(0, 10).replace("-", "/")));*/
                            //####
                            if(DatabaseService.getContractorById(contractor.getContractorId())==null) {
                                DatabaseService.addContractor(contractor);
                            }else
                            {
                                DatabaseService.UpdateContract(contractor);
                            }
                        }
                        Toast.makeText(getActivity(), "موفقیت آمیز بود", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<List<Contractor>> call, Throwable t) {
                        Toast.makeText(getActivity(), "دریافت اطلاعات پیمانکاران موفقیت آمیز نبود", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
                break;
            case R.id.btnUpdateCar:

                date = Utils.GetDateTime1(SharedPrefs.DATE_CARS);

                Call<List<Car>> carOwners = shahrdariWebService.LoadCarsByTime(Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS), date);
                carOwners.enqueue(new Callback<List<Car>>() {
                    @Override
                    public void onResponse(Call<List<Car>> call, Response<List<Car>> response) {

                        Configuration.getInstance().setString(SharedPrefs.DATE_CARS, Utils.getCurrentGaregoryDate1().replace("/","-"));

                        for (Car carOwner : response.body()) {
                            /*Calendar calendar = Calendar.getInstance();
                            calendar.setTime(new Date(carOwner.getActivityDate().substring(0, 10).replace("-", "/")));
                            DatabaseService.addCar(new CarOLD(carOwner.getCarOwnerId(), carOwner.getCarTitle(),
                                    carOwner.getOwnerName(), carOwner.getNumberPlate(), calendar.getTime(),
                                    DatabaseService.getContractorById(carOwner.getContractorId())));*/
                        }
                        Toast.makeText(getActivity(), "موفقیت آمیز بود", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<List<Car>> call, Throwable t) {
                        Toast.makeText(getActivity(), "دریافت اطلاعات خودروها موفقیت آمیز نبود", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
                break;
            case R.id.btnUpdateLicense:
                Call<List<License>> Licenses = shahrdariWebService.getLicenses(Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS), "", "");
                Licenses.enqueue(new Callback<List<License>>() {
                    @Override
                    public void onResponse(Call<List<License>> call, Response<List<License>> response) {
                        for (License license : response.body()) {
                            /*Calendar calendarFrom = Calendar.getInstance();
                            calendarFrom.setTime(new Date(license.getFromDate().substring(0, 10).replace("-", "/")));

                            Calendar calendarTo = Calendar.getInstance();
                            calendarTo.setTime(new Date(license.getToDate().substring(0, 10).replace("-", "/")));

                            DatabaseService.addLicense(new License(license.getLicenseId(),
                                    license.getEmployerName(), license.getDischarge(), license.getAddress(), calendarFrom.getTime(),
                                    calendarTo.getTime(), license.isConfirmLicense(), license.getConfirmDescription(),
                                    DatabaseService.getContractorById(license.getContractorId())));*/
                        }
                        Toast.makeText(getActivity(), "موفقیت آمیز بود", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<List<License>> call, Throwable t) {
                        Toast.makeText(getActivity(), "دریافت اطلاعات مجوزها موفقیت آمیز نبود", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
                break;
            case R.id.btnUpdateViolationMember:
                List<StaffCarViolation> violationMembers = DatabaseService.getNotSendViolationMember();
                List<CarViolation> carOwnerViolations = new ArrayList<>();
                for (StaffCarViolation violationMember : violationMembers) {
                    /*calendar.setTime(violationMember.getData());
                    String date = CalendarTool.SiminehCalendar.getIranianDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                    carOwnerViolations.add(new CarViolation(violationMember.getId(), violationMember.getPersonnelId(),
                            violationMember.getCar().getCarId(), violationMember.getAddress(), date, violationMember.getGpsLocation(),
                            violationMember.getIssuedCertificate().getId(), violationMember.getViolationList(), violationMember.getPhoto()));*/
                }
                CarOwnerViolations cov = new CarOwnerViolations(carOwnerViolations);
                Call<WebResult> webResults = shahrdariWebService.addViolationMember(Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS), cov);
                webResults.enqueue(new Callback<WebResult>() {
                    @Override
                    public void onResponse(Call<WebResult> call, Response<WebResult> response) {
                        if (response.body().isData()) {
                            DatabaseService.setAllViolationMemberSent();
                            Toast.makeText(getActivity(), "موفقیت آمیز بود", Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(getActivity(), "خطا درارسال تخلفات", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<WebResult> call, Throwable t) {
                        Toast.makeText(getActivity(), "خطا درارسال تخلفات", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
                break;
            case R.id.btnUpdateViolationNotMember:
                /*List<CarViolation> violationNotMembers = DatabaseService.getNotSendViolationNotMember();
                List<NoMemberCarViolation> noMemberCarViolations = new ArrayList<>();
                for (CarViolation violationNotMember : violationNotMembers){
                    calendar.setTime(violationNotMember.getData());
                    String date = CalendarTool.SiminehCalendar.getIranianDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                    noMemberCarViolations.add(new NoMemberCarViolation(violationNotMember.getId(), violationNotMember.getPersonnelId(),
                            violationNotMember.getCarType().getId(), violationNotMember.isPlateType(), violationNotMember.getPlateNumber(),
                            violationNotMember.getColor(), violationNotMember.getOwnerName(), violationNotMember.getMobile(),
                            violationNotMember.getAddress(), violationNotMember.getAddressViolation(),date,violationNotMember.getGpsLocation(),
                            violationNotMember.getIssuedCertificate().getId(), violationNotMember.getViolationList(), violationNotMember.getPhoto()));
                }
                NoMemberCarViolations nmcv = new NoMemberCarViolations(noMemberCarViolations);
                Call<WebResult> nmcvWebResults = shahrdariWebService.updateViolationNotMember(nmcv);
                nmcvWebResults.enqueue(new Callback<WebResult>() {
                    @Override
                    public void onResponse(Call<WebResult> call, Response<WebResult> response) {
                        if (response.body().isData()) {
                            DatabaseService.setAllViolationNotMemberSent();
                            Toast.makeText(getActivity(), "موفقیت آمیز بود", Toast.LENGTH_SHORT).show();
                        }
                        else
                            Toast.makeText(getActivity(), "خطا درارسال تخلفات", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<WebResult> call, Throwable t) {
                        Toast.makeText(getActivity(), "خطا درارسال تخلفات", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });*/
                break;
            case R.id.btnUpdateViolationEstate:
                List<HomeViolation> violationEstates = DatabaseService.getNotSendHomeViolation();
                List<HomeViolation> homeViolations = new ArrayList<>();
                for (HomeViolation violationEstate : violationEstates) {
                    /*calendar.setTime(violationEstate.getData());
                    String date = CalendarTool.SiminehCalendar.getIranianDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                    homeViolations.add(new HomeViolation(violationEstate.getId(), violationEstate.getPersonnelId(),
                            violationEstate.getOwnerName(), violationEstate.getMobile(), violationEstate.getAddress(),
                            date, violationEstate.getViolationList(), violationEstate.getPhoto()));*/
                }
                HomeViolations hv = new HomeViolations(homeViolations);
                Call<WebResult> hvWebResults = shahrdariWebService.addViolationEstate(Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS), hv);
                hvWebResults.enqueue(new Callback<WebResult>() {
                    @Override
                    public void onResponse(Call<WebResult> call, Response<WebResult> response) {
                        if (response.body().isData()) {
                            DatabaseService.setAllViolationEstateSent();
                            Toast.makeText(getActivity(), "موفقیت آمیز بود", Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(getActivity(), "خطا درارسال تخلفات", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<WebResult> call, Throwable t) {
                        Toast.makeText(getActivity(), "خطا درارسال تخلفات", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
                break;
            case R.id.btnUpdateConfirmLicense:
                List<License> licenses = DatabaseService.getNotSendLicense(0);
                List<ConfirmLicense> confirmLicenses = new ArrayList<>();
                /*for (License license : licenses){
                    confirmLicenses.add(new ConfirmLicense(license.getConfirmPersonnelId(), license.getLicenseId(),
                            license.isVerified(), license.getVerifyNote()));
                }*/

                ConfirmLicenses cl = new ConfirmLicenses(confirmLicenses);
                Call<WebResult> clWebResults = shahrdariWebService.confirmLicense(Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS), cl);
                clWebResults.enqueue(new Callback<WebResult>() {
                    @Override
                    public void onResponse(Call<WebResult> call, Response<WebResult> response) {
                        if (response.body().isData()) {
                            DatabaseService.setAllConfirmLicenseSent();
                            Toast.makeText(getActivity(), "موفقیت آمیز بود", Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(getActivity(), "خطا درارسال تأیید مجوزها", Toast.LENGTH_SHORT).show();

                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<WebResult> call, Throwable t) {
                        Toast.makeText(getActivity(), "خطا درارسال تأیید مجوزها", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
                break;
        }
    }

    private class DatabaseUpdater extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            return WebServiceSyncImp.updateAll2();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setCancelable(false);
            builder.setTitle("توجه");
            if (result.equals("NoConnection"))
                builder.setMessage(getString(R.string.cant_connect_to_server));
            else
                builder.setMessage(result.split("-")[1] + " مورد از " + result.split("-")[0] + " مورد با موفقیت دریافت شد");

            builder.setNeutralButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }
}
