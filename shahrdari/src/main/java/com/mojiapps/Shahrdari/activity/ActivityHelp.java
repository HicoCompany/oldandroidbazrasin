package com.mojiapps.Shahrdari.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.dialog.CustomAlertDialog;
import com.mojiapps.Shahrdari.dialog.DialogClickInterface;

public class ActivityHelp extends AppCompatActivity implements DialogClickInterface {

    String Result="";

    private Toolbar toolbar;
    ActionBar actionBar;
    TextView txtHelp;
    TextView txtVersion;
    Button btnDownloadHelp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.activity_help);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
      //  actionBar.setIcon(R.drawable.hico_logo_last);//ic_launcher_light
     //   actionBar.setTitle(getString(R.string.add_violation));
        actionBar.setSubtitle(("راهنما"));
        actionBar.setDisplayHomeAsUpEnabled(true);
       // txtAmount=(TextView)findViewById(R.id.txtAmount);

        setFinishOnTouchOutside(false);
       // getAmount();
//        txtHelp=(TextView)findViewById(R.id.txtHelp);
//        txtHelp.setText("filkhjhdlkfhal;khhhhhhhhhhhhhhhfdgdg");
        txtVersion=(TextView)findViewById(R.id.txtVersion);
        txtVersion.setText("نسخه  "+getAppVersion());
        btnDownloadHelp=(Button) findViewById(R.id.btnDownloadHelp);
        btnDownloadHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CustomAlertDialog.getInstance().showConfirmDialog("توجه", "مایل به دانلود راهنمای اندروید مدیریت پسماند هیکو هستید؟", "بله", "خیر", ActivityHelp.this, 0);
            }
        });

    }
    public String getAppVersion() {
        String versionCode = "1.0";
        try {

            versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return versionCode;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClickPositiveButton(DialogInterface pDialog, int pDialogIntefier) {
        pDialog.dismiss();

        //http://waste.looptaxi.ir/MAP.HE.02.04.pdf
        String url = "http://88.99.79.99:7195//Files//MAP.HE.02.04.pdf";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @Override
    public void onClickNegativeButton(DialogInterface pDialog, int pDialogIntefier) {
        pDialog.dismiss();
    }
}
