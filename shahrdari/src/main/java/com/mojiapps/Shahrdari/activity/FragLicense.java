package com.mojiapps.Shahrdari.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import com.ahmadian.ruhallah.commons.utils.text.span.CustomSpannableStringConstructor;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.ShahrdariApp;

/**
 * Created by mojtaba on 9/21/14.
 */
public class FragLicense extends Fragment implements AdapterView.OnItemSelectedListener//, SearchView.OnQueryTextListener
 {

    FragmentManager fragmentManager;
    Spinner spLicenseType;
    public static int mode;
    public static String strSearch;
    public static int SearchType;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_license, container, false);

        setHasOptionsMenu(true);
        spLicenseType = (Spinner) view.findViewById(R.id.spLicenseCategory);
        spLicenseType.setAdapter(new ViolationAdapter(getActivity(), R.id.txtCategory, getResources().getStringArray(R.array.license_category)));
        spLicenseType.setOnItemSelectedListener(this);

        fragmentManager = getActivity().getSupportFragmentManager();
        //fragViolationList = fragmentManager.findFragmentById(R.id.fragViolationList);
        return view;
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        if (getArguments() == null) {
//            inflater.inflate(R.menu.menu_license, menu);
//
//            MenuItem searchItem = menu.findItem(R.id.action_search);
//            SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
//            searchView.setOnQueryTextListener(this);
//            searchView.setInputType(InputType.TYPE_CLASS_NUMBER);
//            searchView.setQueryHint(new CustomSpannableStringConstructor(getContext())
//                    .setText(getString(R.string.search_hint_license))
//                    .setForegroundColor(ContextCompat.getColor(getContext(), R.color.blue_100))
//                    .setRelativeSize(0.6f)
//                    .build());
//            ///////////////////////////////////////
//            MenuItem searchContractorItem = menu.findItem(R.id.action_search_contractor);
//            SearchView searchContractorView = (SearchView) MenuItemCompat.getActionView(searchContractorItem);
//            searchContractorView.setOnQueryTextListener(this);
//           // searchContractorView.setInputType(InputType.TYPE_CLASS_NUMBER);
//            searchContractorView.setQueryHint(new CustomSpannableStringConstructor(getContext())
//                    .setText(getString(R.string.search_hint_license_contractor))
//                    .setForegroundColor(ContextCompat.getColor(getContext(), R.color.blue_100))
//                    .setRelativeSize(0.6f)
//                    .build());
//
//        }
//
//
//        super.onCreateOptionsMenu(menu, inflater);
//    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.menu_license, menu);
//
//        MenuItem searchItem = menu.findItem(R.id.action_search);
//            SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
//            searchView.setOnQueryTextListener(this);
//            searchView.setInputType(InputType.TYPE_CLASS_NUMBER);
//            searchView.setQueryHint(new CustomSpannableStringConstructor(getContext())
//                    .setText(getString(R.string.search_hint_license))
//                    .setForegroundColor(ContextCompat.getColor(getContext(), R.color.blue_100))
//                    .setRelativeSize(0.6f)
//                    .build());
//            ///////////////////////////////////////
//            MenuItem searchContractorItem = menu.findItem(R.id.action_search_contractor);
//            SearchView searchContractorView = (SearchView) MenuItemCompat.getActionView(searchContractorItem);
//            searchContractorView.setOnQueryTextListener(this);
//           // searchContractorView.setInputType(InputType.TYPE_CLASS_NUMBER);
//            searchContractorView.setQueryHint(new CustomSpannableStringConstructor(getContext())
//                    .setText(getString(R.string.search_hint_license_contractor))
//                    .setForegroundColor(ContextCompat.getColor(getContext(), R.color.blue_100))
//                    .setRelativeSize(0.6f)
//                    .build());
//        //apply font
//        super.onCreateOptionsMenu(menu, inflater);
//    }

//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()){
//            case R.id.action_search_contractor:
//                SearchType=1;
//
//                return true;
//            case R.id.action_search:
//                SearchType=2;
//                return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()){
//
//            case R.id.action_search: {
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                mode=5;
//                fragmentTransaction.replace(R.id.fragLicenseList, new FragmentLicenseList());
//            }
//                return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (position==0) {
            mode=0;
            fragmentTransaction.replace(R.id.fragLicenseList, new FragmentLicenseList());
       }
        else if (position == 1)
        {
            fragmentTransaction.replace(R.id.fragLicenseList, new FragmentLicenseList());
            mode=1;
        }
        else if (position == 2) {

            fragmentTransaction.replace(R.id.fragLicenseList, new FragmentLicenseList());
            mode=2;
        }
        else if (position == 3) {

            fragmentTransaction.replace(R.id.fragLicenseList, new FragmentLicenseList());
            mode=3;
        }  else if (position == 4) {

            fragmentTransaction.replace(R.id.fragLicenseList, new FragmentLicenseList());
            mode=4;
        }
        fragmentTransaction.commit();
      //  position=5;

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        spLicenseType.setSelected(true);

    }

//    @Override
//    public boolean onQueryTextSubmit(String query) {
//        return false;
//    }
//
//    @Override
//    public boolean onQueryTextChange(String s) {
//
//        strSearch=s;
//
//
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        mode=5;
//        fragmentTransaction.replace(R.id.fragLicenseList, new FragmentLicenseList());
//        fragmentTransaction.commit();
//
//        return true;
//    }

    private class ViolationAdapter extends ArrayAdapter<String>{
        private String[] values;
        TextView txtCategory;
        ImageView imgCategory;

        public ViolationAdapter(Context context, int textViewResourceId,   String[] data){
            super(context, textViewResourceId, data);
            this.values = data;
        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) ShahrdariApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row= convertView;

            if(row == null){
                row = inflater.inflate(R.layout.license_category_spinner_row, parent, false);
                txtCategory = (TextView) row.findViewById(R.id.txtCategory);
                imgCategory = (ImageView) row.findViewById(R.id.imgCategory);
            }
            else
            {
                txtCategory = (TextView) convertView.findViewById(R.id.txtCategory);
                imgCategory = (ImageView) convertView.findViewById(R.id.imgCategory);
            }

            txtCategory.setText(values[position]);
            /*txtCategory.setTypeface(Texts.getFont(Fonts.DEFAULT));*/


            String pngName = "";
            if (position == 0)
                pngName = "license_all";
            else if (position == 1)
                pngName = "license_area";
            else if (position == 2){
                pngName = "license_date";
            }
            else if (position == 3){
                pngName = "license_my";
            }
            else if (position == 4){
                pngName = "license_ok";
            }

            imgCategory.setImageResource(getContext().getResources().getIdentifier(pngName, "drawable", getContext().getPackageName()));
            //imgCategory.setImageResource(R.drawable.arrow_down_drop_sppiner);
            return row;
        }

    }

    public int getMyMode(){
        return mode;
    }
}
