package com.mojiapps.Shahrdari.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.ShahrdariApp;
import com.mojiapps.Shahrdari.database.DatabaseService;
import com.mojiapps.Shahrdari.dialog.CustomAlertDialog;
import com.mojiapps.Shahrdari.dialog.CustomAlertDialogOk;
import com.mojiapps.Shahrdari.dialog.DialogClickInterface;
import com.mojiapps.Shahrdari.dialog.DialogClickInterfaceOk;
import com.mojiapps.Shahrdari.enums.Fonts;
import com.mojiapps.Shahrdari.utils.ConnectionDetector;
import com.mojiapps.Shahrdari.utils.Texts;
import com.mojiapps.Shahrdari.webservice.WebServiceSyncImp;

public class ActLogin extends ActivityConfigAware implements OnClickListener,DialogClickInterface,DialogClickInterfaceOk{

    EditText txtUserName;
    EditText txtPassword;
    TextView txtVersion;
   // EditText txtServerAddress;
    Button btnLogin;

   // ActionProcessButton btnUpdate;
    ConnectionDetector cd;
    private int identifier = 0;
    boolean isFirstInstall=false;
    int CountPersonnel=-1;
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);
        outState.putString("DO NOT CRASH", "OK");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.act_login);

        txtUserName = (EditText) findViewById(R.id.txtUserName);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        //txtServerAddress = (EditText) findViewById(R.id.txtServerAddress);

        btnLogin = (Button) findViewById(R.id.cmdLogin);
        btnLogin.setOnClickListener(this);
        btnLogin.setTypeface(Texts.getFont(Fonts.DEFAULT));

//        txtUserName.setTypeface(Texts.getFont(Fonts.DEFAULT));
//        txtPassword.setTypeface(Texts.getFont(Fonts.DEFAULT));

     //   btnUpdate = (ActionProcessButton) findViewById(R.id.cmdUpdate);
     //   btnUpdate.setMode(ActionProcessButton.Mode.PROGRESS);
     //   btnUpdate.setOnClickListener(this);
       // btnLogin.setBackgroundColor(R.color.HicoRed);
    //    btnUpdate.setTypeface(Texts.getFont(Fonts.DEFAULT));

        cd = new ConnectionDetector(getApplicationContext());

        SharedPreferences sharedPreferences = getSharedPreferences("UserLogin", MODE_PRIVATE);
        txtUserName.setText(sharedPreferences.getString("user", ""));
        //txtPassword.setText(sharedPreferences.getString("password", ""));

        txtVersion=(TextView)findViewById(R.id.txtVersion);
        txtVersion.setText("نسخه  "+getAppVersion());

    }

    public String getAppVersion() {
        String versionCode = "1.0";
        try {

            versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return versionCode;
    }

    @Override
    public void onClick(View v) {


        if (v.getId() == R.id.cmdLogin) {
            if (txtUserName.getText().toString().equals("") || txtPassword.getText().toString().equals("")) {
                Toast.makeText(this, "نام کاربری و رمز عبور را وارد کنید", Toast.LENGTH_LONG).show();
                return;
            }

            CountPersonnel=DatabaseService.getPersonnelsCount();
            if(CountPersonnel==-1 || CountPersonnel==0)
            {

                new DatabaseUpdater().execute();

            }
            else
            {


                String userName = ShahrdariApp.replaceNumbers(txtUserName.getText().toString());
                String password = ShahrdariApp.replaceNumbers(txtPassword.getText().toString());
                WifiManager manager = (WifiManager)getApplicationContext(). getSystemService(Context.WIFI_SERVICE);
                WifiInfo info = manager.getConnectionInfo();
                String address = info.getMacAddress();

                if (DatabaseService.checkLogin(userName, password,address))
                 {
                    SharedPreferences sharedPreferences = getSharedPreferences("UserLogin", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("user", txtUserName.getText().toString());
                    //editor.putString("password", txtUserName.getText().toString());
                    editor.commit();
//                int a=-1;
//                a=DatabaseService.getPersonnelsCount();

                    Intent i = new Intent(this, ActMain.class);
                    i.putExtra("isFirstInstall", isFirstInstall);
                    startActivity(i);
                    finish();
                } else {


                    int a = -1;
                    a = DatabaseService.getPersonnelsCount();
                    CustomAlertDialog.getInstance().showConfirmDialog("خطا", "\"نام کاربری یا رمز عبور واردشده صحیح نیست!\""/* + "تعداد پرسنل=" + String.valueOf(a)*/, "تلاش مجدد", "خروج", this, identifier);

                }
            }
        }
//        else if (v.getId() == R.id.cmdUpdate) {
//            new DatabaseUpdater().execute();
//        }
    }

    @Override
    public void onClickPositiveButton(DialogInterface pDialog, int pDialogIntefier) {
        if (pDialogIntefier == 0)
          pDialog.cancel();
    }

    @Override
    public void onClickNegativeButton(DialogInterface pDialog, int pDialogIntefier) {
        if (pDialogIntefier == 0)
           finish();
    }

    @Override
    public void onClickOkButton(DialogInterface pDialog, int pDialogIntefier) {
        if (pDialogIntefier == 0)
            pDialog.cancel();

    }

    private class DatabaseUpdater extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(ActLogin.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
                progressDialog.setProgressNumberFormat("");
                progressDialog.setProgressPercentFormat(null);
            }
            progressDialog.setMessage(getResources().getString(R.string.fetching_data_from_server));
            progressDialog.setCanceledOnTouchOutside(false);

            progressDialog.show();
           // btnUpdate.setProgress(1);
            //Configuration.getInstance().setString(SharedPrefs.SERVER_ADDRESS, txtServerAddress.getText().toString());
        }

        @Override
        protected String doInBackground(Void... voids) {
            return WebServiceSyncImp.getPersonnelForLogin();
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("NoConnection"))
                CustomAlertDialogOk.getInstance().showConfirmDialog("توجه", getString(R.string.cant_connect_to_server), "تایید", ActLogin.this, identifier);
            else {


              //  isFirstInstall = true;
                String userName = ShahrdariApp.replaceNumbers(txtUserName.getText().toString());
                String password = ShahrdariApp.replaceNumbers(txtPassword.getText().toString());

                WifiManager manager =
                        (WifiManager)getApplicationContext().
                        getSystemService(Context.WIFI_SERVICE);
                WifiInfo info = manager.getConnectionInfo();
                String address = info.getMacAddress();
                if (DatabaseService.checkLogin(userName, password,address)) {
                    SharedPreferences sharedPreferences = getSharedPreferences("UserLogin", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("user", txtUserName.getText().toString());
                    //editor.putString("password", txtUserName.getText().toString());
                    editor.commit();
//                int a=-1;
//                a=DatabaseService.getPersonnelsCount();

                    Intent i = new Intent(ActLogin.this, ActMain.class);
                    i.putExtra("isFirstInstall", isFirstInstall);
                    startActivity(i);
                    finish();
                } else {


//                    int a = -1;
//                    a = DatabaseService.getPersonnelsCount();
                    CustomAlertDialog.getInstance().showConfirmDialog("خطا", "\"نام کاربری یا رمز عبور واردشده صحیح نیست!\"" , "تلاش مجدد", "خروج", ActLogin.this, identifier);

//                    + "تعداد پرسنل=" + String.valueOf(CountPersonnel)

                }
            }
        }
    }
}
