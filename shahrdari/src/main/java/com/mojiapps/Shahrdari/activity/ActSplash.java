package com.mojiapps.Shahrdari.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import com.mojiapps.Shahrdari.Configuration;
import com.mojiapps.Shahrdari.R;

import java.io.File;

public class ActSplash extends ActivityConfigAware {
    private final int SPLASH_DELAY_TIME = 1000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.act_splash);

        //createFolders();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(ActSplash.this, ActLogin.class);
                ActSplash.this.startActivity(intent);
                ActSplash.this.finish();
            }
        },SPLASH_DELAY_TIME);
    }


    private void createFolders(){
        File folder = new File(Configuration.getInstance().getDatabaseFolder());
        folder.mkdirs();
    }
}
