package com.mojiapps.Shahrdari.activity;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.models.Pictures;
import com.mojiapps.Shahrdari.utils.Utils;
import com.mojiapps.Shahrdari.webservice.WebServiceSyncImp;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PicturesActivity extends AppCompatActivity {

    @Bind(R.id.imgViolation)
    ImageView imgViolation;
    @Bind(R.id.imgViolation2)
    ImageView imgViolation2;
    @Bind(R.id.imgViolation3)
    ImageView imgViolation3;
    @Bind(R.id.imgViolation4)
    ImageView imgViolation4;
    @Bind(R.id.circleProgressBar)
    com.ahmadian.ruhallah.commons.widgets.SmartCircularProgressBar SmartCircularProgressBar;

    public enum ViolationType {
        HOME, CAR, STAFF_CAR
    }

    private ViolationType violationType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pictures);

        ButterKnife.bind(this, this);

        new getImagesLoader(getIntent().getIntExtra("id", -1), (ViolationType) getIntent().getSerializableExtra("type")).execute();
    }

    private void setImages(Pictures pictures) {
        if (pictures == null) {
            Toast.makeText(getApplicationContext(), getString(R.string.error_in_get_data), Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        SmartCircularProgressBar.setVisibility(View.GONE);
        if (pictures.getImage1() != null) {
            imgViolation.setVisibility(View.VISIBLE);
            Bitmap b=Utils.GetBitmapFromBase64(pictures.getImage1());
            imgViolation.setImageBitmap(b);
        }
        else {
            imgViolation.setVisibility(View.GONE);
        }
        if (pictures.getImage2() != null) {
            imgViolation2.setVisibility(View.VISIBLE);
            imgViolation2.setImageBitmap(Utils.GetBitmapFromBase64(pictures.getImage2()));
        }
        else {
            imgViolation2.setVisibility(View.GONE);
        }
        if (pictures.getImage3() != null) {
            imgViolation3.setVisibility(View.VISIBLE);
            imgViolation3.setImageBitmap(Utils.GetBitmapFromBase64(pictures.getImage3()));
        }
        else {
            imgViolation3.setVisibility(View.GONE);
        }
        if (pictures.getImage4() != null) {
            imgViolation4.setVisibility(View.VISIBLE);
            imgViolation4.setImageBitmap(Utils.GetBitmapFromBase64(pictures.getImage4()));
        }
        else{
            imgViolation4.setVisibility(View.GONE);
        }

        // TODO: 27/05/2018 Moghari
        if(pictures.getImage1() == null && pictures.getImage2() == null && pictures.getImage3() == null
                && pictures.getImage4() == null)
        {
            //Toast.makeText(getApplicationContext(), getString(R.string.error_in_get_data), Toast.LENGTH_SHORT).show();
            Toast.makeText(getApplicationContext(), getString(R.string.error_Moghari_no_image_found), Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
    }
    private class getImagesLoader extends AsyncTask<Void, Void, Pictures> {

        private int id;
        private ViolationType violationType;

        public getImagesLoader(int id, ViolationType violationType) {
            this.id = id;
            this.violationType = violationType;
        }

        @Override
        protected Pictures doInBackground(Void... voids) {
            switch (violationType) {
                case HOME:
                    return WebServiceSyncImp.getHomeViolationImages(id);
                case CAR:
                    return WebServiceSyncImp.getCarViolationImages(id);
                case STAFF_CAR:
                    return WebServiceSyncImp.getStaffCarViolationImages(id);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Pictures result) {
            super.onPostExecute(result);
            setImages(result);
        }
    }
}
