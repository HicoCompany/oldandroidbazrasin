package com.mojiapps.Shahrdari.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.*;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.adapters.CarAdapter;
import com.mojiapps.Shahrdari.database.DatabaseService;
import com.mojiapps.Shahrdari.enums.Fonts;
import com.mojiapps.Shahrdari.models.Car;
import com.mojiapps.Shahrdari.utils.Texts;
import com.mojiapps.Shahrdari.webservice.WebServiceSyncImp;

import java.util.List;

/**
 * Created by mojtaba on 9/21/14.
 */
public class FragCar extends Fragment implements AdapterView.OnItemLongClickListener {

    ListView lstCar;
    CarAdapter adapter;
    List<Car> cars;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_car, container, false);
        setHasOptionsMenu(true);

        lstCar = (ListView) view.findViewById(R.id.lstCar);
        lstCar.setOnItemLongClickListener(this);

        //View headerView = View.inflate(getActivity(), R.layout.license_row_header, null);
        /*((TextView) view.findViewById(R.id.txtCodeHeader)).setTypeface(Texts.getFont(Fonts.YEKAN));
        ((TextView) view.findViewById(R.id.txtCarHeader)).setTypeface(Texts.getFont(Fonts.YEKAN));
        ((TextView) view.findViewById(R.id.txtDateHeader)).setTypeface(Texts.getFont(Fonts.YEKAN));*/

        //lstLicense.addHeaderView(headerView, null, false);
        lstCar.setHeaderDividersEnabled(true);
        lstCar.setDivider(new ColorDrawable(getResources().getColor(R.color.fragment_backcolor)));
        lstCar.setDividerHeight(2);

        if (getArguments() == null)
            cars = DatabaseService.getCars();
        else
            cars = DatabaseService.getCarsByContractorId(getArguments().getInt("contractorId"));

        adapter = new CarAdapter(getActivity(), R.layout.car_row, R.id.txtID, cars);
        lstCar.setAdapter(adapter);
        lstCar.setOnItemLongClickListener(this);


        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_car, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item=menu.findItem(R.id.action_search_license);
        item.setVisible(false);
        MenuItem item1=menu.findItem(R.id.action_search_contractor);
        item1.setVisible(false);
        MenuItem item2=menu.findItem(R.id.action_search_staff_car);
        item2.setVisible(false);

        // menu.clear();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        }
        return super.onOptionsItemSelected(item);
    }

    ProgressDialog progressDialog;

    private void updateCars() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            progressDialog.setProgressNumberFormat("");
            progressDialog.setProgressPercentFormat(null);
        }
        //TextView txtMessage = (TextView)progressDialog.findViewById(getResources().getIdentifier("title", "id", "android"));
        //txtMessage.setTypeface(Texts.getFont(getActivity(), Fonts.YEKAN));
        progressDialog.setMessage(getResources().getString(R.string.fetching_data_from_server));
        progressDialog.setCanceledOnTouchOutside(false);

        new CarUpdater().execute();
    }

    private class CarUpdater extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            progressDialog.dismiss();
            if (result)
                Toast.makeText(getActivity(), "موفقیت آمیز بود", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(getActivity(), "دریافت اطلاعات مجوزها موفقیت آمیز نبود", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            return WebServiceSyncImp.updateStaffCars();
        }

    }

    @Override
    public boolean onItemLongClick(final AdapterView<?> adapterView, View view, final int i, long l) {
        final String[] menuItems = getResources().getStringArray(R.array.car_context_menu_items);

        ListAdapter adapter = new ArrayAdapter<CharSequence>(getActivity(), android.R.layout.select_dialog_item,
                android.R.id.text1, menuItems) {

            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView tView = (TextView) view.findViewById(android.R.id.text1);

                //tView.setTypeface(Texts.getFont(Fonts.YEKAN));
                tView.setTextSize(17);
                return view;
            }
        };


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        TextView txtTitle = new TextView(getActivity());
        txtTitle.setText(getResources().getString(R.string.choose));
        //txtTitle.setTypeface(Texts.getFont(Fonts.DEFAULT));
        txtTitle.setTextSize(21);
        builder.setCustomTitle(txtTitle);
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int position) {
                if (menuItems[position].equalsIgnoreCase(getString(R.string.add_car_violation))) {
                    Intent intent = new Intent(getActivity(), ActivityStaffCarViolation.class);
                    intent.putExtra("carId", ((Car)adapterView.getItemAtPosition(i)).getCarId());
                    startActivity(intent);
                }
            }
        });

        builder.setCancelable(true);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return true;
    }
}
