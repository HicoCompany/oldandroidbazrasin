package com.mojiapps.Shahrdari.activity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.mojiapps.Shahrdari.R;

public class ActivityCallHico extends AppCompatActivity {

    String Result="";

    private Toolbar toolbar;
    ActionBar actionBar;
    TextView txtAmount;
    Button btnOK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.activity_call_hico);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
      //  actionBar.setIcon(R.drawable.hico_logo_last);//ic_launcher_light
     //   actionBar.setTitle(getString(R.string.add_violation));
        actionBar.setSubtitle(("تماس با ما"));
        actionBar.setDisplayHomeAsUpEnabled(true);
       // txtAmount=(TextView)findViewById(R.id.txtAmount);

        setFinishOnTouchOutside(false);
       // getAmount();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }




}
