package com.mojiapps.Shahrdari.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mojiapps.Shahrdari.Configuration;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.dialog.CustomAlertDialog;
import com.mojiapps.Shahrdari.dialog.CustomAlertDialogOk;
import com.mojiapps.Shahrdari.dialog.DialogClickInterface;
import com.mojiapps.Shahrdari.dialog.DialogClickInterfaceOk;
import com.mojiapps.Shahrdari.enums.Fonts;
import com.mojiapps.Shahrdari.enums.SharedPrefs;
import com.mojiapps.Shahrdari.utils.ConnectionDetector;
import com.mojiapps.Shahrdari.utils.Texts;
import com.mojiapps.Shahrdari.utils.Utils;
import com.mojiapps.Shahrdari.webservice.WebServiceSyncImp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by mojtaba on 9/20/14.
 */
public class ActMain extends ActivityConfigAware implements NavigationView.OnNavigationItemSelectedListener, DialogClickInterfaceOk, DialogClickInterface {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private Toolbar toolbar;
    private static Intent UpdateIntent;
    private static Context mContext;
    private static boolean isFirstInstall;
    private static ServiceConnection mServerConn;
    private int identifier = 0;
    ActionBar actionBar;
    private static CountDownTimer timer;
    private static boolean isFirst = true;
    ///////////////////////////////////////////////////////////

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        setContentView(R.layout.act_main);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.get("isFirstInstall") != null)
            isFirstInstall = bundle.getBoolean("isFirstInstall");
        viewPager = (ViewPager) findViewById(R.id.pager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
        viewPager.setPageMargin(pageMargin);

        mContext = ActMain.this;
        changeTabsFont();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(" ");
//        toolbar.setSubtitle(Texts.ChangeToUnicode(Utils.getCurrentDate()));
        toolbar.setLogo(R.drawable.toolbar_icons_padding);//ic_launcher_light
        setSupportActionBar(toolbar);

        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(2014, Calendar.OCTOBER/*9-1*/, 20);

        Calendar calendar2 = Calendar.getInstance();
        calendar2.set(2014, Calendar.OCTOBER/*9-1*/, 30);

        timer = new CountDownTimer(600000, 1000) {

            public void onTick(long millisUntilFinished) {

                if (isFirst)

                {
                    if (!isFirstInstall)
                        synk();
                    isFirst = false;
                }
            }

            public void onFinish() {
                if (!((Activity) mContext).isFinishing()) {
                    synk();
                    timer.start();
                    isFirst = true;
//              Toast.makeText(SelectDriverActivity.this, "راننده ای پیدا نشد...", Toast.LENGTH_SHORT).show();
                }
            }
        }.start();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Creating a ToggleButton for NavigationDrawer with drawer event listener
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {


            /** Called when drawer is closed */
            public void onDrawerClosed(View view) {
                // highlightSelectedCountry();
                supportInvalidateOptionsMenu();
            }

            /** Called when a drawer is opened */
            public void onDrawerOpened(View drawerView) {
                //  getSupportActionBar().setTitle("Select a Country");
                supportInvalidateOptionsMenu();
            }
        };

//         Setting event listener for the drawer
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        // ItemClick event handler for the drawer items
//        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
//                                    long arg3) {
//
//               // setMenuActionClick(position);
//
//
//            }
//        });
        /////////////////////////////
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        ///////////////////////////////////////////////////////////////////////////
        toolbar.setNavigationIcon(R.drawable.drawer_menu_w);
        View hView = navigationView.inflateHeaderView(R.layout.nav_header_main);
        TextView imgProfile = (TextView) hView.findViewById(R.id.profile);
        TextView tv = (TextView) hView.findViewById(R.id.Date);
        imgProfile.setText(Configuration.getInstance().getString(SharedPrefs.NAME));
        tv.setText(Texts.ChangeToUnicode(Utils.getCurrentDate()));
        ////////////////////////////////////////////////////////////////////////////////////
//        mServerConn = new ServiceConnection() {
//            @Override
//            public void onServiceConnected(ComponentName name, IBinder binder) {
////                Log.d(LOG_TAG, "onServiceConnected");
//            }
//
//            @Override
//            public void onServiceDisconnected(ComponentName name) {
////                Log.d(LOG_TAG, "onServiceDisconnected");
//            }
//        };
//        UpdateIntent = new Intent(ActMain.this, UpdateService.class);
//        //endregion
//
//        if (android.os.Build.VERSION.SDK_INT > 9) {
//            StrictMode.ThreadPolicy policy =
//                    new StrictMode.ThreadPolicy.Builder().permitAll().build();
//            StrictMode.setThreadPolicy(policy);
//        }
//
//        startService();
        /////////////////////////////////////////////////////////////
//        DatabaseService.addContractor(new ContractorOLD(1,"مجتبی محامد1", "مجتبی", "قم", new Date()));
//        DatabaseService.addContractor(new ContractorOLD(2,"مجتبی محامد2", "محمد", "آبودان", new Date()));
//        DatabaseService.addContractor(new ContractorOLD(3,"مجتبی محامد3", "حسن", "تهران", new Date()));
//        DatabaseService.addContractor(new ContractorOLD(4,"مجتبی محامد4", "جواد", "یزد", new Date()));
//        DatabaseService.addContractor(new ContractorOLD(5,"مجتبی محامد5", "گلی", "قم", new Date()));
//
//        DatabaseService.addLicense(new License(1, "کارفرما 1", "کوه سفید", "کوه", calendar1.getTime(), calendar2.getTime(), true, "", DatabaseService.getContractorById(1)));
//        DatabaseService.addLicense(new License(2, "کارفرما 2", "بیابون", "ب",calendar1.getTime(), calendar2.getTime(), false, "", DatabaseService.getContractorById(2)));
//        DatabaseService.addLicense(new License(3, "کارفرما 3", "یالغوزآباد", "آبادی", calendar1.getTime(), calendar2.getTime(), true, "", DatabaseService.getContractorById(3)));
//        DatabaseService.addLicense(new License(4, "کارفرما 4", "بیابون", "کوه", calendar1.getTime(),calendar2.getTime(), false, "", DatabaseService.getContractorById(2)));
//        DatabaseService.addLicense(new License(5, "کارفرما 5", "یالغوزآباد", "آبادی", calendar1.getTime(), calendar2.getTime(), false, "", DatabaseService.getContractorById(3)));
//
//
//
//        CarOLD car1 = new CarOLD(0, "بنز ده تن", "مجتبی 1", "83 د 255 ایران 16", new Date(), DatabaseService.getContractorById(1));
//        DatabaseService.addCar(car1);
//
//        CarOLD car2 = new CarOLD(1, "وانت نیسان", "مجتبی 2", "81 د 255 ایران 16", new Date(), DatabaseService.getContractorById(2));
//        DatabaseService.addCar(car2);
//
//        CarOLD car3 = new CarOLD(2, "وانت پیکان", "مجتبی 3", "80 د 255 ایران 16", new Date(), DatabaseService.getContractorById(3));
//        DatabaseService.addCar(car3);
//
//        LicenseCar licenseCar1 = new LicenseCar(DatabaseService.getLicenseById(1), car1);
//        DatabaseService.addLicenseCar(licenseCar1);
//
//        LicenseCar licenseCar2 = new LicenseCar(DatabaseService.getLicenseById(1), car3);
//        DatabaseService.addLicenseCar(licenseCar2);
//
//        LicenseCar licenseCar3 = new LicenseCar(DatabaseService.getLicenseById(2), car2);
//        DatabaseService.addLicenseCar(licenseCar3);
//
//        LicenseCar licenseCar4 = new LicenseCar(DatabaseService.getLicenseById(3), car1);
//        DatabaseService.addLicenseCar(licenseCar4);
//
//        LicenseCar licenseCar5 = new LicenseCar(DatabaseService.getLicenseById(3), car2);
//        DatabaseService.addLicenseCar(licenseCar5);
//
//        LicenseCar licenseCar6 = new LicenseCar(DatabaseService.getLicenseById(5), car3);
//        DatabaseService.addLicenseCar(licenseCar6);
//
//        LicenseCar licenseCar7 = new LicenseCar(DatabaseService.getLicenseById(5), car2);
//        DatabaseService.addLicenseCar(licenseCar7);
//
////        License l1 = DatabaseService.getLicenseById(1);
////        ForeignCollection<CarOLD> cars = l1.getCars();
////        if (cars == null) {
////            try {
////                cars = getShahrdariDatabaseHelper().getLicenseDao().getEmptyForeignCollection("cars");
////            } catch (SQLException e) {
////                e.printStackTrace();
////            }
////        }
////        cars.add(car1);
////        cars.add(car3);
////        DatabaseService.updateLicense(l1);
//
//
//        CertificateType certificateType1 = new CertificateType(0, "گواهینامه رانندگی");
//        CertificateType certificateType2 = new CertificateType(1, "شناسنامه");
//        CertificateType certificateType3 = new CertificateType(2, "کارت ملی");
//
//        DatabaseService.addCertificateType(certificateType1);
//        DatabaseService.addCertificateType(certificateType2);
//        DatabaseService.addCertificateType(certificateType3);
//
//        ViolationType violationType1 = new ViolationType(0, "پوشش نامناسب خودرو حین انتقال پسماند");
//        ViolationType violationType2 = new ViolationType(1, "تخلیه غیر مجاز پسماندهای عمرانی و ساخنمانی");
//        ViolationType violationType3 = new ViolationType(2, "ارتفاع غیر مجاز بار خودرو");
//
//        DatabaseService.addViolation(violationType1);
//        DatabaseService.addViolation(violationType2);
//        DatabaseService.addViolation(violationType3);
//
//        ViolationEstate violationEstate = new ViolationEstate(0, 1, "مجی محی", "09211101211", "23423423, -2342332","قم", "", "");
//        DatabaseService.updateHomeViolation(violationEstate);
//
////        try {
////            ForeignCollection<ViolationEstateViolationType> violationTypes = new EagerForeignCollection<ViolationEstateViolationType, Integer>();
////            violationTypes.add(violationEstateViolationType1)
////        } catch (SQLException e) {
////            e.printStackTrace();
////        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        //adapter.addFragment(new FragUpdate(), getString(R.string.update).toUpperCase());

        adapter.addFragment(new FragLicense(), getString(R.string.license).toUpperCase());
        adapter.addFragment(new FragViolation(), getString(R.string.violation).toUpperCase());
        adapter.addFragment(new FragmentStaffCarList(), getString(R.string.car).toUpperCase());
        adapter.addFragment(new FragmentContractorList(), getString(R.string.contractor).toUpperCase());
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
    }

    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(Texts.getFont(Fonts.IRAN_SANS), Typeface.NORMAL);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //  getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sync:
                synk();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setActionBarCustomView() {
        actionBar.setCustomView(R.layout.title_actionbar);

        TextView tvOfficerName = (TextView) actionBar.getCustomView().findViewById(R.id.txtOfficerName);
        TextView tvDateTop = (TextView) actionBar.getCustomView().findViewById(R.id.txtDateTop);
        ImageView imgOfficer = (ImageView) actionBar.getCustomView().findViewById(R.id.imgOfficer);

        tvOfficerName.setTypeface(Texts.getFont(Fonts.YEKAN));
        tvDateTop.setTypeface(Texts.getFont(Fonts.YEKAN));

        tvOfficerName.setText(Configuration.getInstance().getString(SharedPrefs.NAME));
        tvDateTop.setText(Texts.ChangeToUnicode(Utils.getCurrentDate()));

        String strImg = Configuration.getInstance().getString(SharedPrefs.IMAGE);
        Bitmap profileImage;
        if (!strImg.equals(""))
            profileImage = Utils.GetBitmapFromBase64(strImg);
        else
            profileImage = BitmapFactory.decodeResource(getResources(), R.drawable.defaultuser);

        imgOfficer.setImageBitmap(Utils.getCircleBitmap(profileImage));

        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME);
    }

    @Override
    public void onClickOkButton(DialogInterface pDialog, int pDialogIntefier) {

        Intent intent = new Intent(ActMain.this, ActMain.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
//
        if (id == R.id.nav_staff_car) {
            Intent i = new Intent(ActMain.this, ActivityCarViolation.class);
            startActivity(i);
        } else if (id == R.id.nav_car) {

            Intent i = new Intent(ActMain.this, ActivityCarViolation.class);
            startActivity(i);

        } else if (id == R.id.nav_home) {

            Intent i = new Intent(ActMain.this, ActivityHomeViolation.class);
            startActivity(i);

        } else if (id == R.id.nav_survey) {

            Intent i = new Intent(ActMain.this, ActivitySurvey.class);
            startActivity(i);

        } else if (id == R.id.nav_survey_site) {

            Intent i = new Intent(ActMain.this, ActivitySurveySite.class);
            startActivity(i);

        } else if (id == R.id.nav_refresh) {

            synk();

        } else if (id == R.id.nav_help) {
            Intent i = new Intent(ActMain.this, ActivityHelp.class);
            startActivity(i);
            // Toast.makeText(ActMain.this,"به زودی این بخش به برنامه اضافه خواهد شد!",Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_call) {
            Intent i = new Intent(ActMain.this, ActivityCallHico.class);
            startActivity(i);

        } else if (id == R.id.nav_exit) {

            CustomAlertDialog.getInstance().showConfirmDialog("توجه", "\"آیا مایل به خروج از برنامه هستید؟!\"", "بله", "خیر", ActMain.this, 1);

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClickPositiveButton(DialogInterface pDialog, int pDialogIntefier) {
        pDialog.dismiss();
        finish();
    }

    @Override
    public void onClickNegativeButton(DialogInterface pDialog, int pDialogIntefier) {
        pDialog.dismiss();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }

    boolean isTimeForClose = true;

    @Override
    public void onBackPressed() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackEntryCount > 0) {
            super.onBackPressed();
            return;
        }
        Toast toast = Toast.makeText(ActMain.this, "برای خروج دکمه بازگشت را دوباره بزنید", Toast.LENGTH_SHORT);
        if (isTimeForClose) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isTimeForClose = true;
                }
            }, 2000);
            toast.show();
            isTimeForClose = false;
        } else {
            super.onBackPressed();
            toast.cancel();
        }
    }

    private class DatabaseUpdater extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            progressDialog = new ProgressDialog(mContext);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                progressDialog.setProgressNumberFormat("");
                progressDialog.setProgressPercentFormat(null);
            }
            progressDialog.setMessage(getResources().getString(R.string.fetching_data_from_server));
            progressDialog.setCanceledOnTouchOutside(false);

            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            return WebServiceSyncImp.updateAll2();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (isFinishing() == false)
                progressDialog.dismiss();

            if (result.equals("NoConnection"))
                CustomAlertDialogOk.getInstance().showConfirmDialog("توجه", getString(R.string.cant_connect_to_server), "تایید", mContext, identifier);
            else {
                String[] error = result.split("-");
                if (error[0].equals("13")) {
                    CustomAlertDialogOk.getInstance().showConfirmDialog("توجه", "بروز رسانی با موفقیت انجام شد", "تایید", mContext, identifier);
                    timer.start();
                } else {
                    String[] errorMessage = error[1].split("%");
                    String message = "";
                    for (int i = 0; i < errorMessage.length; i++) {
                        if (errorMessage[i].equals("1"))
                            message = "به روز رسانی لیست خودروها با موفقیت انجام شد.";
                        else if (errorMessage[i].equals("11"))
                            message = "به روز رسانی لیست خودروها با موفقیت انجام نشد.";
                        if (errorMessage[i].equals("6"))
                            message = message + "\n" + "به روز رسانی لیست پیمانکاران با موفقیت انجام شد.";
                        else if (errorMessage[i].equals("66"))
                            message = message + "\n" + "به روز رسانی لیست پیمانکاران با موفقیت انجام نشد.";
                        if (errorMessage[i].equals("l"))
                            message = message + "\n" + "به روز رسانی لیست مجوزها با موفقیت انجام شد.";
                        else if (errorMessage[i].equals("ll"))
                            message = message + "\n" + "به روز رسانی لیست مجوزها با موفقیت انجام نشد.";
                        if (errorMessage[i].equals("s"))
                            message = message + "\n" + "به روز رسانی لیست تخلف های عضوکمیته با موفقیت انجام شد.";
                        else if (errorMessage[i].equals("ss"))
                            message = message + "\n" + "به روز رسانی لیست تخلف های عضوکمیته با موفقیت انجام نشد.";
                        if (errorMessage[i].equals("c"))
                            message = message + "\n" + "به روز رسانی لیست تخلف های غیرعضوکمیته با موفقیت انجام شد.";
                        else if (errorMessage[i].equals("cc"))
                            message = message + "\n" + "به روز رسانی لیست تخلف های غیرعضوکمیته با موفقیت انجام نشد.";
                        if (errorMessage[i].equals("h"))
                            message = message + "\n" + "به روز رسانی لیست تخلف های املاک با موفقیت انجام شد.";
                        else if (errorMessage[i].equals("hh"))
                            message = message + "\n" + "به روز رسانی لیست تخلف های املاک با موفقیت انجام نشد.";


                    }
                    CustomAlertDialogOk.getInstance().showConfirmDialog("توجه", message, "تایید", mContext, identifier);
                }
            }
            //
//            AlertDialog.Builder builder = new AlertDialog.Builder(ActMain.this);
//            builder.setCancelable(false);
//            builder.setTitle("توجه");
//
//
//            if (result.equals("NoConnection"))
//                builder.setMessage(getString(R.string.cant_connect_to_server));
//            else
//                //builder.setMessage(result.split("-")[1] + " مورد از " + result.split("-")[0] + " مورد با موفقیت دریافت شد");
//                builder.setMessage("بروز رسانی با موفقیت انجام شد");
//
//            builder.setNeutralButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    Intent intent = new Intent(ActMain.this, ActMain.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
//                }
//            });
//            AlertDialog alert = builder.create();
//            alert.show();
        }
    }

    public void synk() {

        timer.cancel();
        ConnectionDetector cd = new ConnectionDetector(this);
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(this, getString(R.string.cant_connect_to_server), Toast.LENGTH_SHORT).show();

        } else
            new DatabaseUpdater().execute();
    }

    public void startService() {
        mContext = ActMain.this;


        startService(UpdateIntent);
        bindService(UpdateIntent, mServerConn, Context.BIND_AUTO_CREATE);
    }

    public void cancelService() {
        if (UpdateIntent != null && mServerConn != null) {
            if (mContext != null && mContext.bindService(UpdateIntent, mServerConn, Context.BIND_AUTO_CREATE)) {
                mContext.unbindService(mServerConn);
            }
            mContext.stopService(UpdateIntent);

        }
    }
}