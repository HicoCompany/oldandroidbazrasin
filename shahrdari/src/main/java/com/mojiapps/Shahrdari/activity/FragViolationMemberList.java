package com.mojiapps.Shahrdari.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.adapters.ViolationMemberAdapter;
import com.mojiapps.Shahrdari.database.DatabaseService;
import com.mojiapps.Shahrdari.database.ViolationMember;
import com.mojiapps.Shahrdari.dialog.DialogVerify;
import com.mojiapps.Shahrdari.enums.Fonts;
import com.mojiapps.Shahrdari.models.StaffCarViolation;
import com.mojiapps.Shahrdari.utils.Texts;

import java.util.List;

/**
 * Created by mojtaba on 10/7/14.
 */
public class FragViolationMemberList extends Fragment implements AdapterView.OnItemLongClickListener{

    ListView lstViolation;
    ViolationMemberAdapter adapter;
    List<StaffCarViolation> violations;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_violation_member_list, container, false);

        setHasOptionsMenu(true);

        lstViolation = (ListView) view.findViewById(R.id.lstViolation);
        lstViolation.setOnItemLongClickListener(this);

        lstViolation.setHeaderDividersEnabled(true);
        lstViolation.setDivider(new ColorDrawable(getResources().getColor(R.color.fragment_backcolor)));
        lstViolation.setDividerHeight(2);

        violations = DatabaseService.getStaffCarViolations();
        adapter = new ViolationMemberAdapter(getActivity(), R.layout.violation_member_row, R.id.txtID, violations);
        lstViolation.setAdapter(adapter);
        lstViolation.setOnItemLongClickListener(this);

        return view;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        final String[] menuItems = getResources().getStringArray(R.array.violation_context_menu_items);

        ListAdapter adapter = new ArrayAdapter<CharSequence>(getActivity(), android.R.layout.select_dialog_item,
                android.R.id.text1, menuItems) {

            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView)view.findViewById(android.R.id.text1);

                textView.setTypeface(Texts.getFont(Fonts.YEKAN));
                textView.setTextSize(17);
                return view;
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        TextView txtTitle = new TextView(getActivity());
        txtTitle.setText(getResources().getString(R.string.choose));
        txtTitle.setTypeface(Texts.getFont(Fonts.DEFAULT));
        txtTitle.setTextSize(21);
        builder.setCustomTitle(txtTitle);
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int position) {
                if (menuItems[position].equalsIgnoreCase(getString(R.string.edit_violation))) {
                    Intent intent = new Intent(getActivity(), DialogVerify.class);
                }
            }
        });

        builder.setCancelable(true);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return true;
    }
}
