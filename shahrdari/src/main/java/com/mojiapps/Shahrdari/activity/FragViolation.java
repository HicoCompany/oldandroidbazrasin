package com.mojiapps.Shahrdari.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.ShahrdariApp;

/**
 * Created by mojtaba on 9/21/14.
 */
public class FragViolation extends Fragment implements AdapterView.OnItemSelectedListener{

    FragmentManager fragmentManager;
    Spinner spViolationType;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_violation, container, false);

        setHasOptionsMenu(true);
        spViolationType = (Spinner) view.findViewById(R.id.spViolationCategory);
        spViolationType.setAdapter(new ViolationAdapter(getActivity(), R.id.txtCategory, getResources().getStringArray(R.array.violation_category)));
        spViolationType.setOnItemSelectedListener(this);

        fragmentManager = getActivity().getSupportFragmentManager();
        //fragViolationList = fragmentManager.findFragmentById(R.id.fragViolationList);
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item=menu.findItem(R.id.action_search_license);
        item.setVisible(false);
        MenuItem item1=menu.findItem(R.id.action_search_contractor);
        item1.setVisible(false);
        MenuItem item2=menu.findItem(R.id.action_search_staff_car);
        item2.setVisible(false);
       // menu.clear();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {


        inflater.inflate(R.menu.menu_violation, menu);
        //apply font
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.mnuAddViolation:
                addViolation();
                return true;
            case R.id.action_search:
                Intent intent = new Intent(getActivity(), ActivityInquiry.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addViolation() {
        Intent intent = null;
        switch (spViolationType.getSelectedItemPosition()){
            case 0:
                intent = new Intent(getActivity(), ActivityStaffCarViolation.class);
                break;
            case 1:
                intent = new Intent(getActivity(), ActivityCarViolation.class);
                break;
            case 2:
                intent = new Intent(getActivity(), ActivityHomeViolation.class);
                break;
        }
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (position==0)
            fragmentTransaction.replace(R.id.fragViolationList, new FragmentStaffCarViolationList());
        else if (position == 1)
            fragmentTransaction.replace(R.id.fragViolationList, new FragmentCarViolationList());
        else if (position == 2)
            fragmentTransaction.replace(R.id.fragViolationList, new FragmentHomeViolationList());

        fragmentTransaction.commit();

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private class ViolationAdapter extends ArrayAdapter<String>{
        private String[] values;
        TextView txtCategory;
        ImageView imgCategory;

        public ViolationAdapter(Context context, int textViewResourceId,   String[] data){
            super(context, textViewResourceId, data);
            this.values = data;
        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) ShahrdariApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row= convertView;

            if(row == null){
                row = inflater.inflate(R.layout.violation_category_spinner_row, parent, false);
                txtCategory = (TextView) row.findViewById(R.id.txtCategory);
                imgCategory = (ImageView) row.findViewById(R.id.imgCategory);
            }
            else{
                txtCategory = (TextView) convertView.findViewById(R.id.txtCategory);
                imgCategory = (ImageView) convertView.findViewById(R.id.imgCategory);
            }

            txtCategory.setText(values[position]);
            /*txtCategory.setTypeface(Texts.getFont(Fonts.DEFAULT));*/


            String pngName = "";
            if (position == 0)
                pngName = "member";
            else if (position == 1)
                pngName = "not_member";
            else if (position == 2){
                pngName = "estate";
            }

            imgCategory.setImageResource(getContext().getResources().getIdentifier(pngName, "drawable", getContext().getPackageName()));
            return row;
        }

    }
}

