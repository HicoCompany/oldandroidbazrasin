package com.mojiapps.Shahrdari.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ahmadian.ruhallah.commons.fragment.list.FactorySmartItemView;
import com.ahmadian.ruhallah.commons.fragment.list.SmartFragment;
import com.ahmadian.ruhallah.commons.fragment.list.SmartItemView;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.database.DatabaseService;
import com.mojiapps.Shahrdari.items.ItemViewCarViolation;
import com.mojiapps.Shahrdari.models.CarViolation;
import com.mojiapps.Shahrdari.models.HomeViolation;
import com.mojiapps.Shahrdari.webservice.Params;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruhallah-PC on 12/17/2016.
 */

public class FragmentCarViolationList extends SmartFragment<CarViolation> {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setType(Type.STATIC);
        View view =  super.onCreateView(inflater, container, savedInstanceState);
        recyclerView.setBackgroundColor(getResources().getColor(R.color.fragment_backcolor));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRefreshing(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        list.clear();
        loadData();
    }

    @Override
    protected FactorySmartItemView getViewFactory() {
        return new FactorySmartItemView() {
            @Override
            public SmartItemView makeView(String searchPhrase) {
                ItemViewCarViolation itemView = new ItemViewCarViolation(getContext());
                itemView.setSearchPhrase(searchPhrase);
                itemView.setOnItemClickListener(new SmartItemView.OnItemClickListener<CarViolation>() {
                    @Override
                    public void click(CarViolation item, View view, int position) {
                    }
                });
                itemView.setOnImageClickListener(new SmartItemView.OnItemClickListener<CarViolation>() {
                    @Override
                    public void click(CarViolation item, View view, int position) {
                        Intent intent = new Intent(getActivity(), PicturesActivity.class);
                        intent.putExtra("id", item.getCarViolationId());
                        intent.putExtra("type", PicturesActivity.ViolationType.CAR);
                        startActivity(intent);
                    }
                });
                return itemView;
            }
        };
    }

    @Override
    protected List<CarViolation> fetchMoreDataAsync(int startPage, int count) {
        return null;
    }

    @Override
    protected List<CarViolation> fetchDataAsync() {
        if (getArguments() == null)
            return DatabaseService.getViolationNotMember();
        else {
            if (getArguments().getBoolean(Params.PLATE_TYPE, false)) {
                return DatabaseService.getViolationNotMemberByNewPlateCode
                        (getArguments().getString(Params.NEW_PLATE_PART_1),
                                getArguments().getString(Params.NEW_PLATE_PART_2),
                                getArguments().getString(Params.NEW_PLATE_PART_3),
                                getArguments().getString(Params.NEW_PLATE_PART_4));
            }else if (!getArguments().getBoolean(Params.PLATE_TYPE, true)){
                return DatabaseService.getViolationNotMemberByOldPlateCode
                        (getArguments().getString(Params.OLD_PLATE));
            }
        }
        return new ArrayList<>();
    }

    @Override
    protected void onOperationFailed() {

    }
}
