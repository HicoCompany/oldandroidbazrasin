package com.mojiapps.Shahrdari.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mikelau.croperino.Croperino;
import com.mikelau.croperino.CroperinoConfig;
import com.mikelau.croperino.CroperinoFileUtil;
import com.mohamadamin.persianmaterialdatetimepicker.date.DatePickerDialog;
import com.mohamadamin.persianmaterialdatetimepicker.time.RadialPickerLayout;
import com.mohamadamin.persianmaterialdatetimepicker.time.TimePickerDialog;
import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;
import com.mojiapps.Shahrdari.Configuration;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.adapters.CarTypeAdapter;
import com.mojiapps.Shahrdari.adapters.CertificateAdapter;
import com.mojiapps.Shahrdari.database.DatabaseService;
import com.mojiapps.Shahrdari.dialog.CustomAlertDialogOk;
import com.mojiapps.Shahrdari.dialog.DialogClickInterfaceOk;
import com.mojiapps.Shahrdari.enums.SharedPrefs;
import com.mojiapps.Shahrdari.models.Car;
import com.mojiapps.Shahrdari.models.CarViolation;
import com.mojiapps.Shahrdari.models.Certificate;
import com.mojiapps.Shahrdari.models.ResultAdd;
import com.mojiapps.Shahrdari.models.Violation;
import com.mojiapps.Shahrdari.utils.DateUtils;
import com.mojiapps.Shahrdari.utils.JalaliCalendar;
import com.mojiapps.Shahrdari.utils.Utils;
import com.mojiapps.Shahrdari.webservice.WebServiceSyncImp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by mojtaba on 10/6/14.
 */
public class ActivityCarViolation extends ActivityConfigAware implements View.OnClickListener,DialogClickInterfaceOk {

    @Bind(R.id.description)
    EditText description;
    @Bind(R.id.submit_date)
    TextView submitDate;
    @Bind(R.id.submit_time)
    TextView submitTime;
    @Bind(R.id.NewPlatePart1)
    EditText newPlatePart1;
    @Bind(R.id.NewPlatePart2)
    EditText newPlatePart2;
    @Bind(R.id.NewPlatePart3)
    EditText newPlatePart3;
    @Bind(R.id.NewPlatePart4)
    EditText newPlatePart4;
    @Bind(R.id.new_plate_field)
    LinearLayout newPlateField;
    @Bind(R.id.spCar)
    Spinner spCar;

    ActionBar actionBar;

    CheckBox chCarType;
    EditText txtOldPlateNumber;
    EditText txtColor;
    EditText txtOwnerName;
    EditText txtMobile;
    ImageButton btnGPS;
    EditText txtGPSLocation;
    Spinner spCertificate;
    EditText txtAddressViolation;
    TextView txtViolationType;
    List<Violation> violations = new ArrayList<>();
    ImageView imgViolation;
    ImageView imgViolation2;
    ImageView imgViolation3;
    ImageView imgViolation4;
    Button btnSave;

    final String TEMP_PHOTO_FILE = "noimage.png";
    boolean hasImage = false;
    Bitmap violationBitmap1 = null;
    Bitmap violationBitmap2 = null;
    Bitmap violationBitmap3 = null;
    Bitmap violationBitmap4 = null;

    LocationManager lm;
    LocationListener locationlistener;

    private Toolbar toolbar;

    private DateFormat datetimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    private DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_violation);
        ButterKnife.bind(this, this);

        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        actionBar.setIcon(R.drawable.hico_logo_last);//ic_launcher_light
        actionBar.setTitle(getString(R.string.add_violation));
        actionBar.setSubtitle(getString(R.string.add_violation_not_member));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        init();

        fillSpinner();

        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationlistener = new MyLocationListener();

        txtMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
             //    txtMobile.setText("0"+s);
                String x = s.toString();
                if(!x.startsWith("0")  &&  x!=null && !x.equals(""))
                {

                    CustomAlertDialogOk.getInstance().showConfirmDialog("توجه","لطفا ابتدای شماره همراه را صفر وارد کنید!", "تایید",ActivityCarViolation.this, 0);
                    //your stuff here
                }
            }
        });
    }

    private void fillSpinner() {
        try {
            CertificateAdapter certificateAdapter = new CertificateAdapter(this, R.id.txtTitle, DatabaseService.getAllCertificateTypes());
            certificateAdapter.add(new Certificate(-1, "مدرک اخذ شده"));
            spCertificate.setAdapter(certificateAdapter);
            spCertificate.setSelection(certificateAdapter.getCount());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            CarTypeAdapter carTypeAdapter = new CarTypeAdapter(this, R.id.txtTitle, DatabaseService.getCars());
            carTypeAdapter.add(new Car(-1, "نوع خودرو", false));
            spCar.setAdapter(carTypeAdapter);
            spCar.setSelection(carTypeAdapter.getCount());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*try {
            List<PlateType> plateTypes = new ArrayList<>();
            plateTypes.add(new PlateType(0, "ملی"));
            plateTypes.add(new PlateType(1, "غیر ملی"));

            PlateTypeAdapter plateTypeAdapter = new PlateTypeAdapter(this, R.id.txtTitle, plateTypes);
            plateTypeAdapter.add(new PlateType(-1, "نوع پلاک"));
            spPlateType.setAdapter(plateTypeAdapter);
            spPlateType.setSelection(plateTypeAdapter.getCount());
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:
                save();
                break;
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void init() {
        chCarType = (CheckBox) findViewById(R.id.spCarType);
        //spPlateType = (Spinner) findViewById(R.id.spPlateType);
        txtOldPlateNumber = (EditText) findViewById(R.id.txtOldPlateNumber);
        txtColor = (EditText) findViewById(R.id.txtColor);
        txtOwnerName = (EditText) findViewById(R.id.txtOwnerName);
        txtMobile = (EditText) findViewById(R.id.txtMobile);

        btnGPS = (ImageButton) findViewById(R.id.cmdGPS);
        txtGPSLocation = (EditText) findViewById(R.id.txtGPSLocation);
        spCertificate = (Spinner) findViewById(R.id.spCertificate);
        txtAddressViolation = (EditText) findViewById(R.id.txtAddressViolation);
        txtViolationType = (TextView) findViewById(R.id.txtViolationType);
        imgViolation = (ImageView) findViewById(R.id.imgViolation);
        imgViolation2 = (ImageView) findViewById(R.id.imgViolation2);
        imgViolation3 = (ImageView) findViewById(R.id.imgViolation3);
        imgViolation4 = (ImageView) findViewById(R.id.imgViolation4);
        btnSave = (Button) findViewById(R.id.btnSaveViolation);

        chCarType.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    newPlateField.setVisibility(View.GONE);
                    txtOldPlateNumber.setVisibility(View.VISIBLE);
                }else {
                    newPlateField.setVisibility(View.VISIBLE);
                    txtOldPlateNumber.setVisibility(View.GONE);
                }
            }
        });

        btnGPS.setOnClickListener(this);
        imgViolation.setOnClickListener(this);
        imgViolation2.setOnClickListener(this);
        imgViolation3.setOnClickListener(this);
        imgViolation4.setOnClickListener(this);
        txtViolationType.setOnClickListener(this);
        btnSave.setOnClickListener(this);

        submitTime.setText(timeFormat.format(Calendar.getInstance().getTime()));
        submitDate.setText(DateUtils.MiladiToPersianDate(Calendar.getInstance().getTime()));

        submitTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (submitTime.getText().toString().equals("--:--:--")) {
                    TimePickerDialog.newInstance(
                            mSubmitTimeSetListener,
                            Calendar.getInstance().get(Calendar.HOUR_OF_DAY),
                            Calendar.getInstance().get(Calendar.MINUTE),
                            false
                    ).show(getFragmentManager(), "Datepickerdialog");
                } else {
                    try {
                        Date date = timeFormat.parse(submitTime.getText().toString());
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(date);
                        TimePickerDialog.newInstance(
                                mSubmitTimeSetListener,
                                calendar.get(Calendar.HOUR_OF_DAY),
                                calendar.get(Calendar.MINUTE),
                                false
                        ).show(getFragmentManager(), "Datepickerdialog");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        submitDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (submitDate.getText().toString().equals("----/--/--")) {
                    PersianCalendar persianCalendar = new PersianCalendar();
                    DatePickerDialog.newInstance(
                            mSubmitDateSetListener,
                            persianCalendar.getPersianYear(),
                            persianCalendar.getPersianMonth(),
                            persianCalendar.getPersianDay()
                    ).show(getFragmentManager(), "Datepickerdialog");
                } else {
                    JalaliCalendar jalaliCalendar = new JalaliCalendar();
                    try {
                        Date date = dateFormat.parse(submitDate.getText().toString());
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(date);
                        DatePickerDialog.newInstance(
                                mSubmitDateSetListener,
                                calendar.get(Calendar.YEAR),
                                calendar.get(Calendar.MONTH),
                                calendar.get(Calendar.DAY_OF_MONTH)
                        ).show(getFragmentManager(), "Datepickerdialog");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cmdGPS:
                getGPSLocation();
                break;
            case R.id.imgViolation:
                setViolationImage(1);
                break;
            case R.id.imgViolation2:
                setViolationImage(2);
                break;
            case R.id.imgViolation3:
                setViolationImage(3);
                break;
            case R.id.imgViolation4:
                setViolationImage(4);
                break;
            /*case R.id.btnSaveViolation:
                save();
                break;*/
            case R.id.txtViolationType:
                showViolationTypesDialog();
                break;
        }
    }

    private void save() {
        ///////////////////////////////////
        if(!isInternetOn()) {
            //new DatabaseSetCarViolation().execute();

            CustomAlertDialogOk.getInstance().showConfirmDialog("توجه", "لطفا اینترنت Wifi یا داده ی خود را روشن نمایید", "تایید", ActivityCarViolation.this, -1);
        }
        //////////////////////////////////
        else {
            if (validate()) {
                CarViolation carViolation = new CarViolation();

                carViolation.setCar((Car) spCar.getItemAtPosition(spCar.getSelectedItemPosition()));
                carViolation.setCertificate((Certificate) spCertificate.getItemAtPosition(spCertificate.getSelectedItemPosition()));
                carViolation.setAddress(txtAddressViolation.getText().toString());
                carViolation.setColor(txtColor.getText().toString());
                carViolation.setImage1(Utils.GetBase64StringFromBitmap(violationBitmap1));
                carViolation.setImage2(Utils.GetBase64StringFromBitmap(violationBitmap2));
                carViolation.setImage3(Utils.GetBase64StringFromBitmap(violationBitmap3));
                carViolation.setImage4(Utils.GetBase64StringFromBitmap(violationBitmap4));
                carViolation.setDescription(description.getText().toString());
                //carViolation.setViolationActDescription(violationActionDescription.getText().toString());
                carViolation.setOwName(txtOwnerName.getText().toString());
                carViolation.setOwTel(txtMobile.getText().toString());
                carViolation.setViolationListJson(violations);
                carViolation.setViolationList(violations);
                carViolation.setPersonnelId(Configuration.getInstance().getInt(SharedPrefs.ID));

                carViolation.setPlateType(!chCarType.isChecked());
                if (chCarType.isChecked())
                    carViolation.setOldPlate(txtOldPlateNumber.getText().toString());
                else {
                    String plate = newPlatePart1.getText().toString() +
                            newPlatePart2.getText().toString() +
                            newPlatePart3.getText().toString() +
                            getString(R.string.iran) +
                            newPlatePart4.getText().toString();
                    carViolation.setNewPlatePart1(newPlatePart1.getText().toString());
                    carViolation.setNewPlatePart2(newPlatePart2.getText().toString());
                    carViolation.setNewPlatePart3(newPlatePart3.getText().toString());
                    carViolation.setNewPlatePart4(newPlatePart4.getText().toString());
                    carViolation.setPlateCode(plate);
                }

                String startDateStr = submitDate.getText() + " " + submitTime.getText();
                try {
                    Date startDate = DateUtils.PersianToMiladi(datetimeFormat.parse(startDateStr));
                    // String strCurrentDate = "Wed, 18 Apr 2012 07:55:29 +0000";
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            String time=sdf.format(startDate);
                    // dateTime.ToString( "yyyy/MM/dd HH:mm:ss"

                    carViolation.setRegisterDate(startDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                carViolation.setSyncable(true);
                new ActivityCarViolation.DatabaseUpdater(carViolation).execute();
            }
        }
    }

    private boolean validate() {

        if (spCar.getSelectedItemPosition() == spCar.getCount()) {
            Toast.makeText(this, "مشخص کردن نوع خودرو الزامی است", Toast.LENGTH_LONG).show();
            return false;
        }

        /*if (spPlateType.getSelectedItemPosition() == spPlateType.getCount()) {
            Toast.makeText(this, "مشخص کردن نوع پلاک الزامی است", Toast.LENGTH_LONG).show();
            return false;
        }*/

        if (chCarType.isChecked()) {
            if (txtOldPlateNumber.getText().toString().equals("")) {
                Toast.makeText(this, "مشخص کردن پلاک خودرو الزامی است", Toast.LENGTH_LONG).show();
                return false;
            }
        }else {
            if (newPlatePart1.getText().toString().equals("") || newPlatePart2.getText().toString().equals("")
                    || newPlatePart3.getText().toString().equals("") || newPlatePart4.getText().toString().equals("")) {
                Toast.makeText(this, "مشخص کردن پلاک خودرو الزامی است", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        if (txtColor.getText().toString().equals("")) {
            Toast.makeText(this, "مشخص کردن رنگ خودرو الزامی است", Toast.LENGTH_LONG).show();
            return false;
        }

        if (txtOwnerName.getText().toString().equals("")) {
            Toast.makeText(this, "مشخص کردن نام مالک الزامی است", Toast.LENGTH_LONG).show();
            return false;
        }

        if (txtMobile.getText().toString().length() != 11) {
            Toast.makeText(this, "مقدار وارد شده برای شماره همراه صحیح نیست", Toast.LENGTH_LONG).show();
            return false;
        }

        if (false)
            if (txtGPSLocation.getText().toString().split(",").length < 2) {
                Toast.makeText(this, "مقدار وارد شده برای موقعیت GPS صحیح نیست", Toast.LENGTH_LONG).show();
                return false;
            }

        if (spCertificate.getSelectedItemPosition() == spCertificate.getCount()) {
            Toast.makeText(this, "مشخص کردن مدرک اخذ شده الزامی است", Toast.LENGTH_LONG).show();
            return false;
        }

        if (txtAddressViolation.getText().toString().length() == 0) {
            Toast.makeText(this, "مشخص کردن آدرس تخلف الزامی است", Toast.LENGTH_LONG).show();
            return false;
        }

        if (txtViolationType.getText().toString().equals("نوع تخلف") || txtViolationType.getText().toString().equals("هیچ موردی انتخاب نشده است")) {
            Toast.makeText(this, "انتخاب حداقل یک نوع تخلف الزامی است", Toast.LENGTH_LONG).show();
            return false;
        }

        if (false)
            if (!hasImage) {
                Toast.makeText(this, "مشخص کردن تصویر محل تخلف الزامی است", Toast.LENGTH_LONG).show();
                return false;
            }

        return true;
    }

    private void getGPSLocation() {
        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER))
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60000, 1000, locationlistener);
        else {
            Toast.makeText(this, "ابتدا جی پی اس را روشن کنید", Toast.LENGTH_LONG).show();
            Intent i = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }
    }

    private void showViolationTypesDialog() {
        final List<Violation> lstViolation = DatabaseService.getViolations();
        final String[] violationTypes = new String[lstViolation.size()];
        final boolean[] violationChecked = new boolean[lstViolation.size()];
        String[] currents = txtViolationType.getText().toString().split(" - ");

        for (int i = 0; i < lstViolation.size(); i++) {
            violationTypes[i] = lstViolation.get(i).getTitle();
            violationChecked[i] = false;
            for (String current : currents) {
                if (lstViolation.get(i).getTitle().equals(current.trim()))
                    violationChecked[i] = true;
            }
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("انتخاب از لیست تخلفات");
        builder.setMultiChoiceItems(violationTypes, violationChecked, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                violationChecked[which] = isChecked;
            }
        });
        builder.setNegativeButton("تأیید", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ListView listView = ((AlertDialog) dialog).getListView();
                SparseBooleanArray CheCked = listView.getCheckedItemPositions();
                String result = "";
                List<Violation> violationList = new ArrayList<Violation>();
                for (int i = 0; i < listView.getCount(); i++) {
                    if (CheCked.get(i)) {
                        result += violationTypes[i] + " - ";
                        violationList.add(lstViolation.get(i));
                    }
                }
                if (result.trim().equals(""))
                    result = "هیچ موردی اتنخاب نشده است";
                else
                result = result.substring(0, result.length() - 2);
                violations.clear();

                txtViolationType.setText(result);
                violations.addAll(violationList);
            }
        });
        builder.setPositiveButton("انصراف", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private int selectedImageNumber;

    private void setViolationImage(final int i) {
        selectedImageNumber = i;
        final CharSequence[] options = {"دوربین عکاسی", "انتخاب از گالری",
                "بدون تصویر", "انصراف"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("انتخاب تصویر پروفایل");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                new CroperinoConfig("IMG_" + System.currentTimeMillis() + ".jpg", "/MikeLau/Pictures", "/sdcard/MikeLau/Pictures");
                CroperinoFileUtil.verifyStoragePermissions(ActivityCarViolation.this);
                CroperinoFileUtil.setupDirectory(ActivityCarViolation.this);
                if (options[item].equals("دوربین عکاسی")) {
                    //Prepare Camera
                    try {
                        Croperino.prepareCamera(ActivityCarViolation.this);
                    } catch (Exception e) {
                        Log.e("Camera", e.toString());
                    }
                } else if (options[item].equals("انتخاب از گالری")) {
                    //Prepare Gallery
                    Croperino.prepareGallery(ActivityCarViolation.this);

                } else if (options[item].equals("بدون تصویر")) {
                    setImage(i, R.drawable.ic_add_a_photo_200dp);
                    hasImage = false;
                } else if (options[item].equals("انصراف")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void setImage(int i, int res) {
        switch (i) {
            case 1:
                imgViolation.setImageResource(res);
                violationBitmap1 = null;
                break;
            case 2:
                imgViolation2.setImageResource(res);
                violationBitmap2 = null;
                break;
            case 3:
                imgViolation3.setImageResource(res);
                violationBitmap3 = null;
                break;
            case 4:
                imgViolation4.setImageResource(res);
                violationBitmap4 = null;
                break;
        }
    }

    private void setImage(int i, Bitmap res) {
        switch (i) {
            case 1:
                imgViolation.setImageBitmap(res);
                violationBitmap1 = res;
                break;
            case 2:
                imgViolation2.setImageBitmap(res);
                violationBitmap2 = res;
                break;
            case 3:
                imgViolation3.setImageBitmap(res);
                violationBitmap3 = res;
                break;
            case 4:
                imgViolation4.setImageBitmap(res);
                violationBitmap4 = res;
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (CroperinoConfig.REQUEST_PICK_FILE == requestCode)
                CroperinoFileUtil.newGalleryFile(data, ActivityCarViolation.this);
            Uri i = Uri.fromFile(CroperinoFileUtil.getmFileTemp());
            Bitmap selectedImage = BitmapFactory.decodeFile(i.getPath());
            //imgViolation.setImageBitmap(selectedImage);

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            selectedImage.compress(Bitmap.CompressFormat.JPEG, 30, out);
            selectedImage = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
            setImage(selectedImageNumber, selectedImage);
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        /*switch (requestCode) {
            case CroperinoConfig.REQUEST_TAKE_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    *//* Parameters of runCropImage = File, Activity Context, Image is Scalable or Not, Aspect Ratio X, Aspect Ratio Y, Button Bar Color, Background Color *//*
                    Croperino.runCropImage(CroperinoFileUtil.getmFileTemp(), ActivityCarViolation.this, true, 1, 1, 0, 0);
                }
                break;
            case CroperinoConfig.REQUEST_PICK_FILE:
                if (resultCode == Activity.RESULT_OK) {
                    CroperinoFileUtil.newGalleryFile(data, ActivityCarViolation.this);
                    Croperino.runCropImage(CroperinoFileUtil.getmFileTemp(), ActivityCarViolation.this, true, 1, 1, 0, 0);
                }
                break;
            case CroperinoConfig.REQUEST_CROP_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    Uri i = Uri.fromFile(CroperinoFileUtil.getmFileTemp());
                    Bitmap selectedImage = BitmapFactory.decodeFile(i.getPath());
                    violationBitmap = selectedImage;
                    imgViolation.setImageBitmap(selectedImage);
                    //Do saving / uploading of photo method here.
                    //The image file can always be retrieved via CroperinoFileUtil.getmFileTemp()
                }
                break;
            default:
                hasImage = false;
                violationBitmap = null;
                break;
        }*/
    }

    private File getTempFile() {

        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {

            File file = new File(Environment.getExternalStorageDirectory(),
                    TEMP_PHOTO_FILE);
            try {
                file.createNewFile();
            } catch (IOException e) {
            }

            return file;
        } else {

            return null;
        }
    }

    @Override
    public void onClickOkButton(DialogInterface pDialog, int pDialogIntefier) {
        txtMobile.setText("");
        pDialog.dismiss();
    }

    private class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
                txtGPSLocation.setText(Double.toString(location.getLatitude()) + "," + Double.toString(location.getLongitude()));
                lm.removeUpdates(locationlistener);
            }
        }

        @Override
        public void onProviderDisabled(String provider) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

    }

    private TimePickerDialog.OnTimeSetListener mSubmitTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
                    submitTime.setText(hourOfDay + ":" + minute + ":00");
                }
            };

    private DatePickerDialog.OnDateSetListener mSubmitDateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                    monthOfYear++;
                    submitDate.setText(year + "/" + monthOfYear + "/" + dayOfMonth);
                }
            };

    public boolean isInternetOn() {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {


            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {


            return false;
        }
        return false;
    }

    private class DatabaseUpdater extends AsyncTask<Void, Void, ResultAdd> {
        ProgressDialog progressDialog;
        CarViolation carViolation;
        DatabaseUpdater(CarViolation carViolation) {
            this.carViolation = carViolation;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ActivityCarViolation.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
                progressDialog.setProgressNumberFormat("");
                progressDialog.setProgressPercentFormat(null);
            }
            progressDialog.setMessage(getResources().getString(R.string.fetching_data_from_server));
            progressDialog.setCanceledOnTouchOutside(false);

            progressDialog.show();
        }

        @Override
        protected ResultAdd doInBackground(Void... voids) {
            int id = Configuration.getInstance().getInt(SharedPrefs.ID);
            return WebServiceSyncImp.addCarViolation(carViolation);
        }

        @Override
        protected void onPostExecute(ResultAdd result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result!= null && result.getState() != null && Integer.valueOf(result.getState()) == 1) {
                if (DatabaseService.updateViolationNotMember(carViolation))
                    finish();
                else
                    Toast.makeText(getApplicationContext(), getString(R.string.un_sucess), Toast.LENGTH_LONG).show();
            }else {
                Toast.makeText(getApplicationContext(), result.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }
    }


}
