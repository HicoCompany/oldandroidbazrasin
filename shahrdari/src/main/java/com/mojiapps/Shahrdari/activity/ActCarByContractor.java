package com.mojiapps.Shahrdari.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.webservice.Params;

/**
 * Created by mojtaba on 21/10/2014.
 */
public class ActCarByContractor extends ActivityConfigAware {

    ActionBar actionBar;
    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_car_by_contractor);

        int contractorId = getIntent().getExtras().getInt("contractorId");
        String contractorName = getIntent().getExtras().getString("contractorName");

        /*FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Bundle bundle = new Bundle();
        bundle.putInt("contractorId", contractorId);
        FragCar fragCar = new FragCar();
        fragCar.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragCarByContractor, fragCar);
        fragmentTransaction.commit();*/

        Fragment fragment = new FragmentStaffCarList();
        Bundle bundle = new Bundle();
        Log.e("1",getIntent().getExtras().getInt("contractorId")+"");
        bundle.putString(Params.FROM, Params.CONTRACTOR);
        bundle.putInt(Params.CONTRACTOR_ID, contractorId);
        fragment.setArguments(bundle);
        FragmentTransaction tran = getSupportFragmentManager().beginTransaction();
        tran.replace(R.id.fragCarByContractor, fragment);
        tran.commit();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setIcon(R.drawable.hico_logo_last);//ic_launcher_light

        actionBar.setTitle("لیست خودروهای");
        actionBar.setSubtitle(contractorName);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_car, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}