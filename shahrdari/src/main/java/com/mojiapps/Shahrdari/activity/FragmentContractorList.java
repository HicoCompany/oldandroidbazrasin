package com.mojiapps.Shahrdari.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.*;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.ahmadian.ruhallah.commons.fragment.list.FactorySmartItemView;
import com.ahmadian.ruhallah.commons.fragment.list.SmartFragment;
import com.ahmadian.ruhallah.commons.fragment.list.SmartItemView;
import com.ahmadian.ruhallah.commons.utils.text.Typefaces;
import com.ahmadian.ruhallah.commons.utils.text.span.CustomSpannableStringConstructor;
import com.ahmadian.ruhallah.commons.widgets.SmartTextView;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.database.DatabaseService;
import com.mojiapps.Shahrdari.enums.Fonts;
import com.mojiapps.Shahrdari.items.ItemViewContractor;
import com.mojiapps.Shahrdari.models.Contractor;
import com.mojiapps.Shahrdari.utils.Texts;
import com.mojiapps.Shahrdari.webservice.WebServiceSyncImp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruhallah-PC on 12/19/2016.
 */

public class FragmentContractorList extends SmartFragment<Contractor>
    implements SearchView.OnQueryTextListener {

    private String searchStr = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setType(Type.STATIC);
        View view =  super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        recyclerView.setBackgroundColor(getResources().getColor(R.color.fragment_backcolor));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRefreshing(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_contractor, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchItem.expandActionView();
        searchView.setIconified(false);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint(new CustomSpannableStringConstructor(getContext())
                .setText(getString(R.string.search_hint_contractor))
                .setForegroundColor(ContextCompat.getColor(getContext(), R.color.blue_100))
                .setRelativeSize(0.6f)
                .build());

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if(menu.findItem(R.id.action_search_license)!=null) {
            MenuItem item = menu.findItem(R.id.action_search_license);
            item.setVisible(false);
        }
        if(menu.findItem(R.id.action_search_contractor)!=null) {
            MenuItem item1 = menu.findItem(R.id.action_search_contractor);
            item1.setVisible(false);}
        if(menu.findItem(R.id.action_search_staff_car)!=null) {
            MenuItem item2 = menu.findItem(R.id.action_search_staff_car);
            item2.setVisible(false);}
    }
    @Override
    protected FactorySmartItemView getViewFactory() {
        return new FactorySmartItemView() {
            @Override
            public SmartItemView makeView(String searchPhrase) {
                ItemViewContractor itemView = new ItemViewContractor(getContext());
                itemView.setSearchPhrase(searchPhrase);
                itemView.setOnItemClickListener(new SmartItemView.OnItemClickListener<Contractor>() {
                    @Override
                    public void click(final Contractor item, View view, int position) {
                        Log.e("StaffCard click" ,item+" "+position);
                    }
                });
                itemView.setOnLongClickListener(new SmartItemView.OnItemClickListener<Contractor>() {
                    @Override
                    public void click(final Contractor item, View view, int position) {
                        final String[] menuItems = getResources().getStringArray(R.array.contractor_context_menu_items);

                        ListAdapter adapter = new ArrayAdapter<CharSequence>(getActivity(), android.R.layout.select_dialog_item,
                                android.R.id.text1, menuItems) {

                            @Override
                            public View getView(final int position, View convertView, ViewGroup parent) {
                                View view = super.getView(position, convertView, parent);

                                TextView textView = (TextView)view.findViewById(android.R.id.text1);

                                textView.setTypeface(Typefaces.get(getContext(), null));
                                textView.setTextSize(17);
                                return view;
                            }
                        };

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        SmartTextView txtTitle = new SmartTextView(getActivity());
                        txtTitle.setText(getResources().getString(R.string.choose));
                        txtTitle.setTextSize(21);
                        txtTitle.setPadding(0,20,20,0);
                        txtTitle.setBackgroundColor(Color.DKGRAY);
                        txtTitle.setTextColor(Color.LTGRAY);
                        txtTitle.setGravity(Gravity.CENTER);
                        builder.setCustomTitle(txtTitle);
                        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int position) {
                                if (menuItems[position].equalsIgnoreCase(getString(R.string.show_licenses))) {
                                    Intent intent = new Intent(getActivity(), ActLicenseByContractor.class);
                                    intent.putExtra("contractorId", item.getContractorId());
                                    intent.putExtra("contractorName", item.getOwName());
                                    startActivity(intent);
                                } else if (menuItems[position].equalsIgnoreCase(getString(R.string.show_cars_contractor))) {
                                    Intent intent = new Intent(getActivity(), ActCarByContractor.class);
                                    intent.putExtra("contractorId", item.getContractorId());
                                    intent.putExtra("contractorName", item.getOwName());
                                    startActivity(intent);
                                }
                            }
                        });

                        builder.setCancelable(true);
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }
                });
                return itemView;
            }
        };
    }

    @Override
    protected List<Contractor> fetchMoreDataAsync(int startPage, int count) {
        return null;
    }

    @Override
    protected List<Contractor> fetchDataAsync() {
        if (searchStr != null && searchStr.length() > 0)
            return DatabaseService.getContractors(searchStr);
        else
            return new ArrayList<>();//DatabaseService.getContractors();
    }

    @Override
    protected void onOperationFailed() {

    }

    ProgressDialog progressDialog;
    private void updateContractors() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            progressDialog.setProgressNumberFormat("");
            progressDialog.setProgressPercentFormat(null);
        }
        //TextView txtMessage = (TextView)progressDialog.findViewById(getResources().getIdentifier("title", "id", "android"));
        //txtMessage.setTypeface(Texts.getFont(getActivity(), Fonts.YEKAN));
        progressDialog.setMessage(getResources().getString(R.string.fetching_data_from_server));
        progressDialog.setCanceledOnTouchOutside(false);

        new ContractorUpdater().execute();
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        searchStr = s;
        list.clear();
        loadData();
        return false;
    }

    private class ContractorUpdater extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            progressDialog.dismiss();
            if (result)
                Toast.makeText(getActivity(), "موفقیت آمیز بود", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(getActivity(), "دریافت اطلاعات مجوزها موفقیت آمیز نبود", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            return WebServiceSyncImp.updateContractor();
        }

    }

}
