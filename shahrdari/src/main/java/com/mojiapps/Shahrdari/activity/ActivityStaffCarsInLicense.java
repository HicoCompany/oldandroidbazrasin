package com.mojiapps.Shahrdari.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.webservice.Params;

public class ActivityStaffCarsInLicense extends ActivityConfigAware {

    private ActionBar actionBar;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_cars_in_license);

        int licenseId = getIntent().getExtras().getInt(Params.LICENSE_ID);

        Fragment fragment = new FragmentStaffCarList();
        Bundle bundle = new Bundle();
        bundle.putString(Params.FROM, Params.LICENSE);
        bundle.putInt(Params.LICENSE_ID, licenseId);
        fragment.setArguments(bundle);
        FragmentTransaction tran = getSupportFragmentManager().beginTransaction();
        tran.replace(R.id.container, fragment);
        tran.commit();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setIcon(R.drawable.hico_logo_last);//ic_launcher_light

        actionBar.setTitle("لیست خودروهای در مجوز "+ licenseId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_car, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
