package com.mojiapps.Shahrdari.activity;

import android.net.ParseException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import com.ahmadian.ruhallah.commons.widgets.SmartSpinner;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.ShahrdariApp;
import com.mojiapps.Shahrdari.webservice.Params;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ruhallah-PC on 12/20/2016.
 */

public class ActivityInquiry extends AppCompatActivity {

    @Bind(R.id.type_inquiry)
    SmartSpinner typeInquiry;
    @Bind(R.id.et_staff_car_id)
    EditText etStaffCarId;
    @Bind(R.id.ch_haffar)
    CheckBox chHaffar;
    @Bind(R.id.et_old_plate_number)
    EditText oldPlateNumber;
    @Bind(R.id.NewPlatePart1)
    EditText newPlatePart1;
    @Bind(R.id.NewPlatePart2)
    EditText newPlatePart2;
    @Bind(R.id.NewPlatePart3)
    EditText newPlatePart3;
    @Bind(R.id.NewPlatePart4)
    EditText newPlatePart4;

    @Bind(R.id.car_search_field)
    RelativeLayout carSearchField;
    @Bind(R.id.new_plate_field)
    LinearLayout newPlateField;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inquiry);
        ButterKnife.bind(this, this);

        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        actionBar.setIcon(R.drawable.hico_logo_last);//ic_launcher_light
        actionBar.setTitle(getString(R.string.inquiry));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        init();

        if (getIntent() != null) {
            if (getIntent().getIntExtra(Params.STAFF_CAR_ID, -1) != -1) {
                Fragment fragment = new FragmentStaffCarViolationList();
                Bundle bundle = new Bundle();
                Log.e("1", getIntent().getIntExtra(Params.STAFF_CAR_ID, -1) + "");
                bundle.putInt(Params.STAFF_CAR_ID, getIntent().getIntExtra(Params.STAFF_CAR_ID, -1));
                fragment.setArguments(bundle);
                FragmentTransaction tran = getSupportFragmentManager().beginTransaction();
                tran.replace(R.id.container, fragment);
                tran.commit();

                typeInquiry.setSelection(1);
                etStaffCarId.setText(getIntent().getIntExtra(Params.STAFF_CAR_ID, -1) + "");
            }
        }
    }

    private void init() {
        typeInquiry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    showCarField();
                    actionBar.setSubtitle(getString(R.string.add_violation_not_member));
                }else {
                    showStaffCarField();
                    actionBar.setSubtitle(getString(R.string.add_violation_member));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        chHaffar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    showOldPlateField();
                }else {
                    showNewPlateField();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_inquiry, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                search();
                break;
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showCarField() {
        carSearchField.setVisibility(View.VISIBLE);
        etStaffCarId.setVisibility(View.INVISIBLE);
    }

    private void showStaffCarField() {
        carSearchField.setVisibility(View.INVISIBLE);
        etStaffCarId.setVisibility(View.VISIBLE);
    }

    private void showOldPlateField() {
        oldPlateNumber .setVisibility(View.VISIBLE);
        newPlateField.setVisibility(View.INVISIBLE);
    }

    private void showNewPlateField() {
        newPlateField.setVisibility(View.VISIBLE);
        oldPlateNumber.setVisibility(View.INVISIBLE);
    }

    private void search() {
        if (typeInquiry.getSelectedItemPosition() == 0) {
            Fragment fragment = new FragmentCarViolationList();
            Bundle bundle = new Bundle();
            Log.e("1",getIntent().getIntExtra(Params.STAFF_CAR_ID, -1)+"");
            bundle.putBoolean(Params.PLATE_TYPE , !chHaffar.isChecked());
            if (chHaffar.isChecked()) {
                String oldPlateNumberStr = ShahrdariApp.replaceNumbers(oldPlateNumber.getText().toString());

                bundle.putString(Params.OLD_PLATE, oldPlateNumberStr);
            }else {
                String newPlatePart1Str = ShahrdariApp.replaceNumbers(newPlatePart1.getText().toString());
                bundle.putString(Params.NEW_PLATE_PART_1, newPlatePart1Str);
                String newPlatePart2Str = ShahrdariApp.replaceNumbers(newPlatePart2.getText().toString());
                bundle.putString(Params.NEW_PLATE_PART_2, newPlatePart2Str);
                String newPlatePart3Str = ShahrdariApp.replaceNumbers(newPlatePart3.getText().toString());
                bundle.putString(Params.NEW_PLATE_PART_3, newPlatePart3Str);
                String newPlatePart4Str = ShahrdariApp.replaceNumbers(newPlatePart4.getText().toString());
                bundle.putString(Params.NEW_PLATE_PART_4, newPlatePart4Str);
            }

            fragment.setArguments(bundle);
            FragmentTransaction tran = getSupportFragmentManager().beginTransaction();
            tran.replace(R.id.container, fragment);
            tran.commit();
        }else {
            Fragment fragment = new FragmentStaffCarViolationList();
            Bundle bundle = new Bundle();
            Log.e("1",getIntent().getIntExtra(Params.STAFF_CAR_ID, -1)+"");
            String etStaffCarIdStr = ShahrdariApp.replaceNumbers(etStaffCarId.getText().toString());
            try {
                bundle.putInt(Params.STAFF_CAR_ID, Integer.valueOf(etStaffCarIdStr));
                fragment.setArguments(bundle);
                FragmentTransaction tran = getSupportFragmentManager().beginTransaction();
                tran.replace(R.id.container, fragment);
                tran.commit();
            }catch (ParseException e) {
                Toast.makeText(getApplicationContext(), "شماره نامعتبر می باشد", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
