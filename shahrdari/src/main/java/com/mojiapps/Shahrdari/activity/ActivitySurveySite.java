package com.mojiapps.Shahrdari.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mikelau.croperino.CroperinoConfig;
import com.mikelau.croperino.CroperinoFileUtil;
import com.mojiapps.Shahrdari.Configuration;
import com.mojiapps.Shahrdari.GPSServices;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.ShahrdariApp;
import com.mojiapps.Shahrdari.dialog.CustomAlertDialogOk;
import com.mojiapps.Shahrdari.dialog.DialogClickInterfaceOk;
import com.mojiapps.Shahrdari.enums.SharedPrefs;
import com.mojiapps.Shahrdari.models.Booth;
import com.mojiapps.Shahrdari.models.QuestionEntry;
import com.mojiapps.Shahrdari.models.SiteEntry;
import com.mojiapps.Shahrdari.models.SurveyEntry;
import com.mojiapps.Shahrdari.utils.ConnectionDetector;
import com.mojiapps.Shahrdari.utils.Texts;
import com.mojiapps.Shahrdari.utils.Utils;
import com.mojiapps.Shahrdari.webservice.WebServiceSyncImp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;

/**
 * Created by mojtaba on 10/6/14.
 */
public class ActivitySurveySite extends ActivityConfigAware implements AdapterView.OnItemSelectedListener,DialogClickInterfaceOk {

    @Bind(R.id.description)
    EditText description;
    @Bind(R.id.submit_date)
    TextView submitDate;
    @Bind(R.id.submit_time)
    TextView submitTime;

    ActionBar actionBar;

    EditText txtReportDescription;
    EditText txtFollowDescription;
    EditText txtFinalDescription;
    private static List<String> QuestionList = new ArrayList<String>();
    Spinner spBooth;
    List<SiteEntry> SitesList = new ArrayList<>();
    SurveyEntry survey = new SurveyEntry();
    private static ListView androidListView;
    Button btnSave;
    static List<Booth> lstBooth=new ArrayList<>();
    static List<SiteEntry> lstSites=new ArrayList<>();
    private Toolbar toolbar;

    Double longitude = 0.0, latitude = 0.0;
    GPSServices mService;
    private static final int INITIAL_REQUEST = 1337;
    SharedPreferences sharedPreferences;
    private static final String[] INITIAL_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.INTERNET};
    private final static int REQUEST_CHECK_SETTINGS_GPS=0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS=0x2;

    private DateFormat datetimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    private DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        setContentView(R.layout.activity_survey);
        // ButterKnife.bind(this, this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        actionBar.setIcon(R.drawable.hico_logo_last);//ic_launcher_light

        toolbar.setTitle(Configuration.getInstance().getString(SharedPrefs.NAME));

        toolbar.setSubtitle(Texts.ChangeToUnicode(Utils.getCurrentDate()));

        actionBar.setDisplayHomeAsUpEnabled(true);


            String[] boothTitle = { "سایت جمکران" ,"سایت شادقلی", "سایت کوه سفید",
                   "سایت پردیسان" };

        spBooth = (Spinner) findViewById(R.id.spBooth);
            spBooth.setAdapter(new BoothAdapter(ActivitySurveySite.this, R.id.txtCategory, boothTitle));
//        spBooth.setAdapter(new BoothAdapter(this, R.id.txtCategory, getResources().getStringArray(R.array.evaluation_category)));
            spBooth.setOnItemSelectedListener(ActivitySurveySite.this);
        ///////////////////////////////////
//        if(isInternetOn()) {
//            new DatabaseUpdaterBooth().execute();
//        }
//        else
//        {
//
//            CustomAlertDialogOk.getInstance().showConfirmDialog("توجه", "لطفا اینترنت Wifi یا داده ی خود را روشن نمایید", "تایید", ActivitySurveySite.this, -1);
//        }
        //////////////////////////////////
        txtReportDescription=(EditText) findViewById(R.id.txtReportDescription);
        txtFollowDescription=(EditText) findViewById(R.id.txtFollowDescription);
        txtFinalDescription=(EditText) findViewById(R.id.txtFinalDescription);

        // lstBooth = DatabaseService.getBooths();

        //////////////////////////////////////////////////
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        if(isInternetOn()) {
            new DatabaseGetQuestions().execute();
        }
        else
        {

            CustomAlertDialogOk.getInstance().showConfirmDialog("توجه", "لطفا اینترنت Wifi یا داده ی خود را روشن نمایید", "تایید", ActivitySurveySite.this, -1);
        }

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.

        int version = Build.VERSION.SDK_INT;
        // String versionRelease = Build.VERSION.RELEASE;
        mService = new GPSServices(ActivitySurveySite.this);
        mService.getLocation();
        if (version >= 23 && mService != null && !mService.isLocationAvailable) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);

            }


//            if (checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, getApplicationContext(), MainActivity.this)) {
//                fetchLocationData();
//            } else {
//                requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, PERMISSION_REQUEST_CODE_LOCATION, getApplicationContext(), MainActivity.this);
//
//            }
        }
        else
        {
            fetchLocationData();
            //getMyLocation();

        }

    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("ActivitySurvey Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    @Override
    public void onClickOkButton(DialogInterface pDialog, int pDialogIntefier) {
        pDialog.dismiss();
        if(pDialogIntefier==1 || pDialogIntefier==-1)
            finish();
    }

    private class BoothAdapter extends ArrayAdapter<String> {
        private String[] values;
        TextView txtCategory;
        ImageView imgCategory;

        public BoothAdapter(Context context, int textViewResourceId, String[] data) {
            super(context, textViewResourceId, data);
            this.values = data;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) ShahrdariApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = convertView;

            if (row == null) {
                row = inflater.inflate(R.layout.survey_category_spinner_row, parent, false);
                txtCategory = (TextView) row.findViewById(R.id.txtCategory);
                imgCategory = (ImageView) row.findViewById(R.id.imgCategory);
            } else {
                txtCategory = (TextView) convertView.findViewById(R.id.txtCategory);
                imgCategory = (ImageView) convertView.findViewById(R.id.imgCategory);
            }

            txtCategory.setText(values[position]);
            /*txtCategory.setTypeface(Texts.getFont(Fonts.DEFAULT));*/


            String pngName = "";
            if (position == 0)
                pngName = "ایستگاه 1";
            else if (position == 1)
                pngName = "ایستگاه 2";
            else if (position == 2) {
                pngName = "ایستگاه 3";
            } else if (position == 3) {
                pngName = "ایستگاه 4";
            } else if (position == 4) {
                pngName = "ایستگاه 5";
            }

//            imgCategory.setImageResource(getContext().getResources().getIdentifier(pngName, "drawable", getContext().getPackageName()));
            imgCategory.setImageResource(R.drawable.arrow_down_drop_sppiner);
            return row;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_done:
                save();
                break;
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void init() {
        // txtCarId = (EditText) findViewById(R.id.txtCarId);
//        btnGPS = (ImageButton) findViewById(R.id.cmdGPS);
//        txtGPSLocation = (EditText) findViewById(R.id.txtGPSLocation);
//        spCertificate = (Spinner) findViewById(R.id.spCertificate);
//        txtAddressViolation = (EditText) findViewById(R.id.txtAddressViolation);
//        txtViolation = (TextView) findViewById(R.id.txtViolationType);
//
//        btnSave = (Button) findViewById(R.id.btnSaveViolation);
//        txtCarOwnerName = (TextView) findViewById(R.id.txtCarOwnerName);

        //txtCarId.setOnFocusChangeListener(this);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (CroperinoConfig.REQUEST_PICK_FILE == requestCode)
                CroperinoFileUtil.newGalleryFile(data, ActivitySurveySite.this);
            Uri i = Uri.fromFile(CroperinoFileUtil.getmFileTemp());
            Bitmap selectedImage = BitmapFactory.decodeFile(i.getPath());
            //imgViolation.setImageBitmap(selectedImage);

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            selectedImage.compress(Bitmap.CompressFormat.JPEG, 30, out);
            selectedImage = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
            // setImage(selectedImageNumber, selectedImage);
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void save() {
        if (validate()) {
            survey = new SurveyEntry();
            survey.setBoothId((spBooth.getSelectedItemPosition()+1));
            survey.setReportDescription(txtReportDescription.getText().toString());
            survey.setFollowDescription(txtFollowDescription.getText().toString());
            survey.setFinalDescription(txtFinalDescription.getText().toString());
            int id = Configuration.getInstance().getInt(SharedPrefs.ID);
            survey.setPersonnelID(id);
            //survey.setQuestionListJson(questionsList);
            survey.setSites(SitesList);

            survey.setLatitudeLocation(Float.valueOf(String.valueOf(latitude)));
            survey.setLongitudeLocation(Float.valueOf(String.valueOf(longitude)));
//جهت ثبت در SQLLite
//            if (DatabaseService.SaveSurvey(survey))
//                finish();
//            else
//                Toast.makeText(getApplicationContext(), getString(R.string.un_sucess), Toast.LENGTH_LONG).show();
            ConnectionDetector cd = new ConnectionDetector(this);
            if (!cd.isConnectingToInternet()){
                Toast.makeText(this, getString(R.string.cant_connect_to_server), Toast.LENGTH_SHORT).show();

            }
            new DatabaseUpdater().execute();

        }
    }

    private boolean validate() {
        boolean b = true;

        if (txtReportDescription.getText()==null ) {
            Toast.makeText(ActivitySurveySite.this,("شرح گزارش را وارد نمایید!"),Toast.LENGTH_SHORT).show();
            b = false;
        }

        if (!false)
            if (txtFollowDescription.getText()==null ) {
                Toast.makeText(ActivitySurveySite.this,("نتیجه ی پیگیری ناظر را وارد نمایید!"),Toast.LENGTH_SHORT).show();
                b = false;
            }

        if (!false)
            if (txtFinalDescription.getText()==null ) {
                Toast.makeText(ActivitySurveySite.this,("نظریه ی نهایی و نتیجه ی پیگیری ریسس را وارد نمایید!"),Toast.LENGTH_SHORT).show();
                b = false;
            }

        return b;
    }


    private void setAdapter() {

        List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();
        for (int i = 0; i < lstSites.size(); i++) {

            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put("listView_title", lstSites.get(i).getTitle());
            //hm.put("listView_title", QuestionList.get(i));

            aList.add(hm);
            //    CodeList.add((hm1));

        }

        String[] from = {"listView_title"};
        int[] to = {R.id.listView_item_title};

        SimpleAdapter simpleAdapter = new SimpleAdapter(getBaseContext(), aList, R.layout.survey_list_view, from, to) {
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                RadioGroup rg=(RadioGroup)view.findViewById(R.id.RG);
                rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        Boolean State=true;
                        int selectedId= group.getCheckedRadioButtonId();
                        if(selectedId==R.id.RG_No){
                            State=false;

                        }
                        else if(selectedId==R.id.RG_Yes){
                            State=true;
                        }
//                    if(position==SitesList.size())
//                        SitesList.get(position-1).setState(State);
//                        else
                        SitesList.get(position).setState(State);
                    }
                });

                return view;

            }
        };

        androidListView = (ListView) findViewById(R.id.list_view);
//        androidListView.setScrollContainer(false);
//        androidListView.setEnabled(false);
        if (androidListView != null) {
            androidListView.setAdapter(simpleAdapter);
            androidListView.setClickable(true);

            androidListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }
            });

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String a;
        a="";
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class DatabaseUpdater extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ActivitySurveySite.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
                progressDialog.setProgressNumberFormat("");
                progressDialog.setProgressPercentFormat(null);
            }
            progressDialog.setMessage(getResources().getString(R.string.fetching_data_from_server));
            progressDialog.setCanceledOnTouchOutside(false);

            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            return WebServiceSyncImp.SaveSurveySite(survey);

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result.equals("NoConnection"))
                CustomAlertDialogOk.getInstance().showConfirmDialog("توجه", getString(R.string.cant_connect_to_server), "تایید", ActivitySurveySite.this, 0);
            else {
                CustomAlertDialogOk.getInstance().showConfirmDialog("توجه", "نظرسنجی با موفقیت ثبت شد", "تایید", ActivitySurveySite.this, 1);
            }

        }
    }

    public List<SiteEntry> getSites(){
        List<SiteEntry> Sites=new ArrayList<>();
        for (int i=0 ;i<lstSites.size();i++)
        {
            Sites.add(new SiteEntry(i,lstSites.get(i).getTitle(),true));

        }
        return Sites;
    }







    private class DatabaseGetQuestions extends AsyncTask<Void, Void, String> {
        // ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog = new ProgressDialog(ActivitySurvey.this);
//            progressDialog.setIndeterminate(true);
//            progressDialog.setCancelable(false);
//            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
//                progressDialog.setProgressNumberFormat("");
//                progressDialog.setProgressPercentFormat(null);
//            }
//            progressDialog.setMessage(getResources().getString(R.string.fetching_data_from_server));
//            progressDialog.setCanceledOnTouchOutside(false);
//
//            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            lstSites= WebServiceSyncImp.getSites();
            return "t";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result.equals("NoConnection"))
                CustomAlertDialogOk.getInstance().showConfirmDialog("توجه", getString(R.string.cant_connect_to_server), "تایید", ActivitySurveySite.this, 0);

            if(lstSites!=null && lstSites.size()>0) {
                SitesList = getSites();
                String[] QuestionTitle = new String[lstSites.size()];

                QuestionList = new ArrayList<String>();
                QuestionList = Arrays.asList(QuestionTitle);
                setAdapter();
            }
            else {
//                String[] boothTitle = new String[1];
//                spBooth = (Spinner) findViewById(R.id.spBooth);
//                boothTitle[0] = "آیتمی وجود ندارد!";
//                spBooth.setAdapter(new BoothAdapter(ActivitySurvey.this, R.id.txtCategory, boothTitle));
////        spBooth.setAdapter(new BoothAdapter(this, R.id.txtCategory, getResources().getStringArray(R.array.evaluation_category)));
//                spBooth.setOnItemSelectedListener(ActivitySurvey.this);
            }

//

        }
    }
    public boolean isInternetOn() {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {


            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {


            return false;
        }
        return false;
    }

    private void fetchLocationData()
    {
        //code to use the granted permission (location)
//        mService = new GPSService(MainActivity.this);

        mService.getLocation();
        latitude=mService.getLatitude();
        longitude=mService.getLongitude();



        if (!mService.isLocationAvailable) {

            // Here you can ask the use
            // r to try again, using return; for that
            Toast.makeText(ActivitySurveySite.this, "موقعیت شما مشخص نیست. لطفا GPS خود را روشن کنید.", Toast.LENGTH_SHORT).show();

        } else {
            // Getting reference to the SupportMapFragment

            ///////current address
            longitude = mService.getLongitude();
            latitude = mService.getLatitude();

            // Toast.makeText(MainActivity.this,latitude.toString(),Toast.LENGTH_SHORT).show();
        }
        Toast.makeText(ActivitySurveySite.this,latitude.toString()+'-'+longitude.toString(),Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onRestart() {
        if(mService!=null)
        {
            mService.getLocation();
            latitude = mService.getLatitude();
            longitude = mService.getLongitude();
            Toast.makeText(ActivitySurveySite.this,latitude.toString()+'-'+longitude.toString(),Toast.LENGTH_SHORT).show();
//            if(mMap!=null) {
//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 14.0f));
//            if (getNearDriverTask != null)
//                getNearDriverTask.cancel();
//            getNearDriverTask = new TaskEx();
//            getNearDriverTask.execute();
            //  }

        }
        super.onRestart();
    }


}

