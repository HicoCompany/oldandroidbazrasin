package com.mojiapps.Shahrdari.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.*;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.ahmadian.ruhallah.commons.fragment.list.FactorySmartItemView;
import com.ahmadian.ruhallah.commons.fragment.list.SmartFragment;
import com.ahmadian.ruhallah.commons.fragment.list.SmartItemView;
import com.ahmadian.ruhallah.commons.utils.text.Typefaces;
import com.ahmadian.ruhallah.commons.utils.text.span.CustomSpannableStringConstructor;
import com.ahmadian.ruhallah.commons.widgets.SmartTextView;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.ShahrdariApp;
import com.mojiapps.Shahrdari.database.DatabaseService;
import com.mojiapps.Shahrdari.items.ItemViewStaffCar;
import com.mojiapps.Shahrdari.items.ItemViewStaffCarViolation;
import com.mojiapps.Shahrdari.models.Car;
import com.mojiapps.Shahrdari.models.HomeViolation;
import com.mojiapps.Shahrdari.models.StaffCar;
import com.mojiapps.Shahrdari.models.StaffCarViolation;
import com.mojiapps.Shahrdari.webservice.Params;
import com.mojiapps.Shahrdari.webservice.WebServiceSyncImp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruhallah-PC on 12/17/2016.
 */

public class FragmentStaffCarList extends SmartFragment<StaffCar>
        implements SearchView.OnQueryTextListener{

    private String searchStr = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        setType(Type.STATIC);
        View view =  super.onCreateView(inflater, container, savedInstanceState);
        recyclerView.setBackgroundColor(getResources().getColor(R.color.fragment_backcolor));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRefreshing(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (getArguments() == null) {
            inflater.inflate(R.menu.menu_car, menu);

            MenuItem searchItem = menu.findItem(R.id.action_search);

            SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
            searchItem.expandActionView();
            searchView.setIconified(false);
            searchView.setOnQueryTextListener(this);
            searchView.setQueryHint(new CustomSpannableStringConstructor(getContext())
                    .setText(getString(R.string.search_hint_staff_car))
                    .setForegroundColor(ContextCompat.getColor(getContext(), R.color.blue_100))
                    .setRelativeSize(0.6f)
                    .build());
        }
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
      if(menu.findItem(R.id.action_search_license)!=null) {
          MenuItem item = menu.findItem(R.id.action_search_license);
          item.setVisible(false);
      }
         if(menu.findItem(R.id.action_search_contractor)!=null) {
          MenuItem item1 = menu.findItem(R.id.action_search_contractor);
          item1.setVisible(false);
      }

        if(menu.findItem(R.id.action_search_staff_car)!=null) {
            MenuItem item2 = menu.findItem(R.id.action_search_staff_car);
            item2.setVisible(false);
        }
        // menu.clear();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected FactorySmartItemView getViewFactory() {
        return new FactorySmartItemView() {
            @Override
            public SmartItemView makeView(String searchPhrase) {
                ItemViewStaffCar itemView = new ItemViewStaffCar(getContext());
                itemView.setSearchPhrase(searchPhrase);
                itemView.setOnItemClickListener(new SmartItemView.OnItemClickListener<StaffCar>() {
                    @Override
                    public void click(final StaffCar item, View view, int position) {
                        Log.e("StaffCard click" ,item+" "+position);
                    }
                });
                itemView.setOnLicenseIdListener(new SmartItemView.OnItemClickListener<StaffCar>() {
                    @Override
                    public void click(StaffCar item, View view, int position) {
                        Fragment fragment = new FragmentLicenseList(item.getLastLicenseId()+"");
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .add(android.R.id.content, fragment)
                                .addToBackStack(null)
                                .commit();
                    }
                });
                itemView.setOnLongClickListener(new SmartItemView.OnItemClickListener<StaffCar>() {
                    @Override
                    public void click(final StaffCar item, View view, int position) {
                        final String[] menuItems = getResources().getStringArray(R.array.car_context_menu_items);

                        ListAdapter adapter = new
                                ArrayAdapter<CharSequence>(getActivity(), android.R.layout.select_dialog_item,
                                android.R.id.text1, menuItems) {

                            @Override
                            public View getView(final int position, View convertView, ViewGroup parent) {
                                View view = super.getView(position, convertView, parent);
                                TextView tView = (TextView) view.findViewById(android.R.id.text1);

                                tView.setTypeface(Typefaces.get(getContext(), null));
                                tView.setTextSize(17);
                                return view;
                            }
                        };


                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        SmartTextView txtTitle = new SmartTextView(getActivity());
                        txtTitle.setText(getResources().getString(R.string.choose));
                        txtTitle.setTextSize(21);
                        txtTitle.setPadding(0,20,20,0);
                        txtTitle.setBackgroundColor(Color.DKGRAY);
                        txtTitle.setTextColor(Color.LTGRAY);
                        txtTitle.setGravity(Gravity.CENTER);
                        builder.setCustomTitle(txtTitle);
                        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int position) {
                                if (menuItems[position].equalsIgnoreCase(getString(R.string.add_car_violation))) {
                                    Intent intent = new Intent(getActivity(), ActivityStaffCarViolation.class);
                                    intent.putExtra("carId", item.getStaffCarId());
                                    startActivity(intent);
                                }else if (menuItems[position].equalsIgnoreCase(getString(R.string.visit_violations))) {
                                    Intent intent = new Intent(getActivity(), ActivityInquiry.class);
                                    intent.putExtra(Params.STAFF_CAR_ID, item.getStaffCarId());
                                    startActivity(intent);
                                }
                            }
                        });

                        builder.setCancelable(true);
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }
                });
                return itemView;
            }
        };
    }

    @Override
    protected List<StaffCar> fetchMoreDataAsync(int startPage, int count) {
        return null;
    }

    @Override
    protected List<StaffCar> fetchDataAsync() {
        if (getArguments() == null) {
            if (searchStr != null && searchStr.length() > 0)
                return DatabaseService.getStaffCars(searchStr);
            else
                return new ArrayList<>();//DatabaseService.getStaffCars();
        }else if (getArguments().getString(Params.FROM,"").equals(Params.CONTRACTOR)){
            Log.e("List<StaffCar>",getArguments().getInt(Params.CONTRACTOR_ID)+"");
            return DatabaseService.getStaffCarsByContractor(getArguments().getInt(Params.CONTRACTOR_ID));
        }else if (getArguments().getString(Params.FROM, "").equals(Params.LICENSE)) {
            Log.e("List<StaffCar>",getArguments().getInt(Params.LICENSE_ID)+"");
            return DatabaseService.getStaffCarsInLicense(getArguments().getInt(Params.LICENSE_ID));
        }
        return new ArrayList<>();
    }

    @Override
    protected void onOperationFailed() {

    }

    ProgressDialog progressDialog;

    private void updateStaffCars() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            progressDialog.setProgressNumberFormat("");
            progressDialog.setProgressPercentFormat(null);
        }
        //TextView txtMessage = (TextView)progressDialog.findViewById(getResources().getIdentifier("title", "id", "android"));
        //txtMessage.setTypeface(Texts.getFont(getActivity(), Fonts.YEKAN));
        progressDialog.setMessage(getResources().getString(R.string.fetching_data_from_server));
        progressDialog.setCanceledOnTouchOutside(false);

        new StaffCarUpdater().execute();
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        searchStr = ShahrdariApp.replaceNumbers(s);
        list.clear();
        loadData();
        return false;
    }

    private class StaffCarUpdater extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            progressDialog.dismiss();
            if (result)
                Toast.makeText(getActivity(), "موفقیت آمیز بود", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(getActivity(), "دریافت اطلاعات مجوزها موفقیت آمیز نبود", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            return WebServiceSyncImp.updateStaffCars();
        }

    }

}
