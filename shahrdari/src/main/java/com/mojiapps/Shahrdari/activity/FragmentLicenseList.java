package com.mojiapps.Shahrdari.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.text.InputType;
import android.util.Log;
import android.view.*;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.ahmadian.ruhallah.commons.fragment.list.FactorySmartItemView;
import com.ahmadian.ruhallah.commons.fragment.list.SmartFragment;
import com.ahmadian.ruhallah.commons.fragment.list.SmartItemView;
import com.ahmadian.ruhallah.commons.utils.text.Typefaces;
import com.ahmadian.ruhallah.commons.utils.text.span.CustomSpannableStringConstructor;
import com.ahmadian.ruhallah.commons.widgets.SmartTextView;
import com.google.gson.reflect.TypeToken;
import com.mojiapps.Shahrdari.Configuration;
import com.mojiapps.Shahrdari.GsonSettings;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.ShahrdariApp;
import com.mojiapps.Shahrdari.database.DatabaseService;
import com.mojiapps.Shahrdari.dialog.CustomAlertDialogOk;
import com.mojiapps.Shahrdari.dialog.DialogClickInterfaceOk;
import com.mojiapps.Shahrdari.dialog.DialogDate;
import com.mojiapps.Shahrdari.dialog.DialogVerify;
import com.mojiapps.Shahrdari.dialog.DialogVerifySecond;
import com.mojiapps.Shahrdari.enums.SharedPrefs;
import com.mojiapps.Shahrdari.items.ItemViewLicense;
import com.mojiapps.Shahrdari.models.Area;
import com.mojiapps.Shahrdari.models.License;
import com.mojiapps.Shahrdari.models.Personnel;
import com.mojiapps.Shahrdari.utils.CustomFragment;
import com.mojiapps.Shahrdari.utils.DateUtils;
import com.mojiapps.Shahrdari.webservice.Params;
import com.mojiapps.Shahrdari.webservice.WebServiceSyncImp;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ruhallah-PC on 12/20/2016.
 */

@SuppressLint("ValidFragment")
public class FragmentLicenseList extends CustomFragment<License> implements SearchView.OnQueryTextListener,DialogClickInterfaceOk
       {

    private static final int REQUEST_CODE_VERIFY_LICENSE = 0;
    private static final int REQUEST_CODE_SELECT_DATE = 1;
    private static final int REQUEST_CODE_VERIFY_NO_VIOLATION_LICENSE = 3;
           Context context;
           SearchView searchView,searchContractorView,searchStaffCarView;
          static boolean searchStaffCar=false;
           @Override
           public boolean onQueryTextSubmit(String query) {

               return false;

           }

           @Override
           public boolean onQueryTextChange(String newText) {
            if( searchView.getQuery().toString().equals("") && searchContractorView.getQuery().toString().equals("") && !searchStaffCarView.getQuery().toString().equals(""))///// search on staff car
                searchStaffCar=true;
               else
                searchStaffCar=false;

               searchStr = newText;
               searchStr = ShahrdariApp.replaceNumbers(searchStr);

               list.clear();
               loadData();
               return false;

           }

           @Override
           public void onClickOkButton(DialogInterface pDialog, int pDialogIntefier) {
               pDialog.dismiss();
           }

           private enum Mod {
        ALL, DATE, AREA, MY_LICENSE, LICENSED
    }

    public Mod mod = Mod.ALL;
    private Date from, to;

    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

    private String searchStr = null;
    private int searchType = 0;

    public FragmentLicenseList() {
        String a="";
    }

    public FragmentLicenseList(String searchStr) {
        this.searchStr = searchStr;
        mod = Mod.ALL;
      //  refreshData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        context = container.getContext();
        setHasOptionsMenu(true);
        setType(Type.STATIC);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        view.setBackgroundColor(getResources().getColor(R.color.fragment_backcolor));

       refreshDataList();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRefreshing(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (getArguments() == null) {
            inflater.inflate(R.menu.menu_license, menu);

            MenuItem searchItem = menu.findItem(R.id.action_search_license);
            searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
            searchView.setOnQueryTextListener(this);
            searchView.setInputType(InputType.TYPE_CLASS_NUMBER);
            searchView.setQueryHint(new CustomSpannableStringConstructor(getContext())
                    .setText(getString(R.string.search_hint_license))
                    .setForegroundColor(ContextCompat.getColor(getContext(), R.color.blue_100))
                    .setRelativeSize(0.6f)
                    .build());

            MenuItem searchContractorItem = menu.findItem(R.id.action_search_contractor);
            searchContractorView = (SearchView) MenuItemCompat.getActionView(searchContractorItem);
            searchContractorView.setOnQueryTextListener(this);
           // searchContractorView.setInputType(InputType.TYPE_CLASS_NUMBER);
            searchContractorView.setQueryHint(new CustomSpannableStringConstructor(getContext())
                    .setText(getString(R.string.search_hint_contractor))
                    .setForegroundColor(ContextCompat.getColor(getContext(), R.color.blue_100))
                    .setRelativeSize(0.6f)
                    .build());

            MenuItem searchStaffCarItem = menu.findItem(R.id.action_search_staff_car);
             searchStaffCarView = (SearchView) MenuItemCompat.getActionView(searchStaffCarItem);
            searchStaffCarView.setOnQueryTextListener(this);
            // searchContractorView.setInputType(InputType.TYPE_CLASS_NUMBER);
            searchStaffCarView.setQueryHint(new CustomSpannableStringConstructor(getContext())
                    .setText(getString(R.string.search_hint_staff_car))
                    .setForegroundColor(ContextCompat.getColor(getContext(), R.color.blue_100))
                    .setRelativeSize(0.6f)
                    .build());
        }
          super.onCreateOptionsMenu(menu, inflater);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.action_evaluation:
//               Intent intent = new Intent(getActivity(), ActivitySurvey.class);
//                startActivity(intent);
//                return true;
            case R.id.action_search_staff_car:
//                Intent intent = new Intent(getActivity(), ActivitySurvey.class);
//                startActivity(intent);
                return true;


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected FactorySmartItemView getViewFactory() {
        return new FactorySmartItemView() {
            @Override
            public SmartItemView makeView(String searchPhrase) {
                ItemViewLicense itemView = new ItemViewLicense(getContext());
                itemView.setSearchPhrase(searchPhrase);
                itemView.setOnItemClickListener(new SmartItemView.OnItemClickListener<License>() {
                    @Override
                    public void click(final License item, View view, final int position) {
                        Log.e("StaffCard click", item + " " + position);
                    }
                });


                itemView.setOnLongClickListener(new SmartItemView.OnItemClickListener<License>() {
                    @Override
                    public void click(final License item, final View view, final int position) {
                        final String[] menuItems;
        /*if (licenses.get(i).isVerified())
            menuItems = getResources().getStringArray(R.array.license_context_menu_items_verified);
        else*/
                        menuItems = getResources().getStringArray(R.array.license_context_menu_items_not_veridied);

                        ListAdapter adapter = new ArrayAdapter<CharSequence>(getActivity(), android.R.layout.select_dialog_item,
                                android.R.id.text1, menuItems) {

                            @Override
                            public View getView(final int position, View convertView, ViewGroup parent) {
                                View view = super.getView(position, convertView, parent);
                                TextView textView = (TextView) view.findViewById(android.R.id.text1);

                                textView.setTypeface(Typefaces.get(getContext(), null));
                                textView.setTextSize(17);
                                return view;
                            }
                        };

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        SmartTextView txtTitle = new SmartTextView(getActivity());
                        txtTitle.setText(getResources().getString(R.string.choose));
                        txtTitle.setPadding(0, 20, 20, 0);
                        txtTitle.setBackgroundColor(Color.DKGRAY);
                        txtTitle.setTextColor(Color.LTGRAY);
                        txtTitle.setGravity(Gravity.CENTER);
                        txtTitle.setTextSize(21);
                        builder.setCustomTitle(txtTitle);
                        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int pos) {
                                if (menuItems[pos].equalsIgnoreCase(getString(R.string.verify_current_license1))) {

                                    String areaListJson = Configuration.getInstance().getString(SharedPrefs.AREA);
                                    List<Area> areaList = GsonSettings.getGson().fromJson(areaListJson, new TypeToken<List<Area>>() {
                                    }.getType());
                                    boolean find = false;
                                    for (Area area : areaList) {
                                        if (item.getArea().getAreaId() == area.getAreaId())
                                            find = true;
                                    }
                                    if (find) {
                                        if((item.getConfirm1())!=null  && item.getConfirm1()) {
                                            int personnelId1=item.getConfirmPersonnelId1();
                                            Personnel personnel1=DatabaseService.getPersonnelById(personnelId1);
                                          //  Toast.makeText(getContext(), getString(R.string.is_confirmed_one)+" "+ personnel1.getName(), Toast.LENGTH_SHORT).show();

                                            CustomAlertDialogOk.getInstance().showConfirmDialog("توجه","این مجوز توسط "+ personnel1.getName() +" "+"تایید اولیه شده است!", "تایید",context, 1);
                                        }
                                        else
                                        {
                                            Intent intent = new Intent(getActivity(), DialogVerify.class);
                                            intent.putExtra(Params.LICENSE_ID, item.getLicenseId());
                                            intent.putExtra("position", position);
                                            intent.putExtra(Params.TITLE, getString(R.string.verify_current_license1));
                                            startActivityForResult(intent, REQUEST_CODE_VERIFY_LICENSE);
                                        }

                                    }
                                    else
                                    {
                                        Toast.makeText(getContext(), getString(R.string.unauthorized), Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else if (menuItems[pos].equalsIgnoreCase(getString(R.string.verify_current_license2))) {

                                    String areaListJson = Configuration.getInstance().getString(SharedPrefs.AREA);
                                    List<Area> areaList = GsonSettings.getGson().fromJson(areaListJson, new TypeToken<List<Area>>() {
                                    }.getType());
                                    boolean find = false;
                                    for (Area area : areaList) {
                                        if (item.getArea().getAreaId() == area.getAreaId() && (item.getConfirm1())!=null  && item.getConfirm1())
                                            find = true;
                                    }
                                    if (find) {
                                        if(item.getConfirm2()!=null  && item.getConfirm2())
                                        {
                                            int personnelId2=item.getConfirmPersonnelId2();
                                            Personnel personnel2=DatabaseService.getPersonnelById(personnelId2);

                                           // Toast.makeText(getContext(), getString(R.string.is_confirmed_two), Toast.LENGTH_SHORT).show();
                                            CustomAlertDialogOk.getInstance().showConfirmDialog("توجه","این مجوز توسط "+ personnel2.getName() +" "+"تایید ثانویه شده است!", "تایید",context, 2);
                                        }
                                        else {
                                            Intent intent = new Intent(getActivity(), DialogVerifySecond.class);
                                            intent.putExtra(Params.LICENSE_ID, item.getLicenseId());
                                            intent.putExtra("position", position);
                                            intent.putExtra(Params.TITLE, getString(R.string.verify_current_license2));
                                            startActivityForResult(intent, REQUEST_CODE_VERIFY_LICENSE);
                                        }
                                    } else {
                                        Toast.makeText(getContext(), getString(R.string.unauthorized), Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else if (menuItems[pos].equalsIgnoreCase(getString(R.string.verify_no_violation_license))) {
                                    Intent intent = new Intent(getActivity(), DialogVerify.class);
                                    intent.putExtra(Params.LICENSE_ID, item.getLicenseId());
                                    intent.putExtra(Params.TITLE, getString(R.string.verify_no_violation_license));
                                    intent.putExtra("position", position);
                                    startActivityForResult(intent, REQUEST_CODE_VERIFY_NO_VIOLATION_LICENSE);
                                } else if (menuItems[pos].equalsIgnoreCase(getString(R.string.show_cars_in_license))) {
                                    Intent intent = new Intent(getActivity(), ActivityStaffCarsInLicense.class);
                                    intent.putExtra(Params.LICENSE_ID, item.getLicenseId());
                                    startActivity(intent);
                                }
                            }
                        });

                        builder.setCancelable(true);
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }
                });
                return itemView;

            }
        };
    }

    @Override
    protected List<License> fetchMoreDataAsync(int startPage, int count) {
        return null;
    }

    @Override
    protected List<License> fetchDataAsync() {
        if (getArguments() == null) {
            int id = Configuration.getInstance().getInt(SharedPrefs.ID);
            switch (mod) {
                case ALL:
                    if (searchStr != null && searchStr.length() > 0 && searchStaffCar)
                        return DatabaseService.getNotSendLicenseStaffCar(id, searchStr);
                    else if(searchStr != null && searchStr.length() > 0 && !searchStaffCar)
                        return DatabaseService.getNotSendLicense(id, searchStr);
                    else
                        return DatabaseService.getNotSendLicense(id);
                case DATE:
                    if (searchStr != null && searchStr.length() > 0 && searchStaffCar)
                        return DatabaseService.getNotSendLicenseByDateStaffCar(id, from, to, searchStr);
                    else  if (searchStr != null && searchStr.length() > 0 && !searchStaffCar)
                        return DatabaseService.getNotSendLicenseByDate(id, from, to, searchStr);
                    else
                        return DatabaseService.getNotSendLicenseByDate(id, from, to);
                case AREA:
                    String areaListJson = Configuration.getInstance().getString(SharedPrefs.AREA);
                    List<Area> areaList =
                            GsonSettings.getGson().fromJson(areaListJson, new TypeToken<List<Area>>() {
                            }.getType());
                    if (searchStr != null && searchStr.length() > 0 && searchStaffCar)
                        return DatabaseService.getNotSendLicenseByAreaIdStaffCar(id, areaList, searchStr);
                    else  if (searchStr != null && searchStr.length() > 0 && !searchStaffCar)
                        return DatabaseService.getNotSendLicenseByAreaId(id, areaList, searchStr);
                    else
                        return DatabaseService.getNotSendLicenseByAreaId(id, areaList);
                case MY_LICENSE:
                    if (searchStr != null && searchStr.length() > 0 && searchStaffCar)
                        return DatabaseService.getLicenseByConfirmIdStaffCar(id, searchStr);
                    else if (searchStr != null && searchStr.length() > 0 && !searchStaffCar)
                        return DatabaseService.getLicenseByConfirmId(id, searchStr);
                    else
                        return DatabaseService.getLicenseByConfirmId(id);
                case LICENSED:
                    if (searchStr != null && searchStr.length() > 0 && searchStaffCar)
                        return DatabaseService.getLicenseByConfirmedStaffCar(id, searchStr);
                    else if (searchStr != null && searchStr.length() > 0 && !searchStaffCar)
                        return DatabaseService.getLicenseByConfirmed(id, searchStr);
                    else
                        return DatabaseService.getLicenseByConfirmed1_2(id);
                default:
                    return new ArrayList<>();
            }
        }else {
            Log.e("1", getArguments().getInt(Params.CONTRACTOR_ID) + "");
            return DatabaseService.getNotSendLicenseByContractor(getArguments().getInt(Params.CONTRACTOR_ID));
        }
    }

    @Override
    protected void onOperationFailed() {

    }

    private void refreshData() {
        list.clear();
        loadData();
    }

    private void selectDate() {
        startActivityForResult(new Intent(getActivity(), DialogDate.class), REQUEST_CODE_SELECT_DATE);
    }



    ProgressDialog progressDialog;

    private void updateLicenses() {
        new LicenseUpdater().execute("", "");
    }

    private class LicenseUpdater extends AsyncTask<String, Void, Boolean> {


        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            progressDialog.dismiss();
            if (result)
                Toast.makeText(getActivity(), "موفقیت آمیز بود", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(getActivity(), "دریافت اطلاعات مجوزها موفقیت آمیز نبود", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            boolean res = WebServiceSyncImp.editLicenseConfirmNoViolation(params[0], params[1]);

            final String start = params[0];
            final String end = params[1];
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateLicenseList(start, end);
                }
            });
            return res;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == REQUEST_CODE_VERIFY_LICENSE) {
                boolean answer = data.getExtras().getBoolean("answer");
                int position = data.getExtras().getInt("position");
                String description = data.getExtras().getString("description");
                boolean synced = data.getExtras().getBoolean("synced");
                list.get(position).setConfirm1(answer);
                list.get(position).setConfirmDescription1(description);
                if (synced) {
                    if (answer) {
                        list.get(position).setLicenseState(1);

                        if (mod != Mod.MY_LICENSE) {
                            list.remove(position);
                            adapter.notifyItemRemoved(position);
                        }
                    }else {
                        list.get(position).setLicenseState(2);
                    }
                }
                adapter.notifyItemChanged(position);

                //DatabaseService.updateLicense(list.get(position));
                //TODO : send this to server
            } else if (requestCode == REQUEST_CODE_VERIFY_NO_VIOLATION_LICENSE) {
                boolean answer = data.getExtras().getBoolean("answer");
                int position = data.getExtras().getInt("position");
                String description = data.getExtras().getString("description");
                list.get(position).setConfirmNoViolation(answer);
                list.get(position).setConfirmNoViolationDescription(description);
                adapter.notifyItemChanged(position);

                //DatabaseService.updateLicense(list.get(position));
                //TODO : send this to server
            } else if (requestCode == REQUEST_CODE_SELECT_DATE) {
                try {
                    from = DateUtils.PersianToMiladi(dateFormat.parse(data.getStringExtra("dateFrom")));
                    to = DateUtils.PersianToMiladi(dateFormat.parse(data.getStringExtra("dateTo")));
                    refreshData();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void updateLicenseList(String startD, String endD) {

        /*if (startD.equals("") || endD.equals(""))

            licenses = DatabaseService.getLicensesByDate(new Date());
        else {
            String[] startDate = startD.split("/");
            String[] endDate = endD.split("/");

            String start = CalendarTool.SiminehCalendar.getGregorianDate(Integer.parseInt(startDate[0]), Integer.parseInt(startDate[1]), Integer.parseInt(startDate[2]));
            String end = CalendarTool.SiminehCalendar.getGregorianDate(Integer.parseInt(endDate[0]), Integer.parseInt(endDate[1]), Integer.parseInt(endDate[2]));

            Calendar calendarStart = Calendar.getInstance();
            calendarStart.setTime(new Date(start));

            Calendar calendarEnd = Calendar.getInstance();
            calendarEnd.setTime(new Date(end));


            licenses = DatabaseService.getLicensesByDate(calendarStart.getTime(), calendarEnd.getTime());
        }
        adapter = new LicenseAdapter(getActivity(), R.layout.license_row, R.id.txtID, licenses);
        lstLicense.setAdapter(adapter);*/
    }

    public  void refreshDataList()
    {
        int spinnerMode=FragLicense.mode;
        if(spinnerMode==0) {
            mod = Mod.ALL;
            refreshData();
        }
        else if(spinnerMode==1) {
            mod = Mod.AREA;
            refreshData();
        }
        else if(spinnerMode==2) {

            mod = Mod.DATE;
            selectDate();
        }
        else if(spinnerMode==3)
        {
            refreshData();

            mod = Mod.MY_LICENSE;
        }
        else if(spinnerMode==4){
            mod = Mod.LICENSED;
            refreshData();
        }
        else if(spinnerMode==5){

            mod=Mod.ALL;

            searchStr = FragLicense.strSearch;
            searchType=FragLicense.SearchType;
            searchStr = ShahrdariApp.replaceNumbers(searchStr);
//            list.clear();
//            loadData();
            refreshData();
        }


    }




}

