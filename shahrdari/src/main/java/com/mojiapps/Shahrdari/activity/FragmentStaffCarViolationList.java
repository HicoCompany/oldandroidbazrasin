package com.mojiapps.Shahrdari.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ahmadian.ruhallah.commons.fragment.list.FactorySmartItemView;
import com.ahmadian.ruhallah.commons.fragment.list.SmartFragment;
import com.ahmadian.ruhallah.commons.fragment.list.SmartItemView;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.database.DatabaseService;
import com.mojiapps.Shahrdari.items.ItemViewStaffCarViolation;
import com.mojiapps.Shahrdari.models.StaffCarViolation;
import com.mojiapps.Shahrdari.webservice.Params;

import java.util.List;

/**
 * Created by ruhallah-PC on 12/17/2016.
 */

public class FragmentStaffCarViolationList extends SmartFragment<StaffCarViolation> {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setType(Type.STATIC);
        View view =  super.onCreateView(inflater, container, savedInstanceState);
        recyclerView.setBackgroundColor(getResources().getColor(R.color.fragment_backcolor));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRefreshing(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        list.clear();
        loadData();
    }

    @Override
    protected FactorySmartItemView getViewFactory() {
        return new FactorySmartItemView() {
            @Override
            public SmartItemView makeView(String searchPhrase) {
                ItemViewStaffCarViolation itemView = new ItemViewStaffCarViolation(getContext());
                itemView.setSearchPhrase(searchPhrase);
                itemView.setOnItemClickListener(new SmartItemView.OnItemClickListener<StaffCarViolation>() {
                    @Override
                    public void click(StaffCarViolation item, View view, int position) {
                    }
                });
                itemView.setOnImageClickListener(new SmartItemView.OnItemClickListener<StaffCarViolation>() {
                    @Override
                    public void click(StaffCarViolation item, View view, int position) {
                        Intent intent = new Intent(getActivity(), PicturesActivity.class);
                        intent.putExtra("id", item.getStaffCarViolationId());
                        intent.putExtra("type", PicturesActivity.ViolationType.STAFF_CAR);
                        startActivity(intent);
                    }
                });
                return itemView;
            }
        };
    }

    @Override
    protected List<StaffCarViolation> fetchMoreDataAsync(int startPage, int count) {
        return null;
    }

    @Override
    protected List<StaffCarViolation> fetchDataAsync() {
        //DatabaseService.getStaffCarViolations();
        if (getArguments() == null)
            return DatabaseService.getStaffCarViolations();
        else {
            return DatabaseService.getStaffCarViolationsByStaffCarId(getArguments().getInt(Params.STAFF_CAR_ID));
        }
    }

    @Override
    protected void onOperationFailed() {

    }
}
