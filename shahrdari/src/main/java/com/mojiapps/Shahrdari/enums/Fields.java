package com.mojiapps.Shahrdari.enums;

/**
 * Created by mojtaba on 9/18/14.
 */
public class Fields {
    public enum DayNight{
        DAY("روز"),
        NIGHT("شب"),
        ;

        private String value;
        DayNight(String value){
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public enum WasteType{
        SAKHTEMANI("پسماند عمرانی ساختمانی"),
        SANATI("پسماند صنعتی"),
        JOZ("پسماند جزء"),
        ;

        private String value;
        WasteType(String value){
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
}
