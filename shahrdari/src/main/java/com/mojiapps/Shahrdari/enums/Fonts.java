package com.mojiapps.Shahrdari.enums;

/**
 * Created by mojtaba on 3/7/14.
 */
public enum  Fonts {
    DEFAULT("XB Zar.ttf"),
    DROID_NASKH("DroidNaskh.ttf"),
    YEKAN("Yekan.ttf"),
    IRAN_SANS("iran_sans.ttf"),
    ;

    private String value;
    Fonts(String s) {
        value = s;
    }

    @Override
    public String toString() {
        return value; // super.toString();
    }
}
