package com.mojiapps.Shahrdari.enums;

/**
 * Created by mojtaba on 3/7/14.
 */
public enum SharedPrefs {
    USER_NAME,
    PASSWORD,
    NAME,
    IMAGE,
    ID,
    AREA,
    MAC_ADDRESS,
    DATE_AREA,
    DATE_CARS,
    DATE_CERTIFICATES,
    DATE_DISCHARGES,
    DATE_VIOLATIONS,
    DATE_CONTRACTORS,
    DATE_STAFF_CARS,
    DATE_PERSONNELS,
    DATE_STAFF_CAR_VIOLATIONS,
    DATE_CAR_VIOLATIONS,
    DATE_HOME_VIOLATIONS,
    DATE_LICENSES,
    DATE_BOOTHES,
    DATE_QUESTIONS,
    DATE_SITES
}
