package com.mojiapps.Shahrdari.enums;

/**
 * Created by mojtaba on 10/9/14.
 */
public enum ImageType {
    VIOLATION_MEMBER,
    VIOLATION_NOT_MEMBER,
    VIOLATION_ESTATE,
}
