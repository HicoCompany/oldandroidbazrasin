package com.mojiapps.Shahrdari;

import android.content.Context;
import android.content.SharedPreferences;

import com.mojiapps.Shahrdari.enums.SharedPrefs;

/**
 * Created by mojtaba on 3/7/14.
 */
public class Configuration {

    private static final String RESTServerAddress ="http://waste.qom.ir";//"http://88.99.79.99:8099"; //
    //private static final String RESTServerAddress = "http://94.183.179.17:9000";
    private static Configuration _singletonInstance;
    private Configuration(){}
    public static synchronized Configuration getInstance(){
        if(_singletonInstance == null)
            _singletonInstance = new Configuration();
        return _singletonInstance;
    }
//239dp
    private SharedPreferences getPrefs(){
        return ShahrdariApp.getContext().getSharedPreferences("Shahrdari", Context.MODE_PRIVATE);
    }

    public String getString(SharedPrefs propertyName) {
        return getPrefs().getString(propertyName.toString(), "");
    }

    public int getInt(SharedPrefs propertyName) {
        return getPrefs().getInt(propertyName.toString(), 0);
    }

    public boolean getBoolean(SharedPrefs propertyName){
        return getPrefs().getBoolean(propertyName.toString(), false);
    }

    public void setString(SharedPrefs propertyName, String value) {
        getPrefs().edit().putString(propertyName.toString(), value).commit();
    }


    public void setInt(SharedPrefs propertyName, int value){
        getPrefs().edit().putInt(propertyName.toString(), value).commit();
    }

    public void setBoolean(SharedPrefs propertyName, boolean value){
        getPrefs().edit().putBoolean(propertyName.toString(), value).commit();
    }

    /*public String getRESTServerAddress(){
        return getString(SharedPrefs.SERVER_ADDRESS).equals("") ?  RESTServerAddress : getString(SharedPrefs.SERVER_ADDRESS);
    }*/

    public String getDatabaseFolder(){
//        try {
//            return ShahrdariApp.getContext().getPackageManager().getPackageInfo(ShahrdariApp.getContext().getPackageName(), 0).applicationInfo.dataDir;
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
//        return "";

        try {
            return ShahrdariApp.getContext().getExternalFilesDir(null).getAbsolutePath();
        } catch (Exception e){
            return ShahrdariApp.getContext().getFilesDir().getAbsolutePath();
        }
    }
}
