package com.mojiapps.Shahrdari.utils;

/**
 * Created by Neda-PC on 1/18/2018.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ahmadian.ruhallah.commons.fragment.list.FactorySmartItemView;
import com.ahmadian.ruhallah.commons.fragment.list.SmartItemView;

import java.util.List;



/**
 * Created by hsiami on 19/11/2016.
 */
public class CustomAdapter<T> extends RecyclerView.Adapter<CustomAdapter<T>.ViewHolder>{
    protected List<T> list;
    protected Context context;
    protected FactorySmartItemView factoryItemView;
    protected String searchPhrase = "";
    int num=1;

    public CustomAdapter(Context context, List<T> list, FactorySmartItemView factory) {
        this.context = context;
        this.list = list;
        factoryItemView = factory;
    }

    @Override
    public CustomAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CustomAdapter.ViewHolder(factoryItemView.makeView(searchPhrase));
    }

    @Override
    public void onBindViewHolder(CustomAdapter.ViewHolder holder, int position) {

        T item = list.get(position);
        holder.setItem(item, position);
    }

    @Override
    public int getItemCount() {
        if(num*10 > list.size()){
            return list.size();

        }else{

            num = num*10;
            return num;

        }
       //list != null ? list.size() : 0;
    }

    public String getSearchPhrase() {
        return searchPhrase;
    }

    public void setSearchPhrase(String searchPhrase) {
        this.searchPhrase = searchPhrase;
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {
        private SmartItemView<T> itemView;
        private T item;

        public ViewHolder(View view) {
            super(view);
            itemView = (SmartItemView<T>) view;
        }

        public void setItem(T item, int position) {
            this.item = item;
            itemView.setSearchPhrase(searchPhrase);
            itemView.setItem(item, position);
        }
    }
}
