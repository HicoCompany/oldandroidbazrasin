package com.mojiapps.Shahrdari.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.mojiapps.Shahrdari.ShahrdariApp;

/**
 * Created by mojtaba on 25/10/2014.
 */
public class ConnectionDetector {

    private Context _context;

    public static int NETWORK_NOT_CONNECTED = 0;
    public static int NETWORK_CONNECTED_WIFI = 1;
    public static int NETWORK_CONNECTED_MOBILE = 2;

    public ConnectionDetector(Context context){
        this._context = context;
    }

    /*public boolean isConnectingToInternet(){
        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }

        }
        return false;
    }*/

    public static boolean isConnectingToInternet() {
        return getConnectivityStatus() != NETWORK_NOT_CONNECTED;
    }

    public static int getConnectivityStatus() {
        ConnectivityManager connectivityManager = (ConnectivityManager) ShahrdariApp.getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return NETWORK_CONNECTED_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return NETWORK_CONNECTED_MOBILE;
        }
        return NETWORK_NOT_CONNECTED;
    }

}
