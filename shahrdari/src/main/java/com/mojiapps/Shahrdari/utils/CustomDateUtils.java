package com.mojiapps.Shahrdari.utils;
/**
 * Created by Neda-PC on 10/9/2017.
 */
import java.util.Calendar;
import java.util.Date;

public class CustomDateUtils {


    public static String  MiladiToPersianDateTime(Date miladi) {
        CustomPersianCalender persianCalendar = new CustomPersianCalender();
        Calendar calendar= Calendar.getInstance();
        calendar.setTime(miladi);
        int[] a=DateConverter.gregorianToJalali( calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));

         return a[2]+"/"+a[1]+"/"+ a[0] +" "+
                persianCalendar.getMyTime();

    }

}
