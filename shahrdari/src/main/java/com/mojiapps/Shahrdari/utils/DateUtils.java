package com.mojiapps.Shahrdari.utils;

import android.util.Log;

import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ruhallah-PC on 11/11/2016.
 */

public class DateUtils {

    public static Date PersianToMiladi(Date persianDate) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        JalaliCalendar jalaliCalendar = new JalaliCalendar();
        Date date = jalaliCalendar.getGregorianDate(dateFormat.format(persianDate));
        date.setHours(persianDate.getHours());
        date.setMinutes(persianDate.getMinutes());
        date.setSeconds(persianDate.getSeconds());
        Log.e("AA", date.toString());
        return date;
    }

    public static String  MiladiToPersianDateTime(Date miladi) {
        PersianCalendar persianCalendar = new PersianCalendar();
        Calendar calendar= Calendar.getInstance();
        calendar.setTime(miladi);
        persianCalendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)
                , calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));
        return persianCalendar.getPersianShortDateTime() +
                " (" + persianCalendar.getPersianWeekDayName() + ")";

    }

    public static String MiladiToPersianDate(Date miladi) {
        PersianCalendar persianCalendar = new PersianCalendar();
        Calendar calendar= Calendar.getInstance();
        calendar.setTime(miladi);
        persianCalendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)
                , calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));
        return persianCalendar.getPersianShortDate();

    }
}
