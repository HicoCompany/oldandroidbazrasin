package com.mojiapps.Shahrdari.utils;

import android.content.Context;
import android.graphics.Typeface;
import com.mojiapps.Shahrdari.ShahrdariApp;
import com.mojiapps.Shahrdari.enums.Fonts;

/**
 * Created by mojtaba on 3/9/14.
 */
public class Texts {
    public static String ChangeToUnicode(String input) {
        return input.replace("0", "۰").replace("1", "۱").replace("2", "۲")
                .replace("3", "۳").replace("4", "۴").replace("5", "۵")
                .replace("6", "۶").replace("7", "۷").replace("8", "۸")
                .replace("9", "۹");
    }

    public static String ChangeToLatin(String input){
        return input.replace("۰", "0").replace("۱", "1").replace("۲", "2")
                .replace("۳", "3").replace("۴", "4").replace("۵", "5")
                .replace("۶", "6").replace("۷", "7").replace("۸", "8")
                .replace("۹", "9");
    }

    public static String RemoveErab(String input) {
        return input.replace("ً", "").replace("ٍ", "").replace("ٌ", "")
                .replace("َ", "").replace("ِ", "").replace("ُ", "")
                .replace("ّ", "").replace("ْ", "").replace("ٰ", "");
    }

    public static Typeface getFont(Fonts type){
        return Typeface.createFromAsset(ShahrdariApp.getContext().getAssets(), "fonts/" + type.toString());
    }

    public static String getDatePersianString(String date){
        return date;
    }
}
