package com.mojiapps.Shahrdari.utils;/*
 * @author S. M. Hossein Mahmoodzade
*/

import java.util.Calendar;

public class DateConverter {

    private final static int[] gDaysInMonth = new int[] {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    private final static int[] jDaysInMonth = new int[] {31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29};
    private final static String[] dayOfWeek = new String[] {"شنبه", "یکشنبه", "دوشنبه", "سه شنبه", "چهارشنبه", "پنح شنبه", "جمعه"};
    private final static String[] months = new String[] {"فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر",
            "آبان", "آذر", "دی", "بهمن", "اسفند"};

    /**
     * Converts gregorian to jalali calendar
     *
     * @param inGYear     The gregorian year
     * @param inGMonth    The gregorian month
     * @param inGDay      The gregorian day
     * @return int[]     The jalali date array
     */
    public static int[] gregorianToJalali (int inGYear, int inGMonth, int inGDay) {
        int gYear = inGYear - 1600;
        int gMonth = inGMonth - 1;
        int gDay = inGDay - 1;

        int gDayNo = 365 * gYear + ((gYear + 3) / 4) - ((gYear + 99) / 100) + ((gYear + 399) / 400);

        for (int i = 0 ; i < gMonth ; i++)
            gDayNo += gDaysInMonth[i];
        if (gMonth > 1 && ((gYear % 4 == 0 && gYear % 100 != 0) || (gYear % 400 == 0)))/* leap and after Feb */
            gDayNo++;
        gDayNo += gDay;

        int jDayNo = gDayNo - 79;

        int jNP = (jDayNo / 12053); /* 12053 = 365*33 + 32/4 */
        jDayNo = jDayNo % 12053;

        int jYear = 979 + 33 * jNP + 4 * (jDayNo / 1461); /* 1461 = 365*4 + 4/4 */

        jDayNo %= 1461;

        if (jDayNo >= 366) {
            jYear += (jDayNo - 1) / 365;
            jDayNo = (jDayNo - 1) % 365;
        }

        int i;
        for (i = 0 ; i < 11 && jDayNo >= jDaysInMonth[i]; i++)
            jDayNo -= jDaysInMonth[i];
        int jMonth = i + 1;
        int jDay = jDayNo + 1;

        return new int[] {jYear, jMonth, jDay};
    }

    /**
     * Converts jalali to gregorian calendar
     *
     * @param inJYear     The jalali year
     * @param inJMonth    The jalali month
     * @param inJDay      The jalali day
     * @return int[] The gregorian date array
     */
    public static int[] jalaliToGregorian (int inJYear, int inJMonth, int inJDay)
    {
        int jYear = inJYear - 979;
        int jMonth = inJMonth - 1;
        int jDay = inJDay - 1;

        int jDayNo = 365 * jYear + (jYear / 33) * 8 + ((jYear % 33 + 3) / 4);
        for (int i = 0 ; i < jMonth ; i++)
            jDayNo += jDaysInMonth[i];

        jDayNo += jDay;

        int gDayNo = jDayNo + 79;

        int gYear = 1600 + 400 * (gDayNo / 146097); /* 146097 = 365*400 + 400/4 - 400/100 + 400/400 */
        gDayNo = gDayNo % 146097;

        boolean leap = true;
        if (gDayNo >= 36525) {
        /* 36525 = 365*100 + 100/4 */
            gDayNo--;
            gYear += 100 * (gDayNo /36524); /* 36524 = 365*100 + 100/4 - 100/100 */
            gDayNo = gDayNo % 36524;

            if (gDayNo >= 365)
                gDayNo++;
            else
                leap = false;
        }

        gYear += 4 * (gDayNo / 1461); /* 1461 = 365*4 + 4/4 */
        gDayNo %= 1461;

        if (gDayNo >= 366) {
            leap = false;

            gDayNo--;
            gYear += gDayNo / 365;
            gDayNo = gDayNo % 365;
        }

        int i;
        for (i = 0 ; gDayNo >= gDaysInMonth[i] + ((i == 1 && leap) ? 1 : 0); i++)
            gDayNo -= gDaysInMonth[i] + ((i == 1 && leap) ? 1 : 0);
        int gMonth = i + 1;
        int gDay = gDayNo+1;

        return new int[] {gYear, gMonth, gDay};
    }

    public static String setDate(int[] temporaryDate)
    {
        String dateString=null;
        int[] tempDate=gregorianToJalali(temporaryDate[0],temporaryDate[1],temporaryDate[2]);
        Calendar c = Calendar.getInstance();
        c.set(temporaryDate[0],temporaryDate[1],temporaryDate[2], 0, 0, 0);
        dateString=dayOfWeek[c.get(Calendar.DAY_OF_WEEK)-1]+" "+ String.valueOf(tempDate[2])+
        " "+months[tempDate[1]-1]+" "+ String.valueOf(tempDate[0])+" "+"ساعت "+temporaryDate[3]+":"+temporaryDate[4];
        return dateString;
    }
}
