package com.mojiapps.Shahrdari.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.format.Time;
import android.util.Log;
import android.util.TypedValue;

import com.mojiapps.Shahrdari.Configuration;
import com.mojiapps.Shahrdari.ShahrdariApp;
import com.mojiapps.Shahrdari.enums.SharedPrefs;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

public class Utils {

    //public static int OfficerID=-1;

    public static String GetBase64StringFromBitmap(Bitmap bitmap) {
        if (bitmap != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
            byte[] b = baos.toByteArray();
            try {
                return Base64.encodeBytes(b, Base64.GZIP);
            } catch (IOException e) {
                e.printStackTrace();
            }
            bitmap.recycle();
        }
        return "";
    }

    public static Bitmap GetBitmapFromBase64(String base64) {
        byte[] decodedByte = null;
        try {
            decodedByte = Base64.decode(base64, Base64.GZIP);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap image = BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
        return image;
    }

    public static Bitmap GetBitmapFromBase64Low(String base64) {
        byte[] decodedByte = null;
        try {
            decodedByte = Base64.decode(base64, Base64.GZIP);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Bitmap image = BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
        image.compress(Bitmap.CompressFormat.JPEG, 30, out);
        image = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
        return image;
    }

    public static Bitmap getCircleBitmap(Bitmap bitmap) {
        Bitmap result = bitmap;
        try {
            bitmap = Bitmap.createScaledBitmap(bitmap, 134, 134, true);
            result = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(result);

            int color = 0xff424242;
            Paint paint = new Paint();
            Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            RectF rectF = new RectF(rect);

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);

            int cx = bitmap.getWidth() / 2;
            int cy = bitmap.getHeight() / 2;
            int r = cx - 3;
            canvas.drawCircle(cx, cy, r, paint);
            //canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);
        } catch (NullPointerException e) {
        } catch (OutOfMemoryError o) {
        }
        return result;
    }

    public static boolean checkUserRegistered(Context ctx) {
        if (Configuration.getInstance().getString(SharedPrefs.USER_NAME).equalsIgnoreCase(""))
            return false;
        else
            return true;
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;

    }

    public static String md5(String s) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes(), 0, s.length());
            String hash = new BigInteger(1, digest.digest()).toString(16);
            return hash;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getCurrentDate() {
        Time now = new Time();
        now.setToNow();
        return CalendarTool.SiminehCalendar.getIranianDate(now.year, now.month + 1, now.monthDay);
    }

    public static String getCurrentGaregoryDate1() {
        Calendar calendar = Calendar.getInstance();

        String year = calendar.get(Calendar.YEAR) + "";
        String month = calendar.get(Calendar.MONTH)+1 + "";
        String day = calendar.get(Calendar.DAY_OF_MONTH) + "";
        String hour = calendar.get(Calendar.HOUR_OF_DAY) + "";
        String minute = calendar.get(Calendar.MINUTE) + "";
        String second = calendar.get(Calendar.SECOND) + "";


        if (month.length() < 2)
            month = "0" + month;

        if (day.length() < 2)
            day = "0" + day;

        if (hour.length() < 2)
            hour = "0" + hour;

        if (minute.length() < 2)
            minute = "0" + minute;

        if (second.length() < 2)
            second = "0" + second;


        return year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second;
    }

    public static String getCurrentTime() {
        Time now = new Time();
        now.setToNow();
        return String.format("%02d", now.hour) + ":" + String.format("%02d", now.minute);
    }

    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] textBytes = text.getBytes("iso-8859-1");
        md.update(textBytes, 0, textBytes.length);
        byte[] sha1hash = md.digest();
        Log.e("Password", sha1hash + "");
        return convertToHex(sha1hash);
    }

    public static Float intToDp(int i) {
        Resources r = ShahrdariApp.getContext().getResources();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, i, r.getDisplayMetrics());
    }

    public static String GetDateTime1(SharedPrefs sharedPrefs) {
        String date = Configuration.getInstance().getString(sharedPrefs);

        if (date.isEmpty()) {
            String currentDate = "2013/05/01" + " " + Utils.getCurrentTime();

            Configuration.getInstance().setString(sharedPrefs, currentDate);

            return currentDate;
        }

        return date;
    }

}
