package com.mojiapps.Shahrdari.items;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ahmadian.ruhallah.commons.fragment.list.SmartItemView;
import com.github.florent37.viewanimator.ViewAnimator;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.models.StaffCarViolation;
import com.mojiapps.Shahrdari.models.Violation;
import com.mojiapps.Shahrdari.utils.DateUtils;
import com.mojiapps.Shahrdari.utils.Utils;

/**
 * Created by ruhallah-PC on 12/17/2016.
 */

public class ItemViewStaffCarViolation extends SmartItemView<StaffCarViolation> {

    ImageView imgViolation;
    TextView txtCode;
    TextView txtOwnerName;
    TextView txtAddress;
    TextView txtViolationType;
    TextView txtDate;
    TextView Description;

    TextView GetsCertificate;
    TextView ViolationId;
//    TextView ViolationActDescription;
//    TableRow txtViolationActDescriptionfield;
    View violationActDescriptionStatus;
    CardView cardView;
    LinearLayout moreLayout;
    ImageButton collapseBtn;

    int minHeightCardView;

    OnItemClickListener<StaffCarViolation> onImageClickListener;

    public ItemViewStaffCarViolation(Context context) {
        super(context);
        View view = View.inflate(context, R.layout.violation_member_row, this);
        init(view);
    }

    @Override
    public void setItem(final StaffCarViolation item, final int position) {
        super.setItem(item, position);
        resetView();


        if (item.getStaffCar() != null) {
            txtCode.setText(item.getStaffCar().getStaffCarId().toString());
            txtOwnerName.setText(item.getStaffCar().getOwName());
        }
        txtAddress.setText(item.getAddress());

        String violations = "";
        if (item.getViolationList() != null &&
                item.getViolationList().size() > 0) {
            for (Violation violation : item.getViolationList()) {
                violations += violation.getTitle() + " - ";
            }
            violations = violations.substring(0, violations.length() - 3);
        }
        txtViolationType.setText(violations);

        txtDate.setText(DateUtils.MiladiToPersianDateTime(item.getRegisterDate()));

        if (item.getDescription() != null)
            Description.setText(item.getDescription());

        if (item.getStaffCarViolationId() != null)
            ViolationId.setText(item.getStaffCarViolationId().toString());
        else
            ViolationId.setText("***");

        if (item.getCertificate() != null)
            GetsCertificate.setText(item.getCertificate().getTitle());
        else
            GetsCertificate.setText(getResources().getString(R.string.no_certificate));

        if (item.getImage1() != null && item.getImage1().length() > 0)
            imgViolation.setImageBitmap(
                    Bitmap.createScaledBitmap(Utils.GetBitmapFromBase64Low(item.getImage1()), 200, 200, true)
            );
        else
            imgViolation.setImageResource(R.drawable.ic_staff_car);
        imgViolation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onImageClickListener.click(item, view, position);
            }
        });

        if (item.getViolationActDescription() == null) {
            violationActDescriptionStatus.setBackgroundColor(getContext().getResources().getColor(R.color.grey_400));
        } else {
            if (item.getViolationActDescription().length() > 0) {
                violationActDescriptionStatus.setBackgroundColor(getContext().getResources().getColor(R.color.colorAccent));
              //  ViolationActDescription.setText(item.getViolationActDescription());
            } else {
                violationActDescriptionStatus.setBackgroundColor(getContext().getResources().getColor(R.color.grey_400));
            }
        }

    }

    public void setOnImageClickListener(OnItemClickListener<StaffCarViolation> onItemClickListener) {
        this.onImageClickListener = onItemClickListener;
    }

    private void init(View cell) {
        imgViolation = (ImageView) cell.findViewById(R.id.imgViolation);
        txtCode = (TextView) cell.findViewById(R.id.txtID);
        txtOwnerName = (TextView) cell.findViewById(R.id.txtOwner);
        txtAddress = (TextView) cell.findViewById(R.id.txtAddress);
        txtViolationType = (TextView) cell.findViewById(R.id.txtViolationType);
        txtDate = (TextView) cell.findViewById(R.id.txtDate);
        Description = (TextView) cell.findViewById(R.id.txtDescription);
        violationActDescriptionStatus = (View) cell.findViewById(R.id.violation_act_description_status);

        ViolationId = (TextView) cell.findViewById(R.id.txtViolationId);
        violationActDescriptionStatus = (View) cell.findViewById(R.id.violation_act_description_status);
        moreLayout = (LinearLayout) cell.findViewById(R.id.more_layout);
//        txtViolationActDescriptionfield = (TableRow) cell.findViewById(R.id.txtViolationActDescriptionField);
//        ViolationActDescription = (TextView) cell.findViewById(R.id.txtViolationActDescription);
        cardView = (CardView) cell.findViewById(R.id.card_view);
        collapseBtn = (ImageButton) cell.findViewById(R.id.collapse_btn);
        GetsCertificate = (TextView) cell.findViewById(R.id.txtGetsCertificate);

        cardView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver
                .OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                cardView.getViewTreeObserver().removeOnPreDrawListener(this);
                minHeightCardView = cardView.getMeasuredHeightAndState() - moreLayout.getMeasuredHeightAndState();
                return true;
            }
        });

        collapseBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                toggleCardViewnHeight();
            }
        });
    }

    private void resetView() {
        moreLayout.setVisibility(GONE);

        ViewGroup.LayoutParams LayoutImageParams = imgViolation.getLayoutParams();
        LayoutImageParams.height = (int) getContext().getResources().getDimension(R.dimen.min_image_list);
        LayoutImageParams.width = (int) getContext().getResources().getDimension(R.dimen.min_image_list);
        imgViolation.setLayoutParams(LayoutImageParams);

        ViewAnimator
                .animate(collapseBtn)
                .rotation(0)
                .duration(0)
                .start();

        CardView.LayoutParams params = new CardView.LayoutParams(CardView.LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        params.setMargins(8, 8, 8, 8);
        cardView.setLayoutParams(params);
    }

    private void toggleCardViewnHeight() {
        if (moreLayout.getVisibility() == GONE) {
            expand();
        } else {
            collapse();
        }
    }

    private void expand() {
        moreLayout.setVisibility(VISIBLE);

        ViewAnimator
                .animate(moreLayout)
                .slideTop()
                .duration(100)
                .start();

        ValueAnimator animImage = ValueAnimator.ofInt(imgViolation.getMeasuredHeightAndState(),
                (int) getContext().getResources().getDimension(R.dimen.max_image_list));
        animImage.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = imgViolation.getLayoutParams();
                layoutParams.height = val;
                layoutParams.width = val;
                imgViolation.setLayoutParams(layoutParams);
            }
        });
        animImage.start();

        ViewAnimator
                .animate(collapseBtn)
                .rotation(90)
                .duration(100)
                .start();
    }

    private void collapse() {
        ValueAnimator animCard = ValueAnimator.ofInt(cardView.getMeasuredHeightAndState(),
                minHeightCardView);
        animCard.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = cardView.getLayoutParams();
                layoutParams.height = val;
                cardView.setLayoutParams(layoutParams);
            }
        });
        animCard.start();
        animCard.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                resetView();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        ValueAnimator animImage = ValueAnimator.ofInt(imgViolation.getMeasuredHeightAndState(),
                (int) getContext().getResources().getDimension(R.dimen.min_image_list));
        animImage.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = imgViolation.getLayoutParams();
                layoutParams.height = val;
                layoutParams.width = val;
                imgViolation.setLayoutParams(layoutParams);
            }
        });
        animImage.start();

        ViewAnimator
                .animate(collapseBtn)
                .rotation(0)
                .duration(100)
                .start();
    }

}
