package com.mojiapps.Shahrdari.items;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ahmadian.ruhallah.commons.fragment.list.SmartItemView;
import com.github.florent37.viewanimator.ViewAnimator;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.models.CarViolation;
import com.mojiapps.Shahrdari.models.StaffCar;
import com.mojiapps.Shahrdari.utils.DateUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ruhallah-PC on 12/17/2016.
 */

public class ItemViewStaffCar extends SmartItemView<StaffCar> {

    @Bind(R.id.txtID)
    TextView txtID;
    @Bind(R.id.txtOwner)
    TextView txtOwner;
    @Bind(R.id.txtPlate)
    TextView txtPlate;
    @Bind(R.id.txtCarType)
    TextView txtCarType;
    @Bind(R.id.txtActToDate)
    TextView txtActToDate;
    @Bind(R.id.txtColor)
    TextView txtColor;
    @Bind(R.id.txtPhoneNumber)
    TextView txtPhoneNumber;
    @Bind(R.id.txtContractor)
    TextView txtContractor;
    @Bind(R.id.last_license_id)
    TextView txtlastLicenseId;

    @Bind(R.id.collapse_btn)
    ImageButton collapseBtn;
    @Bind(R.id.more_layout)
    LinearLayout moreLayout;
    @Bind(R.id.card_view)
    CardView cardView;
    @Bind(R.id.violation_act_description_status)
    View violationActDescriptionStatus;

    int minHeightCardView;

    private View view;

    public ItemViewStaffCar(Context context) {
        super(context);
        view = View.inflate(context, R.layout.car_row, this);
        ButterKnife.bind(this, view);
        view.setOnClickListener(this);
        init(view);
    }

    @Override
    public void setItem(final StaffCar item, int position) {
        super.setItem(item, position);
        resetView();
        txtID.setText(item.getStaffCarId().toString());
        txtOwner.setText(item.getOwName());
        if (item.getContractor() != null)
            txtContractor.setText(" ("+item.getContractor().getOwName()+")");
        txtPlate.setText(!item.getPlateType() ? item.getPlateCode() : item.getOldPlate());
        txtCarType.setText(!item.getPlateType() ? getResources().getString(R.string.non_haffar) : getResources().getString(R.string.haffar));
        if (item.getCar() != null)
            txtCarType.setText(txtCarType.getText() + " ("+item.getCar().getTitle()+") ");
        txtActToDate.setText(DateUtils.MiladiToPersianDate(item.getActToDate()));
        txtColor.setText(item.getColor());
        txtPhoneNumber.setText(item.getOwTel());
        txtlastLicenseId.setPaintFlags(txtlastLicenseId.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        txtlastLicenseId.setText(item.getLastLicenseId()+"");

        if (item.getHaveViolation() == null) {
            violationActDescriptionStatus.setBackgroundColor(getContext().getResources().getColor(R.color.grey_400));
        } else {
            if (item.getHaveViolation()) {
                violationActDescriptionStatus.setBackgroundColor(getContext().getResources().getColor(R.color.red_A700));
            } else {
                violationActDescriptionStatus.setBackgroundColor(getContext().getResources().getColor(R.color.grey_400));
            }
        }
    }

    private void init(View cell) {
        cardView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver
                .OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                cardView.getViewTreeObserver().removeOnPreDrawListener(this);
                minHeightCardView = cardView.getMeasuredHeightAndState() - moreLayout.getMeasuredHeightAndState();
                return true;
            }
        });

        collapseBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                toggleCardViewnHeight();

            }
        });
    }

    private void resetView() {
        moreLayout.setVisibility(GONE);

        txtID.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);

        ViewAnimator
                .animate(collapseBtn)
                .rotation(0)
                .duration(0)
                .start();

        CardView.LayoutParams params = new CardView.LayoutParams(CardView.LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        params.setMargins(8, 8, 8, 8);
        cardView.setLayoutParams(params);
    }

    private void toggleCardViewnHeight() {
        if (moreLayout.getVisibility() == GONE) {
            expand();
        } else {
            collapse();
        }
    }

    private void expand() {
        moreLayout.setVisibility(VISIBLE);

        ViewAnimator
                .animate(moreLayout)
                .slideTop()
                .duration(100)
                .start();

        ValueAnimator animImage = ValueAnimator.ofInt(16, 25);
        animImage.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                txtID.setTextSize(TypedValue.COMPLEX_UNIT_SP, val);
            }
        });
        animImage.start();

        ViewAnimator
                .animate(collapseBtn)
                .rotation(90)
                .duration(100)
                .start();
    }

    private void collapse() {
        ValueAnimator animCard = ValueAnimator.ofInt(cardView.getMeasuredHeightAndState(),
                minHeightCardView);
        animCard.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = cardView.getLayoutParams();
                layoutParams.height = val;
                cardView.setLayoutParams(layoutParams);
            }
        });
        animCard.start();
        animCard.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                resetView();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        ValueAnimator animImage = ValueAnimator.ofInt(25, 16);
        animImage.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                txtID.setTextSize(TypedValue.COMPLEX_UNIT_SP, val);
            }
        });
        animImage.start();

        ViewAnimator
                .animate(collapseBtn)
                .rotation(0)
                .duration(100)
                .start();
    }

    public void setOnLongClickListener(final OnItemClickListener<StaffCar> onItemClickListener) {
        view.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onItemClickListener.click(item, view, position);
                return false;
            }
        });
    }

    public void setOnLicenseIdListener(final OnItemClickListener<StaffCar> onItemClickListener) {
        txtlastLicenseId.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.click(item, view, position);
            }
        });
    }

}
