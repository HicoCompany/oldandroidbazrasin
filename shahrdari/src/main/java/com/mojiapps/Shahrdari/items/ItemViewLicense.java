package com.mojiapps.Shahrdari.items;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ahmadian.ruhallah.commons.fragment.list.SmartItemView;
import com.github.florent37.viewanimator.ViewAnimator;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.models.License;
import com.mojiapps.Shahrdari.utils.DateUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ruhallah-PC on 12/20/2016.
 */


public class ItemViewLicense extends SmartItemView<License> {

    @Bind(R.id.txtID)
    TextView txtID;
    @Bind(R.id.txtTitle)
    TextView txtTitle;
    @Bind(R.id.txtContractor)
    TextView txtContractor;
    @Bind(R.id.txtEmployer)
    TextView txtEmployer;
    @Bind(R.id.txtPhoneNumber)
    TextView txtPhoneNumber;
    @Bind(R.id.txtToDate)
    TextView txtToDate;
    @Bind(R.id.txtFromDate)
    TextView txtFromDate;
    @Bind(R.id.txtAddress)
    TextView txtAddress;
    @Bind(R.id.txtModernizationCode)
    TextView txtModernizationCode;
    @Bind(R.id.txtDischarge)
    TextView txtDischarge;
    @Bind(R.id.txtDimension)
    TextView txtDimension;
    @Bind(R.id.txtRfidM3)
    TextView txtRfidM3;
    @Bind(R.id.txtSoilM3)
    TextView txtSoilM3;
    @Bind(R.id.txtDescription)
    TextView txtDescription;
    @Bind(R.id.txtRfidTagsCount)
    TextView txtRfidTagsCount;

//    @Bind(R.id.txtRfidManualM3)
//    TextView txtRfidManualM3;
    @Bind(R.id.collapse_btn)
    ImageButton collapseBtn;
    @Bind(R.id.more_layout)
    LinearLayout moreLayout;
    @Bind(R.id.card_view)
    CardView cardView;
//    @Bind(R.id.violation_act_description_status_right)
//    View violationActDescriptionStatusRight;
    @Bind(R.id.violation_act_description_status_left)
    View violationActDescriptionStatusLeft;
    @Bind(R.id.txtState)
    TextView txtState;

    int minHeightCardView;

    View view;

    public ItemViewLicense(Context context) {
        super(context);
        view = View.inflate(context, R.layout.license_row, this);
        ButterKnife.bind(this, view);
        view.setOnClickListener(this);
        init(view);
    }

    @Override
    public void setItem(final License item, int position) {
        super.setItem(item, position);
        resetView();

        txtID.setText(item.getLicenseId() + "");
        if (item.getContractor() != null) {
            txtTitle.setText(item.getContractor().getTitle());
            txtContractor.setText(" (" + item.getContractor().getOwName() + ")");
        }
        txtEmployer.setText(item.getEmpName());
        txtPhoneNumber.setText(item.getEmpTel());
        txtToDate.setText(DateUtils.MiladiToPersianDate(item.getToDate()));
        txtFromDate.setText(DateUtils.MiladiToPersianDate(item.getFromDate()));
        txtAddress.setText(item.getAddress());
        txtModernizationCode.setText(item.getModernizationCode());
        if (item.getDischarge() != null)
            txtDischarge.setText(item.getDischarge().getTitle());
        try {
            float resultMM = item.getLength() * item.getWidth() * item.getHeight();
            txtDimension.setText(item.getLength() + "*" + item.getWidth() + "*" + item.getHeight() +
             "(" + resultMM + "متر مکعب" + ")" );
        }catch (Exception e) {
            txtDimension.setText(item.getLength() + "*" + item.getWidth() + "*" + item.getHeight());
        }
        txtRfidM3.setText(item.getRfidM3().toString());
        txtSoilM3.setText(item.getSoilM3().toString());
        txtDescription.setText(item.getDescription());
        txtRfidTagsCount.setText(item.getRfidTagsCount() + "");
       // txtRfidManualM3.setText(item.getRfidManualM3().toString());

        /*if (item.getConfirm() != null) {
            if (item.getConfirm()) {
                violationActDescriptionStatusRight.setBackgroundColor(getResources().getColor(R.color.lime_A700));
            } else {
                violationActDescriptionStatusRight.setBackgroundColor(getResources().getColor(R.color.deep_orange_A700));
            }
        } else {
            violationActDescriptionStatusRight.setBackgroundColor(getResources().getColor(R.color.grey_400));
        }*/

        if (item.getLicenseState() == null) {
           // violationActDescriptionStatusRight.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            txtState.setText("--");
        }
        else {
            switch (item.getLicenseState()) {
                case 1:
                    txtState.setText("درحال انجام");
                    //violationActDescriptionStatusRight.setBackgroundColor(getResources().getColor(R.color.lime_A700));
                    break;
                case 2:
                   // violationActDescriptionStatusRight.setBackgroundColor(getResources().getColor(R.color.deep_orange_A700));
                    txtState.setText("اتمام یافته");
                    break;
                case 3:
                  //  violationActDescriptionStatusRight.setBackgroundColor(getResources().getColor(R.color.orange_A400));
                    txtState.setText("تعلیق");
                    break;
                case 4:
                   // violationActDescriptionStatusRight.setBackgroundColor(getResources().getColor(R.color.grey_400));
                    txtState.setText("تایید بازرسی");
                    break;
                case 5:
                    //violationActDescriptionStatusRight.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                    txtState.setText("اتمام مهلت مجوز");
                    break;
                case 6:
                    //violationActDescriptionStatusRight.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                    txtState.setText("عدم شروع مجوز");
                    break;
            }
        }

        if (item.getConfirmNoViolation() != null) {
            if (item.getConfirmNoViolation()) {
                violationActDescriptionStatusLeft.setBackgroundColor(getResources().getColor(R.color.lime_A700));
            } else {
                violationActDescriptionStatusLeft.setBackgroundColor(getResources().getColor(R.color.deep_orange_A700));
            }
        } else {
            violationActDescriptionStatusLeft.setBackgroundColor(getResources().getColor(R.color.grey_400));
        }
    }

    private void init(View cell) {
        cardView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver
                .OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                cardView.getViewTreeObserver().removeOnPreDrawListener(this);
                minHeightCardView = cardView.getMeasuredHeightAndState() - moreLayout.getMeasuredHeightAndState();
                return true;
            }
        });

        collapseBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                toggleCardViewnHeight();

            }
        });
    }

    private void resetView() {
        moreLayout.setVisibility(GONE);

        txtID.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);

        ViewAnimator
                .animate(collapseBtn)
                .rotation(0)
                .duration(0)
                .start();

        CardView.LayoutParams params = new CardView.LayoutParams(CardView.LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        params.setMargins(8, 8, 8, 8);
        cardView.setLayoutParams(params);
    }

    private void toggleCardViewnHeight() {
        if (moreLayout.getVisibility() == GONE) {
            expand();
        } else {
            collapse();
        }
    }

    private void expand() {
        moreLayout.setVisibility(VISIBLE);

        ViewAnimator
                .animate(moreLayout)
                .slideTop()
                .duration(100)
                .start();

        ValueAnimator animImage = ValueAnimator.ofInt(16, 25);
        animImage.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                txtID.setTextSize(TypedValue.COMPLEX_UNIT_SP, val);
            }
        });
        animImage.start();

        ViewAnimator
                .animate(collapseBtn)
                .rotation(90)
                .duration(100)
                .start();
    }

    private void collapse() {
        ValueAnimator animCard = ValueAnimator.ofInt(cardView.getMeasuredHeightAndState(),
                minHeightCardView);
        animCard.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = cardView.getLayoutParams();
                layoutParams.height = val;
                cardView.setLayoutParams(layoutParams);
            }
        });
        animCard.start();
        animCard.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                resetView();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        ValueAnimator animImage = ValueAnimator.ofInt(25, 16);
        animImage.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                txtID.setTextSize(TypedValue.COMPLEX_UNIT_SP, val);
            }
        });
        animImage.start();

        ViewAnimator
                .animate(collapseBtn)
                .rotation(0)
                .duration(100)
                .start();
    }

    public void setOnLongClickListener(final OnItemClickListener<License> onItemClickListener) {
        view.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onItemClickListener.click(item, view, position);
                return false;
            }
        });
    }

    public void setOnLicenseIdListener(final OnItemClickListener<License> onItemClickListener) {
        txtTitle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.click(item, view, position);
            }
        });
    }

}
