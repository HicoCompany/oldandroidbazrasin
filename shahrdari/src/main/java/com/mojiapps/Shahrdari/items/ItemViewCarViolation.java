package com.mojiapps.Shahrdari.items;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.ahmadian.ruhallah.commons.fragment.list.SmartItemView;
import com.github.florent37.viewanimator.ViewAnimator;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.models.Car;
import com.mojiapps.Shahrdari.models.CarViolation;
import com.mojiapps.Shahrdari.models.HomeViolation;
import com.mojiapps.Shahrdari.models.Violation;
import com.mojiapps.Shahrdari.utils.DateUtils;
import com.mojiapps.Shahrdari.utils.Utils;

/**
 * Created by ruhallah-PC on 12/17/2016.
 */

public class ItemViewCarViolation extends SmartItemView<CarViolation> {

    ImageView imgViolation;
    TextView txtCode;
    TextView txtPlateNumber;
    TextView txtOwnerName;
    TextView txtAddress;
    TextView txtViolationType;
    TextView txtDate;
    TextView Description;
    TextView ViolationActDescription;
    TextView ViolationId;
    TableRow txtViolationActDescriptionfield;
    View violationActDescriptionStatus;
    CardView cardView;
    LinearLayout moreLayout;
    ImageButton collapseBtn;
    TextView txtColor;
    TextView txtPhoneNumber;
    TextView txtCarType;
    TextView GetsCertificate;

    int minHeightCardView;

    OnItemClickListener<CarViolation> onImageClickListener;

    public ItemViewCarViolation(Context context) {
        super(context);
        View view = View.inflate(context, R.layout.violation_not_member_row, this);
        init(view);
    }

    @Override
    public void setItem(final CarViolation item, final int position) {
        super.setItem(item, position);
        resetView();

        txtOwnerName.setText(item.getOwName());

        txtAddress.setText(item.getAddress());

        String violations = "";
        if (item.getViolationList() != null &&
                item.getViolationList().size() > 0) {
            for (Violation violation : item.getViolationList()) {
                violations += violation.getTitle() + " - ";
            }
            violations = violations.substring(0, violations.length() - 3);
        }
        txtViolationType.setText(violations);

        txtDate.setText(DateUtils.MiladiToPersianDateTime(item.getRegisterDate()));

        if (item.getDescription() != null)
            Description.setText(item.getDescription());

        if (item.getCar() != null)
            txtCode.setText(item.getCar().getCarId() + "");

        txtPlateNumber.setText(item.getPlateType() ? item.getPlateCode() : item.getOldPlate());

        txtColor.setText(item.getColor());
        txtPhoneNumber.setText(item.getOwTel());
        txtCarType.setText(item.getPlateType() ? getResources().getString(R.string.non_haffar) : getResources().getString(R.string.haffar));

        if (item.getCertificate() != null)
            GetsCertificate.setText(item.getCertificate().getTitle());
        else
            GetsCertificate.setText(getResources().getString(R.string.no_certificate));

        if (item.getCarViolationId() != null)
            ViolationId.setText(item.getCarViolationId().toString());
        else
            ViolationId.setText("***");

        if (item.getImage1() != null && item.getImage1().length() > 0)
            imgViolation.setImageBitmap(Utils.GetBitmapFromBase64Low(item.getImage1()));
        else
            imgViolation.setImageResource(R.drawable.ic_car);
        imgViolation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onImageClickListener.click(item, view, position);
            }
        });

        if (item.getViolationActDescription() != null)
            if (item.getViolationActDescription().length() > 0) {
                txtViolationActDescriptionfield.setVisibility(VISIBLE);
                violationActDescriptionStatus.setBackgroundColor(getContext().getResources().getColor(R.color.colorAccent));
                ViolationActDescription.setText(item.getViolationActDescription());
            }
    }

    public void setOnImageClickListener(OnItemClickListener<CarViolation> onItemClickListener) {
        this.onImageClickListener = onItemClickListener;
    }

    private void init(View cell) {
        imgViolation = (ImageView) cell.findViewById(R.id.imgViolation);
        txtOwnerName = (TextView) cell.findViewById(R.id.txtOwner);
        txtAddress = (TextView) cell.findViewById(R.id.txtAddress);
        txtViolationType = (TextView) cell.findViewById(R.id.txtViolationType);
        txtDate = (TextView) cell.findViewById(R.id.txtDate);
        Description = (TextView) cell.findViewById(R.id.txtDescription);
        violationActDescriptionStatus = (View) cell.findViewById(R.id.violation_act_description_status);
        moreLayout = (LinearLayout) cell.findViewById(R.id.more_layout);
        txtViolationActDescriptionfield = (TableRow) cell.findViewById(R.id.txtViolationActDescriptionField);
        ViolationActDescription = (TextView) cell.findViewById(R.id.txtViolationActDescription);
        ViolationId = (TextView) cell.findViewById(R.id.txtViolationId);
        cardView = (CardView) cell.findViewById(R.id.card_view);
        collapseBtn = (ImageButton) cell.findViewById(R.id.collapse_btn);
        txtColor = (TextView) cell.findViewById(R.id.txtColor);
        txtPhoneNumber = (TextView) cell.findViewById(R.id.txtPhoneNumber);
        txtCarType = (TextView) cell.findViewById(R.id.txtCarType);
        txtCode = (TextView) cell.findViewById(R.id.txtID);
        txtPlateNumber = (TextView) cell.findViewById(R.id.txtPlateNumber);
        GetsCertificate = (TextView) cell.findViewById(R.id.txtGetsCertificate);

        cardView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver
                .OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                cardView.getViewTreeObserver().removeOnPreDrawListener(this);
                minHeightCardView = cardView.getMeasuredHeightAndState() - moreLayout.getMeasuredHeightAndState();
                return true;
            }
        });

        collapseBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                toggleCardViewnHeight();

            }
        });
    }

    private void resetView() {
        moreLayout.setVisibility(GONE);

        ViewGroup.LayoutParams LayoutImageParams = imgViolation.getLayoutParams();
        LayoutImageParams.height = (int) getContext().getResources().getDimension(R.dimen.min_image_list);
        LayoutImageParams.width = (int) getContext().getResources().getDimension(R.dimen.min_image_list);
        imgViolation.setLayoutParams(LayoutImageParams);

        ViewAnimator
                .animate(collapseBtn)
                .rotation(0)
                .duration(0)
                .start();

        CardView.LayoutParams params = new CardView.LayoutParams(CardView.LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        params.setMargins(8, 8, 8, 8);
        cardView.setLayoutParams(params);
    }

    private void toggleCardViewnHeight() {
        if (moreLayout.getVisibility() == GONE) {
            expand();
        } else {
            collapse();
        }
    }

    private void expand() {
        moreLayout.setVisibility(VISIBLE);

        ViewAnimator
                .animate(moreLayout)
                .slideTop()
                .duration(100)
                .start();

        ValueAnimator animImage = ValueAnimator.ofInt(imgViolation.getMeasuredHeightAndState(),
                (int) getContext().getResources().getDimension(R.dimen.max_image_list));
        animImage.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = imgViolation.getLayoutParams();
                layoutParams.height = val;
                layoutParams.width = val;
                imgViolation.setLayoutParams(layoutParams);
            }
        });
        animImage.start();

        ViewAnimator
                .animate(collapseBtn)
                .rotation(90)
                .duration(100)
                .start();
    }

    private void collapse() {
        ValueAnimator animCard = ValueAnimator.ofInt(cardView.getMeasuredHeightAndState(),
                minHeightCardView);
        animCard.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = cardView.getLayoutParams();
                layoutParams.height = val;
                cardView.setLayoutParams(layoutParams);
            }
        });
        animCard.start();
        animCard.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                resetView();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        ValueAnimator animImage = ValueAnimator.ofInt(imgViolation.getMeasuredHeightAndState(),
                (int) getContext().getResources().getDimension(R.dimen.min_image_list));
        animImage.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = imgViolation.getLayoutParams();
                layoutParams.height = val;
                layoutParams.width = val;
                imgViolation.setLayoutParams(layoutParams);
            }
        });
        animImage.start();

        ViewAnimator
                .animate(collapseBtn)
                .rotation(0)
                .duration(100)
                .start();
    }


}
