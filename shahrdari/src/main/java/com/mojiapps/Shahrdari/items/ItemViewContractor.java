package com.mojiapps.Shahrdari.items;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.ahmadian.ruhallah.commons.fragment.list.SmartItemView;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.models.Contractor;
import com.mojiapps.Shahrdari.models.StaffCar;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ruhallah-PC on 12/19/2016.
 */

public class ItemViewContractor extends SmartItemView<Contractor> {

    @Bind(R.id.txtID)
    TextView txtID;
    @Bind(R.id.txtTitle)
    TextView txtTitle;
    @Bind(R.id.txtName)
    TextView txtName;
    @Bind(R.id.OwMobile)
    TextView OwMobile;

    @Bind(R.id.card_view)
    CardView cardView;
    @Bind(R.id.violation_act_description_status)
    View violationActDescriptionStatus;

    private View view;

    public ItemViewContractor(Context context) {
        super(context);
        view = View.inflate(context, R.layout.contractor_row, this);
        ButterKnife.bind(this, view);
        view.setOnClickListener(this);
    }

    @Override
    public void setItem(final Contractor item, int position) {
        super.setItem(item, position);

        txtID.setText(item.getContractorId()+"");
        txtTitle.setText(item.getTitle());
        txtName.setText(item.getOwName());
        OwMobile.setText(item.getOwMobile());
        if (item.getHaveViolation()!= null && item.getHaveViolation()) {
            violationActDescriptionStatus.setBackgroundColor(getContext().getResources().getColor(R.color.red_A700));
        }else {
            violationActDescriptionStatus.setBackgroundColor(getContext().getResources().getColor(R.color.grey_400));
        }
    }

    public void setOnLongClickListener(final OnItemClickListener<Contractor> onItemClickListener) {
        view.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onItemClickListener.click(item, view, position);
                return false;
            }
        });
    }
}
