package com.mojiapps.Shahrdari.models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.webservice.Params;

import java.util.Date;

/**
 * Created by ruhallah-PC on 12/6/2016.
 */
@DatabaseTable(tableName = "StaffCar")
public class StaffCar implements Params {

    @SerializedName(STAFF_CAR_ID)
    @DatabaseField(id = true, columnName = STAFF_CAR_ID)
    //شناسه خودروی عضو ستاد
    private Integer StaffCarId;
    @SerializedName(CAR)
    @DatabaseField(columnName = CAR, foreign = true, foreignAutoRefresh = true)
    //شناسه خودرو
    /*private Integer CarId;*/
    //recommended
    private Car car;
    @SerializedName(COLOR)
    @DatabaseField(columnName = COLOR)
    // رنگ
    private String Color;
    @SerializedName(PLATE_TYPE)
    @DatabaseField(columnName = PLATE_TYPE)
    //حفار صفر و غیر حفار یک
    private Boolean PlateType;
    @SerializedName(NEW_PLATE_PART_1)
    @DatabaseField(columnName = NEW_PLATE_PART_1)
    //بخش اول پلاک کد ملی
    private String NewPlatePart1;
    @SerializedName(NEW_PLATE_PART_2)
    @DatabaseField(columnName = NEW_PLATE_PART_2)
    private String NewPlatePart2;
    @SerializedName(NEW_PLATE_PART_3)
    @DatabaseField(columnName = NEW_PLATE_PART_3)
    private String NewPlatePart3;
    @SerializedName(NEW_PLATE_PART_4)
    @DatabaseField(columnName = NEW_PLATE_PART_4)
    private String NewPlatePart4;
    @SerializedName(OLD_PLATE)
    @DatabaseField(columnName = OLD_PLATE)
    //پلاک قدیمی
    private String OldPlate;
    @SerializedName(PLATE_CODE)
    @DatabaseField(columnName = PLATE_CODE)
    //پلاک
    private String PlateCode;
    @SerializedName(OW_NAME)
    @DatabaseField(columnName = OW_NAME)
    //نام مالک
    private String OwName;
    @SerializedName(OW_TEL)
    @DatabaseField(columnName = OW_TEL)
    //شماره تلفن تماس
    private String OwTel;
    @SerializedName(CONTRACTOR)
    @DatabaseField(columnName = CONTRACTOR, foreign = true, foreignAutoRefresh = true)
    /*//شناسه پیمانکار
    private Integer ContractorId;*/
    //recommended
    private Contractor contractor;
    @SerializedName(OLD_RECORD)
    @DatabaseField(columnName = OLD_RECORD)
    //شماره پرونده قدیم خودروهای حفار
    private String OldRecord;
    @SerializedName(ACT_FROM_DATE)
    @DatabaseField(columnName = ACT_FROM_DATE, dataType = DataType.DATE_STRING)
    //تاریخ شروع فعالیت
    private Date ActFromDate;
    @SerializedName(ACT_TO_DATE)
    @DatabaseField(columnName = ACT_TO_DATE, dataType = DataType.DATE_STRING)
    //تاریخ اتمام فعالیت
    private Date ActToDate;
    @SerializedName(HAVE_VIOLATION)
    @DatabaseField(columnName = HAVE_VIOLATION)
    //؟ آیا دارای تخلف است
    private Boolean HaveViolation;

    @SerializedName(LAST_LICENSE_ID)
    @DatabaseField(columnName = LAST_LICENSE_ID)
    int LastLicenseId;

    public Integer getStaffCarId() {
        return StaffCarId;
    }

    public void setStaffCarId(Integer staffCarId) {
        StaffCarId = staffCarId;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public Boolean getPlateType() {
        return PlateType;
    }

    public void setPlateType(Boolean plateType) {
        PlateType = plateType;
    }

    public String getNewPlatePart1() {
        return NewPlatePart1;
    }

    public void setNewPlatePart1(String newPlatePart1) {
        NewPlatePart1 = newPlatePart1;
    }

    public String getNewPlatePart2() {
        return NewPlatePart2;
    }

    public void setNewPlatePart2(String newPlatePart2) {
        NewPlatePart2 = newPlatePart2;
    }

    public String getNewPlatePart3() {
        return NewPlatePart3;
    }

    public void setNewPlatePart3(String newPlatePart3) {
        NewPlatePart3 = newPlatePart3;
    }

    public String getNewPlatePart4() {
        return NewPlatePart4;
    }

    public void setNewPlatePart4(String newPlatePart4) {
        NewPlatePart4 = newPlatePart4;
    }

    public String getOldPlate() {
        return OldPlate;
    }

    public void setOldPlate(String oldPlate) {
        OldPlate = oldPlate;
    }

    public String getPlateCode() {
        return PlateCode;
    }

    public void setPlateCode(String plateCode) {
        PlateCode = plateCode;
    }

    public String getOwName() {
        return OwName;
    }

    public void setOwName(String owName) {
        OwName = owName;
    }

    public String getOwTel() {
        return OwTel;
    }

    public void setOwTel(String owTel) {
        OwTel = owTel;
    }

    public Contractor getContractor() {
        return contractor;
    }

    public void setContractor(Contractor contractor) {
        this.contractor = contractor;
    }

    public String getOldRecord() {
        return OldRecord;
    }

    public void setOldRecord(String oldRecord) {
        OldRecord = oldRecord;
    }

    public Date getActFromDate() {
        return ActFromDate;
    }

    public void setActFromDate(Date actFromDate) {
        ActFromDate = actFromDate;
    }

    public Date getActToDate() {
        return ActToDate;
    }

    public void setActToDate(Date actToDate) {
        ActToDate = actToDate;
    }

    public Boolean getHaveViolation() {
        return HaveViolation;
    }

    public void setHaveViolation(Boolean haveViolation) {
        HaveViolation = haveViolation;
    }

    public int getLastLicenseId() {
        return LastLicenseId;
    }

    public void setLastLicenseId(int lastLicenseId) {
        LastLicenseId = lastLicenseId;
    }
}
