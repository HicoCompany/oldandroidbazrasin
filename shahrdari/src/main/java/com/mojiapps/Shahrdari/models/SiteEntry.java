package com.mojiapps.Shahrdari.models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.webservice.Params;

/**
 * Created by mojtaba on 24/10/2014.
 */
@DatabaseTable(tableName = "SITE_ID")
public class SiteEntry implements Params {

    @SerializedName(SITE_ID)
    @DatabaseField(id = true, columnName = SITE_ID)
    //شناسه سوال
    private int SiteId;
    /////////////////////////
    @SerializedName(TITLE)
    @DatabaseField(columnName = TITLE)
    //وضعیت سوال
    private String Title;
    ////////////////////////

    @SerializedName(STATE)
    @DatabaseField(columnName = STATE)
    //وضعیت سوال
    private boolean State;

    public SiteEntry(int siteId, String title , boolean state) {
        SiteId = siteId;
        Title=title;
        State = state;
    }

    public SiteEntry() {
    }


    public int getSiteId() {
        return SiteId;
    }

    public void setSiteId(int siteId) {
        SiteId = siteId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public boolean getState() {
        return State;
    }

    public void setState(boolean state) {
        State = state;
    }

}
