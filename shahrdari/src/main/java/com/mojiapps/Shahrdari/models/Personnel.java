package com.mojiapps.Shahrdari.models;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.GsonSettings;
import com.mojiapps.Shahrdari.webservice.Params;

import java.util.List;

/**
 * Created by mojtaba on 23/10/2014.
 */
@DatabaseTable(tableName = "Personnel")
public class Personnel implements Params {

    @SerializedName(PERSONNEL_ID)
    @DatabaseField(id = true, columnName = PERSONNEL_ID)
    //شناسه پرسنل
    private int PersonnelId;
    @SerializedName(NAME)
    @DatabaseField(columnName = NAME)
    //نام
    private String Name;
    @SerializedName(USER_NAME)
    @DatabaseField(columnName = USER_NAME)
    //نام کاربری
    private String UserName;
    @SerializedName(PASSWORD)
    @DatabaseField(columnName = PASSWORD)
    //رمز عبور
    private String Password;
    //// TODO: 2/17/2017 needs to fixed
    @SerializedName(AREA)
    private List<Area> areaList;
    @DatabaseField(columnName = AREA)
    private String areaListJson;

    @SerializedName(IS_ACTIVE)
    @DatabaseField(columnName = IS_ACTIVE)
    //فعال بودن
    private Boolean isActive;

    public int getPersonnelId() {
        return PersonnelId;
    }

    public void setPersonnelId(int personnelId) {
        PersonnelId = personnelId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    /*public Integer getAreaId() {
        return AreaId;
    }

    public void setAreaId(Integer areaId) {
        AreaId = areaId;
    }*/

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public List<Area> getAreaList() {
        return areaList;
    }

    public void setAreaList(List<Area> areas) {
        this.areaList = areas;
    }

    public void setAreaList(String areaListJson) {
        this.areaList = GsonSettings.getGson().fromJson(areaListJson, new TypeToken<List<Area>>(){}.getType());
    }

    public String getAreaListJson() {
        return areaListJson;
    }

    public void setAreaListJson(String areaListJson) {
        areaListJson = areaListJson;
    }

    public void setAreaListJson(List<Area> areas) {
        areaListJson = GsonSettings.getGson().toJson(areas).toString();
    }
}