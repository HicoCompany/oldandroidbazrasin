package com.mojiapps.Shahrdari.models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.webservice.Params;

/**
 * Created by ruhallah-PC on 12/6/2016.
 */

@DatabaseTable(tableName = "Area")
public class Area implements Params {

    @SerializedName(AREA_ID)
    @DatabaseField(id = true, columnName = AREA_ID)
    //شناسه منطقه گشتی
    private Integer areaId;
    @SerializedName(TITLE)
    @DatabaseField(columnName = TITLE)
    //عنوان
    private String title;

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
