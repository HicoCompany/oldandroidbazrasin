package com.mojiapps.Shahrdari.models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.webservice.Params;

/**
 * Created by mojtaba on 24/10/2014.
 */
@DatabaseTable(tableName = "Car")
public class Car implements Params {

    @SerializedName(CAR_ID)
    @DatabaseField(id = true, columnName = CAR_ID)
    //شناسه خودرو
    private int CarId;
    @SerializedName(TITLE)
    @DatabaseField(columnName = TITLE)
    //عنوان خودرو
    private String Title;
    @SerializedName(IS_DIGGER)
    @DatabaseField(columnName = IS_DIGGER)
    //صفر: غیر حفار و یک: حفار
    private Boolean IsDigger;

    public Car(int carId, String title, Boolean isDigger) {
        CarId = carId;
        Title = title;
        IsDigger = isDigger;
    }

    public Car() {
    }

    public int getCarId() {
        return CarId;
    }

    public void setCarId(int carId) {
        CarId = carId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Boolean getDigger() {
        return IsDigger;
    }

    public void setDigger(Boolean digger) {
        IsDigger = digger;
    }
}
