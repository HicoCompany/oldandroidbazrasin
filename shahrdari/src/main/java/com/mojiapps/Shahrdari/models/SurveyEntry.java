package com.mojiapps.Shahrdari.models;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.GsonSettings;
import com.mojiapps.Shahrdari.webservice.Params;

import java.util.List;

/**
 * Created by mojtaba on 23/10/2014.
 */
@DatabaseTable(tableName = "Personnel")
public class SurveyEntry implements Params {

    @SerializedName(Survey_ID)
    //@DatabaseField(id = true, columnName = Booth_ID)
    //شناسه نظرسنجی
    private int SurveyId;

    @SerializedName(Booth_ID)
    //@DatabaseField(id = true, columnName = Booth_ID)
    //شناسه غرفه
    private int BoothId;

    @SerializedName(Report_Description)
    // @DatabaseField(columnName = Report_Description)
    //شرح گزارش
    private String ReportDescription;

    @SerializedName(Follow_Description)
    // @DatabaseField(columnName = Follow_Description)
    //نظریه ی ناظر
    private String FollowDescription;

    @SerializedName(Final_Description)
    // @DatabaseField(columnName = Final_Description)
    //نظریه ی نهایی
    private String FinalDescription;

    @SerializedName(PERSONNEL_ID)
    //  @DatabaseField(columnName = PERSONNEL_ID)
    //نظریه ی نهایی
    private int PersonnelID;

    //// TODO: 2/17/2017 needs to fixed
    @SerializedName(QUESTION)
    private List<QuestionEntry> Questions;

    // private String QuestionListJson;

    @SerializedName(SITES)
    private List<SiteEntry> Sites;

    @SerializedName(Latitude_Location)
    //@DatabaseField(id = true, columnName = Booth_ID)
    private float LatitudeLocation;

    @SerializedName(Longitude_Location)
    //@DatabaseField(id = true, columnName = Booth_ID)
    private float LongitudeLocation;

    public int getSurveyId() {
        return SurveyId;
    }

    public void setSurveyId(int surveyId) {
        SurveyId = surveyId;
    }


    public int getBoothId() {
        return BoothId;
    }

    public void setBoothId(int boothId) {
        BoothId = boothId;
    }

    public String getReportDescription() {
        return ReportDescription;
    }

    public void setReportDescription(String reportDescription) {
        ReportDescription = reportDescription;
    }

    public String getFollowDescription() {
        return FollowDescription;
    }

    public void setFollowDescription(String followDescription) {
        FollowDescription = followDescription;
    }

    public String getFinalDescription() {
        return FinalDescription;
    }

    public void setFinalDescription(String finalDescription) {
        FinalDescription = finalDescription;
    }

    public int getPersonnelID() {
        return PersonnelID;
    }

    public void setPersonnelID(int personnelID) {
        PersonnelID = personnelID;
    }

    public List<QuestionEntry> getQuestionsList() {
        return Questions;
    }

    public void setQuestions(List<QuestionEntry> questions) {
        this.Questions = questions;
    }

    public void setQuestions(String questionListJson) {
        this.Questions = GsonSettings.getGson().fromJson(questionListJson, new TypeToken<List<Area>>() {
        }.getType());
    }

    public List<SiteEntry> getSitesList() {
        return Sites;
    }

    public void setSites(List<SiteEntry> sites) {
        this.Sites = sites;
    }

    public void setSites(String sitesListJson) {
        this.Sites = GsonSettings.getGson().fromJson(sitesListJson, new TypeToken<List<Area>>() {
        }.getType());
    }

    public float getLatitudeLocation() {
        return LatitudeLocation;
    }

    public void setLatitudeLocation(float latitudeLocation) {
        LatitudeLocation = latitudeLocation;
    }
    public float getLongitudeLocation() {
        return LongitudeLocation;
    }

    public void setLongitudeLocation(float longitudeLocation) {
        LongitudeLocation = longitudeLocation;
    }


//    public String getQuestionListJson() {
//        return QuestionListJson;
//    }
//
//    public void setQuestionListJson(String questionListJson) {
//        QuestionListJson = questionListJson;
//    }

//    public void setQuestionListJson(List<Question> questions) {
//        QuestionListJson = GsonSettings.getGson().toJson(questions).toString();
//    }


}