package com.mojiapps.Shahrdari.models;

import com.google.gson.annotations.SerializedName;
import com.mojiapps.Shahrdari.webservice.Params;

/**
 * Created by ruhallah-PC on 2/22/2017.
 */

public class Pictures {

    @SerializedName(Params.IMAGE_1)
    String image1;
    @SerializedName(Params.IMAGE_2)
    String image2;
    @SerializedName(Params.IMAGE_3)
    String image3;
    @SerializedName(Params.IMAGE_4)
    String image4;

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }
}
