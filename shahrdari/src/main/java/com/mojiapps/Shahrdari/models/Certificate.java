package com.mojiapps.Shahrdari.models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.webservice.Params;

/**
 * Created by ruhallah-PC on 12/6/2016.
 */

@DatabaseTable(tableName = "Certificate")
public class Certificate implements Params {

    @SerializedName(CERTIFICATE_ID)
    @DatabaseField(id = true, columnName = CERTIFICATE_ID)
    //شناسه مدارک احراز هویت
    private Integer CertificateId;
    @SerializedName(TITLE)
    @DatabaseField(columnName = TITLE)
    //عنوان
    private String Title;

    public Certificate() {
    }

    public Certificate(Integer certificateId, String title) {
        CertificateId = certificateId;
        Title = title;
    }

    public Integer getCertificateId() {
        return CertificateId;
    }

    public void setCertificateId(Integer certificateId) {
        CertificateId = certificateId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }
}
