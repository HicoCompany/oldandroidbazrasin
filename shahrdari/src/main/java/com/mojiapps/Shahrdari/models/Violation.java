package com.mojiapps.Shahrdari.models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.webservice.Params;

/**
 * Created by mojtaba on 23/10/2014.
 */
@DatabaseTable(tableName = "Violation")
public class Violation implements Params {

    @SerializedName(VIOLATION_ID)
    @DatabaseField(id = true, columnName = VIOLATION_ID)
    //شناسه نوع تخلف
    private int ViolationId;
    @SerializedName(TITLE)
    @DatabaseField(columnName = TITLE)
    //عنوان
    private String Title;

    public Violation() {
    }

    public Violation(int violationId, String title) {
        ViolationId = violationId;
        Title = title;
    }

    public int getViolationId() {
        return ViolationId;
    }

    public void setViolationId(int violationId) {
        ViolationId = violationId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }
}
