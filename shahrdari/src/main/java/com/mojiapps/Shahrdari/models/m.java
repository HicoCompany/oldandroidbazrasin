package com.mojiapps.Shahrdari.models;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.GsonSettings;
import com.mojiapps.Shahrdari.webservice.Params;

import java.util.Date;
import java.util.List;

/**
 * Created by mojtaba on 24/10/2014.
 */

public class m implements Params {

    private int id;
    @SerializedName(CAR_VIOLATION_ID)
    @DatabaseField(columnName = CAR_VIOLATION_ID, unique = true)
    //شناسه تخلف خودروهای غیر عضو ستاد
    private Integer CarViolationId;
    @SerializedName(OW_NAME)
    @DatabaseField(columnName = OW_NAME)
    //نام مالک
    private String OwName;
    @SerializedName(OW_TEL)
    @DatabaseField(columnName = OW_TEL)
    //شماره تماس مالک
    private String OwTel;
    @SerializedName(REGISTER_DATE)
    @DatabaseField(columnName = REGISTER_DATE, dataType = DataType.DATE_STRING)
    //تاریخ ثبت تخلف
    private Date RegisterDate;
    ////////////////////////////////////
//    @SerializedName(REGISTER_DATE1)
//    @DatabaseField(columnName = REGISTER_DATE1, dataType = DataType.DATE_STRING)
//    //تاریخ ثبت تخلف
//    private String RegisterDate1;

    @SerializedName(CAR)
    @DatabaseField(columnName = CAR,foreign = true, foreignAutoRefresh = true)
    //شناسه خودرو
    //recommended
    private Car car;
    @SerializedName(ADDRESS)
    @DatabaseField(columnName = ADDRESS)
    //آدرس
    private String Address;
    @SerializedName(COLOR)
    @DatabaseField(columnName = COLOR)
    //رنگ
    private String Color;
    @SerializedName(PLATE_TYPE)
    @DatabaseField(columnName = PLATE_TYPE)
    //حفار صفر و غیر حفار یک
    private Boolean PlateType;
    @SerializedName(NEW_PLATE_PART_1)
    @DatabaseField(columnName = NEW_PLATE_PART_1)
    //بخش اول پلاک ملی
    private String NewPlatePart1;
    @SerializedName(NEW_PLATE_PART_2)
    @DatabaseField(columnName = NEW_PLATE_PART_2)
    private String NewPlatePart2;
    @SerializedName(NEW_PLATE_PART_3)
    @DatabaseField(columnName = NEW_PLATE_PART_3)
    private String NewPlatePart3;
    @SerializedName(NEW_PLATE_PART_4)
    @DatabaseField(columnName = NEW_PLATE_PART_4)
    private String NewPlatePart4;
    @SerializedName(OLD_PLATE)
    @DatabaseField(columnName = OLD_PLATE)
    //پلاک قدیمی
    private String OldPlate;
    @SerializedName(PLATE_CODE)
    @DatabaseField(columnName = PLATE_CODE)
    //پلاک
    private String PlateCode;
    @SerializedName(CERTIFICATE)
    @DatabaseField(columnName = CERTIFICATE,foreign = true, foreignAutoRefresh = true)
    //شناسه مدرک احراز هویت
    /*private Integer CertificateId;*/
    //recommended
    private Certificate certificate;
    @SerializedName(PERSONNEL_ID)
    @DatabaseField(columnName = PERSONNEL_ID)
    //شناسه پرسنل
    private Integer PersonnelId;
    @SerializedName(DESCRIPTION)
    @DatabaseField(columnName = DESCRIPTION)
    //شرح
    private String Description;

    @SerializedName(VIOLATION_LIST)
    //لیست تخلفات
    private List<Violation> ViolationList;
    @DatabaseField(columnName = VIOLATION_LIST)
    private String ViolationListJson;

    @SerializedName(IMAGE_1)
    @DatabaseField(columnName = IMAGE_1)
    //تصویر تخلف
    private String Image1;
    @SerializedName(IMAGE_2)
    @DatabaseField(columnName = IMAGE_2)
    //تصویر تخلف
    private String Image2;
    @SerializedName(IMAGE_3)
    @DatabaseField(columnName = IMAGE_3)
    //تصویر تخلف
    private String Image3;
    @SerializedName(IMAGE_4)
    @DatabaseField(columnName = IMAGE_4)
    //تصویر تخلف
    private String Image4;

    @SerializedName(VIOLATION_ACT_DESCRIPTION)
    @DatabaseField(columnName = VIOLATION_ACT_DESCRIPTION)
    //شرح اقدام تخلف
    private String ViolationActDescription;

    @DatabaseField(columnName = IS_SYNCABLE)
    private boolean isSyncable = false;

    public m() {
    }

    public Integer getCarViolationId() {
        return CarViolationId;
    }

    public void setCarViolationId(Integer carViolationId) {
        CarViolationId = carViolationId;
    }

    public String getOwName() {
        return OwName;
    }

    public void setOwName(String owName) {
        OwName = owName;
    }

    public String getOwTel() {
        return OwTel;
    }

    public void setOwTel(String owTel) {
        OwTel = owTel;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public Boolean getPlateType() {
        return PlateType;
    }

    public void setPlateType(Boolean plateType) {
        PlateType = plateType;
    }

    public String getNewPlatePart1() {
        return NewPlatePart1;
    }

    public void setNewPlatePart1(String newPlatePart1) {
        NewPlatePart1 = newPlatePart1;
    }

    public String getNewPlatePart2() {
        return NewPlatePart2;
    }

    public void setNewPlatePart2(String newPlatePart2) {
        NewPlatePart2 = newPlatePart2;
    }

    public String getNewPlatePart3() {
        return NewPlatePart3;
    }

    public void setNewPlatePart3(String newPlatePart3) {
        NewPlatePart3 = newPlatePart3;
    }

    public String getNewPlatePart4() {
        return NewPlatePart4;
    }

    public void setNewPlatePart4(String newPlatePart4) {
        NewPlatePart4 = newPlatePart4;
    }

    public String getOldPlate() {
        return OldPlate;
    }

    public void setOldPlate(String oldPlate) {
        OldPlate = oldPlate;
    }

    public String getPlateCode() {
        return PlateCode;
    }

    public void setPlateCode(String plateCode) {
        PlateCode = plateCode;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    public Integer getPersonnelId() {
        return PersonnelId;
    }

    public void setPersonnelId(Integer personnelId) {
        PersonnelId = personnelId;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public List<Violation> getViolationList() {
        return ViolationList;
    }

    public void setViolationList(List<Violation> violationList) {
        ViolationList = violationList;
    }

    public void setViolationList(String violationListJson) {
        ViolationList = GsonSettings.getGson().fromJson(violationListJson, new TypeToken<List<Violation>>(){}.getType());
    }

    public String getViolationListJson() {
        return ViolationListJson;
    }

    public void setViolationListJson(String violationListJson) {
        ViolationListJson = violationListJson;
    }

    public void setViolationListJson(List<Violation> violationList) {
        ViolationListJson = GsonSettings.getGson().toJson(violationList).toString();
    }

    public String getImage1() {
        return Image1;
    }

    public void setImage1(String image1) {
        Image1 = image1;
    }

    public String getImage2() {
        return Image2;
    }

    public void setImage2(String image2) {
        Image2 = image2;
    }

    public String getImage3() {
        return Image3;
    }

    public void setImage3(String image3) {
        Image3 = image3;
    }

    public String getImage4() {
        return Image4;
    }

    public void setImage4(String image4) {
        Image4 = image4;
    }

    public String getViolationActDescription() {
        return ViolationActDescription;
    }

    public void setViolationActDescription(String violationActDescription) {
        ViolationActDescription = violationActDescription;
    }

//    public String getRegisterDate1() {
//        return RegisterDate1;
//    }
//
//    public void setRegisterDate1(String registerDate1) {
//        RegisterDate1 = registerDate1;
//    }

    public int getId() {
        return id;
    }
    public Date getRegisterDate() {
        return RegisterDate;
    }

    public void setRegisterDate(Date registerDate) {
        RegisterDate = registerDate;
    }

    public boolean isSyncable() {
        return isSyncable;
    }

    public void setSyncable(boolean syncable) {
        isSyncable = syncable;
    }
}
