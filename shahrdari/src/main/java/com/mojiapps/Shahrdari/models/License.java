package com.mojiapps.Shahrdari.models;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.GsonSettings;
import com.mojiapps.Shahrdari.webservice.Params;

import java.util.Date;
import java.util.List;

/**
 * Created by mojtaba on 24/10/2014.
 */
@DatabaseTable(tableName = "License")
public class License implements Params {

    @SerializedName(LICENSE_ID)
    @DatabaseField(id = true, columnName = LICENSE_ID)
    //شناسه مجوز
    private int LicenseId;
    @SerializedName(CONTRACTOR)
    @DatabaseField(columnName = CONTRACTOR, foreign = true, foreignAutoRefresh = true)
    //شناسه پیمانکار
    /*private int ContractorId;*/
    //recommended
    private Contractor contractor;
    @SerializedName(EMP_NAME)
    @DatabaseField(columnName = EMP_NAME)
    //نام کارفرما
    private String EmpName;
    @SerializedName(EMP_TEL)
    @DatabaseField(columnName = EMP_TEL)
    //شماره تماس کارفرما
    private String EmpTel;
    @SerializedName(WASTE_TYPE)
    @DatabaseField(columnName = WASTE_TYPE)
    //شناسه نوع پسماند
    private Integer WasteType;
    @SerializedName(MODERNIZATION_CODE)
    @DatabaseField(columnName = MODERNIZATION_CODE)
    //کد نوسازی یا شماره قرارداد مجوز
    private String ModernizationCode;
    @SerializedName(ADDRESS)
    @DatabaseField(columnName = ADDRESS)
    //آدرس
    private String Address;
    @SerializedName(AREA)
    @DatabaseField(columnName = AREA, foreign = true, foreignAutoRefresh = true)
    //recommended
    private Area area;
    //شناسه منطقه گشتی
    private Integer AreaId;
    @SerializedName(DISCHARGE)
    @DatabaseField(columnName = DISCHARGE, foreign = true, foreignAutoRefresh = true)
    //شناسه سایت تخلیه پسماند
    /*private String DischargeId;*/
    //recommended
    private Discharge discharge;
    @SerializedName(FROM_DATE)
    @DatabaseField(columnName = FROM_DATE, dataType = DataType.DATE_STRING)
    //تاریخ شروع مجوز
    private Date FromDate;
    @SerializedName(TO_DATE)
    @DatabaseField(columnName = TO_DATE, dataType = DataType.DATE_STRING)
    //تاریخ اتمام مجوز
    private Date ToDate;
    @SerializedName(ACT_IN_MORNING)
    @DatabaseField(columnName = ACT_IN_MORNING)
    //فعالیت در شیفت صبح
    private Boolean ActInMorning;
    @SerializedName(ACT_IN_EVENING)
    @DatabaseField(columnName = ACT_IN_EVENING)
    //فعالیت در شیفت عصر
    private Boolean ActInEvening;
    @SerializedName(ACT_IN_NIGHT)
    @DatabaseField(columnName = ACT_IN_NIGHT)
    //فعالیت در شیفت شب
    private Boolean ActInNight;
    @SerializedName(LENGTH)
    @DatabaseField(columnName = LENGTH)
    //طول
    private Float Length;
    @SerializedName(WIDTH)
    @DatabaseField(columnName = WIDTH)
    //عرض
    private Float Width;
    @SerializedName(HEIGHT)
    @DatabaseField(columnName = HEIGHT)
    //ارتفاع
    private Float Height;
    @SerializedName(DESCRIPTION)
    @DatabaseField(columnName = DESCRIPTION)
    //شرح
    private String Description;
    @SerializedName(MANUAL_TRANSFER_M_3)
    @DatabaseField(columnName = MANUAL_TRANSFER_M_3)
    //حجم انتقال دستی
    private Float ManualTransferM3;
    @SerializedName(MANUAL_TRANSFER_DESCRIPTION)
    @DatabaseField(columnName = MANUAL_TRANSFER_DESCRIPTION)
    //...
    private String ManualTransferDescription;

    //لیست خودروهای شرکت کننده در مجوز،
    @SerializedName(STAFF_CAR_LIST)
    private List<StaffCar> staffCarList;
    @DatabaseField(columnName = STAFF_CAR_LIST)
    private String StaffCarIdListJson;
///////////////////////////////////////////////////////
    @SerializedName(IS_CONFIRM_From_Personnel_1)
    @DatabaseField(columnName = IS_CONFIRM_From_Personnel_1)
    //تایید مجوز توسط بازرس اول
    private Boolean IsConfirmFromPersonnel1;
    @SerializedName(CONFIRM_PERSONNEL_ID_1)
    @DatabaseField(columnName = CONFIRM_PERSONNEL_ID_1)
    //کد بازرس اول
    private int ConfirmPersonnelId1;
    @SerializedName(CONFIRM_DESCRIPTION1)
    @DatabaseField(columnName = CONFIRM_DESCRIPTION1)
     //شرح تایید اول
    private String ConfirmDescription1;
    ///////////////////////////////////////////////////////////
    @SerializedName(IS_CONFIRM_From_Personnel_2)
    @DatabaseField(columnName = IS_CONFIRM_From_Personnel_2)
    //تایید مجوز توسط بازرس دوم
    private Boolean IsConfirmFromPersonnel2;
    @SerializedName(CONFIRM_PERSONNEL_ID_2)
    @DatabaseField(columnName = CONFIRM_PERSONNEL_ID_2)
    //کد بازرس دوم
    private int ConfirmPersonnelId2;
    @SerializedName(CONFIRM_DESCRIPTION2)
    @DatabaseField(columnName = CONFIRM_DESCRIPTION2)
    //شرح تایید دوم
    private String ConfirmDescription2;
    ///////////////////////////////////////////////////////////
    @SerializedName(IS_CONFIRM_NO_VIOLATION1)
    @DatabaseField(columnName = IS_CONFIRM_NO_VIOLATION2)
    //تایید عدم خلافی
    private Boolean IsConfirmNoViolation1;
    @SerializedName(CONFIRM_NO_VIOLATION_DESCRIPTION1)
    @DatabaseField(columnName = CONFIRM_NO_VIOLATION_DESCRIPTION1)
    //شرح تایید
    private String ConfirmNoViolationDescription1;
    @SerializedName(PERSONNEL_ID)
    @DatabaseField(columnName = PERSONNEL_ID)
    //شناسه پرسنل
    private Integer PersonnelId;
    @SerializedName(LICENSE_STOP)
    @DatabaseField(columnName = LICENSE_STOP)
    //مجوز به حالت تعلیق درآمده
    private Boolean LicenseStop;
    @SerializedName(CLOSE_LICENSE)
    @DatabaseField(columnName = CLOSE_LICENSE)
    //اتمام مجوز
    private Boolean CloseLicense;
    @SerializedName(RFID_TAGS_COUNT)
    @DatabaseField(columnName = RFID_TAGS_COUNT)
    //تعداد تگ های
    // خوانده شده از مجوز
    private Integer RfidTagsCount;
    @SerializedName(RFID_MRFID_M_3)
    @DatabaseField(columnName = RFID_MRFID_M_3)
    //حجم پسماند انتقالی به سایت
    private Float RfidM3;
    @SerializedName(SOIL_M_3)
    @DatabaseField(columnName = SOIL_M_3)
    //حجم خاک مرغوب فروشی
    private Float SoilM3;

    @SerializedName(RFID_MANUAL_M_3)
    @DatabaseField(columnName = RFID_MANUAL_M_3)
    //حجم خاک مرغوب فروشی
    private Float RfidManualM3;

    @SerializedName(LICENSE_STATE)
    @DatabaseField(columnName = LICENSE_STATE)
    private Integer LicenseState;

    @DatabaseField(columnName = IS_EDITED)
    private boolean isEdited;

    public int getLicenseId() {
        return LicenseId;
    }

    public void setLicenseId(int licenseId) {
        LicenseId = licenseId;
    }

    public Contractor getContractor() {
        return contractor;
    }

    public void setContractor(Contractor contractor) {
        this.contractor = contractor;
    }

    public String getEmpName() {
        return EmpName;
    }

    public void setEmpName(String empName) {
        EmpName = empName;
    }

    public String getEmpTel() {
        return EmpTel;
    }

    public void setEmpTel(String empTel) {
        EmpTel = empTel;
    }

    public Integer getWasteType() {
        return WasteType;
    }

    public void setWasteType(Integer wasteType) {
        WasteType = wasteType;
    }

    public String getModernizationCode() {
        return ModernizationCode;
    }

    public void setModernizationCode(String modernizationCode) {
        ModernizationCode = modernizationCode;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Integer getAreaId() {
        return AreaId;
    }

    public void setAreaId(Integer areaId) {
        AreaId = areaId;
    }

    public Discharge getDischarge() {
        return discharge;
    }

    public void setDischarge(Discharge discharge) {
        this.discharge = discharge;
    }

    public Date getFromDate() {
        return FromDate;
    }

    public void setFromDate(Date fromDate) {
        FromDate = fromDate;
    }

    public Date getToDate() {
        return ToDate;
    }

    public void setToDate(Date toDate) {
        ToDate = toDate;
    }

    public Boolean getActInMorning() {
        return ActInMorning;
    }

    public void setActInMorning(Boolean actInMorning) {
        ActInMorning = actInMorning;
    }

    public Boolean getActInEvening() {
        return ActInEvening;
    }

    public void setActInEvening(Boolean actInEvening) {
        ActInEvening = actInEvening;
    }

    public Boolean getActInNight() {
        return ActInNight;
    }

    public void setActInNight(Boolean actInNight) {
        ActInNight = actInNight;
    }

    public Float getLength() {
        return Length;
    }

    public void setLength(Float length) {
        Length = length;
    }

    public Float getWidth() {
        return Width;
    }

    public void setWidth(Float width) {
        Width = width;
    }

    public Float getHeight() {
        return Height;
    }

    public void setHeight(Float height) {
        Height = height;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Float getManualTransferM3() {
        return ManualTransferM3;
    }

    public void setManualTransferM3(Float manualTransferM3) {
        ManualTransferM3 = manualTransferM3;
    }

    public String getManualTransferDescription() {
        return ManualTransferDescription;
    }

    public void setManualTransferDescription(String manualTransferDescription) {
        ManualTransferDescription = manualTransferDescription;
    }

    public Boolean getConfirm1() {
        return IsConfirmFromPersonnel1;
    }

    public void setConfirm1(Boolean confirm) {
        IsConfirmFromPersonnel1 = confirm;
    }

    public String getConfirmDescription1() {
        return ConfirmDescription1;
    }

    public void setConfirmDescription1(String confirmDescription) {
        ConfirmDescription1 = confirmDescription;
    }
    public int getConfirmPersonnelId1() {
        return ConfirmPersonnelId1;
    }

    public void setConfirmPersonnelId1(int ConfirmPersonnelId1) {
        ConfirmPersonnelId1 = ConfirmPersonnelId1;
    }

    public int getConfirmPersonnelId2() {
        return ConfirmPersonnelId2;
    }

    public void setConfirmPersonnelId2(int ConfirmPersonnelId2) {
        ConfirmPersonnelId2 = ConfirmPersonnelId2;
    }
    public Boolean getConfirm2() {
        return IsConfirmFromPersonnel2;
    }

    public void setConfirm2(Boolean confirm) {
        IsConfirmFromPersonnel2 = confirm;
    }

    public String getConfirmDescription2() {
        return ConfirmDescription2;
    }

    public void setConfirmDescription2(String confirmDescription) {
        ConfirmDescription2 = confirmDescription;
    }


    public Boolean getConfirmNoViolation() {
        return IsConfirmNoViolation1;
    }

    public void setConfirmNoViolation(Boolean confirmNoViolation) {
        IsConfirmNoViolation1 = confirmNoViolation;
    }

    public String getConfirmNoViolationDescription() {
        return ConfirmNoViolationDescription1;
    }

    public void setConfirmNoViolationDescription(String confirmNoViolationDescription) {
        ConfirmNoViolationDescription1 = confirmNoViolationDescription;
    }

    public Integer getPersonnelId() {
        return PersonnelId;
    }

    public void setPersonnelId(Integer personnelId) {
        PersonnelId = personnelId;
    }

    public Boolean getLicenseStop() {
        return LicenseStop;
    }

    public void setLicenseStop(Boolean licenseStop) {
        LicenseStop = licenseStop;
    }

    public Boolean getCloseLicense() {
        return CloseLicense;
    }

    public void setCloseLicense(Boolean closeLicense) {
        CloseLicense = closeLicense;
    }

    public Integer getRfidTagsCount() {
        return RfidTagsCount;
    }

    public void setRfidTagsCount(Integer rfidTagsCount) {
        RfidTagsCount = rfidTagsCount;
    }

    public Float getRfidM3() {
        return RfidM3;
    }

    public void setRfidM3(Float rfidM3) {
        RfidM3 = rfidM3;
    }

    public Float getRfidManualM3() {
        return RfidM3;
    }

    public void setRfidManualM3(Float rfidM3) {
        RfidM3 = rfidM3;
    }

    public Float getSoilM3() {
        return SoilM3;
    }

    public void setSoilM3(Float soilM3) {
        SoilM3 = soilM3;
    }

    public List<StaffCar> getStaffCarList() {
        return staffCarList;
    }

    public void setStaffCarList(List<StaffCar> staffCarList) {
        this.staffCarList = staffCarList;
    }

    public void setStaffCarList(String staffCarListJson) {
        this.staffCarList = GsonSettings.getGson().fromJson(staffCarListJson, new TypeToken<List<StaffCar>>(){}.getType());
    }

    public String getStaffCarIdListJson() {
        return StaffCarIdListJson;
    }

    public void setStaffCarIdListJson(String staffCarIdListJson) {
        StaffCarIdListJson = staffCarIdListJson;
    }

    public void setStaffCarIdListJson(List<StaffCar> staffCarList) {
        StaffCarIdListJson = GsonSettings.getGson().toJson(staffCarList).toString();
    }

    public boolean isEdited() {
        return isEdited;
    }

    public void setEdited(boolean edited) {
        isEdited = edited;
    }

    public Integer getLicenseState() {
        return LicenseState;
    }

    public void setLicenseState(Integer licenseState) {
        LicenseState = licenseState;
    }
}
