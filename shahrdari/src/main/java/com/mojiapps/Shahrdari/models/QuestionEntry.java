package com.mojiapps.Shahrdari.models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.webservice.Params;

/**
 * Created by mojtaba on 24/10/2014.
 */
@DatabaseTable(tableName = "Question_ID")
public class QuestionEntry implements Params {

    @SerializedName(Question_ID)
    @DatabaseField(id = true, columnName = Question_ID)
    //شناسه سوال
    private int QuestionId;
    /////////////////////////
    @SerializedName(TITLE)
    @DatabaseField(columnName = TITLE)
    //وضعیت سوال
    private String Title;
    ////////////////////////

    @SerializedName(STATE)
    @DatabaseField(columnName = STATE)
    //وضعیت سوال
    private boolean State;

    public QuestionEntry(int questionId,String title ,boolean state) {
        QuestionId = questionId;
        Title=title;
        State = state;
    }

    public QuestionEntry() {
    }


    public int getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(int questionId) {
        QuestionId = questionId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public boolean getState() {
        return State;
    }

    public void setState(boolean state) {
        State = state;
    }

}
