package com.mojiapps.Shahrdari.models;

import com.google.gson.annotations.SerializedName;
import com.mojiapps.Shahrdari.webservice.Params;

/**
 * Created by ruhallah-PC on 2/1/2017.
 */

public class ResultAdd implements Params {

    @SerializedName(DATA)
    private String data;

    @SerializedName(STATE)
    private String state;

    @SerializedName(MESSAGE)
    private String message;

    public ResultAdd() {
    }

    public ResultAdd(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
