package com.mojiapps.Shahrdari.models;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.GsonSettings;
import com.mojiapps.Shahrdari.webservice.Params;

import java.util.Date;
import java.util.List;

/**
 * Created by mojtaba on 24/10/2014.
 */

@DatabaseTable(tableName = "HomeViolation")
public class HomeViolationPost implements Params {

    @DatabaseField(columnName = ID, generatedId = true, allowGeneratedIdInsert = true)
    private int id;
    @SerializedName(HOME_VIOLATION_ID)
    @DatabaseField(columnName = HOME_VIOLATION_ID, unique = true)
    //شناسه تخلف املاک
    private Integer HomeViolationId;
    @SerializedName(OW_NAME)
    @DatabaseField(columnName = OW_NAME)
    //نام مالک
    private String OwName;
    @SerializedName(REGISTER_DATE)
    @DatabaseField(columnName = REGISTER_DATE, dataType = DataType.DATE_STRING)
    //تاریخ ثبت تخلف
    private String RegisterDate;
    @SerializedName(ADDRESS)
    @DatabaseField(columnName = ADDRESS)
    //آدرس
    private String Address;
    @SerializedName(PERSONNEL_ID)
    @DatabaseField(columnName = PERSONNEL_ID)
    //شناسه پرسنل
    private Integer PersonnelId;
    @SerializedName(DESCRIPTION)
    @DatabaseField(columnName = DESCRIPTION)
    //شرح
    private String Description;

    @SerializedName(VIOLATION_LIST)
    //لیست تخلفات
    private List<Violation> ViolationList;
    @DatabaseField(columnName = VIOLATION_LIST)
    private String ViolationListJson;

    @SerializedName(IMAGE_1)
    @DatabaseField(columnName = IMAGE_1)
    //تصویر تخلف
    private String Image1;
    @SerializedName(IMAGE_2)
    @DatabaseField(columnName = IMAGE_2)
    //تصویر تخلف
    private String Image2;
    @SerializedName(IMAGE_3)
    @DatabaseField(columnName = IMAGE_3)
    //تصویر تخلف
    private String Image3;
    @SerializedName(IMAGE_4)
    @DatabaseField(columnName = IMAGE_4)
    //تصویر تخلف
    private String Image4;

    @SerializedName(VIOLATION_ACT_DESCRIPTION)
    @DatabaseField(columnName = VIOLATION_ACT_DESCRIPTION)
    //شرح اقدام تخلف
    private String ViolationActDescription;

    @DatabaseField(columnName = IS_SYNCABLE)
    private boolean isSyncable = false;

    public HomeViolationPost() {
    }
    public HomeViolationPost(int Id, String OwName,
                            String RegisterDate, String Address,
                             Integer PersonnelId, String Description,
                            List<Violation> ViolationList, String  ViolationListJson,
                             String Image1, String Image2, String Image3, String Image4,
                            String ViolationActDescription) {
        this. id=Id;
        this.OwName=OwName;
        this.RegisterDate=RegisterDate;
        this.Address=Address;
        this.PersonnelId=PersonnelId;
        this.Description=Description;
        this.ViolationList=ViolationList;
        this.ViolationListJson=ViolationListJson;
        this.Image1=Image1;
        this.Image2=Image2;
        this.Image3=Image3;
        this.Image4=Image4;
        this.ViolationActDescription=ViolationActDescription;



    }
    public Integer getHomeViolationId() {
        return (Integer) HomeViolationId;
    }

    public int getId() {
        return id;
    }

    public void setHomeViolationId(Integer homeViolationId) {
        HomeViolationId = homeViolationId;
    }

    public String getOwName() {
        return OwName;
    }

    public void setOwName(String owName) {
        OwName = owName;
    }

    public String getRegisterDate() {
        return RegisterDate;
    }

    public void setRegisterDate(String registerDate) {
        RegisterDate = registerDate;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public Integer getPersonnelId() {
        return PersonnelId;
    }

    public void setPersonnelId(Integer personnelId) {
        PersonnelId = personnelId;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public List<Violation> getViolationList() {
        return ViolationList;
    }

    public void setViolationList(List<Violation> violationList) {
        ViolationList = violationList;
    }

    public void setViolationList(String violationListJson) {
        ViolationList = GsonSettings.getGson().fromJson(violationListJson, new TypeToken<List<Violation>>(){}.getType());
    }

    public String getViolationListJson() {
        return ViolationListJson;
    }

    public void setViolationListJson(String violationListJson) {
        ViolationListJson = violationListJson;
    }

    public void setViolationListJson(List<Violation> violationList) {
        ViolationListJson = GsonSettings.getGson().toJson(violationList).toString();
    }

    public String getImage1() {
        return Image1;
    }

    public void setImage1(String image1) {
        Image1 = image1;
    }

    public String getImage2() {
        return Image2;
    }

    public void setImage2(String image2) {
        Image2 = image2;
    }

    public String getImage3() {
        return Image3;
    }

    public void setImage3(String image3) {
        Image3 = image3;
    }

    public String getImage4() {
        return Image4;
    }

    public void setImage4(String image4) {
        Image4 = image4;
    }

    public String getViolationActDescription() {
        return ViolationActDescription;
    }

    public void setViolationActDescription(String violationActDescription) {
        ViolationActDescription = violationActDescription;
    }

    public boolean isSyncable() {
        return isSyncable;
    }

    public void setSyncable(boolean syncable) {
        isSyncable = syncable;
    }
}
