package com.mojiapps.Shahrdari.models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.webservice.Params;

/**
 * Created by ruhallah-PC on 12/8/2016.
 */

@DatabaseTable(tableName = "Discharge")
public class Discharge implements Params {

    @SerializedName(DISCHARGE_ID)
    @DatabaseField(id = true, columnName = DISCHARGE_ID)
    //شناسه سایت تخلیه پسماند
    private Integer DischargeId;
    @SerializedName(TITLE)
    @DatabaseField(columnName = TITLE)
    //عنوان
    private String Title;

    public Discharge() {

    }

    public Discharge(Integer dischargeId, String title) {
        DischargeId = dischargeId;
        Title = title;
    }

    public Integer getDischargeId() {
        return DischargeId;
    }

    public void setDischargeId(Integer dischargeId) {
        DischargeId = dischargeId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }
}
