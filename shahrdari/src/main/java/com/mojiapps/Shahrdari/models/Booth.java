package com.mojiapps.Shahrdari.models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.webservice.Params;

/**
 * Created by mojtaba on 24/10/2014.
 */
@DatabaseTable(tableName = "Booth")
public class Booth implements Params {

    @SerializedName(Booth_ID )
    @DatabaseField(id = true, columnName = Booth_ID)
    //شناسه غرفه
    private int BoothId ;
    @SerializedName(TITLE)
    @DatabaseField(columnName = TITLE)
    //عنوان غرفه
    private String Title;

    public Booth(int boothId, String title) {
        BoothId = boothId;
        Title = title;
    }

    public Booth() {
    }

    public int getBoothId() {
        return BoothId;
    }

    public void setBoothId(int boothId) {
        BoothId = boothId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

}
