package com.mojiapps.Shahrdari.models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.webservice.Params;

import java.util.Date;

/**
 * Created by mojtaba on 24/10/2014.
 */
@DatabaseTable(tableName = "Contractor")
public class Contractor implements Params {

    @SerializedName(CONTRACTOR_ID)
    @DatabaseField(id = true, columnName = CONTRACTOR_ID)
    //شناسه پیمانکاران
    private int ContractorId;
    @SerializedName(TITLE)
    @DatabaseField(columnName = TITLE)
    //عنوان پیمانکار
    private String Title;
    @SerializedName(OW_NAME)
    @DatabaseField(columnName = OW_NAME)
    //نام مالک
    private String OwName;
    @SerializedName(OW_MOBILE)
    @DatabaseField(columnName = OW_MOBILE)
    //شماره تماس مالک
    private String OwMobile;

    @SerializedName(HAVE_VIOLATION)
    @DatabaseField(columnName = HAVE_VIOLATION)
    private Boolean haveViolation;

    public Contractor() {

    }

    public Contractor(int contractorId, String title, String owName, String owMobile) {
        ContractorId = contractorId;
        Title = title;
        OwName = owName;
        OwMobile = owMobile;
    }

    public int getContractorId() {
        return ContractorId;
    }

    public void setContractorId(int contractorId) {
        ContractorId = contractorId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getOwName() {
        return OwName;
    }

    public void setOwName(String owName) {
        OwName = owName;
    }

    public String getOwMobile() {
        return OwMobile;
    }

    public void setOwMobile(String owMobile) {
        OwMobile = owMobile;
    }

    public Boolean getHaveViolation() {
        return haveViolation;
    }

    public void setHaveViolation(Boolean haveViolation) {
        this.haveViolation = haveViolation;
    }
}
