package com.mojiapps.Shahrdari;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Toast;

public class GPSService extends Service {
	 private final IBinder mBinder = new GPSServiceBinder();
	 LocationManager lm;
	 LocationListener locationlistener;
	@Override
	public IBinder onBind(Intent arg0) {
		return mBinder;
	}
	
    public class GPSServiceBinder extends Binder {
    	public GPSService getService() {
            return GPSService.this;
        }
    }
    
    @Override
    public void onCreate() {
    	super.onCreate();
    	lm = (LocationManager) getSystemService(LOCATION_SERVICE);
    	locationlistener  = new MyLocationListener();
    	lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 900000, 1000, locationlistener);
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
    	lm = (LocationManager) getSystemService(LOCATION_SERVICE);
    	locationlistener  = new MyLocationListener();
    	return START_STICKY;
    }

    private class MyLocationListener implements LocationListener{

		@Override
		public void onLocationChanged(Location location) {
			if(location != null){
				String url = "http://maps.google.com/maps?q=" + Double.toString(location.getLatitude()) + "," + Double.toString(location.getLongitude());
				Toast.makeText(getBaseContext(), url, 10000).show();
//				Intent i = new Intent(Intent.ACTION_VIEW);
//				i.setData(Uri.parse(url));
//				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//				startActivity(i);
			}
		}

		@Override
		public void onProviderDisabled(String provider) {
			
		}

		@Override
		public void onProviderEnabled(String provider) {
			
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			
		}
    	
    }
}