package com.mojiapps.Shahrdari;

import com.google.gson.*;

import java.util.Date;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class GsonSettings {

    private static Gson gson;
    public static final String DATE_TIME_PATTERN = "yyyy/MM/dd HH:mm:ss";
    public static final String DATE_TIME_PATTERN2 = "yyyy/MM/dd HH:mm:ss.SSS";

    public synchronized static Gson getGson() {
        if (gson == null) {
            gson = new GsonBuilder()
                    .setDateFormat(DATE_TIME_PATTERN)
                    .registerTypeAdapter(Date.class, new DateTypeAdapter())
                    .create();
        }
        return gson;
    }

    private static class DateTypeAdapter implements JsonSerializer<Date>, JsonDeserializer<Date> {
        @Override
        public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(new SimpleDateFormat(DATE_TIME_PATTERN, Locale.US).format(src));
        }

        @Override
        public Date deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
            String jsonStr = json.getAsString().replaceAll("T", " ");
            jsonStr = jsonStr.replaceAll("-", "/");
            /*if (!jsonStr.contains("PM") && !jsonStr.contains("AM"))
                jsonStr += " AM";*/
            try {
                return new SimpleDateFormat(DATE_TIME_PATTERN, Locale.US).parse(jsonStr);
            } catch (ParseException e) {
                try {
                    return new SimpleDateFormat(DATE_TIME_PATTERN2, Locale.US).parse(jsonStr);
                } catch (ParseException e1) {
                    e1.printStackTrace();
                    return null;
                }
            }
        }
    }
}