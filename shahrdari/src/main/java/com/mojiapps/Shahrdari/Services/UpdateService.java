package com.mojiapps.Shahrdari.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;


import com.mojiapps.Shahrdari.activity.ActMain;

import java.net.UnknownHostException;

public class UpdateService extends Service {


    private final static int notificationID = 0x45879;
//    private final static int notificationID2 = 0x45878;
//    private final static int notificationID3 = 0x45877;
//    private final static int notificationID4 = 0x45876;

    private static NotificationManager notificationManager;
    public static int FOREGROUND_SERVICE = 101;

    private static Intent myIntent;
    private CountDownTimer countDownTimer;
    private static Boolean isStart = false;
    private static String RequestCode = null;

    public static void setNotificationManager(NotificationManager notificationManager)
    {
        UpdateService.notificationManager = notificationManager;
    }

//    private void TryDataConnection () {
//        //if ()
//        dbAccess = wiFiDBAccess;
//    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        myIntent=intent;
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        countDownTimer = new CountDownTimer(99999999, 20000) {
            @Override
            public void onTick(long l) {
                //.Toast.makeText(this,"Service created at " + time.getTime(), Toast.LENGTH_LONG).show();

                showNotification();
            }

            @Override
            public void onFinish() {
                countDownTimer.start();
            }
        };

        countDownTimer.start();


        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        countDownTimer.cancel();

        super.onDestroy();
        // Cancel the persistent notification.
//		shutdownCounter();
//        nm.cancel(R.string.service_started);
//		Toast.makeText(this, "Service destroyed at " + time.getTime() + "; counter is at: " + counter, Toast.LENGTH_LONG).show();
//		counter=null;
    }

    /**
     * Show a notification while this service is running.
     */
    private void showNotification() {

//        Intent notificationIntent = new Intent(this, MainActivity.class);
//        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
//                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
//                notificationIntent, 0);
//
//        Notification notification = new NotificationCompat.Builder(this)
//                .setTicker(getString(R.string.carnet))
//                .setSmallIcon(R.drawable.driver2)
//                .setContentTitle(getString(R.string.carnet))
//                .setContentText(getString(R.string.active))
//                .setAutoCancel(false)
//                .build();
//
//        startForeground(FOREGROUND_SERVICE,
//                notification);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ActMain mainActivity=new ActMain();
        mainActivity.synk();

        isStart = true;


    }

    public void cancelNotify() {
        countDownTimer.cancel();

        if (isStart && (notificationManager != null)) {
            notificationManager.cancel(notificationID);
            isStart = false;

        }
    }

    @Override
    public boolean stopService(Intent name) {
        //RequestCode=null;
        countDownTimer.cancel();
        return super.stopService(myIntent);
    }

    @Override
    public void unbindService(ServiceConnection conn)
    {
        unbindService(conn);
    }


}
