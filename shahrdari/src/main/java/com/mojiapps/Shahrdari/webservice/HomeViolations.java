package com.mojiapps.Shahrdari.webservice;

import com.mojiapps.Shahrdari.models.HomeViolation;

import java.util.Arrays;
import java.util.List;

/**
 * Created by mojtaba on 01/11/2014.
 */
public class HomeViolations {
    public List<HomeViolation> homeViolations;

    public HomeViolations(HomeViolation[] homeViolations){
        this.homeViolations = Arrays.asList(homeViolations);
    }

    public HomeViolations(HomeViolation homeViolation){
        this.homeViolations = Arrays.asList(homeViolation);
    }

    public HomeViolations(List<HomeViolation> homeViolations){
        this.homeViolations = homeViolations;
    }
}
