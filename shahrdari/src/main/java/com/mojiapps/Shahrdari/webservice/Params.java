package com.mojiapps.Shahrdari.webservice;

/**
 * Created by ruhallah-PC on 12/6/2016.
 */

public interface Params {

    String VIOLATION_ID = "ViolationId";
    String TITLE = "Title";
    String CAR_ID = "CarId";
    String IS_DIGGER = "IsDigger";
    String CERTIFICATE_ID = "CertificateId";
    String HOME_VIOLATION_ID = "HomeViolationId";
    String REGISTER_DATE = "RegisterDate";
    String REGISTER_DATE1 = "RegisterDate1";
    String DESCRIPTION = "Description";
    String VIOLATION_ID_LIST = "ViolationIdList";
    String IMAGE_1 = "Image1";
    String IMAGE_2 = "Image2";
    String IMAGE_3 = "Image3";
    String IMAGE_4 = "Image4";
    String VIOLATION_ACT_DESCRIPTION = "ViolationActDescription";
    String NAME = "Name";
    String USER_NAME = "UserName";
    String PASSWORD = "Password";
    String AREA_ID = "AreaId";
    String IS_ACTIVE = "isActive";
    String ID = "Id";
    String OWNER_NAME = "OwnerName";
    String MOBILE = "Mobile";
    String ADDRESS = "Address";
    String VIOLATION_LIST_STRING = "ViolationListString";
    String DATE = "Date";
    String SEND = "Send";
    String STAFF_CAR_ID = "StaffCarId";
    String COLOR = "Color";
    String PLATE_TYPE = "PlateType";
    String NEW_PLATE_PART_1 = "NewPlatePart1";
    String NEW_PLATE_PART_2 = "NewPlatePart2";
    String NEW_PLATE_PART_3 = "NewPlatePart3";
    String NEW_PLATE_PART_4 = "NewPlatePart4";
    String OLD_PLATE = "OldPlate";
    String PLATE_CODE = "PlateCode";
    String OW_NAME = "OwName";
    String OW_TEL = "OwTel";
    String CONTRACTOR_ID = "ContractorId";
    String OLD_RECORD = "OldRecord";
    String ACT_FROM_DATE = "ActFromDate";
    String ACT_TO_DATE = "ActToDate";
    String HAVE_VIOLATION = "HaveViolation";
    String CAR_OWNER_VIOLATION_ID = "CarOwnerViolationId";
    String PERSONNEL_ID = "PersonnelId";
    String CAR_OWNER_ID = "CarOwnerId";
    String VIOLATION_ADDRESS = "ViolationAddress";
    String REGISTER_DATE_TIME_P = "RegisterDateTimeP";
    String GPS_LOCATION = "GpsLocation";
    String ISSUED_CERTIFICATE_ID = "IssuedCertificateId";
    String VIOLATION_LIST = "ViolationList";
    String PHOTO = "Photo";
    String CAR_VIOLATION_ID = "CarViolationId";
    String DISCHARGE_ID = "DischargeId";
    String STAFF_CAR_VIOLATION_ID = "StaffCarViolationId";
    String OW_MOBILE = "OwMobile";
    String LICENSE_ID = "LicenseId";
    String EMP_NAME = "EmpName";
    String EMP_TEL = "EmpTel";
    String WASTE_TYPE = "WasteType";
    String MODERNIZATION_CODE = "ModernizationCode";
    String FROM_DATE = "FromDate";
    String TO_DATE = "ToDate";
    String ACT_IN_MORNING = "ActInMorning";
    String ACT_IN_EVENING = "ActInEvening";
    String ACT_IN_NIGHT = "ActInNight";
    String LENGTH = "Length";
    String WIDTH = "Width";
    String HEIGHT = "Height";
    String SOIL_M_3 = "SoilM3";
    String RFID_MANUAL_M_3 = "RfidManualM3";
    String RFID_MRFID_M_3 = "RfidM3";
    String RFID_TAGS_COUNT = "RfidTagsCount";
    String CLOSE_LICENSE = "CloseLicense";
    String LICENSE_STOP = "LicenseStop";
    String CONFIRM_NO_VIOLATION_DESCRIPTION1 = "ConfirmNoViolationDescription1";
    String IS_CONFIRM_NO_VIOLATION1 = "IsConfirmNoViolation1";
    String CONFIRM_DESCRIPTION1 = "ConfirmDescription1";
    String IS_CONFIRM_From_Personnel_1 = "IsConfirmFromPersonnel1";
    String CONFIRM_PERSONNEL_ID_1 = "ConfirmPersonnelId1";

    String CONFIRM_NO_VIOLATION_DESCRIPTION2 = "ConfirmNoViolationDescription2";
    String IS_CONFIRM_NO_VIOLATION2 = "IsConfirmNoViolation2";
    String CONFIRM_DESCRIPTION2 = "ConfirmDescription2";
    String IS_CONFIRM_From_Personnel_2 = "IsConfirmFromPersonnel2";
    String CONFIRM_PERSONNEL_ID_2 = "ConfirmPersonnelId2";
    String STAFF_CAR_ID_LIST = "StaffCarIdList";
    String MANUAL_TRANSFER_DESCRIPTION = "ManualTransferDescription";
    String MANUAL_TRANSFER_M_3 = "ManualTransferM3";
    String CONTRACTOR = "Contractor";
    String CAR = "Car";
    String STAFF_CAR = "StaffCar";
    String CERTIFICATE = "Certificate";
    String AREA = "Area";
    String DISCHARGE = "Discharge";
    //TODO: maybe changed
    String STAFF_CAR_LIST = "StaffCarList";
    String FROM = "From";
    String LICENSE = "license";
    String IS_SYNCABLE = "isEdited";
    String DATA = "Data";
    String STATE = "State";
    String MESSAGE = "Message";
    String IS_EDITED = "isEdited";
    String LICENSE_STATE = "LicenseState";
    String LAST_LICENSE_ID = "LastLicenseId";
    String Survey_ID="SurveyId";
    String Booth_ID="BoothId";//غرفه
    String QUESTION="Questions";//سوال
    String Question_ID="QuestionId";//سوال
    String SITES="Sites";//سوال
    String SITE_ID="SiteId";
    String Report_Description="ReportDescription";
    String Follow_Description="FollowDescription";
    String Final_Description ="FinalDescription";
    String Latitude_Location="LatitudeLocation";
    String Longitude_Location ="LongitudeLocation";


}
