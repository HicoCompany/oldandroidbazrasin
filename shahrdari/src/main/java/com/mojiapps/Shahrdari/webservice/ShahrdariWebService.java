package com.mojiapps.Shahrdari.webservice;


import com.mojiapps.Shahrdari.models.Area;
import com.mojiapps.Shahrdari.models.Booth;
import com.mojiapps.Shahrdari.models.Car;
import com.mojiapps.Shahrdari.models.CarViolation;
import com.mojiapps.Shahrdari.models.CarViolationPost;
import com.mojiapps.Shahrdari.models.Certificate;
import com.mojiapps.Shahrdari.models.Contractor;
import com.mojiapps.Shahrdari.models.Discharge;
import com.mojiapps.Shahrdari.models.HomeViolation;
import com.mojiapps.Shahrdari.models.HomeViolationPost;
import com.mojiapps.Shahrdari.models.License;
import com.mojiapps.Shahrdari.models.Personnel;
import com.mojiapps.Shahrdari.models.Pictures;
import com.mojiapps.Shahrdari.models.QuestionEntry;
import com.mojiapps.Shahrdari.models.ResultAdd;
import com.mojiapps.Shahrdari.models.SiteEntry;
import com.mojiapps.Shahrdari.models.StaffCar;
import com.mojiapps.Shahrdari.models.StaffCarViolation;
import com.mojiapps.Shahrdari.models.StaffCarViolationPost;
import com.mojiapps.Shahrdari.models.SurveyEntry;
import com.mojiapps.Shahrdari.models.Violation;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by mojtaba on 9/24/14.
 */
public interface ShahrdariWebService {

//
//    public List<Book> listBooks(@Query("start") String start, @Query("count") String count);


    /////////////////////////////GET SERVICES/////////////////////////////
    @GET("/api/services/LoadCertificates")
    public Call<List<Certificate>> getCertificateTypes1(@Header("mac") String mac);

    @GET("/api/services/LoadCertificatesByTime")
    public Call<List<Certificate>> getCertificateTypesByTime(@Header("mac") String mac,@Query("lastUpdateDate")String lastUpdateDate);

    @GET("/api/services/IssuedCertificateListService")
    public void getCertificateTypes(Callback<List<IssuedCertificate>> callback);

    @GET("/api/services/LoadViolations")
    public Call<List<Violation>> getViolations1(@Header("mac") String mac);

    @GET("/api/services/LoadViolationsByTime")
    public Call<List<Violation>> getViolationsByTime(@Header("mac") String mac,@Query("lastUpdateDate")String lastUpdateDate);

    @GET("/api/services/LoadBoothes")
    public Call<List<Booth>> getBooths1(@Header("mac") String mac);

    @GET("/api/services/LoadBoothesByTime")
    public Call<List<Booth>> getBoothsByTime(@Header("mac") String mac,@Query("lastUpdateDate")String lastUpdateDate);

    @GET("/api/services/LoadQuestions")
    public Call<List<QuestionEntry>> getQuestions1(@Header("mac") String mac);

    @GET("/api/services/LoadQuestionsByTime")
    public Call<List<QuestionEntry>> getQuestionsByTime(@Header("mac") String mac,@Query("lastUpdateDate")String lastUpdateDate);

    @GET("/api/services/LoadSites")
    public Call<List<SiteEntry>> getSites1(@Header("mac") String mac);

    @GET("/api/services/LoadSitesByTime")
    public Call<List<SiteEntry>> getSitesByTime(@Header("mac") String mac,@Query("lastUpdateDate")String lastUpdateDate);

    @POST("/api/services/SaveSurveyQuestions")
    public Call<ResultAdd> addSurvey(@Header("mac") String mac,
            @Body SurveyEntry map);
    @POST("/api/services/SaveSurveySites")
    public Call<ResultAdd> addSurveySite(@Header("mac") String mac,
            @Body SurveyEntry map);
    @GET("/api/services/ViolationListService")
    public void getViolations(Callback<List<Violation>> callback);

    @GET("/api/services/LoadPersonnels")
    public Call<List<Personnel>> getPersonnels1(@Header("mac") String mac);

    @GET("/api/services/LoadPersonnelsByTime")
    public Call<List<Personnel>> getPersonnelsByTime(@Header("mac") String mac,@Query("lastUpdateDate")String lastUpdateDate);

    @GET("/api/services/PersonnelListService")
    public void getPersonnels(Callback<List<Personnel>> callback );

    @GET("/api/services/CarListService")
    public Call<List<Car>> getCarTypes(@Header("mac") String mac);

    @GET("/api/services/CarListService")
    public void getCarTypes(Callback<List<Car>> callback);

    @GET("/api/services/LoadContractors")
    public Call<List<Contractor>> getContractors1(@Header("mac") String mac);

    @GET("/api/services/LoadContractorsByTime")
    public Call<List<Contractor>> getContractorsByTime(@Header("mac") String mac,@Query("lastUpdateDate")String lastUpdateDate);

    @GET("/api/services/ContractorListService")
    public void getContractors(Callback<List<Contractor>> callback);

    @GET("/api/services/LoadCars")
    public Call<List<Car>> getCars1(@Header("mac") String mac);

    @GET("/api/services/LoadCarsByTime")
    public Call<List<Car>> LoadCarsByTime(@Header("mac") String mac,@Query("lastUpdateDate")String lastUpdateDate);

    @GET("/api/services/LoadStaffCars")
    public Call<List<StaffCar>> getStaffCars1(@Header("mac") String mac);

    @GET("/api/services/LoadStaffCarsByTime")
    public Call<List<StaffCar>> getStaffCarsByTime(@Header("mac") String mac,@Query("lastUpdateDate")String lastUpdateDate);

    @GET("/api/services/LoadStaffCarViolations")
    public Call<List<StaffCarViolation>> getStaffCarViolations1(@Header("mac") String mac);

    @GET("/api/services/LoadStaffCarViolationsByTime")
    public Call<List<StaffCarViolation>> getStaffCarViolationsByTime(@Header("mac") String mac,@Query("lastUpdateDate")String lastUpdateDate);

    @GET("/api/services/LoadCarViolations")
    public Call<List<CarViolation>> getCarViolations1(@Header("mac") String mac);

    @GET("/api/services/LoadCarViolationsByTime")
    public Call<List<CarViolation>> getCarViolationsByTime(@Header("mac") String mac,@Query("lastUpdateDate")String lastUpdateDate);

    @GET("/api/services/LoadHomeViolations")
    public Call<List<HomeViolation>> getHomeViolations1(@Header("mac") String mac);

    @GET("/api/services/LoadHomeViolationsByTime")
    public Call<List<HomeViolation>> getHomeViolationsByTime(@Header("mac") String mac,@Query("lastUpdateDate")String lastUpdateDate);

    @POST("/api/services/AddHomeViolation")
    public Call<ResultAdd> addHomeViolations(@Header("mac") String mac,
            @Body HomeViolationPost map);

    @POST("/api/services/AddStaffCarViolation")
    public Call<ResultAdd> addStaffCarViolation(@Header("mac") String mac,
            @Body StaffCarViolationPost map);

    @POST("/api/services/AddCarViolation")
    public Call<ResultAdd> addCarViolation(@Header("mac") String mac,
            @Body CarViolationPost map);
///////////////////////////////


    @FormUrlEncoded
    @POST("/api/services/EditLicenseConfirm")
    public Call<ResultAdd> editLicenseConfirm(@Header("mac") String mac,
            @Field("licenseId") int licenseId,
            @Field("personnelId") int personnelId,
            @Field("isConfirm") boolean isConfirm,
            @Field("confirmDescription") String confirmDescription
            );

    @GET("/api/services/getHomeViolationImages")
    public Call<Pictures> getHomeViolationImages(@Header("mac") String mac,
            @Query("homeViolationId") Integer id);

    @GET("/api/services/getCarViolationImages")
    public Call<Pictures> getCarViolationImages(@Header("mac") String mac,
            @Query("carViolationId") Integer id);

    @GET("/api/services/getStaffCarViolationImages")
    public Call<Pictures> getStaffCarViolationImages(@Header("mac") String mac,
            @Query("staffCarViolationId") Integer id);

    @FormUrlEncoded
    @POST("/api/services/EditLicenseConfirmNoViolation")
    public Call<ResultAdd> editLicenseConfirmNoViolation(@Header("mac") String mac,
            @Field("licenseId") int licenseId,
            @Field("isConfirmNoViolation") boolean isConfirmNoViolation,
            @Field("confirmNoViolationDescription") String confirmNoViolationDescription,
            @Field("personnelId") int personnelId);

    @GET("/api/services/CarOwnerListService")
    public void getCars(Callback<List<CarOwner>> callback);

    @GET("/api/services/LoadLicenses")
    public Call<List<License>> getLicenses(@Header("mac") String mac,@Query("fromDate") String fromDate, @Query("toDate") String toDate);
    @GET("/api/services/LoadLicenses")
    public Call<List<License>> getLicenses1();

    @GET("/api/services/LoadLicensesByTime")
    public Call<List<License>> getLicensesByTime(@Query("lastUpdateDate")String lastUpdateDate);

    @GET("/api/services/LoadLicenses")
    public Call<List<License>> getLicenses1(@Header("mac") String mac);

    @GET("/api/services/LoadLicensesByTime")
    public Call<List<License>> getLicensesByTime(@Header("mac") String mac,@Query("lastUpdatedate")String lastUpdateDate);

    @GET("/api/services/LoadDischarges")
    public Call<List<Discharge>> getDischarges1(@Header("mac") String mac);

    @GET("/api/services/LoadDischargesByTime")
    public Call<List<Discharge>> LoadDischargesByTime(@Header("mac") String mac,@Query("lastUpdateDate")String lastUpdateDate);

    @GET("/api/services/LoadAreas")
    public Call<List<Area>> getAreas1(@Header("mac") String mac);
    @GET("/api/services/LoadAreasByTime")
    public Call<List<Area>> LoadAreasByTime(@Header("mac")String mac,@Query("lastUpdateDate")String lastUpdateDate);
    @GET("/api/services/LicenseListService")
    public void getLicenses(@Header("mac") String mac,@Query("fromDate") String fromDate, @Query("toDate") String toDate, Callback<List<License>> callback);

    /////////////////////////////POST SERVICES/////////////////////////////

    @POST("/api/services/HomeViolationService")
    public Call<WebResult> addViolationEstate(@Header("mac") String mac,@Body HomeViolations homeViolation);
    @POST("/api/services/HomeViolationService")
    public void addViolationEstate(@Header("mac") String mac,@Body HomeViolations homeViolation, Callback<WebResult> callback);

    @POST("/api/services/CarOwnerViolationService")
    public Call<WebResult> addViolationMember(@Header("mac") String mac,@Body CarOwnerViolations carOwnerViolations);

    @POST("/api/services/CarOwnerViolationService")
    public void addViolationMember(@Header("mac") String mac,@Body CarOwnerViolations carOwnerViolations, Callback<WebResult> callback);

    @POST("/api/services/NoMemberCarViolationService")
    public Call<WebResult> addViolationNotMember(@Header("mac") String mac,@Body NoMemberCarViolations noMemberCarViolations);

    @POST("/api/services/NoMemberCarViolationService")
    public void addViolationNotMember(@Header("mac") String mac,@Body NoMemberCarViolations noMemberCarViolations, Callback<WebResult> callback);

    @POST("/api/services/ConfirmLicensService")
    public Call<WebResult> confirmLicense(@Header("mac") String mac,@Body ConfirmLicenses confirmLicense);

    @POST("/api/services/ConfirmLicensService")
    public void confirmLicense(@Header("mac") String mac,@Body ConfirmLicenses confirmLicense, Callback<WebResult> callback);
}
