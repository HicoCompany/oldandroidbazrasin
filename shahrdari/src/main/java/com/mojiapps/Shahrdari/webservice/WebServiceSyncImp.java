package com.mojiapps.Shahrdari.webservice;

import android.util.Log;

import com.mojiapps.Shahrdari.Configuration;
import com.mojiapps.Shahrdari.ShahrdariApp;
import com.mojiapps.Shahrdari.database.CarType;
import com.mojiapps.Shahrdari.database.DatabaseService;
import com.mojiapps.Shahrdari.enums.SharedPrefs;
import com.mojiapps.Shahrdari.models.Area;
import com.mojiapps.Shahrdari.models.Booth;
import com.mojiapps.Shahrdari.models.Car;
import com.mojiapps.Shahrdari.models.CarViolation;
import com.mojiapps.Shahrdari.models.CarViolationPost;
import com.mojiapps.Shahrdari.models.Certificate;
import com.mojiapps.Shahrdari.models.Contractor;
import com.mojiapps.Shahrdari.models.Discharge;
import com.mojiapps.Shahrdari.models.HomeViolation;
import com.mojiapps.Shahrdari.models.HomeViolationPost;
import com.mojiapps.Shahrdari.models.License;
import com.mojiapps.Shahrdari.models.Personnel;
import com.mojiapps.Shahrdari.models.Pictures;
import com.mojiapps.Shahrdari.models.QuestionEntry;
import com.mojiapps.Shahrdari.models.ResultAdd;
import com.mojiapps.Shahrdari.models.SiteEntry;
import com.mojiapps.Shahrdari.models.StaffCar;
import com.mojiapps.Shahrdari.models.StaffCarViolation;
import com.mojiapps.Shahrdari.models.StaffCarViolationPost;
import com.mojiapps.Shahrdari.models.SurveyEntry;
import com.mojiapps.Shahrdari.models.Violation;
import com.mojiapps.Shahrdari.utils.ConnectionDetector;
import com.mojiapps.Shahrdari.utils.CustomDateUtils;
import com.mojiapps.Shahrdari.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mojtaba on 25/10/2014.
 */
public class WebServiceSyncImp {

    private static ShahrdariWebService shahrdariWebService = ShahrdariApp.getShahrdariWebService();
    private static ConnectionDetector cd = new ConnectionDetector(ShahrdariApp.getContext());

    /*public static String updateAll(){
        int totalCount = 12;
        int count = 0;
        if (!cd.isConnectingToInternet()){
            return "NoConnection";
        }

        try {
            List<Car> cars = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getCars();
            for (Car car : cars) {
                DatabaseService.addCar(car);
            }
            count++;
        }
        catch (Exception e){
            e.printStackTrace();
        }

        ///////////////////////////////////////////

        try {
            List<Certificate> certificates = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getCertificateTypes();
            for (Certificate certificate : certificates) {
                DatabaseService.addCertificateType(certificate);
            }
            count++;
        }
        catch (Exception e){
            e.printStackTrace();

        }

        /////////////////////////////////////////////

        try {
            List<Violation> violations = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getViolations();
            for (Violation violation : violations) {
                DatabaseService.addViolation(violation);
            }
            count++;
        }
        catch (Exception e){
            e.printStackTrace();

        }

        //////////////////////////////////////////////////

        try {
            List<Discharge> discharges = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getDischarges();
            for (Discharge discharge : discharges) {
                *//*Calendar calendarFrom = Calendar.getInstance();
                calendarFrom.setTime(new Date(license.getFromDate().substring(0, 10).replace("-", "/")));

                Calendar calendarTo = Calendar.getInstance();
                calendarTo.setTime(new Date(license.getToDate().substring(0, 10).replace("-", "/")));

                DatabaseService.addLicense(new com.mojiapps.Shahrdari.database.License(license.getLicenseId(),
                        license.getEmployerName(), license.getDischarge(), license.getAddress(), calendarFrom.getTime(),
                        calendarTo.getTime(), license.isConfirmLicense(), license.getConfirmDescription(),
                        DatabaseService.getContractorById(license.getContractorId())));*//*
                DatabaseService.addDischarge(discharge);
            }
            count++;
        }
        catch (Exception e){
            e.printStackTrace();

        }

        //////////////////////////////////////////////////

        try {
            List<Area> areas = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getAreas();
            for (Area area : areas) {
                *//*Calendar calendarFrom = Calendar.getInstance();
                calendarFrom.setTime(new Date(license.getFromDate().substring(0, 10).replace("-", "/")));

                Calendar calendarTo = Calendar.getInstance();
                calendarTo.setTime(new Date(license.getToDate().substring(0, 10).replace("-", "/")));

                DatabaseService.addLicense(new com.mojiapps.Shahrdari.database.License(license.getLicenseId(),
                        license.getEmployerName(), license.getDischarge(), license.getAddress(), calendarFrom.getTime(),
                        calendarTo.getTime(), license.isConfirmLicense(), license.getConfirmDescription(),
                        DatabaseService.getContractorById(license.getContractorId())));*//*
                DatabaseService.addArea(area);
            }
            count++;
        }
        catch (Exception e){
            e.printStackTrace();

        }

        //////////////////////////////////////////////

        try {
            List<Personnel> personnels = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getPersonnels();
            for (Personnel personnel : personnels) {
                DatabaseService.addPersonnel(personnel);
                *//*DatabaseService.addOfficer(new Officer(personnel.getUserName(), personnel.getPassword(), personnel.getPersonnelId(),
                        personnel.getName() + " " + personnel.getFamily(), personnel.getImage(), false));*//*
            }
            count++;
        }
        catch (Exception e){
            e.printStackTrace();

        }

        ///////////////////////////////////////////////

        try {
            List<Contractor> contractors = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getContractors();
            for (Contractor contractor : contractors) {
                DatabaseService.addContractor(contractor);
                *//*Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date(contractor.getActivityDate().substring(0, 10).replace("-", "/")));
                DatabaseService.addContractor(new com.mojiapps.Shahrdari.database.ContractorOLD(contractor.getContractorId(),
                        contractor.getTitle(), contractor.getOwnerName(), contractor.getAddress(),
                        calendar.getTime()));*//*
            }
            count++;
        }
        catch (Exception e){
            e.printStackTrace();

        }

        //////////////////////////////////////////////////////

        try {
            List<StaffCar> staffCars = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getStaffCars();
            for (StaffCar staffCar : staffCars) {
                //Calendar calendar = Calendar.getInstance();
                //calendar.setTime(new Date(carOwner.getActivityDate().substring(0, 10).replace("-", "/")));
                DatabaseService.addStaffCar(staffCar);
                        *//*new com.mojiapps.Shahrdari.database.CarOLD(carOwner.getCarOwnerId(), carOwner.getCarTitle(),
                        carOwner.getOwnerName(), carOwner.getNumberPlate(), calendar.getTime(),
                        DatabaseService.getContractorById(carOwner.getContractorId())));*//*
            }
            count++;
        }
        catch (Exception e){
            e.printStackTrace();
        }

        *//*try {
            List<Car> staffCars = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getCars();
            for (Car staffCar : staffCars) {
                //Calendar calendar = Calendar.getInstance();
                //calendar.setTime(new Date(carOwner.getActivityDate().substring(0, 10).replace("-", "/")));
                DatabaseService.addCar(staffCar);
                        *//**//*new com.mojiapps.Shahrdari.database.CarOLD(carOwner.getCarOwnerId(), carOwner.getCarTitle(),
                        carOwner.getOwnerName(), carOwner.getNumberPlate(), calendar.getTime(),
                        DatabaseService.getContractorById(carOwner.getContractorId())));*//**//*
            }
            count++;
        }
        catch (Exception e){
            e.printStackTrace();
        }*//*

        //////////////////////////////////////////////////

        try {
            List<License> licenses = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getLicenses();
            for (License license : licenses) {
                *//*Calendar calendarFrom = Calendar.getInstance();
                calendarFrom.setTime(new Date(license.getFromDate().substring(0, 10).replace("-", "/")));

                Calendar calendarTo = Calendar.getInstance();
                calendarTo.setTime(new Date(license.getToDate().substring(0, 10).replace("-", "/")));

                DatabaseService.addLicense(new com.mojiapps.Shahrdari.database.License(license.getLicenseId(),
                        license.getEmployerName(), license.getDischarge(), license.getAddress(), calendarFrom.getTime(),
                        calendarTo.getTime(), license.isConfirmLicense(), license.getConfirmDescription(),
                        DatabaseService.getContractorById(license.getContractorId())));*//*
                license.setStaffCarIdListJson(license.getStaffCarList());
                DatabaseService.addLicense(license);
            }
            count++;
        }
        catch (Exception e){
            e.printStackTrace();

        }

        //////////////////////////////////////////////////

        try {
            List<StaffCarViolation> staffCarViolations = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getStaffCarViolations();
            for (StaffCarViolation staffCarViolation : staffCarViolations) {
                *//*Calendar calendarFrom = Calendar.getInstance();
                calendarFrom.setTime(new Date(license.getFromDate().substring(0, 10).replace("-", "/")));

                Calendar calendarTo = Calendar.getInstance();
                calendarTo.setTime(new Date(license.getToDate().substring(0, 10).replace("-", "/")));

                DatabaseService.addLicense(new com.mojiapps.Shahrdari.database.License(license.getLicenseId(),
                        license.getEmployerName(), license.getDischarge(), license.getAddress(), calendarFrom.getTime(),
                        calendarTo.getTime(), license.isConfirmLicense(), license.getConfirmDescription(),
                        DatabaseService.getContractorById(license.getContractorId())));*//*
                staffCarViolation.setViolationListJson(staffCarViolation.getViolationList());
                boolean b = DatabaseService.updateStaffCarViolation(staffCarViolation);
                Log.e("updateStaffCarViolation", b+" "+staffCarViolation.getStaffCarViolationId());
            }
            count++;
        }
        catch (Exception e){
            e.printStackTrace();

        }

        //////////////////////////////////////////////////

        try {
            List<CarViolation> carViolations = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getCarViolations();
            for (CarViolation carViolation : carViolations) {
                *//*Calendar calendarFrom = Calendar.getInstance();
                calendarFrom.setTime(new Date(license.getFromDate().substring(0, 10).replace("-", "/")));

                Calendar calendarTo = Calendar.getInstance();
                calendarTo.setTime(new Date(license.getToDate().substring(0, 10).replace("-", "/")));

                DatabaseService.addLicense(new com.mojiapps.Shahrdari.database.License(license.getLicenseId(),
                        license.getEmployerName(), license.getDischarge(), license.getAddress(), calendarFrom.getTime(),
                        calendarTo.getTime(), license.isConfirmLicense(), license.getConfirmDescription(),
                        DatabaseService.getContractorById(license.getContractorId())));*//*
                DatabaseService.updateViolationNotMember(carViolation);
            }
            count++;
        }
        catch (Exception e){
            e.printStackTrace();

        }

        //////////////////////////////////////////////////

        try {
            List<HomeViolation> homeViolations = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getHomeViolations();
            for (HomeViolation homeViolation : homeViolations) {
                *//*Calendar calendarFrom = Calendar.getInstance();
                calendarFrom.setTime(new Date(license.getFromDate().substring(0, 10).replace("-", "/")));

                Calendar calendarTo = Calendar.getInstance();
                calendarTo.setTime(new Date(license.getToDate().substring(0, 10).replace("-", "/")));

                DatabaseService.addLicense(new com.mojiapps.Shahrdari.database.License(license.getLicenseId(),
                        license.getEmployerName(), license.getDischarge(), license.getAddress(), calendarFrom.getTime(),
                        calendarTo.getTime(), license.isConfirmLicense(), license.getConfirmDescription(),
                        DatabaseService.getContractorById(license.getContractorId())));*//*
                DatabaseService.updateHomeViolation(homeViolation);
            }
            count++;
        }
        catch (Exception e){
            e.printStackTrace();

        }

        return Integer.toString(totalCount) + "-" + Integer.toString(count);
    }*/

    public static String updateAll2() {
        String error="";
        int totalCount = 13;
        int count = 0;
        if (!cd.isConnectingToInternet()) {
            return "NoConnection";
        }

//1
        try {
            List<Car> cars = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getCars();

            Configuration.getInstance().setString(SharedPrefs.DATE_CARS, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (Car car : cars) {

                if(DatabaseService.getCarById(car.getCarId())==null) {
                    DatabaseService.addCar(car);
                }
                else
                {
                    DatabaseService.updateCar(car);
                }
            }
            error="1";
            count++;
        } catch (Exception e) {
            error="11";
            e.printStackTrace();


        }

        //////////////////////////////////////////////////
//2
        try {
            List<Discharge> discharges = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getDischarges();

            Configuration.getInstance().setString(SharedPrefs.DATE_DISCHARGES, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (Discharge discharge : discharges) {
                /*Calendar calendarFrom = Calendar.getInstance();
                calendarFrom.setTime(new Date(license.getFromDate().substring(0, 10).replace("-", "/")));

                Calendar calendarTo = Calendar.getInstance();
                calendarTo.setTime(new Date(license.getToDate().substring(0, 10).replace("-", "/")));

                DatabaseService.addLicense(new com.mojiapps.Shahrdari.database.License(license.getLicenseId(),
                        license.getEmployerName(), license.getDischarge(), license.getAddress(), calendarFrom.getTime(),
                        calendarTo.getTime(), license.isConfirmLicense(), license.getConfirmDescription(),
                        DatabaseService.getContractorById(license.getContractorId())));*/
                if(DatabaseService.getDischargeById(discharge.getDischargeId())==null) {
                    DatabaseService.addDischarge(discharge);
                }
                else {
                    DatabaseService.UpdateDischarge(discharge);
                }
            }
            error +="%2";
            count++;
        } catch (Exception e) {
            error +="%22";
            e.printStackTrace();

        }

        ///////////////////////////////////////////
//3
        try {
            List<Certificate> certificates = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getCertificateTypes();

            Configuration.getInstance().setString(SharedPrefs.DATE_CERTIFICATES, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (Certificate certificate : certificates) {

                if(DatabaseService.getCertificateById(certificate.getCertificateId())==null) {
                    DatabaseService.addCertificateType(certificate);
                }
                else {
                    DatabaseService.UpdateCertificate(certificate);
                }
            }
            count++;
            error +="%3";
        } catch (Exception e) {
            e.printStackTrace();
            error +="%33";

        }

        /////////////////////////////////////////////
//4
        try {
            List<Violation> violations = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getViolations();

            Configuration.getInstance().setString(SharedPrefs.DATE_VIOLATIONS, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (Violation violation : violations) {
                if(DatabaseService.getViolationById(violation.getViolationId())==null) {
                    DatabaseService.addViolation(violation);
                }
                else {
                    DatabaseService.UpdateViolation(violation);
                }
            }
            count++;
            error +="%4";
        } catch (Exception e) {
            e.printStackTrace();
            error +="%44";

        }

        //////////////////////////////////////////////
//5
        try {
            List<Personnel> personnels = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getPersonnels();

            Configuration.getInstance().setString(SharedPrefs.DATE_PERSONNELS, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (Personnel personnel : personnels) {
                personnel.setAreaListJson(personnel.getAreaList());

                if(DatabaseService.getPersonnelById(personnel.getPersonnelId())==null) {
                    DatabaseService.addPersonnel(personnel);
                }
                else
                {
                    DatabaseService.UpdatePersonnels(personnel);
                }
                /*DatabaseService.addOfficer(new Officer(personnel.getUserName(), personnel.getPassword(), personnel.getPersonnelId(),
                        personnel.getName() + " " + personnel.getFamily(), personnel.getImage(), false));*/
            }
            count++;
            error +="%5";
        } catch (Exception e) {
            e.printStackTrace();
            error +="%55";

        }

        ///////////////////////////////////////////////
//6
        try {
            List<Contractor> contractors = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getContractors();

            Configuration.getInstance().setString(SharedPrefs.DATE_CONTRACTORS, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (Contractor contractor : contractors) {
                if(DatabaseService.getContractorById(contractor.getContractorId())==null) {
                    DatabaseService.addContractor(contractor);
                }
                else
                {
                    DatabaseService.UpdateContract(contractor);
                }
                /*Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date(contractor.getActivityDate().substring(0, 10).replace("-", "/")));
                DatabaseService.addContractor(new com.mojiapps.Shahrdari.database.ContractorOLD(contractor.getContractorId(),
                        contractor.getTitle(), contractor.getOwnerName(), contractor.getAddress(),
                        calendar.getTime()));*/
            }
            count++;
            error +="%6";
        } catch (Exception e) {
            e.printStackTrace();
            error +="%66";

        }

        //////////////////////////////////////////////////////
//7
        try {
            List<StaffCar> staffCars = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getStaffCars();

            Configuration.getInstance().setString(SharedPrefs.DATE_STAFF_CARS, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (StaffCar staffCar : staffCars) {
                //Calendar calendar = Calendar.getInstance();
                //calendar.setTime(new Date(carOwner.getActivityDate().substring(0, 10).replace("-", "/")));
                if(DatabaseService.getStaffCarById(staffCar.getStaffCarId())==null) {
                    DatabaseService.addStaffCar(staffCar);
                }
                else
                {
                    DatabaseService.UpdateStaffCar(staffCar);
                }

                        /*new com.mojiapps.Shahrdari.database.CarOLD(carOwner.getCarOwnerId(), carOwner.getCarTitle(),
                        carOwner.getOwnerName(), carOwner.getNumberPlate(), calendar.getTime(),
                        DatabaseService.getContractorById(carOwner.getContractorId())));*/
            }
            count++;
            error +="%7";
        } catch (Exception e) {
            e.printStackTrace();
            error +="%77";
        }

        /*try {
            List<Car> staffCars = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getCars();
            for (Car staffCar : staffCars) {
                //Calendar calendar = Calendar.getInstance();
                //calendar.setTime(new Date(carOwner.getActivityDate().substring(0, 10).replace("-", "/")));
                DatabaseService.addCar(staffCar);
                        *//*new com.mojiapps.Shahrdari.database.CarOLD(carOwner.getCarOwnerId(), carOwner.getCarTitle(),
                        carOwner.getOwnerName(), carOwner.getNumberPlate(), calendar.getTime(),
                        DatabaseService.getContractorById(carOwner.getContractorId())));*//*
            }
            count++;
        }
        catch (Exception e){
            e.printStackTrace();
        }*/

        //////////////////////////////////////////////////
//8
        try {
            List<Area> areas = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getAreas();

            Configuration.getInstance().setString(SharedPrefs.DATE_AREA, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (Area area : areas) {
                /*Calendar calendarFrom = Calendar.getInstance();
                calendarFrom.setTime(new Date(license.getFromDate().substring(0, 10).replace("-", "/")));

                Calendar calendarTo = Calendar.getInstance();
                calendarTo.setTime(new Date(license.getToDate().substring(0, 10).replace("-", "/")));

                DatabaseService.addLicense(new com.mojiapps.Shahrdari.database.License(license.getLicenseId(),
                        license.getEmployerName(), license.getDischarge(), license.getAddress(), calendarFrom.getTime(),
                        calendarTo.getTime(), license.isConfirmLicense(), license.getConfirmDescription(),
                        DatabaseService.getContractorById(license.getContractorId())));*/
                if(DatabaseService.getAreaById(area.getAreaId())==null) {
                    DatabaseService.addArea(area);
                }
                else {
                    DatabaseService.UpdateArea(area);
                }
            }
            count++;
            error +="%8";
        } catch (Exception e) {
            error +="%88";
            e.printStackTrace();

        }



        ////////////////////////////////////////////////////
//9
        try {
            List<License> licenses = DatabaseService.getLicensesSyncable();
            for (License license : licenses) {
                ResultAdd resultAdd
                        = ServiceWrapper.getInstance(ShahrdariApp.getContext()).editLicenseConfirm(
                        license.getLicenseId(), license.getConfirm1(), license.getConfirmDescription1()
                        , Configuration.getInstance().getInt(SharedPrefs.ID));
                if (resultAdd != null
                        && Integer.valueOf(resultAdd.getState()) == 1) {
                    license.setEdited(false);
                    Log.e("sync", "License " + license.getLicenseId());
                    DatabaseService.addLicense(license);
                }
            }
            count++;
            error +="%9";
        } catch (Exception e) {
            error +="%99";
            e.printStackTrace();
        }
        /////////////////////////////////////////////////////////////
//        try {
//            List<License> licenses = DatabaseService.getLicensesSyncable();
//            for (License license : licenses) {
//                ResultAdd resultAdd
//                        = ServiceWrapper.getInstance(ShahrdariApp.getContext()).editLicenseConfirm2(
//                        license.getLicenseId(),license.getConfirm2(),license.getConfirmDescription2()
//                        , Configuration.getInstance().getInt(SharedPrefs.ID));
//                if (resultAdd != null
//                        && Integer.valueOf(resultAdd.getState()) == 1) {
//                    license.setEdited(false);
//                    Log.e("sync","License "+license.getLicenseId());
//                    DatabaseService.addLicense(license);
//                }
//            }
//            count++;
//        }
//        catch (Exception e){
//            e.printStackTrace();
//        }

//
        //region violation comment
//        try {
//            List<HomeViolation> homeViolations = DatabaseService.getViolationEstateSyncable();
//            for (HomeViolation homeViolation : homeViolations) {
//
//                if (homeViolation.getImage1() != null &&
//                        homeViolation.getImage1().length() == 0)
//                    homeViolation.setImage1(null);
//                if (homeViolation.getImage2() != null &&
//                        homeViolation.getImage2().length() == 0)
//                    homeViolation.setImage2(null);
//                if (homeViolation.getImage3() != null &&
//                        homeViolation.getImage3().length() == 0)
//                    homeViolation.setImage3(null);
//                if (homeViolation.getImage4() != null &&
//                        homeViolation.getImage4().length() == 0)
//                    homeViolation.setImage4(null);
//
//                HomeViolationPost homeViolationPost = new HomeViolationPost(homeViolation.getId(), homeViolation.getOwName()
//                        , CustomDateUtils.MiladiToPersianDateTime(homeViolation.getRegisterDate()), homeViolation.getAddress(),
//                        homeViolation.getPersonnelId(), homeViolation.getDescription(),
//                        homeViolation.getViolationList(), homeViolation.getViolationListJson(), homeViolation.getImage1(), homeViolation.getImage2(), homeViolation.getImage3(), homeViolation.getImage4(),
//                        homeViolation.getViolationActDescription());
//
//                ResultAdd resultAdd
//                        = ServiceWrapper.getInstance(ShahrdariApp.getContext()).addHomeViolations(homeViolationPost);
//                if (resultAdd != null
//                        && Integer.valueOf(resultAdd.getState()) == 1) {
//                    homeViolation.setHomeViolationId(Integer.valueOf(resultAdd.getData()));
//                    homeViolation.setSyncable(false);
//                    boolean b = DatabaseService.updateHomeViolation(homeViolation);
//                    if (b)
//                        Log.e("updateHomeViolation", b + "");
//                }
//            }
//            count++;
//            error +="%1";
//        } catch (Exception e) {
//            error +="%0";
//            e.printStackTrace();
//        }
//
//        ////////////////////////////////////////////////////
////11
//        try {
//            List<StaffCarViolation> staffCarViolations = DatabaseService.getStaffCarViolationSyncable();
//            for (StaffCarViolation staffCarViolation : staffCarViolations) {
//                if (staffCarViolation.getImage1() != null &&
//                        staffCarViolation.getImage1().length() == 0)
//                    staffCarViolation.setImage1(null);
//                if (staffCarViolation.getImage2() != null &&
//                        staffCarViolation.getImage2().length() == 0)
//                    staffCarViolation.setImage2(null);
//                if (staffCarViolation.getImage3() != null &&
//                        staffCarViolation.getImage3().length() == 0)
//                    staffCarViolation.setImage3(null);
//                if (staffCarViolation.getImage4() != null &&
//                        staffCarViolation.getImage4().length() == 0)
//                    staffCarViolation.setImage4(null);
//                StaffCarViolationPost staffCarViolationPost = new StaffCarViolationPost(staffCarViolation.getId(), staffCarViolation.getStaffCar()
//                        , CustomDateUtils.MiladiToPersianDateTime(staffCarViolation.getRegisterDate()), staffCarViolation.getAddress(),
//                        staffCarViolation.getCertificate(), staffCarViolation.getPersonnelId(), staffCarViolation.getDescription(),
//                        staffCarViolation.getViolationList(), staffCarViolation.getViolationListJson(), staffCarViolation.getImage1(), staffCarViolation.getImage2(), staffCarViolation.getImage3(), staffCarViolation.getImage4(),
//                        staffCarViolation.getViolationActDescription());
//                ResultAdd resultAdd
//                        = ServiceWrapper.getInstance(ShahrdariApp.getContext()).addStaffCarViolation(staffCarViolationPost);
//                if (resultAdd != null
//                        && Integer.valueOf(resultAdd.getState()) == 1) {
//                    staffCarViolation.setStaffCarViolationId(Integer.valueOf(resultAdd.getData()));
//                    staffCarViolation.setSyncable(false);
//                    boolean b = DatabaseService.updateStaffCarViolation(staffCarViolation);
//                    if (b)
//                        Log.e("updateStaffCarViolation", b + "");
//                }
//            }
//            count++;
//            error +="%1";
//        } catch (Exception e) {
//            error +="%0";
//            e.printStackTrace();
//        }
//
//        ////////////////////////////////////////////////////
////12
//        try {
//            List<CarViolation> carViolations = DatabaseService.getCarViolationSyncable();
//            for (CarViolation carViolation : carViolations) {
//                if (carViolation.getImage1() != null &&
//                        carViolation.getImage1().length() == 0)
//                    carViolation.setImage1(null);
//                if (carViolation.getImage2() != null &&
//                        carViolation.getImage2().length() == 0)
//                    carViolation.setImage2(null);
//                if (carViolation.getImage3() != null &&
//                        carViolation.getImage3().length() == 0)
//                    carViolation.setImage3(null);
//                if (carViolation.getImage4() != null &&
//                        carViolation.getImage4().length() == 0)
//                    carViolation.setImage4(null);
//
//                CarViolationPost carViolationt = new CarViolationPost(carViolation.getId(), carViolation.getOwName(),
//                        carViolation.getOwTel(), CustomDateUtils.MiladiToPersianDateTime(carViolation.getRegisterDate()), carViolation.getCar(), carViolation.getAddress(),
//                        carViolation.getColor(), carViolation.getPlateType(), carViolation.getNewPlatePart1(), carViolation.getNewPlatePart2(),
//                        carViolation.getNewPlatePart3(), carViolation.getNewPlatePart4(), carViolation.getOldPlate(), carViolation.getPlateCode(),
//                        carViolation.getCertificate(), carViolation.getPersonnelId(), carViolation.getDescription(),
//                        carViolation.getViolationList(), carViolation.getViolationListJson(), carViolation.getImage1(), carViolation.getImage2(), carViolation.getImage3(), carViolation.getImage4(),
//                        carViolation.getViolationActDescription());
//
//                ResultAdd resultAdd
//                        = ServiceWrapper.getInstance(ShahrdariApp.getContext()).addCarViolation(carViolationt);
//                if (resultAdd != null
//                        && Integer.valueOf(resultAdd.getState()) == 1) {
//                    carViolation.setCarViolationId(Integer.valueOf(resultAdd.getData()));
//                    carViolation.setSyncable(false);
//                    boolean b = DatabaseService.updateViolationNotMember(carViolation);
//                    if (b)
//                        Log.e("updateViolationNotMem", b + "");
//                }
//            }
//            count++;
//            error +="%1";
//        } catch (Exception e) {
//            e.printStackTrace();
//            error +="%0";
//        }
//        //////////////////////////////////////////////////
//        //endregion
////13
        try {
            List<License> licenses = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getLicenses();

            Configuration.getInstance().setString(SharedPrefs.DATE_LICENSES, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (License license : licenses) {
                /*Calendar calendarFrom = Calendar.getInstance();
                calendarFrom.setTime(new Date(license.getFromDate().substring(0, 10).replace("-", "/")));

                Calendar calendarTo = Calendar.getInstance();
                calendarTo.setTime(new Date(license.getToDate().substring(0, 10).replace("-", "/")));

                DatabaseService.addLicense(new com.mojiapps.Shahrdari.database.License(license.getLicenseId(),
                        license.getEmployerName(), license.getDischarge(), license.getAddress(), calendarFrom.getTime(),
                        calendarTo.getTime(), license.isConfirmLicense(), license.getConfirmDescription(),
                        DatabaseService.getContractorById(license.getContractorId())));*/
                license.setStaffCarIdListJson(license.getStaffCarList());
                license.setStaffCarList(license.getStaffCarList());
                license.getLicenseId();
                if(DatabaseService.getLicenseById(license.getLicenseId())==null) {
                    DatabaseService.addLicense(license);
                }
                else
                {
                    DatabaseService.updateLicense(license);
                }
            }
            count++;
            error +="%l";
        } catch (Exception e) {
            e.printStackTrace();
            error +="%ll";

        }

        //////////////////////////////////////////////////
//14
        try {
            List<StaffCarViolation> staffCarViolations = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getStaffCarViolations();

            Configuration.getInstance().setString(SharedPrefs.DATE_STAFF_CAR_VIOLATIONS, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (StaffCarViolation staffCarViolation : staffCarViolations) {
                /*Calendar calendarFrom = Calendar.getInstance();
                calendarFrom.setTime(new Date(license.getFromDate().substring(0, 10).replace("-", "/")));

                Calendar calendarTo = Calendar.getInstance();
                calendarTo.setTime(new Date(license.getToDate().substring(0, 10).replace("-", "/")));

                DatabaseService.addLicense(new com.mojiapps.Shahrdari.database.License(license.getLicenseId(),
                        license.getEmployerName(), license.getDischarge(), license.getAddress(), calendarFrom.getTime(),
                        calendarTo.getTime(), license.isConfirmLicense(), license.getConfirmDescription(),
                        DatabaseService.getContractorById(license.getContractorId())));*/
                staffCarViolation.setViolationListJson(staffCarViolation.getViolationList());
                boolean b = DatabaseService.updateStaffCarViolation(staffCarViolation);
                Log.e("updateStaffCarViolation", b + " " + staffCarViolation.getStaffCarViolationId());
            }
            count++;
            error +="%s";
        } catch (Exception e) {
            e.printStackTrace();
            error +="%ss";

        }

        //////////////////////////////////////////////////
//15
        try {
            List<CarViolation> carViolations = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getCarViolations();

            Configuration.getInstance().setString(SharedPrefs.DATE_CAR_VIOLATIONS, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (CarViolation carViolation : carViolations) {
                /*Calendar calendarFrom = Calendar.getInstance();
                calendarFrom.setTime(new Date(license.getFromDate().substring(0, 10).replace("-", "/")));

                Calendar calendarTo = Calendar.getInstance();
                calendarTo.setTime(new Date(license.getToDate().substring(0, 10).replace("-", "/")));

                DatabaseService.addLicense(new com.mojiapps.Shahrdari.database.License(license.getLicenseId(),
                        license.getEmployerName(), license.getDischarge(), license.getAddress(), calendarFrom.getTime(),
                        calendarTo.getTime(), license.isConfirmLicense(), license.getConfirmDescription(),
                        DatabaseService.getContractorById(license.getContractorId())));*/
             //   DatabaseService.updateViolationNotMember(carViolation);

                carViolation.setViolationListJson(carViolation.getViolationList());
                boolean b = DatabaseService.updateViolationNotMember(carViolation);
                Log.e("updateCarViolation", b + " " + carViolation.getCarViolationId());

            }
            count++;
            error +="%c";
        } catch (Exception e) {
            e.printStackTrace();
            error +="%cc";

        }

        //////////////////////////////////////////////////
//16
        try {
            List<HomeViolation> homeViolations = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getHomeViolations();

            Configuration.getInstance().setString(SharedPrefs.DATE_HOME_VIOLATIONS, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (HomeViolation homeViolation : homeViolations) {
                /*Calendar calendarFrom = Calendar.getInstance();
                calendarFrom.setTime(new Date(license.getFromDate().substring(0, 10).replace("-", "/")));

                Calendar calendarTo = Calendar.getInstance();
                calendarTo.setTime(new Date(license.getToDate().substring(0, 10).replace("-", "/")));

                DatabaseService.addLicense(new com.mojiapps.Shahrdari.database.License(license.getLicenseId(),
                        license.getEmployerName(), license.getDischarge(), license.getAddress(), calendarFrom.getTime(),
                        calendarTo.getTime(), license.isConfirmLicense(), license.getConfirmDescription(),
                        DatabaseService.getContractorById(license.getContractorId())));*/

                homeViolation.setViolationListJson(homeViolation.getViolationList());
                DatabaseService.updateHomeViolation(homeViolation);
            }
            count++;
            error +="%h";
        } catch (Exception e) {
            e.printStackTrace();
            error +="%hh";
        }

        ////////////////////////////////////////////////////


        return Integer.toString(count)+"-"+error;
    }

    public static ResultAdd editLicenseConfirmNoViolation(int licenseId, boolean isConfirmNoViolation, String confirmNoViolationDescription, int personnelId)
    {
        try {
            ResultAdd aBoolean = ServiceWrapper.getInstance(ShahrdariApp.getContext()).editLicenseConfirmNoViolation(licenseId, isConfirmNoViolation, confirmNoViolationDescription, personnelId);
            return aBoolean;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ResultAdd editLicenseConfirm(int licenseId, boolean isConfirmNoViolation, String confirmNoViolationDescription, int personnelId) {
        try {
            ResultAdd aBoolean = ServiceWrapper.getInstance(ShahrdariApp.getContext()).editLicenseConfirm(licenseId, isConfirmNoViolation, confirmNoViolationDescription, personnelId);
            return aBoolean;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ResultAdd addHomeViolation(HomeViolation homeViolation) {
        try {
            if (homeViolation.getImage1() != null &&
                    homeViolation.getImage1().length() == 0)
                homeViolation.setImage1(null);
            if (homeViolation.getImage2() != null &&
                    homeViolation.getImage2().length() == 0)
                homeViolation.setImage2(null);
            if (homeViolation.getImage3() != null &&
                    homeViolation.getImage3().length() == 0)
                homeViolation.setImage3(null);
            if (homeViolation.getImage4() != null &&
                    homeViolation.getImage4().length() == 0)
                homeViolation.setImage4(null);

            HomeViolationPost homeViolationPost = new HomeViolationPost(homeViolation.getId(), homeViolation.getOwName()
                    , CustomDateUtils.MiladiToPersianDateTime(homeViolation.getRegisterDate()), homeViolation.getAddress(),
                    homeViolation.getPersonnelId(), homeViolation.getDescription(),
                    homeViolation.getViolationList(), homeViolation.getViolationListJson(), homeViolation.getImage1(), homeViolation.getImage2(), homeViolation.getImage3(), homeViolation.getImage4(),
                    homeViolation.getViolationActDescription());

            ResultAdd resultAdd
                    = ServiceWrapper.getInstance(ShahrdariApp.getContext()).addHomeViolations(homeViolationPost);
            if (resultAdd != null
                    && Integer.valueOf(resultAdd.getState()) == 1) {
                homeViolation.setHomeViolationId(Integer.valueOf(resultAdd.getData()));
                homeViolation.setSyncable(false);
                boolean b = DatabaseService.updateHomeViolation(homeViolation);
                if (b)
                    Log.e("updateHomeViolation", b + "");
            }
            return resultAdd;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ResultAdd addStaffCarViolation(StaffCarViolation staffCarViolation) {
        try {
            if (staffCarViolation.getImage1() != null &&
                    staffCarViolation.getImage1().length() == 0)
                staffCarViolation.setImage1(null);
            if (staffCarViolation.getImage2() != null &&
                    staffCarViolation.getImage2().length() == 0)
                staffCarViolation.setImage2(null);
            if (staffCarViolation.getImage3() != null &&
                    staffCarViolation.getImage3().length() == 0)
                staffCarViolation.setImage3(null);
            if (staffCarViolation.getImage4() != null &&
                    staffCarViolation.getImage4().length() == 0)
                staffCarViolation.setImage4(null);
            StaffCarViolationPost staffCarViolationPost = new StaffCarViolationPost(staffCarViolation.getId(), staffCarViolation.getStaffCar()
                    , CustomDateUtils.MiladiToPersianDateTime(staffCarViolation.getRegisterDate()), staffCarViolation.getAddress(),
                    staffCarViolation.getCertificate(), staffCarViolation.getPersonnelId(), staffCarViolation.getDescription(),
                    staffCarViolation.getViolationList(), staffCarViolation.getViolationListJson(), staffCarViolation.getImage1(), staffCarViolation.getImage2(), staffCarViolation.getImage3(), staffCarViolation.getImage4(),
                    staffCarViolation.getViolationActDescription());
            ResultAdd resultAdd
                    = ServiceWrapper.getInstance(ShahrdariApp.getContext()).addStaffCarViolation(staffCarViolationPost);
            if (resultAdd != null
                    && Integer.valueOf(resultAdd.getState()) == 1) {
                staffCarViolation.setStaffCarViolationId(Integer.valueOf(resultAdd.getData()));
                staffCarViolation.setSyncable(false);
                boolean b = DatabaseService.updateStaffCarViolation(staffCarViolation);
                if (b)
                    Log.e("updateStaffCarViolation", b + "");
            }
            return resultAdd;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static ResultAdd addCarViolation(CarViolation carViolation) {
        if (carViolation.getImage1() != null &&
                carViolation.getImage1().length() == 0)
            carViolation.setImage1(null);
        if (carViolation.getImage2() != null &&
                carViolation.getImage2().length() == 0)
            carViolation.setImage2(null);
        if (carViolation.getImage3() != null &&
                carViolation.getImage3().length() == 0)
            carViolation.setImage3(null);
        if (carViolation.getImage4() != null &&
                carViolation.getImage4().length() == 0)
            carViolation.setImage4(null);

        CarViolationPost carViolationt = new CarViolationPost(carViolation.getId(), carViolation.getOwName(),
                carViolation.getOwTel(), CustomDateUtils.MiladiToPersianDateTime(carViolation.getRegisterDate()), carViolation.getCar(), carViolation.getAddress(),
                carViolation.getColor(), carViolation.getPlateType(), carViolation.getNewPlatePart1(), carViolation.getNewPlatePart2(),
                carViolation.getNewPlatePart3(), carViolation.getNewPlatePart4(), carViolation.getOldPlate(), carViolation.getPlateCode(),
                carViolation.getCertificate(), carViolation.getPersonnelId(), carViolation.getDescription(),
                carViolation.getViolationList(), carViolation.getViolationListJson(), carViolation.getImage1(), carViolation.getImage2(), carViolation.getImage3(), carViolation.getImage4(),
                carViolation.getViolationActDescription());

        /////////////
        try {
            ResultAdd resultAdd
                    = ServiceWrapper.getInstance(ShahrdariApp.getContext()).addCarViolation(carViolationt);
            if (resultAdd != null
                    && Integer.valueOf(resultAdd.getState()) == 1) {
                carViolation.setCarViolationId(Integer.valueOf(resultAdd.getData()));
                carViolation.setSyncable(false);
                boolean b = DatabaseService.updateViolationNotMember(carViolation);
                if (b)
                    Log.e("updateViolationNotMem", b + "");
            }
            /////

            return resultAdd;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public static Pictures getHomeViolationImages(int id) {
        Pictures pictures=null;
        try {
             pictures = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getHomeViolationImages(id);
            return pictures;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Pictures getCarViolationImages(int id) {
        try {
            Pictures pictures = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getCarViolationImages(id);
            return pictures;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Pictures getStaffCarViolationImages(int id) {
        try {
            Pictures pictures = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getStaffCarViolationImages(id);
            return pictures;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean updateCarType() {
        try {
            List<com.mojiapps.Shahrdari.models.Car> cars = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getCars();
            for (com.mojiapps.Shahrdari.models.Car car : cars) {
                DatabaseService.addCarType(new CarType(car.getCarId(), car.getTitle()));
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean updateCertificateType() {
        try {
            List<Certificate> issuedCertificates = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getCertificateTypes();

            Configuration.getInstance().setString(SharedPrefs.DATE_CERTIFICATES, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (Certificate issuedCertificate : issuedCertificates) {
                if(DatabaseService.getCertificateById(issuedCertificate.getCertificateId())==null) {
                    DatabaseService.addCertificateType(issuedCertificate);
                }
                else
                {
                    DatabaseService.UpdateCertificate(issuedCertificate);
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean updateViolation() {
        try {
            List<Violation> violations = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getViolations();

            Configuration.getInstance().setString(SharedPrefs.DATE_VIOLATIONS, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (Violation violation : violations) {
                if (DatabaseService.getViolationById(violation.getViolationId()) == null)
                {
                    DatabaseService.addViolation(new Violation(violation.getViolationId(), violation.getTitle()));
            }
            else {
                    DatabaseService.UpdateViolation(new Violation(violation.getViolationId(), violation.getTitle()));
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean updatePersonnels() {
        try {
            List<Personnel> personnels = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getPersonnels();

            Configuration.getInstance().setString(SharedPrefs.DATE_PERSONNELS, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (Personnel personnel : personnels) {
                /*DatabaseService.addOfficer(new Officer(personnel.getUserName(), personnel.getPassword(), personnel.getPersonnelId(),
                        personnel.getName() + " " + personnel.getFamily(), personnel.getImage(), false));*/
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean updateContractor() {
        try {
            List<Contractor> contractors = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getContractors();
            contractors = new ArrayList<>();
            for (int i = 0; i < 20; i++) {
                Contractor con = new Contractor();
                con.setContractorId(i);
                con.setOwMobile("09191534180");
                con.setOwName("علی زارعی");
                con.setTitle("روح اله احمدیان");
                contractors.add(con);
            }


            Configuration.getInstance().setString(SharedPrefs.DATE_CONTRACTORS, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (Contractor contractor : contractors) {
                if(DatabaseService.getContractorById(contractor.getContractorId())==null) {
                    DatabaseService.addContractor(contractor);
                }
                else
                {
                    DatabaseService.UpdateContract(contractor);
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean updateStaffCars() {
        try {
            List<StaffCar> staffCars = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getStaffCars();

            Configuration.getInstance().setString(SharedPrefs.DATE_STAFF_CARS, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (StaffCar staffCar : staffCars) {
                if(DatabaseService.getStaffCarById(staffCar.getStaffCarId())==null) {
                    DatabaseService.addStaffCar(staffCar);
                }
                else {
                    DatabaseService.UpdateStaffCar(staffCar);
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean editLicenseConfirmNoViolation(String startDate, String endDate) {
        try {
            List<com.mojiapps.Shahrdari.models.License> licenses = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getLicenses();
            //startDate, endDate);
            Configuration.getInstance().setString(SharedPrefs.DATE_LICENSES, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (com.mojiapps.Shahrdari.models.License license : licenses) {
                /*Calendar calendarFrom = Calendar.getInstance();
                calendarFrom.setTime(new Date(license.getFromDate().substring(0, 10).replace("-", "/")));

                Calendar calendarTo = Calendar.getInstance();
                calendarTo.setTime(new Date(license.getToDate().substring(0, 10).replace("-", "/")));

                DatabaseService.addLicense(new com.mojiapps.Shahrdari.database.License(license.getLicenseId(),
                        license.getEmployerName(), license.getDischarge(), license.getAddress(), calendarFrom.getTime(),
                        calendarTo.getTime(), license.isConfirmLicense(), license.getConfirmDescription(),
                        DatabaseService.getContractorById(license.getContractorId())));*/
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean confirmLicense() {
        try {
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static List<Booth> getBooth() {
        List<Booth> Booths = null;
        try {
            Booths = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getBooths();

            Configuration.getInstance().setString(SharedPrefs.DATE_BOOTHES, Utils.getCurrentGaregoryDate1().replace("/","-"));

//            for (Booth booth : Booths) {
//                DatabaseService.addBooth(booth);
//            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return Booths;
    }


    public static List<SiteEntry> getSites() {
        List<SiteEntry> sites = null;
        try {
            sites = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getSites();

            Configuration.getInstance().setString(SharedPrefs.DATE_SITES, Utils.getCurrentGaregoryDate1().replace("/","-"));


//            for (Booth booth : Booths) {
//                DatabaseService.addBooth(booth);
//            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return sites;
    }


    public static List<QuestionEntry> getQuestions() {
        List<QuestionEntry> questionEntries = null;
        try {
            questionEntries = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getQuestions();

            Configuration.getInstance().setString(SharedPrefs.DATE_QUESTIONS, Utils.getCurrentGaregoryDate1().replace("/","-"));


//            for (Booth booth : Booths) {
//                DatabaseService.addBooth(booth);
//            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return questionEntries;
    }

    public static String SaveSurveySite(SurveyEntry survey) {
        try {
            ResultAdd resultAdd
                    = ServiceWrapper.getInstance(ShahrdariApp.getContext()).addSurveySite(survey);
//                if (resultAdd != null
//                        && Integer.valueOf(resultAdd.getState()) == 1) {
//                    survey.setStaffCarViolationId(Integer.valueOf(resultAdd.getData()));
//                    staffCarViolation.setSyncable(false);
//                    boolean b = DatabaseService.updateStaffCarViolation(staffCarViolation);
//                    if (b)
//                        Log.e("updateStaffCarViolation", b + "");


        } catch (Exception e) {
            e.printStackTrace();
        }

        return "true";
    }

    public static String SaveSurvey(SurveyEntry survey) {
        try {
            ResultAdd resultAdd
                    = ServiceWrapper.getInstance(ShahrdariApp.getContext()).addSurvey(survey);
//                if (resultAdd != null
//                        && Integer.valueOf(resultAdd.getState()) == 1) {
//                    survey.setStaffCarViolationId(Integer.valueOf(resultAdd.getData()));
//                    staffCarViolation.setSyncable(false);
//                    boolean b = DatabaseService.updateStaffCarViolation(staffCarViolation);
//                    if (b)
//                        Log.e("updateStaffCarViolation", b + "");


        } catch (Exception e) {
            e.printStackTrace();
        }

        return "true";
    }


    public static String getPersonnelForLogin() {

        List<Personnel> personnels=null;
        try

        {
            personnels = ServiceWrapper.getInstance(ShahrdariApp.getContext()).getPersonnels();

            Configuration.getInstance().setString(SharedPrefs.DATE_PERSONNELS, Utils.getCurrentGaregoryDate1().replace("/","-"));

            for (Personnel personnel : personnels) {
                personnel.setAreaListJson(personnel.getAreaList());
                if(DatabaseService.getPersonnelById(personnel.getPersonnelId())==null) {
                    DatabaseService.addPersonnel(personnel);
                }else
                {
                    DatabaseService.UpdatePersonnels(personnel);
                }
                /*DatabaseService.addOfficer(new Officer(personnel.getUserName(), personnel.getPassword(), personnel.getPersonnelId(),
                        personnel.getName() + " " + personnel.getFamily(), personnel.getImage(), false));*/
            }
            // count++;
        } catch (Exception e)

        {
            e.printStackTrace();

        }
        return "";
    }



}