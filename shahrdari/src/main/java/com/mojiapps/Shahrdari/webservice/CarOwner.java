package com.mojiapps.Shahrdari.webservice;

import java.util.Date;

/**
 * Created by mojtaba on 24/10/2014.
 */
public class CarOwner {
    private int CarOwnerId;
    private int ContractorId;
    private String OwnerName;
    private String CarTitle;
    private String NumberPlate;
    private String ActivityDate;

    public int getCarOwnerId() {
        return CarOwnerId;
    }

    public int getContractorId() {
        return ContractorId;
    }

    public String getOwnerName() {
        return OwnerName;
    }

    public String getCarTitle() {
        return CarTitle;
    }

    public String getNumberPlate() {
        return NumberPlate;
    }

    public String getActivityDate() {
        return ActivityDate;
    }
}
