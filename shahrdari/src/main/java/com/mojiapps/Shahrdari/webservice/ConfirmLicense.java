package com.mojiapps.Shahrdari.webservice;

/**
 * Created by mojtaba on 24/10/2014.
 */
public class ConfirmLicense {

    private int TourPersonnelId;
    private int LicenseId;
    private boolean ConfirmLicense;
    private String ConfirmDescription;

    public ConfirmLicense(int tourPersonnelId, int licenseId, boolean confirmLicense, String confirmDescription){
        this.TourPersonnelId = tourPersonnelId;
        this.LicenseId = licenseId;
        this.ConfirmLicense = confirmLicense;
        this.ConfirmDescription = confirmDescription;
    }

    public void setTourPersonnelId(int tourPersonnelId) {
        TourPersonnelId = tourPersonnelId;
    }

    public void setConfirmLicense(boolean confirmLicense) {
        ConfirmLicense = confirmLicense;
    }

    public void setConfirmDescription(String confirmDescription) {
        ConfirmDescription = confirmDescription;
    }

    public void setLicenseId(int licenseId) {
        LicenseId = licenseId;
    }
}
