package com.mojiapps.Shahrdari.webservice;

import java.util.Arrays;
import java.util.List;

/**
 * Created by mojtaba on 02/11/2014.
 */
public class NoMemberCarViolations {

    public List<NoMemberCarViolation> noMemberCarViolations;

    public NoMemberCarViolations(NoMemberCarViolation[] noMemberCarViolations){
        this.noMemberCarViolations = Arrays.asList(noMemberCarViolations);
    }

    public NoMemberCarViolations(NoMemberCarViolation noMemberCarViolation){
        this.noMemberCarViolations = Arrays.asList(noMemberCarViolation);
    }

    public NoMemberCarViolations(List<NoMemberCarViolation> noMemberCarViolations){
        this.noMemberCarViolations = noMemberCarViolations;
    }

}
