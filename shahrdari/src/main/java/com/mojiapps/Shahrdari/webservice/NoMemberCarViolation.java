package com.mojiapps.Shahrdari.webservice;

/**
 * Created by mojtaba on 24/10/2014.
 */
public class NoMemberCarViolation {
    private int NoMemberCarViolationId;
    private int PersonnelId;
    private int CarId;
    private boolean PlateType;
    private String NumberPlate;
    private String Color;
    private String OwnerName;
    private String OwnerMobile;
    private String OwnerAddress;
    private String ViolationAddress;
    private String RegisterDateTimeP;
    private String GpsLocation;
    private int IssuedCertificateId;
    private String ViolationList;
    private String Photo;

    public NoMemberCarViolation(int noMemberCarViolationId, int personnelId, int carId, boolean plateType, String numberPlate, String color, String ownerName, String ownerMobile, String ownerAddress, String violationAddress, String registerDateTime, String gpsLocation, int issuedCertificateId, String violationList, String photo) {
        NoMemberCarViolationId = noMemberCarViolationId;
        PersonnelId = personnelId;
        CarId = carId;
        PlateType = plateType;
        NumberPlate = numberPlate;
        Color = color;
        OwnerName = ownerName;
        OwnerMobile = ownerMobile;
        OwnerAddress = ownerAddress;
        ViolationAddress = violationAddress;
        RegisterDateTimeP = registerDateTime;
        GpsLocation = gpsLocation;
        IssuedCertificateId = issuedCertificateId;
        ViolationList = violationList;
        Photo = photo;
    }

    public void setNoMemberCarViolationId(int noMemberCarViolationId) {
        NoMemberCarViolationId = noMemberCarViolationId;
    }

    public void setPersonnelId(int personnelId) {
        PersonnelId = personnelId;
    }

    public void setCarId(int carId) {
        CarId = carId;
    }

    public void setPlateType(boolean plateType) {
        PlateType = plateType;
    }

    public void setNumberPlate(String numberPlate) {
        NumberPlate = numberPlate;
    }

    public void setColor(String color) {
        Color = color;
    }

    public void setOwnerName(String ownerName) {
        OwnerName = ownerName;
    }

    public void setOwnerMobile(String ownerMobile) {
        OwnerMobile = ownerMobile;
    }

    public void setOwnerAddress(String ownerAddress) {
        OwnerAddress = ownerAddress;
    }

    public void setViolationAddress(String violationAddress) {
        ViolationAddress = violationAddress;
    }

    public void setRegisterDateTime(String registerDateTime) {
        RegisterDateTimeP = registerDateTime;
    }

    public void setGpsLocation(String gpsLocation) {
        GpsLocation = gpsLocation;
    }

    public void setIssuedCertificateId(int issuedCertificateId) {
        IssuedCertificateId = issuedCertificateId;
    }

    public void setViolationList(String violationList) {
        ViolationList = violationList;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }
}
