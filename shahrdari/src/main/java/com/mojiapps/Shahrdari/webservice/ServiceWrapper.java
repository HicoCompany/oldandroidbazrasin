package com.mojiapps.Shahrdari.webservice;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.mojiapps.Shahrdari.Configuration;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.ShahrdariApp;
import com.mojiapps.Shahrdari.WebClient;
import com.mojiapps.Shahrdari.enums.SharedPrefs;
import com.mojiapps.Shahrdari.models.Area;
import com.mojiapps.Shahrdari.models.Booth;
import com.mojiapps.Shahrdari.models.Car;
import com.mojiapps.Shahrdari.models.CarViolation;
import com.mojiapps.Shahrdari.models.CarViolationPost;
import com.mojiapps.Shahrdari.models.Certificate;
import com.mojiapps.Shahrdari.models.Contractor;
import com.mojiapps.Shahrdari.models.Discharge;
import com.mojiapps.Shahrdari.models.HomeViolation;
import com.mojiapps.Shahrdari.models.HomeViolationPost;
import com.mojiapps.Shahrdari.models.License;
import com.mojiapps.Shahrdari.models.Personnel;
import com.mojiapps.Shahrdari.models.Pictures;
import com.mojiapps.Shahrdari.models.QuestionEntry;
import com.mojiapps.Shahrdari.models.ResultAdd;
import com.mojiapps.Shahrdari.models.SiteEntry;
import com.mojiapps.Shahrdari.models.StaffCar;
import com.mojiapps.Shahrdari.models.StaffCarViolation;
import com.mojiapps.Shahrdari.models.StaffCarViolationPost;
import com.mojiapps.Shahrdari.models.SurveyEntry;
import com.mojiapps.Shahrdari.models.Violation;
import com.mojiapps.Shahrdari.utils.Utils;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Query;

/**
 * Created by ruhallah-PC on 10/28/2016.
 */

public class ServiceWrapper {

    private static ServiceWrapper singleton;
    private Context context;

    private ServiceWrapper(Context context) {
        this.context = context;
    }



    public static ServiceWrapper getInstance(Context context) {
        if (singleton == null)
            singleton = new ServiceWrapper(context);
        return singleton;
    }

    public List<Certificate> getCertificateTypes() {
        List<Certificate> result = null;

        String date=Utils.GetDateTime1(SharedPrefs.DATE_CERTIFICATES);

        Call<List<Certificate>> serviceResult = ShahrdariApp.getShahrdariWebService().getCertificateTypesByTime( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),date);
        try {
            Response<List<Certificate>> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public void getCertificateTypes(List<IssuedCertificate> callback) {

    }

    public List<Violation> getViolations() {
        List<Violation> result = null;

        String date=Utils.GetDateTime1(SharedPrefs.DATE_VIOLATIONS);

        Call<List<Violation>> serviceResult = ShahrdariApp.getShahrdariWebService().getViolationsByTime( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),date);
        try {
            Response<List<Violation>> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public List<Booth> getBooths() {
        List<Booth> result = null;
        String date=Utils.GetDateTime1(SharedPrefs.DATE_BOOTHES);
        Call<List<Booth>> serviceResult = ShahrdariApp.getShahrdariWebService().getBoothsByTime( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),date);
        try {
            Response<List<Booth>> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public List<QuestionEntry> getQuestions() {
        List<QuestionEntry> result = null;

        String date=Utils.GetDateTime1(SharedPrefs.DATE_QUESTIONS);

        Call<List<QuestionEntry>> serviceResult = ShahrdariApp.getShahrdariWebService().getQuestionsByTime( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),date);
        try {
            Response<List<QuestionEntry>> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public List<SiteEntry> getSites() {
        List<SiteEntry> result = null;

        String date=Utils.GetDateTime1(SharedPrefs.DATE_SITES);

        Call<List<SiteEntry>> serviceResult = ShahrdariApp.getShahrdariWebService().getSitesByTime( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),date);
        try {
            Response<List<SiteEntry>> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }
    public void getViolations(Callback<List<Violation>> callback) {

    }

    public List<Personnel> getPersonnels() {
        List<Personnel> result = null;

        String date=Utils.GetDateTime1(SharedPrefs.DATE_PERSONNELS);

        Call<List<Personnel>> serviceResult = ShahrdariApp.getShahrdariWebService().getPersonnelsByTime( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),date);
        //Call<List<Personnel>> serviceResult = ShahrdariApp.getShahrdariWebService().getPersonnels();
        try {
            Response<List<Personnel>> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public void getPersonnels(Callback<List<Personnel>> callback) {

    }

    public List<Car> getCars() {
        List<Car> result = null;

        String date=Utils.GetDateTime1(SharedPrefs.DATE_CARS);

        Call<List<Car>> serviceResult = ShahrdariApp.getShahrdariWebService().LoadCarsByTime( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),date);
        try {
            Response<List<Car>> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }
/*
    public void getCars(Callback<List<Car>> callback) {

    }*/

    public List<Contractor> getContractors() {
        List<Contractor> result = null;

        String date=Utils.GetDateTime1(SharedPrefs.DATE_CONTRACTORS);

        Call<List<Contractor>> serviceResult = ShahrdariApp.getShahrdariWebService().getContractorsByTime( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),date);
        try {
            Response<List<Contractor>> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public void getContractors(Callback<List<Contractor>> callback) {

    }

    /*public List<Car> getCars() {
        List<Car> result = null;
        Call<List<Car>> serviceResult = ShahrdariApp.getShahrdariWebService().getCars();
        try {
            Response<List<Car>> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        result = new ArrayList<>();
        for (int i=0;i<20;i++) {
            Car car = new Car();
            car.setCarId(i);
            car.setTitle("ماشین "+i);
            car.setDigger(false);
            result.add(car);
        }
        return result;
    }*/

    public List<StaffCar> getStaffCars() {
        List<StaffCar> result = null;

        String date=Utils.GetDateTime1(SharedPrefs.DATE_STAFF_CARS);

        Call<List<StaffCar>> serviceResult = ShahrdariApp.getShahrdariWebService().getStaffCarsByTime( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),date);
        try {
            Response<List<StaffCar>> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public List<StaffCarViolation> getStaffCarViolations() {
        List<StaffCarViolation> result = null;

        String date=Utils.GetDateTime1(SharedPrefs.DATE_STAFF_CAR_VIOLATIONS);

        Call<List<StaffCarViolation>> serviceResult = ShahrdariApp.getShahrdariWebService().getStaffCarViolationsByTime( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),date);
        try {
            Response<List<StaffCarViolation>> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public List<CarViolation> getCarViolations() {
        List<CarViolation> result = null;

        String date=Utils.GetDateTime1(SharedPrefs.DATE_CAR_VIOLATIONS);

        Call<List<CarViolation>> serviceResult = ShahrdariApp.getShahrdariWebService().getCarViolationsByTime( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),date);
        try {
            Response<List<CarViolation>> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public List<HomeViolation> getHomeViolations() {
        List<HomeViolation> result = null;

        String date=Utils.GetDateTime1(SharedPrefs.DATE_HOME_VIOLATIONS);

        Call<List<HomeViolation>> serviceResult = ShahrdariApp.getShahrdariWebService().getHomeViolationsByTime( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),date);
        try {
            Response<List<HomeViolation>> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public ResultAdd addHomeViolations(HomeViolationPost s) {
        ResultAdd result = null;
        try {
            Call<ResultAdd> serviceResult = ShahrdariApp.getShahrdariWebService().addHomeViolations( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),s);
            Response<ResultAdd> response = serviceResult.execute();
            result = response.body();
            Log.e("addHomeViolations", result.getMessage());
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public ResultAdd addStaffCarViolation(StaffCarViolationPost s) {
        ResultAdd result = null;
        Call<ResultAdd> serviceResult = ShahrdariApp.getShahrdariWebService().addStaffCarViolation( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),s);
        try {
            Response<ResultAdd> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public ResultAdd addSurveySite(SurveyEntry s) {
        ResultAdd result = null;
        Call<ResultAdd> serviceResult = ShahrdariApp.getShahrdariWebService().addSurveySite( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),s);
        try {
            Response<ResultAdd> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }
    public ResultAdd addSurvey(SurveyEntry s) {
        ResultAdd result = null;
        Call<ResultAdd> serviceResult = ShahrdariApp.getShahrdariWebService().addSurvey( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),s);
        try {
            Response<ResultAdd> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public ResultAdd addCarViolation(CarViolationPost s) {
        ResultAdd result = null;
        Call<ResultAdd> serviceResult = ShahrdariApp.getShahrdariWebService().addCarViolation( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),s);
        try {
            Response<ResultAdd> response = serviceResult.execute();
            result = response.body();

        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public ResultAdd editLicenseConfirmNoViolation(int licenseId, boolean isConfirmNoViolation, String confirmNoViolationDescription, int personnelId) {
        ResultAdd result = null;
        Call<ResultAdd> serviceResult = ShahrdariApp.getShahrdariWebService().editLicenseConfirmNoViolation( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),licenseId, isConfirmNoViolation, confirmNoViolationDescription, personnelId);
        try {
            Response<ResultAdd> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            result = new ResultAdd(context.getString(R.string.un_sucess));
        }
        return result;
    }

    public ResultAdd editLicenseConfirm(int licenseId, boolean isConfirmNoViolation
            , String confirmNoViolationDescription, int personnelId) {
        ResultAdd result = null;
        try {
            Call<ResultAdd> serviceResult = ShahrdariApp.getShahrdariWebService().editLicenseConfirm( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),licenseId,personnelId, isConfirmNoViolation, confirmNoViolationDescription);
            Response<ResultAdd> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            result = new ResultAdd(exceptionHandling(e));
        }
        return result;
    }



    public Pictures getHomeViolationImages(int id) {
        Pictures result = null;
        try {
            Call<Pictures> serviceResult = ShahrdariApp.getShahrdariWebService().getHomeViolationImages( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),id);
            Response<Pictures> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public Pictures getCarViolationImages(int id) {
        Pictures result = null;
        try {
            Call<Pictures> serviceResult = ShahrdariApp.getShahrdariWebService().getCarViolationImages( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),id);
            Response<Pictures> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public Pictures getStaffCarViolationImages(int id) {
        Pictures result = null;
        try {
            Call<Pictures> serviceResult = ShahrdariApp.getShahrdariWebService().getStaffCarViolationImages( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),id);
            Response<Pictures> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public void getCars(Callback<List<CarOwner>> callback) {

    }

    public List<License> getLicenses() {
        List<License> result = null;

        String date=Utils.GetDateTime1(SharedPrefs.DATE_LICENSES);

       Call<List<License>> serviceResult = ShahrdariApp.getShahrdariWebService().getLicensesByTime(Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),"2019-07-11 10:46:00");

      // Call<List<License>> serviceResult = ShahrdariApp.getShahrdariWebService().getLicenses();
        try {
            Response<List<License>> response = serviceResult.execute();
            int a=response.code();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public List<Discharge> getDischarges() {
        List<Discharge> result = null;

        String date=Utils.GetDateTime1(SharedPrefs.DATE_DISCHARGES);

        Call<List<Discharge>> serviceResult = ShahrdariApp.getShahrdariWebService().LoadDischargesByTime( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),date);
        try {
            Response<List<Discharge>> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public List<Area> getAreas() {
        List<Area> result = null;

        String date=Utils.GetDateTime1(SharedPrefs.DATE_AREA);

        Call<List<Area>> serviceResult = ShahrdariApp.getShahrdariWebService().LoadAreasByTime( Configuration.getInstance().getString(SharedPrefs.MAC_ADDRESS),date);
        try {
            Response<List<Area>> response = serviceResult.execute();
            result = response.body();
        } catch (Exception e) {
            exceptionHandling(e);
        }
        return result;
    }

    public void getLicenses(@Query("fromDate") String fromDate, @Query("toDate") String toDate, Callback<List<License>> callback) {

    }

    public void addViolationEstate(@Body HomeViolations homeViolation) {

    }

    public void addViolationEstate(@Body HomeViolations homeViolation, Callback<WebResult> callback) {

    }

    public void addViolationMember(@Body CarOwnerViolations carOwnerViolations) {

    }

    public void addViolationMember(@Body CarOwnerViolations carOwnerViolations, Callback<WebResult> callback) {

    }

    public void addViolationNotMember(@Body NoMemberCarViolations noMemberCarViolations) {

    }

    public void addViolationNotMember(@Body NoMemberCarViolations noMemberCarViolations, Callback<WebResult> callback) {

    }

    public void confirmLicense(@Body ConfirmLicenses confirmLicense) {

    }

    public void confirmLicense(@Body ConfirmLicenses confirmLicense, Callback<WebResult> callback) {

    }

    /*private void exceptionHandling(Exception e) {
        if (!WebClient.isNetworkAvailable()) {
            Log.e("NetworkAvailable", "false");
            //showMassage(R.string.message_network_unavailable);
        } else if (e instanceof ConnectException) {
            Log.e("ConnectException", e.toString());
            //showMassage(R.string.message_network_connect);
        } else if (e instanceof SocketTimeoutException) {
            Log.e("SocketTimeoutException", e.toString());
            //showMassage(R.string.message_network_socket_timeout);
        } else if (e instanceof UnknownHostException) {
            Log.e("UnknownHostException", e.toString());
            //showMassage(R.string.message_network_unknown_host);
        } else if (e instanceof IOException) {
            Log.e("IOException", e.toString());
            //showMassage(R.string.message_network_io);
        } else {
            Log.e("Exception", e.toString());
            //showMassage(R.string.message_network_exception);
        }
    }*/

    private static void showMassage(final int textId) {
        new Thread(new Runnable() {
            public Handler mHandler;
            @Override
            public void run() {
                Looper.prepare();
                mHandler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        Toast.makeText(ShahrdariApp.getContext(), textId ,Toast.LENGTH_LONG).show();
                    }
                };
                mHandler.handleMessage(new Message());
                Looper.loop();
            }
        }).start();
    }

    @NonNull
    private String exceptionHandling(Exception e) {
        if (!WebClient.isNetworkAvailable()) {
            return ShahrdariApp.getContext().getResources().getString(R.string.message_network_unavailable);
        } else if (e instanceof ConnectException) {
            return ShahrdariApp.getContext().getResources().getString(R.string.message_network_connect);
        } else if (e instanceof SocketTimeoutException) {
            return ShahrdariApp.getContext().getResources().getString(R.string.message_network_socket_timeout);
        } else if (e instanceof UnknownHostException) {
            return ShahrdariApp.getContext().getResources().getString(R.string.message_network_unknown_host);
        } else if (e instanceof IOException) {
            return ShahrdariApp.getContext().getResources().getString(R.string.message_network_io);
        } else {
            return ShahrdariApp.getContext().getResources().getString(R.string.message_network_exception);
        }
    }
}
