package com.mojiapps.Shahrdari.webservice;

import java.util.Arrays;
import java.util.List;

/**
 * Created by mojtaba on 02/11/2014.
 */
public class ConfirmLicenses {

    public List<ConfirmLicense> confirmLicenses;

    public ConfirmLicenses(ConfirmLicense[] confirmLicenses){
        this.confirmLicenses = Arrays.asList(confirmLicenses);
    }

    public ConfirmLicenses(ConfirmLicense confirmLicense){
        this.confirmLicenses = Arrays.asList(confirmLicense);
    }

    public ConfirmLicenses(List<ConfirmLicense> confirmLicenses){
        this.confirmLicenses = confirmLicenses;
    }

}
