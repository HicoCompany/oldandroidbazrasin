package com.mojiapps.Shahrdari.webservice;

/**
 * Created by mojtaba on 23/10/2014.
 */
public class IssuedCertificate {

    private int IssuedCertificateId;
    private String Title;

    public int getIssuedCertificateId() {
        return IssuedCertificateId;
    }

    public String getTitle() {
        return Title;
    }
}
