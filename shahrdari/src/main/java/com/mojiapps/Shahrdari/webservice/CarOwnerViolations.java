package com.mojiapps.Shahrdari.webservice;

import com.mojiapps.Shahrdari.models.CarViolation;

import java.util.Arrays;
import java.util.List;

/**
 * Created by mojtaba on 02/11/2014.
 */
public class CarOwnerViolations {
    public List<CarViolation> carOwnerViolations;

    public CarOwnerViolations(CarViolation[] carOwnerViolations){
        this.carOwnerViolations = Arrays.asList(carOwnerViolations);
    }

    public CarOwnerViolations(CarViolation carOwnerViolation){
        this.carOwnerViolations = Arrays.asList(carOwnerViolation);
    }

    public CarOwnerViolations(List<CarViolation> carOwnerViolations){
        this.carOwnerViolations = carOwnerViolations;
    }
}
