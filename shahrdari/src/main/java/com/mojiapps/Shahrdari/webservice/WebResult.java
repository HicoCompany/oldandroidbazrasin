package com.mojiapps.Shahrdari.webservice;

/**
 * Created by mojtaba on 02/11/2014.
 */
public class WebResult {
    private boolean Data;
    private int State;
    private String Message;
    private WebException Exception;
    private String ExtraData;

    //{"Data":true,"State":1,"Message":"","Exception":null,"ExtraData":null}
    public boolean isData() {
        return Data;
    }

    public int getState() {
        return State;
    }

    public String getMessage() {
        return Message;
    }

    public WebException getException() {
        return Exception;
    }

    public String getExtraData() {
        return ExtraData;
    }

    public class WebException{
        private String Message;
        private String InnerException;
        private String StackTrace;
        private String Source;

        public String getMessage() {
            return Message;
        }

        public String getInnerException() {
            return InnerException;
        }

        public String getStackTrace() {
            return StackTrace;
        }

        public String getSource() {
            return Source;
        }
    }
}
