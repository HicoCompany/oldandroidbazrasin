package com.mojiapps.Shahrdari.webservice;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import com.mojiapps.Shahrdari.R;

/**
 * Created by mojtaba on 25/10/2014.
 */
public class BaseUpdateAsyncTask extends AsyncTask {

    ProgressDialog progressDialog;
    public BaseUpdateAsyncTask(Context context){
        progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            progressDialog.setProgressNumberFormat("");
            progressDialog.setProgressPercentFormat(null);
        }
        progressDialog.setMessage(context.getResources().getString(R.string.fetching_data_from_server));
        progressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.show();
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
    }

    @Override
    protected Object doInBackground(Object[] params) {
        return null;
    }
}
