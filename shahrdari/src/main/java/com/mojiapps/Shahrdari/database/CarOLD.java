package com.mojiapps.Shahrdari.database;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.models.License;

import java.util.Date;

/**
 * Created by mojtaba on 9/18/14.
 */

@DatabaseTable
public class CarOLD {

    public static final String FIELD_CAR_ID = "CarId";
    public static final String FIELD_TITLE = "TITLE";
    public static final String FIELD_PLATE = "Plate";
    public static final String FIELD_DATE_ACTIVITY = "DateActivity";
    public static final String FIELD_OWNER_NAME = "OwnerName";
    public static final String FIELD_CONTRACTOR_ID = "ContractorId";
    public static final String FIELD_LICENSE_ID = "LicenseId";

    @DatabaseField(id = true, columnName = FIELD_CAR_ID)
    private int carId;

    @DatabaseField(columnName = FIELD_TITLE)
    private String type;

    @DatabaseField(columnName = FIELD_OWNER_NAME)
    private String ownerName;

    @DatabaseField(columnName = FIELD_PLATE)
    private String plate;

    @DatabaseField(dataType = DataType.DATE_STRING, columnName = FIELD_DATE_ACTIVITY)
    private Date dateActivity;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = FIELD_CONTRACTOR_ID)
    private ContractorOLD contractorOLD;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = FIELD_LICENSE_ID)
    private License license;

    CarOLD(){}

    public CarOLD(int carId, String type, String owner, String plate, Date dateActivity, ContractorOLD contractorOLD/*, License license*/) {
        this.carId = carId;
        this.type = type;
        this.ownerName = owner;
        this.plate = plate;
        this.dateActivity = dateActivity;
        this.contractorOLD = contractorOLD;
//        this.license = license;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public Date getDateActivity() {
        return dateActivity;
    }

    public void setDateActivity(Date dateActivity) {
        this.dateActivity = dateActivity;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public ContractorOLD getContractorOLD() {
        return contractorOLD;
    }

    public void setContractorOLD(ContractorOLD contractorOLD) {
        this.contractorOLD = contractorOLD;
    }

/*    public License getLicense() {
        return license;
    }

    public void setLicense(License license) {
        this.license = license;
    }*/
}
