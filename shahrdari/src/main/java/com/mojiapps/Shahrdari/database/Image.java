package com.mojiapps.Shahrdari.database;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.enums.ImageType;

/**
 * Created by mojtaba on 10/9/14.
 */

@DatabaseTable(tableName = "Image")
public class Image {

    public static final String FIELD_ID = "Id";
    public static final String FIELD_TYPE = "Type";
    public static final String FIELD_PHOTO = "Photo";

    @DatabaseField(id = true, columnName = FIELD_ID)
    private int id;

    @DatabaseField(dataType = DataType.ENUM_INTEGER, columnName = FIELD_TYPE)
    private ImageType imageType;

    @DatabaseField(columnName = FIELD_PHOTO)
    private String photo;

    Image(){}

    public Image(int id, ImageType imageType, String photo) {
        this.id = id;
        this.imageType = imageType;
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ImageType getImageType() {
        return imageType;
    }

    public void setImageType(ImageType imageType) {
        this.imageType = imageType;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
