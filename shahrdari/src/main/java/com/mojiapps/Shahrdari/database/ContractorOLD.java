package com.mojiapps.Shahrdari.database;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by mojtaba on 9/26/14.
 */

@DatabaseTable
public class ContractorOLD {

    private static final String FIELD_CONTRACTOR_ID = "ContractorId";
    private static final String FIELD_NAME = "Name";
    private static final String FIELD_MANAGER = "Manager";
    private static final String FIELD_ADDRESS = "Address";
    private static final String FIELD_DATE_ACTIVITY = "DateActivity";

    @DatabaseField(id = true, columnName = FIELD_CONTRACTOR_ID)
    private int contractorId;

    @DatabaseField(columnName = FIELD_NAME)
    private String name;

    @DatabaseField(columnName = FIELD_MANAGER)
    private String manager;

    @DatabaseField(columnName = FIELD_ADDRESS)
    private String address;

    @DatabaseField(dataType = DataType.DATE_STRING, columnName = FIELD_DATE_ACTIVITY)
    private Date dateActivity;

/*
    @ForeignCollectionField(eager = true)
    private ForeignCollection<CarOLD> cars;
*/

/*
    @ForeignCollectionField(eager = true)
    private ForeignCollection<License> licenses;
*/


    ContractorOLD(){}

    public ContractorOLD(int contractorId, String name, String manager, String address, Date dateActivity) {
        this.contractorId = contractorId;
        this.name = name;
        this.manager = manager;
        this.address = address;
        this.dateActivity = dateActivity;
    }

    public int getContractorId() {
        return contractorId;
    }

    public void setContractorId(int contractorId) {
        this.contractorId = contractorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDateActivity() {
        return dateActivity;
    }

    public void setDateActivity(Date dateActivity) {
        this.dateActivity = dateActivity;
    }

/*    public ForeignCollection<CarOLD> getCars() {
        return cars;
    }*/

/*    public ForeignCollection<License> getLicenses() {
        return licenses;
    }*/
}
