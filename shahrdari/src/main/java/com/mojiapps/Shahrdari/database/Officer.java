package com.mojiapps.Shahrdari.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by mojtaba on 9/18/14.
 */

@DatabaseTable(tableName = "Officer")
public class Officer {

    public static final String FIELD_USER_NAME = "UserName";
    public static final String FIELD_PASSWORD = "Password";
    public static final String FIELD_OFFICER_ID = "OfficerId";
    public static final String FIELD_NAME = "Name";
    public static final String FIELD_PHOTO = "PhotoFile";
    public static final String FIELD_LOCK = "Lock";

    @DatabaseField (unique = true, columnName = FIELD_USER_NAME)
    private String userName;

    @DatabaseField(columnName = FIELD_PASSWORD)
    private String password;

    @DatabaseField(id = true, columnName = FIELD_OFFICER_ID)
    private int officerId;

    @DatabaseField(columnName = FIELD_NAME)
    private String name;

    @DatabaseField(columnName = FIELD_PHOTO)
    private String photoFile;

    @DatabaseField(columnName = FIELD_LOCK)
    private boolean lock;

    Officer(){}

    public Officer(String userName, String password, int officerId, String name, String photoFile, boolean lock) {
        this.userName = userName;
        this.password = password;
        this.officerId = officerId;
        this.name = name;
        this.photoFile = photoFile;
        this.lock = lock;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getOfficerId() {
        return officerId;
    }

    public void setOfficerId(int officerId) {
        this.officerId = officerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoFile() {
        return photoFile;
    }

    public void setPhotoFile(String photoFile) {
        this.photoFile = photoFile;
    }

    public boolean isLock() {
        return lock;
    }

    public void setLock(boolean lock) {
        this.lock = lock;
    }
}
