package com.mojiapps.Shahrdari.database;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by mojtaba on 10/9/14.
 */

@DatabaseTable(tableName = "ViolationNotMember")
public class ViolationNotMember {

    public static final String FIELD_ID = "Id";
    public static final String FIELD_PERSONNEL_ID = "PersonnelId";
    public static final String FIELD_CAR_TYPE = "CarType";
    public static final String FIELD_PLATE_TYPE = "PlateType";
    public static final String FIELD_PLATE_NUMBER = "PlateNumber";
    public static final String FIELD_COLOR = "Color";
    public static final String FIELD_OWNER_NAME = "OwnerName";
    public static final String FIELD_MOBILE = "Mobile";
    public static final String FIELD_ADDRESS = "Address";
    public static final String FIELD_GPS_LOCATION = "GpsLocation";
    public static final String FIELD_ISSUED_CERTIFICATE_ID = "IssuedCertificateId";
    public static final String FIELD_ADDRESS_VIOLATION = "AddressViolation";
    public static final String FIELD_VIOLATION_LIST = "ViolationList";
    public static final String FIELD_VIOLATION_LIST_STRING = "ViolationListString";
    public static final String FIELD_DATE = "Date";
    public static final String FIELD_PHOTO = "Photo";
    public static final String FIELD_SEND = "Send";

    @DatabaseField(generatedId = true, columnName = FIELD_ID)
    private int id;

    @DatabaseField(columnName = FIELD_PERSONNEL_ID)
    private int personnelId;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = FIELD_CAR_TYPE)
    private CarType carType;

    @DatabaseField(columnName = FIELD_PLATE_TYPE)
    private boolean plateType;

    @DatabaseField(columnName = FIELD_PLATE_NUMBER)
    private String plateNumber;

    @DatabaseField(columnName = FIELD_COLOR)
    private String color;

    @DatabaseField(columnName = FIELD_OWNER_NAME)
    private String ownerName;

    @DatabaseField(columnName = FIELD_MOBILE)
    private String mobile;

    @DatabaseField(columnName = FIELD_ADDRESS)
    private String address;

    @DatabaseField(columnName = FIELD_ADDRESS_VIOLATION)
    private String addressViolation;

    @DatabaseField(columnName = FIELD_GPS_LOCATION)
    private String gpsLocation;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = FIELD_ISSUED_CERTIFICATE_ID)
    private CertificateType issuedCertificate;

    @DatabaseField(columnName = FIELD_VIOLATION_LIST)
    private String violationList;

    @DatabaseField(columnName = FIELD_VIOLATION_LIST_STRING)
    private String violationListString;

    @DatabaseField(dataType = DataType.DATE_STRING, columnName = FIELD_DATE)
    private Date date;

    @DatabaseField(columnName = FIELD_PHOTO)
    private String photo;

    @DatabaseField(columnName = FIELD_SEND)
    private boolean send;

    ViolationNotMember(){}

    public ViolationNotMember(int personnelId, CarType carType, boolean plateType, String plateNumber, String color, String ownerName, String mobile, String address, String gpsLocation, CertificateType issuedCertificate, String addressViolation, String violationList, String violationListString, Date date, String photo) {
        this.personnelId = personnelId;
        this.carType = carType;
        this.plateType = plateType;
        this.plateNumber = plateNumber;
        this.color = color;
        this.ownerName = ownerName;
        this.mobile = mobile;
        this.address = address;
        this.gpsLocation = gpsLocation;
        this.issuedCertificate = issuedCertificate;
        this.addressViolation = addressViolation;
        this.violationList = violationList;
        this.violationListString = violationListString;
        this.date= date;
        this.photo = photo;
        this.send = false;
    }

    public int getId() {
        return id;
    }

    public int getPersonnelId() {
        return personnelId;
    }

    public void setPersonnelId(int personnelId) {
        this.personnelId = personnelId;
    }

    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    public boolean isPlateType() {
        return plateType;
    }

    public void setPlateType(boolean plateType) {
        this.plateType = plateType;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public CertificateType getIssuedCertificate() {
        return issuedCertificate;
    }

    public void setIssuedCertificate(CertificateType issuedCertificate) {
        this.issuedCertificate = issuedCertificate;
    }

    public String getAddressViolation() {
        return addressViolation;
    }

    public void setAddressViolation(String addressViolation) {
        this.addressViolation = addressViolation;
    }

    public String getViolationList() {
        return violationList;
    }

    public void setViolationList(String violationList) {
        this.violationList = violationList;
    }

    public String getViolationListString() {
        return violationListString;
    }

    public void setViolationListString(String violationListString) {
        this.violationListString = violationListString;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public boolean isSend() {
        return send;
    }

    public void setSend(boolean send) {
        this.send = send;
    }
}
