package com.mojiapps.Shahrdari.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.models.License;

/**
 * Created by mojtaba on 22/10/2014.
 */

@DatabaseTable(tableName = "LicenseCar")
public class LicenseCar {

    public static final String FIELD_ID = "id";
    public static final String FIELD_LICENSE_ID = "LicenseId";
    public static final String FIELD_CAR_ID = "CarId";

    @DatabaseField(generatedId = true, columnName = FIELD_ID)
    private int id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = FIELD_LICENSE_ID)
    private License license;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = FIELD_CAR_ID)
    private CarOLD car;

    LicenseCar(){}

    public LicenseCar(License license, CarOLD car) {
        this.license = license;
        this.car = car;
    }

    public int getId() {
        return id;
    }

    public License getLicense() {
        return license;
    }

    public void setLicense(License license) {
        this.license = license;
    }

    public CarOLD getCar() {
        return car;
    }

    public void setCar(CarOLD car) {
        this.car = car;
    }
}
