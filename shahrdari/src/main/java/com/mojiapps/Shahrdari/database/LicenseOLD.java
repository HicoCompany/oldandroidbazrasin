package com.mojiapps.Shahrdari.database;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by mojtaba on 9/18/14.
 */

@DatabaseTable
public class LicenseOLD {

    public static final String FIELD_LICENSE_ID = "LicenseId";
    public static final String FIELD_CONTRACTOR_ID = "ContractorId";
    public static final String FIELD_EMPLOYER_NAME = "EmployerName";
    public static final String FIELD_DISCHARGE_LOCATION = "DischargeLocation";
    public static final String FIELD_ADDRESS = "Address";
    public static final String FIELD_FROM_DATE = "FromDate";
    public static final String FIELD_TO_DATE = "ToDate";
    public static final String FIELD_VERIFIED = "Verified";
    public static final String FIELD_VERIFY_NOTE = "VerifyNote";
    public static final String FIELD_CONFIRM_PERSONNEL_ID = "ConfirmPersonnelId";

    @DatabaseField(id = true, columnName = FIELD_LICENSE_ID)
    private int licenseId;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = FIELD_CONTRACTOR_ID)
    private ContractorOLD contractorOLD;

    @DatabaseField(columnName = FIELD_EMPLOYER_NAME)
    private String employerName;

    @DatabaseField(columnName = FIELD_DISCHARGE_LOCATION)
    private String dischargeLocation;

    @DatabaseField(columnName = FIELD_ADDRESS)
    private String address;

    @ForeignCollectionField(eager = true)
    private ForeignCollection<CarOLD> cars;

    @DatabaseField(dataType = DataType.DATE_STRING, columnName = FIELD_FROM_DATE)
    private Date fromDate;

    @DatabaseField(dataType = DataType.DATE_STRING, columnName = FIELD_TO_DATE)
    private Date toDate;

    @DatabaseField(columnName = FIELD_VERIFIED)
    private boolean verified;

    @DatabaseField(columnName = FIELD_VERIFY_NOTE)
    private String verifyNote;

    @DatabaseField(columnName = FIELD_CONFIRM_PERSONNEL_ID)
    private int confirmPersonnelId;

    LicenseOLD(){}

    public LicenseOLD(int licenseId, String employerName, String dischargeLocation, String address, Date fromDate, Date toDate, boolean verified, String verifyNote, ContractorOLD contractorOLD) {
        this.licenseId = licenseId;
        this.employerName = employerName;
        this.dischargeLocation = dischargeLocation;
        this.address = address;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.verified = verified;
        this.verifyNote = verifyNote;
        this.contractorOLD = contractorOLD;
        this.confirmPersonnelId = -1;
    }

    public int getLicenseId() {
        return licenseId;
    }

    public String getEmployerName() {
        return employerName;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    public String getDischargeLocation() {
        return dischargeLocation;
    }

    public void setDischargeLocation(String dischargeLocation) {
        this.dischargeLocation = dischargeLocation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ForeignCollection<CarOLD> getCars() {
        return cars;
    }

    public void setCars(ForeignCollection<CarOLD> cars) {
        this.cars = cars;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getVerifyNote() {
        return verifyNote;
    }

    public void setVerifyNote(String verifyNote) {
        this.verifyNote = verifyNote;
    }

    public ContractorOLD getContractorOLD() {
        return contractorOLD;
    }

    public void setContractorOLD(ContractorOLD contractorOLD) {
        this.contractorOLD = contractorOLD;
    }

    public int getConfirmPersonnelId() {
        return confirmPersonnelId;
    }

    public void setConfirmPersonnelId(int confirmPersonnelId) {
        this.confirmPersonnelId = confirmPersonnelId;
    }
}
