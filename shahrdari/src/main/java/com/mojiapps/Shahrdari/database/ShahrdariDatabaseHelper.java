package com.mojiapps.Shahrdari.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.mojiapps.Shahrdari.Configuration;
import com.mojiapps.Shahrdari.models.*;

import java.io.File;
import java.sql.SQLException;

public class ShahrdariDatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static String DATABASE_NAME = "shahrdari.db";
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_ADDRESS = Configuration.getInstance().getDatabaseFolder();
    private static ShahrdariDatabaseHelper helper = null;

    private Dao<Officer, Integer> officerDao = null;
    private Dao<Car, Integer> carDao = null;
    private Dao<StaffCar, Integer> staffCarDao = null;
    private Dao<Certificate, Integer> certificateDao = null;
    private Dao<License, Integer> licenseDao = null;
    private Dao<Contractor, Integer> contractorDao = null;
    private Dao<StaffCarViolation, Integer> staffCarViolationDao = null;
    private Dao<SurveyEntry, Integer> SurveyDao = null;
    private Dao<CarViolation, Integer> violationNotMemberDao = null;
    private Dao<HomeViolation, Integer> homeViolationDao = null;
    private Dao<Image, Integer> imageDao = null;
    private Dao<Violation, Integer> violationDao = null;
    private Dao<Booth, Integer> boothDao = null;
    private Dao<LicenseCar, Integer> licenseCarDao = null;
    private Dao<CarType, Integer> carTypeDao = null;
    private Dao<PlateType, Integer> plateTypeDao = null;
    private Dao<Discharge, Integer> dischargeDoa = null;
    private Dao<Area, Integer> areaDao = null;
    private Dao<Personnel, Integer> personnelDao = null;

    public ShahrdariDatabaseHelper(Context context) {
        super(context, DATABASE_ADDRESS + File.separator + DATABASE_NAME, null, DATABASE_VERSION); //, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Car.class);
            TableUtils.createTable(connectionSource, Certificate.class);
            TableUtils.createTable(connectionSource, Contractor.class);
            TableUtils.createTable(connectionSource, Discharge.class);
            TableUtils.createTable(connectionSource, StaffCar.class);
            TableUtils.createTable(connectionSource, Area.class);
            TableUtils.createTable(connectionSource, License.class);
            TableUtils.createTable(connectionSource, Personnel.class);
            TableUtils.createTable(connectionSource, StaffCarViolation.class);
            TableUtils.createTable(connectionSource, HomeViolation.class);
            TableUtils.createTable(connectionSource, CarViolation.class);

            TableUtils.createTable(connectionSource, Officer.class);
            TableUtils.createTable(connectionSource, Image.class);
            TableUtils.createTable(connectionSource, ViolationNotMember.class);
            TableUtils.createTable(connectionSource, ViolationEstate.class);
            TableUtils.createTable(connectionSource, Violation.class);
            TableUtils.createTable(connectionSource, LicenseCar.class);
            TableUtils.createTable(connectionSource, CarType.class);
            TableUtils.createTable(connectionSource, PlateType.class);

        } catch (SQLException e) {
            Log.e(ShahrdariDatabaseHelper.class.getName(), "Can't create database", e);
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i2) {

    }

    public Dao<Officer, Integer> getOfficerDao() throws SQLException {
        if(officerDao == null)
            officerDao = getDao(Officer.class);
        return officerDao;
    }

    public Dao<Car, Integer> getCarDao() throws SQLException {
        if(carDao == null)
            carDao = getDao(Car.class);
        return carDao;
    }

    public Dao<StaffCar, Integer> getStaffCarDao() throws SQLException {
        if(staffCarDao == null)
            staffCarDao = getDao(StaffCar.class);
        return staffCarDao;
    }

    public Dao<Certificate, Integer> getCertificateDao() throws SQLException {
        if(certificateDao == null)
            certificateDao = getDao(Certificate.class);
        return certificateDao;
    }

    public Dao<License, Integer> getLicenseDao() throws SQLException {
        if(licenseDao == null)
            licenseDao = getDao(License.class);
        return licenseDao;
    }

    public Dao<Contractor, Integer> getContractorDao() throws SQLException {
        if(contractorDao == null)
            contractorDao = getDao(Contractor.class);
        return contractorDao;
    }


    public Dao<Discharge, Integer> getDischargeDao() throws SQLException {
        if(dischargeDoa == null)
            dischargeDoa = getDao(Discharge.class);
        return dischargeDoa;
    }

    public Dao<StaffCarViolation, Integer> getStaffCarViolationDao() throws SQLException {
        if(staffCarViolationDao == null)
            staffCarViolationDao = getDao(StaffCarViolation.class);
        return staffCarViolationDao;
    }

    public Dao<SurveyEntry, Integer> getSurveyDao() throws SQLException {
        if(SurveyDao == null)
            SurveyDao = getDao(SurveyEntry.class);
        return getSurveyDao();
    }

    public Dao<CarViolation, Integer> getCarViolationDao() throws SQLException {
        if(violationNotMemberDao == null)
            violationNotMemberDao = getDao(CarViolation.class);
        return violationNotMemberDao;
    }

    public Dao<HomeViolation, Integer> getHomeViolationDao() throws SQLException {
        if(homeViolationDao == null)
            homeViolationDao = getDao(HomeViolation.class);
        return homeViolationDao;
    }

    public Dao<Image, Integer> getImageDao() throws SQLException {
        if(imageDao == null)
            imageDao = getDao(Image.class);
        return imageDao;
    }

    public Dao<Area, Integer> getAreaDao() throws SQLException {
        if(areaDao == null)
            areaDao = getDao(Area.class);
        return areaDao;
    }

    public Dao<Violation, Integer> getViolationDao() throws SQLException {
        if(violationDao == null)
            violationDao = getDao(Violation.class);
        return violationDao;
    }

    public Dao<Booth, Integer> getBoothDao() throws SQLException {
        if(boothDao == null)
            boothDao = getDao(Booth.class);
        return boothDao;
    }

    public Dao<LicenseCar, Integer> getLicenseCarDao() throws SQLException {
        if(licenseCarDao == null)
            licenseCarDao = getDao(LicenseCar.class);
        return licenseCarDao;
    }

    public Dao<CarType, Integer> getCarTypeDao() throws SQLException {
        if(carTypeDao == null)
            carTypeDao = getDao(CarType.class);
        return carTypeDao;
    }

    public Dao<PlateType, Integer> getPlateTypeDao() throws SQLException {
        if(plateTypeDao == null)
            plateTypeDao = getDao(PlateType.class);
        return plateTypeDao;
    }

    public Dao<Personnel, Integer> getPersonnelDao() throws SQLException {
        if (personnelDao == null)
            personnelDao = getDao(Personnel.class);
        return personnelDao;
    }

    @Override
    public void close() {
        super.close();
        officerDao = null;
        carDao = null;
        certificateDao = null;
        licenseDao = null;
        contractorDao = null;
        staffCarViolationDao = null;
        violationNotMemberDao = null;
        homeViolationDao = null;
        imageDao = null;
        violationDao = null;
        licenseCarDao = null;
        carTypeDao = null;
        plateTypeDao = null;
        personnelDao = null;
    }

    public static synchronized ShahrdariDatabaseHelper getHelper(Context context) {
        if (helper == null) {
            helper = new ShahrdariDatabaseHelper(context);
        }
        return helper;
    }
}
