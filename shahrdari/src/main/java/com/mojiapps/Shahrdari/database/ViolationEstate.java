package com.mojiapps.Shahrdari.database;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mojiapps.Shahrdari.webservice.Params;

import java.util.Date;

/**
 * Created by mojtaba on 10/9/14.
 */

@DatabaseTable(tableName = "ViolationEstate")
public class ViolationEstate implements Params {

    @DatabaseField(generatedId = true, columnName = ID)
    private int id;

    @DatabaseField(columnName = PERSONNEL_ID)
    private int personnelId;

    @DatabaseField(columnName = OWNER_NAME)
    private String ownerName;

    @DatabaseField(columnName = MOBILE)
    private String mobile;

    @DatabaseField(columnName = GPS_LOCATION)
    private String gpsLocation;

    @DatabaseField(columnName = ADDRESS)
    private String address;

    @DatabaseField(columnName = VIOLATION_LIST)
    private String violationList;

    @DatabaseField(columnName = VIOLATION_LIST_STRING)
    private String violationListString;

    @DatabaseField(dataType = DataType.DATE_STRING, columnName = DATE)
    private Date date;

    @DatabaseField(columnName = PHOTO)
    private String photo;

    @DatabaseField(columnName = SEND)
    private boolean send;

    ViolationEstate(){}

    public ViolationEstate(int personnelId, String ownerName, String mobile, String gpsLocation, String address, String violationList, String violationListString, Date date, String photo) {
        this.personnelId = personnelId;
        this.ownerName = ownerName;
        this.mobile = mobile;
        this.gpsLocation = gpsLocation;
        this.address = address;
        this.violationList = violationList;
        this.violationListString = violationListString;
        this.date = date;
        this.photo = photo;
        this.send = false;
    }

    public int getId() {
        return id;
    }

    public int getPersonnelId() {
        return personnelId;
    }

    public void setPersonnelId(int personnelId) {
        this.personnelId = personnelId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getViolationList() {
        return violationList;
    }

    public void setViolationList(String violationList) {
        this.violationList = violationList;
    }

    public String getViolationListString() {
        return violationListString;
    }

    public void setViolationListString(String violationListString) {
        this.violationListString = violationListString;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public boolean isSend() {
        return send;
    }

    public void setSend(boolean send) {
        this.send = send;
    }
}
