package com.mojiapps.Shahrdari.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by mojtaba on 24/10/2014.
 */

@DatabaseTable(tableName = "PlateType")
public class PlateType {
    public static final String FIELD_ID = "Id";
    public static final String FIELD_TITLE = "title";

    @DatabaseField(id = true, columnName = FIELD_ID)
    private int id;

    @DatabaseField(columnName = FIELD_TITLE)
    private String title;

    PlateType(){}

    public PlateType(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
