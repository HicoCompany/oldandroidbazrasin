package com.mojiapps.Shahrdari.database;

import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import com.mojiapps.Shahrdari.Configuration;
import com.mojiapps.Shahrdari.ShahrdariApp;
import com.mojiapps.Shahrdari.enums.SharedPrefs;
import com.mojiapps.Shahrdari.models.Area;
import com.mojiapps.Shahrdari.models.Booth;
import com.mojiapps.Shahrdari.models.Car;
import com.mojiapps.Shahrdari.models.CarViolation;
import com.mojiapps.Shahrdari.models.Certificate;
import com.mojiapps.Shahrdari.models.Contractor;
import com.mojiapps.Shahrdari.models.Discharge;
import com.mojiapps.Shahrdari.models.HomeViolation;
import com.mojiapps.Shahrdari.models.License;
import com.mojiapps.Shahrdari.models.Personnel;
import com.mojiapps.Shahrdari.models.StaffCar;
import com.mojiapps.Shahrdari.models.StaffCarViolation;
import com.mojiapps.Shahrdari.models.SurveyEntry;
import com.mojiapps.Shahrdari.models.Violation;
import com.mojiapps.Shahrdari.utils.Utils;
import com.mojiapps.Shahrdari.webservice.Params;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mojtaba on 9/18/14.
 */
public class DatabaseService {

    public static boolean login(String userName, String password,String MacAddress){
        try {
            password = Utils.md5(password);
            Dao<Officer, Integer> officerDao = ShahrdariApp.getShahrdariDatabaseHelper().getOfficerDao();
            QueryBuilder<Officer, Integer> queryBuilder = officerDao.queryBuilder();
            queryBuilder.selectColumns(Officer.FIELD_OFFICER_ID, Officer.FIELD_NAME, Officer.FIELD_USER_NAME, Officer.FIELD_PASSWORD, Officer.FIELD_LOCK, Officer.FIELD_PHOTO);
            Where<Officer, Integer> where = queryBuilder.where();
            where.eq(Officer.FIELD_USER_NAME, userName).and().eq(Officer.FIELD_PASSWORD, password);
            queryBuilder.setWhere(where);
            List<Officer> officers = queryBuilder.query();
            if(officers.size() > 0) {
                Configuration.getInstance().setString(SharedPrefs.USER_NAME, userName);
                Configuration.getInstance().setString(SharedPrefs.PASSWORD, password);
                Configuration.getInstance().setString(SharedPrefs.NAME, officers.get(0).getName());
                Configuration.getInstance().setString(SharedPrefs.IMAGE, officers.get(0).getPhotoFile());
                Configuration.getInstance().setInt(SharedPrefs.ID, officers.get(0).getOfficerId());
                Configuration.getInstance().setString(SharedPrefs.MAC_ADDRESS,MacAddress);
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean  checkLogin(String userName, String password,String MacAddress){
        try {
            /*try {
                password = Utils.SHA1(password);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                return false;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;
            }*/
            Dao<Personnel, Integer> personnelDao = ShahrdariApp.getShahrdariDatabaseHelper().getPersonnelDao();
            List<Personnel> personnels = personnelDao.queryBuilder().where().eq(Params.USER_NAME, userName).and().eq(Params.PASSWORD, password).query();

            /*QueryBuilder<Personnel, Integer> queryBuilder = personnelDao.queryBuilder();
            queryBuilder.selectColumns(Officer.FIELD_OFFICER_ID, Officer.FIELD_NAME, Officer.FIELD_USER_NAME, Officer.FIELD_PASSWORD, Officer.FIELD_LOCK, Officer.FIELD_PHOTO);
            Where<Personnel, Integer> where = queryBuilder.where();
            where.eq(Personnel.USER_NAME, userName).and().eq(Personnel.PASSWORD, password);
            queryBuilder.setWhere(where);
            List<Personnel> personnels = queryBuilder.query();*/
            if(personnels.size() > 0) {
                Configuration.getInstance().setString(SharedPrefs.USER_NAME, userName);
                Configuration.getInstance().setString(SharedPrefs.PASSWORD, password);
                Configuration.getInstance().setString(SharedPrefs.NAME, personnels.get(0).getName());
                /*if (personnels.get(0).getArea() == null)
                    personnels.get(0).getArea().setAreaId(1);*/
                 Configuration.getInstance().setString(SharedPrefs.AREA, personnels.get(0).getAreaListJson());
                //Configuration.getInstance().setString(SharedPrefs.IMAGE, personnels.get(0).getPhotoFile());
                Configuration.getInstance().setInt(SharedPrefs.ID, personnels.get(0).getPersonnelId());
                Configuration.getInstance().setString(SharedPrefs.MAC_ADDRESS, MacAddress);
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void addOfficer(Officer officer){
        try {
            Dao<Officer, Integer> officerDao = ShahrdariApp.getShahrdariDatabaseHelper().getOfficerDao();
            officerDao.createOrUpdate(officer);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addPersonnel(Personnel personnel){
        try {
            Dao<Personnel, Integer> officerDao = ShahrdariApp.getShahrdariDatabaseHelper().getPersonnelDao();
            officerDao.createOrUpdate(personnel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Boolean UpdateOfficer(Officer personnel)  {
        int i=0;
        try {
            Dao<Officer, Integer> officerDao = ShahrdariApp.getShahrdariDatabaseHelper().getOfficerDao();

//            UpdateBuilder<Officer, Integer> updateBuilder = officerDao.updateBuilder();
//            int id = Configuration.getInstance().getInt(SharedPrefs.ID);
//            updateBuilder.updateColumnValue(Officer.FIELD_OFFICER_ID,personnel.getOfficerId());
//            updateBuilder.updateColumnValue(Officer.FIELD_NAME,personnel.getName());
//            updateBuilder.updateColumnValue(Officer.FIELD_USER_NAME,personnel.getUserName());
//            updateBuilder.updateColumnValue(Officer.FIELD_PASSWORD,personnel.getPassword());
//
//            updateBuilder.where().idEq(id);
//             i = updateBuilder.update();

            try {
                officerDao.createOrUpdate(personnel);
            } catch (SQLException e) {
                e.printStackTrace();
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return  i>0;
    }

    public static List<License> getLicensesByDate(Date date){
        List<License> lst = null;
        /*try {
            Dao<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao();
            lst = licenseDao.queryBuilder().where().le(License.FROM_DATE, date).and().ge(License.TO_DATE, date).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }*/
        return lst==null ? new ArrayList<License>() : lst;
    }

    public static List<License> getLicensesByContractorId(int contractorId){
        List<License> lst = null;
        try {
            Dao<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao();
            lst = new ArrayList<> (licenseDao.queryBuilder().where().eq(CarOLD.FIELD_CONTRACTOR_ID, DatabaseService.getContractorById(contractorId)).query());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<License> getLicensesSyncable(){
        List<License> lst = null;
        try {
            Dao<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao();
            lst = licenseDao.queryBuilder().where().in(Params.IS_EDITED, true).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<License> getLicensesByDate(Date startDate, Date endDate){
        List<License> lst = null;
        try {
            Dao<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao();
            lst = licenseDao.queryBuilder().where().le(License.FROM_DATE, endDate).and().ge(License.TO_DATE, startDate).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst==null ? new ArrayList<License>() : lst;
    }

    public static License getLicenseById(int licenseId){
        try {
            Dao<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao();
            return licenseDao.queryForId(licenseId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void addLicense(License license){
        try {
            Dao<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao();
            licenseDao.createOrUpdate(license);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addArea(Area area){
        try {
            Dao<Area, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getAreaDao();
            licenseDao.createOrUpdate(area);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addDischarge(Discharge discharge){
        try {
            Dao<Discharge, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getDischargeDao();
            licenseDao.createOrUpdate(discharge);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void verifyLicense1(int licenseId, boolean verified, String note){
        try {
            Dao<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao();
            /*License license = licenseDao.queryForId(licenseId);
            license.setEdited(true);
            license.setConfirm(verified);
            license.setConfirmDescription(note);
            Dao.CreateOrUpdateStatus s = licenseDao.createOrUpdate(license);*/

            UpdateBuilder<License, Integer> updateBuilder = licenseDao.updateBuilder();
            int id = Configuration.getInstance().getInt(SharedPrefs.ID);
            updateBuilder.updateColumnValue(License.CONFIRM_PERSONNEL_ID_1, id);
            updateBuilder.updateColumnValue(License.IS_CONFIRM_From_Personnel_1, verified);
            updateBuilder.updateColumnValue(License.CONFIRM_DESCRIPTION1, note);
            updateBuilder.updateColumnValue(License.IS_SYNCABLE, true);
            if (verified)
                updateBuilder.updateColumnValue(License.LICENSE_STATE, 4);
            else
                updateBuilder.updateColumnValue(License.LICENSE_STATE, 5);
            updateBuilder.where().idEq(licenseId);
            int i = updateBuilder.update();
            List<License> l = licenseDao.queryForEq(Params.IS_EDITED, true);
            Log.e("verifyLicense", (licenseId+" || "+l.size()+"size"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static void verifyLicense2(int licenseId, boolean verified, String note){
        try {
            Dao<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao();
            /*License license = licenseDao.queryForId(licenseId);
            license.setEdited(true);
            license.setConfirm(verified);
            license.setConfirmDescription(note);
            Dao.CreateOrUpdateStatus s = licenseDao.createOrUpdate(license);*/

            UpdateBuilder<License, Integer> updateBuilder = licenseDao.updateBuilder();
            int id = Configuration.getInstance().getInt(SharedPrefs.ID);
            updateBuilder.updateColumnValue(License.CONFIRM_PERSONNEL_ID_2, id);
            updateBuilder.updateColumnValue(License.IS_CONFIRM_From_Personnel_2, verified);
            updateBuilder.updateColumnValue(License.CONFIRM_DESCRIPTION2, note);
            updateBuilder.updateColumnValue(License.IS_SYNCABLE, true);
            if (verified)
                updateBuilder.updateColumnValue(License.LICENSE_STATE, 4);
            else
                updateBuilder.updateColumnValue(License.LICENSE_STATE, 5);
            updateBuilder.where().idEq(licenseId);
            int i = updateBuilder.update();
            List<License> l = licenseDao.queryForEq(Params.IS_EDITED, true);
            Log.e("verifyLicense", (licenseId+" || "+l.size()+"size"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addContractor(Contractor contractor){
        try {
            Dao<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao();
            contractorDao.createOrUpdate(contractor);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<Contractor> getContractors(){
        List<Contractor> lst = null;
        try {
            Dao<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao();
            lst = contractorDao.queryBuilder().orderBy(Params.CONTRACTOR_ID, false).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<Contractor> getContractors(String searchStr){
        List<Contractor> lst = null;
        try {
            Dao<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao();
            try {
                int i = Integer.valueOf(searchStr);
                lst = contractorDao.queryBuilder()
                        .orderBy(Params.CONTRACTOR_ID, false)
                        .where().eq(Params.CONTRACTOR_ID, i)
                        .query();
            }catch (NumberFormatException e) {
                lst = contractorDao.queryBuilder()
                        .orderBy(Params.CONTRACTOR_ID, false)
                        .where().like(Params.TITLE, "%"+searchStr+"%")
                        .query();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static Contractor getContractorById(int contractorId){
        try {
            Dao<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao();
            return contractorDao.queryForId(contractorId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void addCar(Car car){
        try {
            Dao<Car, Integer> carDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarDao();
            carDao.createOrUpdate(car);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addStaffCar(StaffCar staffCar){
        try {
            Dao<StaffCar, Integer> staffCarDao = ShahrdariApp.getShahrdariDatabaseHelper().getStaffCarDao();
            staffCarDao.createOrUpdate(staffCar);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<StaffCar> getStaffCars(){
        List<StaffCar> lst = new ArrayList<>();
        try {
            QueryBuilder<StaffCar, Integer> staffCarDao = ShahrdariApp.getShahrdariDatabaseHelper().getStaffCarDao().queryBuilder();

            QueryBuilder<Car, Integer> carQueryBuilder = ShahrdariApp.getShahrdariDatabaseHelper().getCarDao().queryBuilder();
            QueryBuilder<Contractor, Integer> contractorQueryBuilder = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao().queryBuilder();
            staffCarDao.leftJoin(carQueryBuilder);
            staffCarDao.leftJoin(contractorQueryBuilder);

            lst = staffCarDao.orderBy(Params.STAFF_CAR_ID, false).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<StaffCar> getStaffCars(String searchStr){
        List<StaffCar> lst = new ArrayList<>();
        try {
            QueryBuilder<StaffCar, Integer> staffCarDao = ShahrdariApp.getShahrdariDatabaseHelper().getStaffCarDao().queryBuilder();
            QueryBuilder<Car, Integer> carQueryBuilder = ShahrdariApp.getShahrdariDatabaseHelper().getCarDao().queryBuilder();
            QueryBuilder<Contractor, Integer> contractorQueryBuilder = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao().queryBuilder();
            /*staffCarDao.leftJoinOr(carQueryBuilder);
            staffCarDao.leftJoin(contractorQueryBuilder);*/
            //lst = staffCarDao.orderBy(Params.STAFF_CAR_ID, false).query();
            try {
                int i =  Integer.valueOf(searchStr);

                lst = staffCarDao
                        .orderBy(Params.STAFF_CAR_ID, false)
                        .where().eq(Params.STAFF_CAR_ID, i)
                        .query();
                if (lst.size() == 0) {
                    lst = staffCarDao
                            .orderBy(Params.STAFF_CAR_ID, false)
                            .where().like(Params.PLATE_CODE, "%"+searchStr+"%")
                            .or().like(Params.OLD_PLATE, "%"+searchStr+"%")
                            .or().like(Params.OLD_RECORD, "%"+searchStr+"%")
                            .query();
                }
            }catch (NumberFormatException e) {
                lst = staffCarDao
                        .orderBy(Params.STAFF_CAR_ID, false)
                        .where().like(Params.PLATE_CODE, "%"+searchStr+"%")
                        .or().like(Params.OLD_PLATE, "%"+searchStr+"%")
                        .or().like(Params.OLD_RECORD, "%"+searchStr+"%")
                        .query();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            lst = new ArrayList<>();
        }
        return lst;
    }

    public static List<StaffCar> getStaffCarsByContractor(int contractorID){
        List<StaffCar> lst = new ArrayList<>();
        try {
            QueryBuilder<StaffCar, Integer> staffCarDao = ShahrdariApp.getShahrdariDatabaseHelper().getStaffCarDao().queryBuilder();
            QueryBuilder<Car, Integer> carQueryBuilder = ShahrdariApp.getShahrdariDatabaseHelper().getCarDao().queryBuilder();
            QueryBuilder<Contractor, Integer> contractorQueryBuilder = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao().queryBuilder();
            staffCarDao.leftJoin(carQueryBuilder);
            contractorQueryBuilder.where().in(Contractor.CONTRACTOR_ID, contractorID);
            staffCarDao.leftJoin(contractorQueryBuilder);
            lst = staffCarDao.orderBy(Params.STAFF_CAR_ID, false).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<StaffCar> getStaffCarsInLicense(int licenceID){
        List<StaffCar> lst = new ArrayList<>();
        try {
            Dao<License, Integer> staLicenses = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao();
            License license = staLicenses.queryForId(licenceID);
            license.setStaffCarList(license.getStaffCarIdListJson());
            if (license.getStaffCarList() != null)
                lst.addAll(license.getStaffCarList());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<Car> getCars(){
        List<Car> lst = null;
        try {
            Dao<Car, Integer> carDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarDao();
            lst = carDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<Car> getCarsByContractorId(int id){
        List<Car> lst = null;
        try {
            Dao<Car, Integer> carDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarDao();
            lst = new ArrayList<> (carDao.queryBuilder().where().eq(Car.CONTRACTOR_ID, DatabaseService.getContractorById(id)).query());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<StaffCarViolation> getStaffCarViolations(){
        List<StaffCarViolation> lst = null;
        try {
            Dao<StaffCarViolation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getStaffCarViolationDao();
            lst = violationDao.queryBuilder().orderBy(Params.REGISTER_DATE, false).query();
            int counter = 0;
            for (StaffCarViolation staffCarViolation : lst) {
                lst.get(counter).setViolationList(staffCarViolation.getViolationListJson());
                counter++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }



    public static List<StaffCarViolation> getStaffCarViolationsByStaffCarId(int staffCarId){
        List<StaffCarViolation> lst = null;
        try {
            QueryBuilder<StaffCarViolation, Integer> violationDao
                    = ShahrdariApp.getShahrdariDatabaseHelper().getStaffCarViolationDao().queryBuilder();
            QueryBuilder<StaffCar, Integer> staffCarDao
                    = ShahrdariApp.getShahrdariDatabaseHelper().getStaffCarDao().queryBuilder();
            QueryBuilder<Certificate, Integer> certificateDao
                    = ShahrdariApp.getShahrdariDatabaseHelper().getCertificateDao().queryBuilder();

            staffCarDao.where().in(Params.STAFF_CAR_ID, staffCarId);
            violationDao.leftJoin(staffCarDao);
            violationDao.leftJoin(certificateDao);
            lst = violationDao.orderBy(Params.REGISTER_DATE, false).query();
            int counter = 0;
            for (StaffCarViolation staffCarViolation : lst) {
                lst.get(counter).setViolationList(staffCarViolation.getViolationListJson());
                counter++;
            }
        } catch (SQLException e) {
            e.printStackTrace(); 
        }
        return lst;
    }

    public static List<HomeViolation> getViolationEstate(){
        List<HomeViolation> lst = new ArrayList<>();
        try {
            Dao<HomeViolation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getHomeViolationDao();
            lst = violationDao.queryBuilder().orderBy(Params.REGISTER_DATE, false).query();
            int counter = 0;
            for (HomeViolation homeViolation : lst) {
                lst.get(counter).setViolationList(homeViolation.getViolationListJson());
                counter++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<HomeViolation> getViolationEstateSyncable(){
        List<HomeViolation> lst = new ArrayList<>();
        try {
            QueryBuilder<HomeViolation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getHomeViolationDao().queryBuilder();
            lst = violationDao.where().in(Params.IS_SYNCABLE, true).query();
            int counter = 0;
            for (HomeViolation homeViolation : lst) {
                lst.get(counter).setViolationList(homeViolation.getViolationListJson());
                counter++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<StaffCarViolation> getStaffCarViolationSyncable(){
        List<StaffCarViolation> lst = new ArrayList<>();
        try {
            QueryBuilder<StaffCarViolation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getStaffCarViolationDao().queryBuilder();
            lst = violationDao.where().in(Params.IS_SYNCABLE, true).query();
            int counter = 0;
            for (StaffCarViolation staffCarViolation : lst) {
                lst.get(counter).setViolationList(staffCarViolation.getViolationListJson());
                counter++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<CarViolation> getCarViolationSyncable(){
        List<CarViolation> lst = new ArrayList<>();
        try {
            QueryBuilder<CarViolation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarViolationDao().queryBuilder();
            lst = violationDao.where().in(Params.IS_SYNCABLE, true).query();
            int counter = 0;
            for (CarViolation carViolation : lst) {
                lst.get(counter).setViolationList(carViolation.getViolationListJson());
                counter++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<CarViolation> getViolationNotMember(){
        List<CarViolation> lst = new ArrayList<>();
        try {
            QueryBuilder<CarViolation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarViolationDao().queryBuilder();
            QueryBuilder<Car, Integer> carViolationDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarDao().queryBuilder();
            QueryBuilder<Certificate, Integer> certificateQueryBuilder = ShahrdariApp.getShahrdariDatabaseHelper().getCertificateDao().queryBuilder();
            violationDao.leftJoin(carViolationDao);
            violationDao.leftJoin(certificateQueryBuilder);
            lst = violationDao.orderBy(Params.REGISTER_DATE, false).query();

            int counter = 0;
            for (CarViolation carViolation : lst) {
                lst.get(counter).setViolationList(carViolation.getViolationListJson());
                counter++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<CarViolation> getViolationNotMemberByOldPlateCode(String plateCode){
        List<CarViolation> lst = new ArrayList<>();
        try {
            QueryBuilder<CarViolation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarViolationDao().queryBuilder();
            QueryBuilder<Car, Integer> carViolationDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarDao().queryBuilder();
            QueryBuilder<Certificate, Integer> certificateQueryBuilder = ShahrdariApp.getShahrdariDatabaseHelper().getCertificateDao().queryBuilder();
            violationDao.leftJoin(carViolationDao);
            violationDao.leftJoin(certificateQueryBuilder);
            lst = violationDao.where().in(Params.OLD_PLATE, plateCode).or().eq(Params.CAR_VIOLATION_ID,plateCode).query();
            int counter = 0;
            for (CarViolation carViolation : lst) {
                lst.get(counter).setViolationList(carViolation.getViolationListJson());
                counter++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<CarViolation> getViolationNotMemberByNewPlateCode
            (String part1,String part2,String part3,String part4){
        List<CarViolation> lst = new ArrayList<>();
        try {
            QueryBuilder<CarViolation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarViolationDao().queryBuilder();
            QueryBuilder<Car, Integer> carViolationDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarDao().queryBuilder();
            QueryBuilder<Certificate, Integer> certificateQueryBuilder = ShahrdariApp.getShahrdariDatabaseHelper().getCertificateDao().queryBuilder();
            violationDao.leftJoin(carViolationDao);
            violationDao.leftJoin(certificateQueryBuilder);
            lst = violationDao
                    .orderBy(Params.REGISTER_DATE, false)
                    .where()
                    .in(Params.NEW_PLATE_PART_1, part1).and()
                    .in(Params.NEW_PLATE_PART_2, part2).and()
                    .in(Params.NEW_PLATE_PART_3, part3).and()
                    .in(Params.NEW_PLATE_PART_4, part4)
                    .query();
            int counter = 0;
            for (CarViolation carViolation : lst) {
                lst.get(counter).setViolationList(carViolation.getViolationListJson());
                counter++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static void addCertificateType(Certificate certificate){
        try {
            Dao<Certificate, Integer> certificateTypeDao = ShahrdariApp.getShahrdariDatabaseHelper().getCertificateDao();
            certificateTypeDao.createOrUpdate(certificate);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



        public static List<Certificate> getAllCertificateTypes(){
        List<Certificate> lst = null;
        try {
            Dao<Certificate, Integer> certificateDao = ShahrdariApp.getShahrdariDatabaseHelper().getCertificateDao();
            lst = certificateDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static void addViolation(Violation violation){
        try {
            Dao<Violation, Integer> violationTypeDao = ShahrdariApp.getShahrdariDatabaseHelper().getViolationDao();
            violationTypeDao.createOrUpdate(violation);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Personnel getPersonnelById(int PersonnelId){
        try {
            Dao<Personnel, Integer> personnelDao = ShahrdariApp.getShahrdariDatabaseHelper().getPersonnelDao();
            return personnelDao.queryForId(PersonnelId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void addBooth(Booth booth){
        try {
            Dao<Booth, Integer> boothTypeDao = ShahrdariApp.getShahrdariDatabaseHelper().getBoothDao();
            boothTypeDao.createOrUpdate(booth);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateLicense(License license) {
        try {
            Dao<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao();
            licenseDao.update(license);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateCar(Car car) {
        try {
            Dao<Car, Integer> carIntegerDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarDao();
            carIntegerDao.update(car);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addLicenseCar(LicenseCar licenseCar){
        try {
            Dao<LicenseCar, Integer> licenseCarDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseCarDao();
            licenseCarDao.create(licenseCar);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<Violation> getViolations(){
        List<Violation> lst = null;
        try {
            Dao<Violation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getViolationDao();
            lst = violationDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }
    public static List<Booth> getBooths(){
        List<Booth> lst = null;
        try {
            Dao<Booth, Integer> BoothDao = ShahrdariApp.getShahrdariDatabaseHelper().getBoothDao();
            lst = BoothDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static String getCarOwnerNameByCarId(int carId){
        String result = "";
        try {
            Dao<StaffCar, Integer> staffCarDao = ShahrdariApp.getShahrdariDatabaseHelper().getStaffCarDao();
            StaffCar car = staffCarDao.queryForId(carId);
            if (car != null)
                result = staffCarDao.queryForId(carId).getOwName();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getCarOwnerNameByIdOrOldRecord(int carId){
        List<StaffCar> lst=null;
        String result = "";
        try {

            Dao<StaffCar, Integer> staffCarDao = ShahrdariApp.getShahrdariDatabaseHelper().getStaffCarDao();
            StaffCar car = staffCarDao.queryForId(carId);
            if (car != null)
                result = staffCarDao.queryForId(carId).getOwName()+'%'+carId;
             else {
                lst = staffCarDao.queryBuilder()
                        .where().eq(Params.OLD_RECORD, carId)
                        .query();
                if(lst.size()>0) {
                    result = lst.get(0).getOwName()+'%'+lst.get(0).getStaffCarId();
                    //carId = lst.get(0).getStaffCarId();
                }

            }
//            if (car != null)
//                result = ((StaffCar)staffCarDao.queryBuilder().where().eq(Params.OLD_RECORD, oldRecord).query()).getOwName();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    public static void addPlateType(PlateType plateType){
        try {
            Dao<PlateType, Integer> plateTypeDao = ShahrdariApp.getShahrdariDatabaseHelper().getPlateTypeDao();
            plateTypeDao.createOrUpdate(plateType);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addCarType(CarType carType){
        try {
            Dao<CarType, Integer> carTypeDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarTypeDao();
            carTypeDao.createOrUpdate(carType);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<CarType> getAllCarTypes(){
        List<CarType> lst = null;
        try {
            Dao<CarType, Integer> carTypeDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarTypeDao();
            lst = carTypeDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static Boolean updateStaffCarViolation(StaffCarViolation staffCarViolation){
        try {
            Dao<StaffCarViolation, Integer> staffCarViolations = ShahrdariApp.getShahrdariDatabaseHelper().getStaffCarViolationDao();
            Dao.CreateOrUpdateStatus createOrUpdateStatus = staffCarViolations.createOrUpdate(staffCarViolation);
            return createOrUpdateStatus.isCreated() || createOrUpdateStatus.isUpdated();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Boolean SaveSurvey(SurveyEntry survey){
        try {
            Dao<SurveyEntry, Integer> surveys = ShahrdariApp.getShahrdariDatabaseHelper().getSurveyDao();
            Dao.CreateOrUpdateStatus createOrUpdateStatus = surveys.createOrUpdate(survey);
            return createOrUpdateStatus.isCreated() || createOrUpdateStatus.isUpdated();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Boolean updateHomeViolation(HomeViolation homeViolation){
        try {
            Dao<HomeViolation, Integer> violationEstateDao = ShahrdariApp.getShahrdariDatabaseHelper().getHomeViolationDao();
            Dao.CreateOrUpdateStatus createOrUpdateStatus = violationEstateDao.createOrUpdate(homeViolation);
            return createOrUpdateStatus.isCreated() || createOrUpdateStatus.isUpdated();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    public static Boolean deleteHomeViolation(HomeViolation homeViolation){
        try {
            Dao<HomeViolation, Integer> violationEstateDao = ShahrdariApp.getShahrdariDatabaseHelper().getHomeViolationDao();
            int createOrUpdateStatus = violationEstateDao.delete(homeViolation);
            return createOrUpdateStatus > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Boolean deleteStaffCarViolation(StaffCarViolation staffCarViolation){
        try {
            Dao<StaffCarViolation, Integer> violationEstateDao = ShahrdariApp.getShahrdariDatabaseHelper().getStaffCarViolationDao();
            int createOrUpdateStatus = violationEstateDao.delete(staffCarViolation);
            return createOrUpdateStatus > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Boolean deleteCarViolation(CarViolation carViolation){
        try {
            Dao<CarViolation, Integer> violationEstateDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarViolationDao();
            int createOrUpdateStatus = violationEstateDao.delete(carViolation);
            return createOrUpdateStatus > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Boolean updateViolationNotMember(CarViolation carViolation){
        try {
            Dao<CarViolation, Integer> violationMemberDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarViolationDao();
            Dao.CreateOrUpdateStatus createOrUpdateStatus = violationMemberDao.createOrUpdate(carViolation);
            return createOrUpdateStatus.isCreated() || createOrUpdateStatus.isUpdated();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public static Car getCarById(int carId){
        try {
            Dao<Car, Integer> carDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarDao();
            return carDao.queryForId(carId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static StaffCar getStaffCarById(int staffCarId) {
        try {
            Dao<StaffCar, Integer> carDao = ShahrdariApp.getShahrdariDatabaseHelper().getStaffCarDao();
            return carDao.queryForId(staffCarId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Certificate getCertificateById(int certificateId){
        try {
            Dao<Certificate, Integer> certificateTypeDao = ShahrdariApp.getShahrdariDatabaseHelper().getCertificateDao();
            return certificateTypeDao.queryForId(certificateId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static CarType getCarTypeById(int carTypeId){
        try {
            Dao<CarType, Integer> carTypeDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarTypeDao();
            return carTypeDao.queryForId(carTypeId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<HomeViolation> getNotSendHomeViolation(){
        List<HomeViolation> lst = null;
        try {
            Dao<HomeViolation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getHomeViolationDao();
            lst = violationDao.queryBuilder().where().eq(ViolationEstate.SEND, false).query();
            int counter = 0;
            for (HomeViolation homeViolation : lst) {
                lst.get(counter).setViolationList(homeViolation.getViolationListJson());
                counter++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<StaffCarViolation> getNotSendViolationMember(){
        List<StaffCarViolation> lst = null;
        try {
            Dao<StaffCarViolation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getStaffCarViolationDao();
            lst = violationDao.queryBuilder()
                    //.where().eq(ViolationMember.FIELD_SEND, false)
                    .query();
            int counter = 0;
            for (StaffCarViolation staffCarViolation : lst) {
                lst.get(counter).setViolationList(staffCarViolation.getViolationListJson());
                counter++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<CarViolation> getNotSendViolationNotMember(){
        List<CarViolation> lst = null;
        try {
            Dao<CarViolation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarViolationDao();
            lst = violationDao.queryBuilder().where().eq(ViolationNotMember.FIELD_SEND, false).query();
            int counter = 0;
            for (CarViolation carViolation : lst) {
                lst.get(counter).setViolationList(carViolation.getViolationListJson());
                counter++;
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
        return lst;
    }

    public static List<License> getNotSendLicense(int id){
        List<License> lst = null;
        try {
            Dao<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao();
            lst = licenseDao.queryBuilder()
                    .orderBy(Params.LICENSE_ID, false)
                    .where().ne(Params.PERSONNEL_ID, id)
                    .query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<License> getNotSendLicense(int id, String searchStr){
        List<License> lst = null;
        int i = 0;
        try {
            QueryBuilder<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao().queryBuilder();
            QueryBuilder<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao().queryBuilder();
            //lst = licenseDao.queryBuilder().orderBy(Params.LICENSE_ID, false).query();

            try {

                i = Integer.valueOf(searchStr);
                lst = licenseDao
                        .orderBy(Params.LICENSE_ID, false)
                        .where().eq(Params.LICENSE_ID, i)

                        .and().ne(Params.PERSONNEL_ID, id)
                      //  .or().eq(Params.CONTRACTOR_ID,i)
                        .query();
                if (lst.size() == 0) {

                    if(i == 0)
                    contractorDao.where().like(Params.TITLE, "%"+searchStr+"%");
                    else
                        contractorDao.where().like(Params.CONTRACTOR_ID, i);
                    licenseDao.leftJoin(contractorDao);
                    lst = licenseDao
                            .orderBy(Params.LICENSE_ID, false)
                            .where().ne(Params.PERSONNEL_ID, id)

                            .query();
                }
            }catch (NumberFormatException e) {
                if(i == 0)
                contractorDao.where().like(Params.TITLE, "%"+searchStr+"%") ;
                else
                    contractorDao.where().like(Params.CONTRACTOR_ID,i) ;
                licenseDao.leftJoin(contractorDao);
                lst = licenseDao
                        .orderBy(Params.LICENSE_ID, false)
                        .where().ne(Params.PERSONNEL_ID, id)

                        .query();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            lst = new ArrayList<>();
        }
        return lst;
    }

    public static List<License> getNotSendLicenseStaffCar(int id, String searchStr){
        List<License> lst = null,lst1 = new ArrayList<License>();

        try {
            QueryBuilder<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao().queryBuilder();
           try {

                lst = licenseDao
                        .orderBy(Params.LICENSE_ID, false)
                        .query();
                for(int j=0;j<lst.size();j++)
                {
                   if(lst.get(j).getStaffCarIdListJson().contains(searchStr)) {

                       lst1.add(lst.get(j));
                   }
                }
            }catch (NumberFormatException e) {
//
            }
        } catch (SQLException e) {
            e.printStackTrace();
            lst1 = new ArrayList<>();
        }
        return lst1;
    }


    public static List<License> getNotSendLicenseByContractor(int contractorID){
        List<License> lst = null;
        try {
            QueryBuilder<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao().queryBuilder();
            QueryBuilder<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao().queryBuilder();
            QueryBuilder<Discharge, Integer> dischargeDao = ShahrdariApp.getShahrdariDatabaseHelper().getDischargeDao().queryBuilder();
            QueryBuilder<Area, Integer> areaDoa = ShahrdariApp.getShahrdariDatabaseHelper().getAreaDao().queryBuilder();
            contractorDao.where().in(Contractor.CONTRACTOR_ID, contractorID);
            licenseDao.leftJoin(contractorDao);
            licenseDao.leftJoin(dischargeDao);
            licenseDao.leftJoin(areaDoa);
            lst = licenseDao.query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<License> getNotSendLicenseById(int id) {
        List<License> lst = null;
        try {
            QueryBuilder<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao().queryBuilder();
            QueryBuilder<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao().queryBuilder();
            QueryBuilder<Discharge, Integer> dischargeDao = ShahrdariApp.getShahrdariDatabaseHelper().getDischargeDao().queryBuilder();
            QueryBuilder<Area, Integer> areaDoa = ShahrdariApp.getShahrdariDatabaseHelper().getAreaDao().queryBuilder();
            licenseDao.leftJoin(contractorDao);
            licenseDao.leftJoin(dischargeDao);
            licenseDao.where().in(Params.LICENSE_ID, id);
            licenseDao.leftJoin(areaDoa);
            lst = licenseDao.orderBy(Params.LICENSE_ID, false).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<License> getNotSendLicenseByAreaId(int id, List<Area> areaList) {
        List<License> lst = null;
        List<Integer> areaIdsList = new ArrayList<>();
        for (int i=0; i<areaList.size(); i++)
            areaIdsList.add(i, areaList.get(i).getAreaId());
        try {
            QueryBuilder<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao().queryBuilder();
            QueryBuilder<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao().queryBuilder();
            QueryBuilder<Discharge, Integer> dischargeDao = ShahrdariApp.getShahrdariDatabaseHelper().getDischargeDao().queryBuilder();
            QueryBuilder<Area, Integer> areaDoa = ShahrdariApp.getShahrdariDatabaseHelper().getAreaDao().queryBuilder();
            licenseDao.leftJoin(contractorDao);
            licenseDao.leftJoin(dischargeDao);
         //   areaDoa.where().in(Params.AREA_ID, areaIdsList);
            licenseDao.leftJoin(areaDoa);

            lst = licenseDao.orderBy(Params.LICENSE_ID, false)
                    .where().in(Params.AREA, areaIdsList)
                    .and().ne(Params.PERSONNEL_ID, id)
                    .query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }


    public static List<License> getNotSendLicenseByAreaId(int id, List<Area> areaList, String searchStr) {
        List<License> lst = null;
        int i=0;
        List<Integer> areaIdsList = new ArrayList<>();
        for (int j=0; j<areaList.size(); j++)
            areaIdsList.add(j, areaList.get(j).getAreaId());
        try {
            QueryBuilder<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao().queryBuilder();
            QueryBuilder<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao().queryBuilder();
            QueryBuilder<Discharge, Integer> dischargeDao = ShahrdariApp.getShahrdariDatabaseHelper().getDischargeDao().queryBuilder();
            QueryBuilder<Area, Integer> areaDoa = ShahrdariApp.getShahrdariDatabaseHelper().getAreaDao().queryBuilder();
            //licenseDao.leftJoin(contractorDao);
            licenseDao.leftJoin(dischargeDao);
            //areaDoa.where().in(Params.AREA_ID, areaIdsList.iterator());
            licenseDao.leftJoin(areaDoa);

            try {
                 i = Integer.valueOf(searchStr);
                lst = licenseDao.orderBy(Params.LICENSE_ID, false)
                        .where().eq(Params.LICENSE_ID, i).and()
                        .in(Params.AREA, areaIdsList)
                        .and().ne(Params.PERSONNEL_ID, id)
                        .query();
                if (lst.size() == 0) {
                    if(i==0)
                      contractorDao.where().like(Params.TITLE, "%"+searchStr+"%");
                    else
                        contractorDao.where().like(Params.CONTRACTOR_ID, i);
                    licenseDao.leftJoin(contractorDao);
                    lst = licenseDao
                            .orderBy(Params.LICENSE_ID, false)
                            .where().in(Params.AREA, areaIdsList)
                            .and().ne(Params.PERSONNEL_ID, id)
                            .query();
                }
            }catch (NumberFormatException e) {
                if(i==0)
                    contractorDao.where().like(Params.TITLE, "%"+searchStr+"%");
                else
                    contractorDao.where().like(Params.CONTRACTOR_ID, i);
                licenseDao.leftJoin(contractorDao);
                lst = licenseDao
                        .orderBy(Params.LICENSE_ID, false)
                        .where().in(Params.AREA, areaIdsList)
                        .and().ne(Params.PERSONNEL_ID, id)
                        .query();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            lst = new ArrayList<>();
        }
        return lst;
    }


    public static List<License> getNotSendLicenseByAreaIdStaffCar(int id, List<Area> areaList, String searchStr) {
        List<License> lst = null,lst1 = new ArrayList<License>();
        int i=0;
        List<Integer> areaIdsList = new ArrayList<>();
        for (int j=0; j<areaList.size(); j++)
            areaIdsList.add(j, areaList.get(j).getAreaId());
        try {
            QueryBuilder<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao().queryBuilder();
            QueryBuilder<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao().queryBuilder();
            QueryBuilder<Discharge, Integer> dischargeDao = ShahrdariApp.getShahrdariDatabaseHelper().getDischargeDao().queryBuilder();
            QueryBuilder<Area, Integer> areaDoa = ShahrdariApp.getShahrdariDatabaseHelper().getAreaDao().queryBuilder();
            //licenseDao.leftJoin(contractorDao);
            licenseDao.leftJoin(dischargeDao);
            //areaDoa.where().in(Params.AREA_ID, areaIdsList.iterator());
            licenseDao.leftJoin(areaDoa);

            try {
                i = Integer.valueOf(searchStr);
                lst = licenseDao.orderBy(Params.LICENSE_ID, false)
                        .where().eq(Params.LICENSE_ID, i).and()
                        .in(Params.AREA, areaIdsList)
                        .and().ne(Params.PERSONNEL_ID, id)
                        .query();
                if (lst.size() == 0) {
//                    if(i==0)
//                        contractorDao.where().like(Params.TITLE, "%"+searchStr+"%");
//                    else
//                        contractorDao.where().like(Params.CONTRACTOR_ID, i);
//                    licenseDao.leftJoin(contractorDao);
                    lst = licenseDao
                            .orderBy(Params.LICENSE_ID, false)
                            .where().in(Params.AREA, areaIdsList)
                            .and().ne(Params.PERSONNEL_ID, id)
                            .query();
                }
            }catch (NumberFormatException e) {
//                if(i==0)
//                    contractorDao.where().like(Params.TITLE, "%"+searchStr+"%");
//                else
//                    contractorDao.where().like(Params.CONTRACTOR_ID, i);
                licenseDao.leftJoin(contractorDao);
                lst = licenseDao
                        .orderBy(Params.LICENSE_ID, false)
                        .where().in(Params.AREA, areaIdsList)
                        .and().ne(Params.PERSONNEL_ID, id)
                        .query();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            lst = new ArrayList<>();
        }
        if(lst.size()>0)
        {
           try {
                 for(int j=0;j<lst.size();j++)
                    {
                        if(lst.get(j).getStaffCarIdListJson().contains(searchStr)) {

                            lst1.add(lst.get(j));
                        }
                    }
                }
           catch (NumberFormatException e) {
               lst1 = new ArrayList<>();
                }
        }

        return lst1;
    }

    public static List<License> getLicenseByConfirmId(int id) {
        List<License> lst = null;
        try {
            QueryBuilder<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao().queryBuilder();
            QueryBuilder<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao().queryBuilder();
            QueryBuilder<Discharge, Integer> dischargeDao = ShahrdariApp.getShahrdariDatabaseHelper().getDischargeDao().queryBuilder();
            QueryBuilder<Area, Integer> areaDoa = ShahrdariApp.getShahrdariDatabaseHelper().getAreaDao().queryBuilder();
           // areaDoa.where().in(Params.AREA_ID, id);

            licenseDao.where().in(Params.IS_CONFIRM_From_Personnel_1, true).and().eq(Params.CONFIRM_PERSONNEL_ID_1, id).or().in(Params.IS_CONFIRM_From_Personnel_2, true).and().eq(Params.CONFIRM_PERSONNEL_ID_2, id);
            licenseDao.leftJoin(contractorDao);
            licenseDao.leftJoin(dischargeDao);
            licenseDao.leftJoin(areaDoa);
            lst = licenseDao.orderBy(Params.LICENSE_ID, false).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<License> getLicenseByConfirmId(int id, String searchStr) {
        List<License> lst = null;
        int i=0;
        try {
            QueryBuilder<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao().queryBuilder();
            QueryBuilder<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao().queryBuilder();
            QueryBuilder<Discharge, Integer> dischargeDao = ShahrdariApp.getShahrdariDatabaseHelper().getDischargeDao().queryBuilder();
            QueryBuilder<Area, Integer> areaDoa = ShahrdariApp.getShahrdariDatabaseHelper().getAreaDao().queryBuilder();
            //licenseDao.leftJoin(contractorDao);
            licenseDao.leftJoin(dischargeDao);
            licenseDao.leftJoin(areaDoa);

            try {
                 i = Integer.valueOf(searchStr);
                lst = licenseDao.orderBy(Params.LICENSE_ID, false)
                        .where().eq(Params.LICENSE_ID, i)
                        .and().in(Params.PERSONNEL_ID, id)
                        .query();
                if (lst.size() == 0) {
                    if(i==0)
                        contractorDao.where().like(Params.TITLE, "%"+searchStr+"%");
                    else
                        contractorDao.where().like(Params.CONTRACTOR_ID, i);
                    licenseDao.leftJoin(contractorDao);
                    lst = licenseDao
                            .orderBy(Params.LICENSE_ID, false)
                            .where().in(Params.PERSONNEL_ID, id)
                            .query();
                }
            }catch (NumberFormatException e) {
                if(i==0)
                    contractorDao.where().like(Params.TITLE, "%"+searchStr+"%");
                else
                    contractorDao.where().like(Params.CONTRACTOR_ID, i);
                licenseDao.leftJoin(contractorDao);
                lst = licenseDao
                        .orderBy(Params.LICENSE_ID, false)
                        .where().in(Params.PERSONNEL_ID, id)
                        .query();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            lst = new ArrayList<>();
        }
        return lst;
    }

    public static List<License> getLicenseByConfirmIdStaffCar(int id, String searchStr) {
        List<License> lst = null,lst1 = new ArrayList<License>();
        int i=0;
        try {
            QueryBuilder<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao().queryBuilder();
            QueryBuilder<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao().queryBuilder();
            QueryBuilder<Discharge, Integer> dischargeDao = ShahrdariApp.getShahrdariDatabaseHelper().getDischargeDao().queryBuilder();
            QueryBuilder<Area, Integer> areaDoa = ShahrdariApp.getShahrdariDatabaseHelper().getAreaDao().queryBuilder();
            //licenseDao.leftJoin(contractorDao);
            licenseDao.leftJoin(dischargeDao);
            licenseDao.leftJoin(areaDoa);

            try {
                i = Integer.valueOf(searchStr);
                lst = licenseDao.orderBy(Params.LICENSE_ID, false)
                        .where().eq(Params.LICENSE_ID, i)
                        .and().in(Params.PERSONNEL_ID, id)
                        .query();
                if (lst.size() == 0) {
//                    if(i==0)
//                        contractorDao.where().like(Params.TITLE, "%"+searchStr+"%");
//                    else
//                        contractorDao.where().like(Params.CONTRACTOR_ID, i);
                    licenseDao.leftJoin(contractorDao);
                    lst = licenseDao
                            .orderBy(Params.LICENSE_ID, false)
                            .where().in(Params.PERSONNEL_ID, id)
                            .query();
                }
            }catch (NumberFormatException e) {
//                if(i==0)
//                    contractorDao.where().like(Params.TITLE, "%"+searchStr+"%");
//                else
//                    contractorDao.where().like(Params.CONTRACTOR_ID, i);
                licenseDao.leftJoin(contractorDao);
//                lst = licenseDao
//                        .orderBy(Params.LICENSE_ID, false)
//                        .where().in(Params.PERSONNEL_ID, id)
//                        .query();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            lst = new ArrayList<>();
        }
        if(lst.size()>0)
        {
            try {
                for(int j=0;j<lst.size();j++)
                {
                    if(lst.get(j).getStaffCarIdListJson().contains(searchStr)) {

                        lst1.add(lst.get(j));
                    }
                }
            }
            catch (NumberFormatException e) {
                lst1 = new ArrayList<>();
            }
        }

        return lst1;
    }

    public static List<License> getLicenseByConfirmed1_2(int id) {
        List<License> lst = null;
        try {
            QueryBuilder<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao().queryBuilder();
            QueryBuilder<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao().queryBuilder();
            QueryBuilder<Discharge, Integer> dischargeDao = ShahrdariApp.getShahrdariDatabaseHelper().getDischargeDao().queryBuilder();
            QueryBuilder<Area, Integer> areaDoa = ShahrdariApp.getShahrdariDatabaseHelper().getAreaDao().queryBuilder();
            licenseDao.leftJoin(contractorDao);
            licenseDao.leftJoin(dischargeDao);
            licenseDao.leftJoin(areaDoa);
            licenseDao.where().in(Params.IS_CONFIRM_From_Personnel_1, true);
            lst = licenseDao.orderBy(Params.LICENSE_ID, false).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<License> getLicenseByConfirmed(int id, String searchStr) {
        List<License> lst = null;
        int i=0;
        try {
            QueryBuilder<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao().queryBuilder();
            QueryBuilder<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao().queryBuilder();
            QueryBuilder<Discharge, Integer> dischargeDao = ShahrdariApp.getShahrdariDatabaseHelper().getDischargeDao().queryBuilder();
            QueryBuilder<Area, Integer> areaDoa = ShahrdariApp.getShahrdariDatabaseHelper().getAreaDao().queryBuilder();
            //licenseDao.leftJoin(contractorDao);
            licenseDao.leftJoin(dischargeDao);
            licenseDao.leftJoin(areaDoa);

            try {
                i = Integer.valueOf(searchStr);
                lst = licenseDao.orderBy(Params.LICENSE_ID, false)
                        .where().eq(Params.LICENSE_ID, i)
                        .and().in(Params.IS_CONFIRM_From_Personnel_1, true)
                        .and().ne(Params.PERSONNEL_ID, id)
                        .query();

                if (lst.size() == 0) {
                    if(i==0)
                        contractorDao.where().like(Params.TITLE, "%"+searchStr+"%");
                    else
                        contractorDao.where().like(Params.CONTRACTOR_ID, i);
                    licenseDao.leftJoin(contractorDao);
                    lst = licenseDao
                            .orderBy(Params.LICENSE_ID, false)
                            .where().in(Params.IS_CONFIRM_From_Personnel_1, true)
                            .and().ne(Params.PERSONNEL_ID, id)
                            .query();
                }
            }catch (NumberFormatException e) {
                if(i==0)
                    contractorDao.where().like(Params.TITLE, "%"+searchStr+"%");
                else
                    contractorDao.where().like(Params.CONTRACTOR_ID, i);
                licenseDao.leftJoin(contractorDao);
                lst = licenseDao
                        .orderBy(Params.LICENSE_ID, false)
                        .where().in(Params.IS_CONFIRM_From_Personnel_1, true)
                        .and().ne(Params.PERSONNEL_ID, id)
                        .query();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            lst = new ArrayList<>();
        }
        return lst;
    }

    public static List<License> getLicenseByConfirmedStaffCar(int id, String searchStr) {
        List<License> lst = null,lst1 = new ArrayList<License>();
        int i=0;
        try {
            QueryBuilder<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao().queryBuilder();
            QueryBuilder<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao().queryBuilder();
            QueryBuilder<Discharge, Integer> dischargeDao = ShahrdariApp.getShahrdariDatabaseHelper().getDischargeDao().queryBuilder();
            QueryBuilder<Area, Integer> areaDoa = ShahrdariApp.getShahrdariDatabaseHelper().getAreaDao().queryBuilder();
            //licenseDao.leftJoin(contractorDao);
            licenseDao.leftJoin(dischargeDao);
            licenseDao.leftJoin(areaDoa);

            try {
                i = Integer.valueOf(searchStr);
                lst = licenseDao.orderBy(Params.LICENSE_ID, false)
                        .where().eq(Params.LICENSE_ID, i)
                        .and().in(Params.IS_CONFIRM_From_Personnel_1, true)
                        .and().ne(Params.PERSONNEL_ID, id)
                        .query();

                if (lst.size() == 0) {
//                    if(i==0)
//                        contractorDao.where().like(Params.TITLE, "%"+searchStr+"%");
//                    else
//                        contractorDao.where().like(Params.CONTRACTOR_ID, i);
                    licenseDao.leftJoin(contractorDao);
                    lst = licenseDao
                            .orderBy(Params.LICENSE_ID, false)
                            .where().in(Params.IS_CONFIRM_From_Personnel_1, true)
                            .and().ne(Params.PERSONNEL_ID, id)
                            .query();
                }
            }catch (NumberFormatException e) {
//                if(i==0)
//                    contractorDao.where().like(Params.TITLE, "%"+searchStr+"%");
//                else
//                    contractorDao.where().like(Params.CONTRACTOR_ID, i);
                licenseDao.leftJoin(contractorDao);
                lst = licenseDao
                        .orderBy(Params.LICENSE_ID, false)
                        .where().in(Params.IS_CONFIRM_From_Personnel_1, true)
                        .and().ne(Params.PERSONNEL_ID, id)
                        .query();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            lst = new ArrayList<>();
        }
        //////////////////////////////////////search on staffcar
        if(lst.size()>0)
        {
            try {
                for(int j=0;j<lst.size();j++)
                {
                    if(lst.get(j).getStaffCarIdListJson().contains(searchStr)) {

                        lst1.add(lst.get(j));
                    }
                }
            }
            catch (NumberFormatException e) {
                lst1 = new ArrayList<>();
            }
        }

        return lst1;
    }

    public static List<License> getNotSendLicenseByDate(int id, Date from, Date to){
        List<License> lst = null;
        try {
            QueryBuilder<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao().queryBuilder();
            QueryBuilder<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao().queryBuilder();
            QueryBuilder<Discharge, Integer> dischargeDao = ShahrdariApp.getShahrdariDatabaseHelper().getDischargeDao().queryBuilder();
            QueryBuilder<Area, Integer> areaDoa = ShahrdariApp.getShahrdariDatabaseHelper().getAreaDao().queryBuilder();
            licenseDao.leftJoin(contractorDao);
            licenseDao.leftJoin(dischargeDao);
            licenseDao.leftJoin(areaDoa);
            lst = licenseDao.orderBy(Params.LICENSE_ID, false)
                    .where()
                    .ne(Params.PERSONNEL_ID, id).and()
                    .ge(License.FROM_DATE, from).and().le(License.TO_DATE, to).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lst;
    }

    public static List<License> getNotSendLicenseByDateStaffCar(int id, Date from, Date to,String searchStr){
        List<License> lst = null,lst1 = new ArrayList<License>();
        int i=0;
        try {
            QueryBuilder<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao().queryBuilder();
            QueryBuilder<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao().queryBuilder();
            QueryBuilder<Discharge, Integer> dischargeDao = ShahrdariApp.getShahrdariDatabaseHelper().getDischargeDao().queryBuilder();
            QueryBuilder<Area, Integer> areaDoa = ShahrdariApp.getShahrdariDatabaseHelper().getAreaDao().queryBuilder();
            /*licenseDao.leftJoin(contractorDao);
            licenseDao.leftJoin(dischargeDao);
            licenseDao.leftJoin(areaDoa);*/

            try {
                 i = Integer.valueOf(searchStr);
                lst = licenseDao.orderBy(Params.LICENSE_ID, false)
                        .where().ge(License.FROM_DATE, from)
                        .and()
                        .le(License.TO_DATE, to)
                        .and()
                        .eq(Params.LICENSE_ID, i)
                        .and().ne(Params.PERSONNEL_ID, id)
                        .query();
                if (lst.size() == 0) {
//                    if(i==0)
//                      contractorDao.where().like(Params.TITLE, "%"+searchStr+"%");
//                    else
//                        contractorDao.where().like(Params.CONTRACTOR_ID, i);
                    licenseDao.leftJoin(contractorDao);
                    lst = licenseDao.orderBy(Params.LICENSE_ID, false)
                            .where().ge(License.FROM_DATE, from)
                            .and()
                            .le(License.TO_DATE, to)
                            .and().ne(Params.PERSONNEL_ID, id)
                            .query();
                }
            }catch (NumberFormatException e) {

            }
        } catch (SQLException e) {
            e.printStackTrace();
            lst = new ArrayList<>();
        }

        if(lst.size()>0)
        {
            try {
                for(int j=0;j<lst.size();j++)
                {
                    if(lst.get(j).getStaffCarIdListJson().contains(searchStr)) {

                        lst1.add(lst.get(j));
                    }
                }
            }
            catch (NumberFormatException e) {
                lst1 = new ArrayList<>();
            }
        }

        return lst1;
    }
    public static List<License> getNotSendLicenseByDate(int id, Date from, Date to,String searchStr){
        List<License> lst = null;
        int i=0;
        try {
            QueryBuilder<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao().queryBuilder();
            QueryBuilder<Contractor, Integer> contractorDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao().queryBuilder();
            QueryBuilder<Discharge, Integer> dischargeDao = ShahrdariApp.getShahrdariDatabaseHelper().getDischargeDao().queryBuilder();
            QueryBuilder<Area, Integer> areaDoa = ShahrdariApp.getShahrdariDatabaseHelper().getAreaDao().queryBuilder();
            /*licenseDao.leftJoin(contractorDao);
            licenseDao.leftJoin(dischargeDao);
            licenseDao.leftJoin(areaDoa);*/

            try {
                i = Integer.valueOf(searchStr);
                lst = licenseDao.orderBy(Params.LICENSE_ID, false)
                        .where().ge(License.FROM_DATE, from)
                        .and()
                        .le(License.TO_DATE, to)
                        .and()
                        .eq(Params.LICENSE_ID, i)
                        .and().ne(Params.PERSONNEL_ID, id)
                        .query();
                if (lst.size() == 0) {
                    if(i==0)
                        contractorDao.where().like(Params.TITLE, "%"+searchStr+"%");
                    else
                        contractorDao.where().like(Params.CONTRACTOR_ID, i);
                    licenseDao.leftJoin(contractorDao);
                    lst = licenseDao.orderBy(Params.LICENSE_ID, false)
                            .where().ge(License.FROM_DATE, from)
                            .and()
                            .le(License.TO_DATE, to)
                            .and().ne(Params.PERSONNEL_ID, id)
                            .query();
                }
            }catch (NumberFormatException e) {
                if(i==0)
                    contractorDao.where().like(Params.TITLE, "%"+searchStr+"%");
                else
                    contractorDao.where().like(Params.CONTRACTOR_ID, i);
                licenseDao.leftJoin(contractorDao);
                lst = licenseDao.orderBy(Params.LICENSE_ID, false)
                        .where().ge(License.FROM_DATE, from)
                        .and()
                        .le(License.TO_DATE, to)
                        .and().ne(Params.PERSONNEL_ID, id)
                        .query();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            lst = new ArrayList<>();
        }
        return lst;
    }

    public static void setSendViolationMember(int staffCarViolationId){
        try {
            Dao<StaffCarViolation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getStaffCarViolationDao();
            StaffCarViolation violationMember = violationDao.queryForId(staffCarViolationId);
            //violationMember.setSend(true);
            violationDao.update(violationMember);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setSendViolationNotMember(int violationNotMemberId){
        try {
            Dao<CarViolation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarViolationDao();
            CarViolation violationNotMember = violationDao.queryForId(violationNotMemberId);
            //violationNotMember.setSend(true);
            violationDao.update(violationNotMember);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setSendViolationEstate(int violationEstateId){
        try {
            Dao<HomeViolation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getHomeViolationDao();
            HomeViolation violationEstate = violationDao.queryForId(violationEstateId);
            //violationEstate.setSend(true);
            violationDao.update(violationEstate);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getViolationTypeIdByTitle(String title){
        String result = "";
        try {
            Dao<Violation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getViolationDao();
            result = Integer.toString(violationDao.queryBuilder().where().eq(ViolationType.FIELD_TITLE, title.trim()).queryForFirst().getViolationId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void setAllViolationMemberSent(){
        try {
            Dao<StaffCarViolation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getStaffCarViolationDao();
            UpdateBuilder<StaffCarViolation, Integer> updateBuilder = violationDao.updateBuilder();
            updateBuilder.updateColumnValue(ViolationMember.FIELD_SEND, true);
            updateBuilder.update();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setAllViolationNotMemberSent(){
        try {
            Dao<CarViolation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getCarViolationDao();
            UpdateBuilder<CarViolation, Integer> updateBuilder = violationDao.updateBuilder();
            updateBuilder.updateColumnValue(ViolationNotMember.FIELD_SEND, true);
            updateBuilder.update();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setAllViolationEstateSent(){
        try {
            Dao<HomeViolation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getHomeViolationDao();
            UpdateBuilder<HomeViolation, Integer> updateBuilder = violationDao.updateBuilder();
            updateBuilder.updateColumnValue(ViolationEstate.SEND, true);
            updateBuilder.update();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setAllConfirmLicenseSent(){
        /*try {
            Dao<License, Integer> licenseDao = ShahrdariApp.getShahrdariDatabaseHelper().getLicenseDao();
            UpdateBuilder<License, Integer> updateBuilder = licenseDao.updateBuilder();
            updateBuilder.updateColumnValue(License.FIELD_CONFIRM_PERSONNEL_ID, -1);
            updateBuilder.update();
        } catch (SQLException e) {
            e.printStackTrace();
        }*/
    }

    public static void UpdateViolation(Violation violation) {
        try {
            Dao<Violation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getViolationDao();
            violationDao.update(violation);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void UpdateCertificate(Certificate certificate) {
        try {
            Dao<Certificate, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getCertificateDao();
            violationDao.update(certificate);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void UpdateContract(Contractor contractor) {
        try {
            Dao<Contractor, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getContractorDao();
            violationDao.update(contractor);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void UpdateArea(Area area) {
        try {
            Dao<Area, Integer> areaDao = ShahrdariApp.getShahrdariDatabaseHelper().getAreaDao();
            areaDao.update(area);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void UpdateDischarge(Discharge discharge) {
        try {
            Dao<Discharge, Integer> dischargeDao = ShahrdariApp.getShahrdariDatabaseHelper().getDischargeDao();
            dischargeDao.update(discharge);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void UpdatePersonnels(Personnel personnel) {
        try {
            Dao<Personnel, Integer> personnelDao = ShahrdariApp.getShahrdariDatabaseHelper().getPersonnelDao();
            personnelDao.update(personnel);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void UpdateStaffCar(StaffCar staffCar) {
        try {
            Dao<StaffCar, Integer> staffCarDao = ShahrdariApp.getShahrdariDatabaseHelper().getStaffCarDao();
            staffCarDao.update(staffCar);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Violation getViolationById(int id){
        try {
            Dao<Violation, Integer> violationDao = ShahrdariApp.getShahrdariDatabaseHelper().getViolationDao();
            return violationDao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Area getAreaById(int id){
        try {
            Dao<Area, Integer> areaDao = ShahrdariApp.getShahrdariDatabaseHelper().getAreaDao();
            return areaDao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Discharge getDischargeById(int id){
        try {
            Dao<Discharge, Integer> dischargeDao = ShahrdariApp.getShahrdariDatabaseHelper().getDischargeDao();
            return dischargeDao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Officer getOfficerById(int id){
        try {
            Dao<Officer, Integer> officerIntegerDao = ShahrdariApp.getShahrdariDatabaseHelper().getOfficerDao();
            return officerIntegerDao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getPersonnelsCount(){
        try {
            List<Personnel> personnels=null;
            Dao<Personnel, Integer> personnelDao = ShahrdariApp.getShahrdariDatabaseHelper().getPersonnelDao();
            personnels= personnelDao.queryForAll();
            if(personnels!=null)
                return personnels.size();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int getOfficerCount(){
        try {
            List<Officer> officers=null;
            Dao<Officer, Integer> personnelDao = ShahrdariApp.getShahrdariDatabaseHelper().getOfficerDao();
            officers= personnelDao.queryForAll();
            if(officers!=null)
                return officers.size();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
