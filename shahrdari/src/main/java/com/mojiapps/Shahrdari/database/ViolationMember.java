package com.mojiapps.Shahrdari.database;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by mojtaba on 10/8/14.
 */

@DatabaseTable
public class ViolationMember {
    public static final String FIELD_ID = "VIOLATION_ID";
    public static final String FIELD_PERSONNEL_ID = "PersonnelId";
    public static final String FIELD_CAR_ID = "CarId";
    public static final String FIELD_ADDRESS_VIOLATION = "AddressViolation";
    public static final String FIELD_DATE = "Date";
    public static final String FIELD_GPS_LOCATION = "GpsLocation";
    public static final String FIELD_ISSUED_CERTIFICATE_ID = "IssuedCertificateId";
    public static final String FIELD_VIOLATION_LIST = "ViolationList";
    public static final String FIELD_VIOLATION_LIST_STRING = "ViolationListString";
    public static final String FIELD_PHOTO = "Photo";
    public static final String FIELD_SEND = "Send";

    @DatabaseField(generatedId = true, columnName = FIELD_ID)
    private int id;

    @DatabaseField(columnName = FIELD_PERSONNEL_ID)
    private int personnelId;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = FIELD_CAR_ID)
    private CarOLD car;

    @DatabaseField(columnName = FIELD_ADDRESS_VIOLATION)
    private String address;

    @DatabaseField(dataType = DataType.DATE_STRING, columnName = FIELD_DATE)
    private Date date;

    @DatabaseField(columnName = FIELD_GPS_LOCATION)
    private String gpsLocation;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = FIELD_ISSUED_CERTIFICATE_ID)
    private CertificateType issuedCertificate;

    @DatabaseField(columnName = FIELD_VIOLATION_LIST)
    private String violationList;

    @DatabaseField(columnName = FIELD_VIOLATION_LIST_STRING)
    private String violationListString;

    @DatabaseField(columnName = FIELD_PHOTO)
    private String photo;

    @DatabaseField(columnName = FIELD_SEND)
    private boolean send;

    ViolationMember(){}

    public ViolationMember(int personnelId, CarOLD car, String address, Date date, String gpsLocation, CertificateType issuedCertificate, String violationList, String violationListString, String photo) {
        this.personnelId = personnelId;
        this.car = car;
        this.address = address;
        this.date = date;
        this.gpsLocation = gpsLocation;
        this.issuedCertificate = issuedCertificate;
        this.violationList = violationList;
        this.violationListString = violationListString;
        this.photo = photo;
        this.send = false;
    }

    public int getId() {
        return id;
    }

    public int getPersonnelId() {
        return personnelId;
    }

    public void setPersonnelId(int personnelId) {
        this.personnelId = personnelId;
    }

    public CarOLD getCar() {
        return car;
    }

    public void setCar(CarOLD car) {
        this.car = car;
    }

    public String getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public CertificateType getIssuedCertificate() {
        return issuedCertificate;
    }

    public void setIssuedCertificate(CertificateType issuedCertificate) {
        this.issuedCertificate = issuedCertificate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getViolationList() {
        return violationList;
    }

    public void setViolationList(String violationList) {
        this.violationList = violationList;
    }

    public String getViolationListString() {
        return violationListString;
    }

    public void setViolationListString(String violationListString) {
        this.violationListString = violationListString;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public boolean isSend() {
        return send;
    }

    public void setSend(boolean send) {
        this.send = send;
    }
}
