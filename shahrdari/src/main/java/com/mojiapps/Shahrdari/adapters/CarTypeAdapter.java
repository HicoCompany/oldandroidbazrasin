package com.mojiapps.Shahrdari.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.ShahrdariApp;
import com.mojiapps.Shahrdari.database.CarType;
import com.mojiapps.Shahrdari.enums.Fonts;
import com.mojiapps.Shahrdari.models.Car;
import com.mojiapps.Shahrdari.utils.Texts;

import java.util.List;

/**
 * Created by mojtaba on 24/10/2014.
 */
public class CarTypeAdapter extends ArrayAdapter<Car> {
    private List<Car> values;
    TextView txtTitle;

    public CarTypeAdapter(Context context, int textViewResourceId, List<Car> data){
        super(context, textViewResourceId, data);
        this.values = data;
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) ShahrdariApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = convertView;

        if(row == null)
            row = inflater.inflate(R.layout.certificate_spinner_row, parent, false);

        txtTitle = (TextView) row.findViewById(R.id.txtTitle);

        if (position == getCount()) {
            txtTitle.setHint(getItem(getCount()).getTitle());
            txtTitle.setText("");
        }
        else {
            txtTitle.setHint("");
            txtTitle.setText(values.get(position).getTitle());
        }
//        txtTitle.setTypeface(Texts.getFont(Fonts.DEFAULT));

        return row;
    }

    @Override
    public int getCount() {
        return super.getCount() - 1;
    }
}
