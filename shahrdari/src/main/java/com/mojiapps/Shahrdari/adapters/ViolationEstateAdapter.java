package com.mojiapps.Shahrdari.adapters;

import android.animation.ValueAnimator;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.models.HomeViolation;
import com.mojiapps.Shahrdari.models.Violation;
import com.mojiapps.Shahrdari.utils.DateUtils;
import com.mojiapps.Shahrdari.utils.Utils;

import java.util.List;

/**
 * Created by mojtaba on 10/10/14.
 */
public class ViolationEstateAdapter extends ArrayAdapter<HomeViolation>{

    private List<HomeViolation> violationList;
    private Context context;

    ImageView imgViolation;
    TextView txtOwnerName;
    TextView txtAddress;
    TextView txtViolationType;
    TextView txtDate;
    TextView Description;
    TextView violationActDescription;
    View violationActDescriptionStatus;
    CardView cardView;
    LinearLayout moreLayout;


    public ViolationEstateAdapter(Context context, int resource, int textViewResourceId, List<HomeViolation> objects) {
        super(context, resource, textViewResourceId, objects);
        this.violationList = objects;
        this.context = context;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View cell = convertView;
        if(cell==null){
            cell = inflater.inflate(R.layout.violation_estate_row, parent,false);
        }
        init(cell);
        cell.setTag(violationList.get(position).getHomeViolationId());

        txtOwnerName.setText(violationList.get(position).getOwName());
        txtAddress.setText(violationList.get(position).getAddress());

        String violations = "";
        for (Violation violation : violationList.get(position).getViolationList()) {
            violations += violation.getTitle()+" - ";
        }
        violations = violations.substring(0, violations.length()-3);
        txtViolationType.setText(violations);

        txtDate.setText(DateUtils.MiladiToPersianDateTime(violationList.get(position).getRegisterDate()));

        Description.setText(violationList.get(position).getDescription());
        // TODO: 18/06/2018 Moghari Home Violation
        violationActDescription.setText(violationList.get(position).getViolationActDescription());

        if (violationList.get(position).getImage1() != null)
            if (violationList.get(position).getImage1().length() > 0)
                imgViolation.setImageBitmap(Utils.GetBitmapFromBase64(violationList.get(position).getImage1()));

        if (violationList.get(position).getViolationActDescription() != null)
            if (violationList.get(position).getViolationActDescription().length() > 0)
                violationActDescriptionStatus.setBackgroundColor(getContext().getResources().getColor(R.color.colorAccent));

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            if (position % 2 == 0)
                cell.setBackground(context.getResources().getDrawable(R.drawable.list_selector1));
            else
                cell.setBackground(context.getResources().getDrawable(R.drawable.list_selector2));
        }
        else{
            if (position % 2 == 0)
                cell.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.list_selector1));
            else
                cell.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.list_selector2));
        }*/
        return cell;
    }
    int descriptionViewFullHeight;
    int minHeight;
    private void init(View cell){
        imgViolation = (ImageView) cell.findViewById(R.id.imgViolation);
        txtOwnerName = (TextView) cell.findViewById(R.id.txtOwner);
        txtAddress = (TextView) cell.findViewById(R.id.txtAddress);
        txtViolationType = (TextView) cell.findViewById(R.id.txtViolationType);
        txtDate = (TextView) cell.findViewById(R.id.txtDate);
        Description = (TextView) cell.findViewById(R.id.txtDescription);
        violationActDescriptionStatus = (View) cell.findViewById(R.id.violation_act_description_status);
        violationActDescription= (TextView) cell.findViewById(R.id.txtViolationActDescription);
        moreLayout = (LinearLayout) cell.findViewById(R.id.more_layout);
        cardView = (CardView) cell.findViewById(R.id.card_view);

        cardView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver
                .OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                cardView.getViewTreeObserver().removeOnPreDrawListener(this);
                // save the full height
                descriptionViewFullHeight  = cardView.getHeight() + moreLayout.getHeight();

                minHeight = cardView.getHeight();

                // initially changing the height to min height
                ViewGroup.LayoutParams layoutParams = cardView.getLayoutParams();
                layoutParams.height = cardView.getHeight();
                cardView.setLayoutParams(layoutParams);

                return true;
            }
        });

        imgViolation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                toggleCardViewnHeight(cardView);

            }
        });
    }

    private void toggleCardViewnHeight(final CardView cardView) {

        if (cardView.getHeight() == minHeight) {
            // expand
            ValueAnimator anim = ValueAnimator.ofInt(cardView.getMeasuredHeightAndState(),
                    descriptionViewFullHeight);
            anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    int val = (Integer) valueAnimator.getAnimatedValue();
                    ViewGroup.LayoutParams layoutParams = cardView.getLayoutParams();
                    layoutParams.height = val;
                    cardView.setLayoutParams(layoutParams);
                }
            });
            anim.start();
        } else {
            // collapse
            ValueAnimator anim = ValueAnimator.ofInt(cardView.getMeasuredHeightAndState(),
                    minHeight);
            anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    int val = (Integer) valueAnimator.getAnimatedValue();
                    ViewGroup.LayoutParams layoutParams = cardView.getLayoutParams();
                    layoutParams.height = val;
                    cardView.setLayoutParams(layoutParams);
                }
            });
            anim.start();
        }
    }

    public void collapseView() {

        ValueAnimator anim = ValueAnimator.ofInt(cardView.getMeasuredHeightAndState(),
                minHeight);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = cardView.getLayoutParams();
                layoutParams.height = val;
                cardView.setLayoutParams(layoutParams);

            }
        });
        anim.start();
    }

    public void expandView(int height) {

        ValueAnimator anim = ValueAnimator.ofInt(cardView.getMeasuredHeightAndState(),
                height);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = cardView.getLayoutParams();
                layoutParams.height = val;
                cardView.setLayoutParams(layoutParams);
            }
        });
        anim.start();

    }

}
