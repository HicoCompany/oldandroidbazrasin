package com.mojiapps.Shahrdari.adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.models.License;
import com.mojiapps.Shahrdari.utils.CalendarTool;
import com.mojiapps.Shahrdari.utils.Texts;

import java.util.Calendar;
import java.util.List;

/**
 * Created by mojtaba on 9/24/14.
 */
public class LicenseAdapter extends ArrayAdapter<License> {

    private List<License> licenseList;
    private Context context;


    TextView txtContractorLabel;
    TextView txtEmployerLabel;
    TextView txtAddressLabel;
    TextView txtDisLocationLabel;
    TextView txtFromDateLabel;
    TextView txtToDateLabel;

    TextView txtCode;
    TextView txtContractor;
    TextView txtEmployer;
    TextView txtAddress;
    TextView txtDisLocation;
    TextView txtFromDate;
    TextView txtToDate;

    ImageView imgVerified;

    public LicenseAdapter(Context context, int resource, int textViewResourceId, List<License> objects) {
        super(context, resource, textViewResourceId, objects);
        this.licenseList = objects;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View cell = convertView;
        if(cell==null){
            cell = inflater.inflate(R.layout.license_row, parent,false);
        }
        init(cell);
        cell.setTag(licenseList.get(position).getLicenseId());

        txtCode.setText(Texts.ChangeToUnicode(Integer.toString(licenseList.get(position).getLicenseId())));

        /*if (licenseList.get(position).isVerified())
            imgVerified.setVisibility(View.VISIBLE);
        else
            imgVerified.setVisibility(View.GONE);*/
        /*txtEmployer.setText(licenseList.get(position).getEmployerName());
        txtContractor.setText(licenseList.get(position).getContractorOLD() == null ? "" : licenseList.get(position).getContractorOLD().getName());
        txtDisLocation.setText(licenseList.get(position).getDischargeLocation());*/
        txtAddress.setText(licenseList.get(position).getAddress());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(licenseList.get(position).getFromDate());
        txtFromDate.setText(Texts.ChangeToUnicode(CalendarTool.SiminehCalendar.getIranianDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))));

        calendar.setTime(licenseList.get(position).getToDate());
        txtToDate.setText(Texts.ChangeToUnicode(CalendarTool.SiminehCalendar.getIranianDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))));


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            if (position % 2 == 0)
                cell.setBackground(context.getResources().getDrawable(R.drawable.list_selector1));
            else
                cell.setBackground(context.getResources().getDrawable(R.drawable.list_selector2));
        }
        else{
            if (position % 2 == 0)
                cell.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.list_selector1));
            else
                cell.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.list_selector2));
        }
        return cell;
    }

    private void init(View cell){
        /*txtContractorLabel = (TextView) cell.findViewById(R.id.txtContractorLabel);
        txtEmployerLabel = (TextView) cell.findViewById(R.id.txtEmployerLabel);
        txtAddressLabel = (TextView) cell.findViewById(R.id.txtAddressLabel);
        txtDisLocationLabel = (TextView) cell.findViewById(R.id.txtDischargeLocationLabel);
        txtFromDateLabel = (TextView) cell.findViewById(R.id.txtFromDateLabel);
        txtToDateLabel = (TextView) cell.findViewById(R.id.txtToDateLabel);

        txtCode = (TextView) cell.findViewById(R.id.txtID);
        txtContractor = (TextView) cell.findViewById(R.id.txtContractor);
        txtEmployer = (TextView) cell.findViewById(R.id.txtEmployer);
        txtAddress = (TextView) cell.findViewById(R.id.txtAddress);
        txtDisLocation = (TextView) cell.findViewById(R.id.txtDischargeLocation);
        txtFromDate = (TextView) cell.findViewById(R.id.txtFromDate);
        txtToDate = (TextView) cell.findViewById(R.id.txtToDate);

        imgVerified = (ImageView) cell.findViewById(R.id.imgVeridied);*/

        /*txtContractorLabel.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtEmployerLabel.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtAddressLabel.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtDisLocationLabel.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtFromDateLabel.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtToDateLabel.setTypeface(Texts.getFont(Fonts.YEKAN));

        txtCode.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtEmployer.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtContractor.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtDisLocation.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtAddress.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtFromDate.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtToDate.setTypeface(Texts.getFont(Fonts.YEKAN));*/
    }
}
