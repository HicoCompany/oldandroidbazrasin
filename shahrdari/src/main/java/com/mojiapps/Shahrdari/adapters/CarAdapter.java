package com.mojiapps.Shahrdari.adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.database.CarOLD;
import com.mojiapps.Shahrdari.enums.Fonts;
import com.mojiapps.Shahrdari.models.Car;
import com.mojiapps.Shahrdari.utils.CalendarTool;
import com.mojiapps.Shahrdari.utils.Texts;

import java.util.Calendar;
import java.util.List;

/**
 * Created by mojtaba on 9/24/14.
 */
public class CarAdapter extends ArrayAdapter<Car> {

    private List<Car> carList;
    private Context context;

    TextView txtContractorLabel;
    TextView txtOwnerLabel;
    TextView txtTypeLabel;
    TextView txtPlateLabel;

    TextView txtCode;
    TextView txtContractor;
    TextView txtOwner;
    TextView txtType;
    TextView txtPlate;
    TextView txtDate;

    public CarAdapter(Context context, int resource, int textViewResourceId, List<Car> objects) {
        super(context, resource, textViewResourceId, objects);
        this.carList = objects;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View cell = convertView;
        if(cell==null){
            cell = inflater.inflate(R.layout.car_row, parent,false);
        }
        init(cell);
        cell.setTag(carList.get(position).getCarId());

        txtCode.setText(Texts.ChangeToUnicode(Integer.toString(carList.get(position).getCarId())));

        txtContractor.setText("-");
        txtOwner.setText(carList.get(position).getTitle());
        txtType.setText(carList.get(position).getDigger().toString());
        txtPlate.setText("-");

        /*Calendar calendar = Calendar.getInstance();
        calendar.setTime(carList.get(position).getDateActivity());
        txtDate.setText(Texts.ChangeToUnicode(CalendarTool.SiminehCalendar.getIranianDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))));*/


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            if (position % 2 == 0)
                cell.setBackground(context.getResources().getDrawable(R.drawable.list_selector1));
            else
                cell.setBackground(context.getResources().getDrawable(R.drawable.list_selector2));
        }
        else{
            if (position % 2 == 0)
                cell.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.list_selector1));
            else
                cell.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.list_selector2));
        }
        return cell;
    }

    private void init(View cell){
        //txtContractorLabel = (TextView) cell.findViewById(R.id.txtContractorLabel);
        /*txtOwnerLabel = (TextView) cell.findViewById(R.id.txtOwnerLabel);
        txtTypeLabel = (TextView) cell.findViewById(R.id.txtTypeLabel);
        txtPlateLabel = (TextView) cell.findViewById(R.id.txtPlateLabel);*/

        txtCode = (TextView) cell.findViewById(R.id.txtID);
        txtContractor = (TextView) cell.findViewById(R.id.txtContractor);
        txtOwner = (TextView) cell.findViewById(R.id.txtOwner);
        //txtType = (TextView) cell.findViewById(R.id.txtType);
        txtPlate = (TextView) cell.findViewById(R.id.txtPlate);
        txtDate = (TextView) cell.findViewById(R.id.txtDate);

        /*txtContractorLabel.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtOwnerLabel.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtTypeLabel.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtPlateLabel.setTypeface(Texts.getFont(Fonts.YEKAN));

        txtCode.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtContractor.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtOwner.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtType.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtPlate.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtDate.setTypeface(Texts.getFont(Fonts.YEKAN));*/
    }
}
