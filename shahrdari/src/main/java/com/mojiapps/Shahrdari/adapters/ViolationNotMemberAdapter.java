package com.mojiapps.Shahrdari.adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.database.ViolationNotMember;
import com.mojiapps.Shahrdari.database.ViolationType;
import com.mojiapps.Shahrdari.enums.Fonts;
import com.mojiapps.Shahrdari.models.CarViolation;
import com.mojiapps.Shahrdari.utils.CalendarTool;
import com.mojiapps.Shahrdari.utils.Texts;

import java.util.Calendar;
import java.util.List;

/**
 * Created by mojtaba on 10/10/14.
 */
public class ViolationNotMemberAdapter extends ArrayAdapter<CarViolation> {

    private List<CarViolation> violationList;
    private Context context;

    TextView txtOwnerNameLabel;
    TextView txtCarTypeLabel;
    TextView txtPlateNumberLabel;
    TextView txtAddressLabel;
    TextView txtViolationTypeLabel;

    TextView txtOwnerName;
    TextView txtCarType;
    TextView txtPlateNumber;
    TextView txtAddress;
    TextView txtViolationType;
    TextView txtDate;

    public ViolationNotMemberAdapter(Context context, int resource, int textViewResourceId, List<CarViolation> objects) {
        super(context, resource, textViewResourceId, objects);
        this.violationList = objects;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View cell = convertView;
        if(cell==null){
            cell = inflater.inflate(R.layout.violation_not_member_row, parent,false);
        }
        init(cell);
        cell.setTag(violationList.get(position).getCarViolationId());

        txtOwnerName.setText(violationList.get(position).getOwName());
        txtCarType.setText(violationList.get(position).getCar().getTitle());
        txtPlateNumber.setText(violationList.get(position).getPlateCode());
        txtAddress.setText(violationList.get(position).getAddress());

        txtViolationType.setText(violationList.get(position).getViolationList().toString());

        /*Calendar calendar = Calendar.getInstance();
        calendar.setTime(violationList.get(position).getData());
        txtDate.setText(Texts.ChangeToUnicode(CalendarTool.SiminehCalendar.getIranianDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))));
*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            if (position % 2 == 0)
                cell.setBackground(context.getResources().getDrawable(R.drawable.list_selector1));
            else
                cell.setBackground(context.getResources().getDrawable(R.drawable.list_selector2));
        }
        else{
            if (position % 2 == 0)
                cell.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.list_selector1));
            else
                cell.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.list_selector2));
        }
        return cell;
    }

    private void init(View cell){
        //txtOwnerNameLabel = (TextView) cell.findViewById(R.id.txtOwnerLabel);
        //txtCarTypeLabel = (TextView) cell.findViewById(R.id.txtCarTypeLabel);
        //txtPlateNumberLabel = (TextView) cell.findViewById(R.id.txtPlateNumberLabel);
        //txtAddressLabel = (TextView) cell.findViewById(R.id.txtAddressLabel);
        //txtViolationTypeLabel = (TextView) cell.findViewById(R.id.txtViolationTypeLabel);

        txtOwnerName = (TextView) cell.findViewById(R.id.txtOwner);
        txtCarType = (TextView) cell.findViewById(R.id.txtCarType);
        txtPlateNumber = (TextView) cell.findViewById(R.id.txtPlateNumber);
        txtAddress = (TextView) cell.findViewById(R.id.txtAddress);
        txtViolationType = (TextView) cell.findViewById(R.id.txtViolationType);
        txtDate = (TextView) cell.findViewById(R.id.txtDate);

        /*txtOwnerNameLabel.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtCarTypeLabel.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtPlateNumberLabel.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtAddressLabel.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtViolationTypeLabel.setTypeface(Texts.getFont(Fonts.YEKAN));*/

        txtOwnerName.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtCarType.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtPlateNumber.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtAddress.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtViolationType.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtDate.setTypeface(Texts.getFont(Fonts.YEKAN));
    }

}
