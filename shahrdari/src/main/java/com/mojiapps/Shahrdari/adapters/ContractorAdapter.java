package com.mojiapps.Shahrdari.adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.enums.Fonts;
import com.mojiapps.Shahrdari.models.Contractor;
import com.mojiapps.Shahrdari.utils.CalendarTool;
import com.mojiapps.Shahrdari.utils.Texts;

import java.util.Calendar;
import java.util.List;

/**
 * Created by mojtaba on 9/24/14.
 */
public class ContractorAdapter extends ArrayAdapter<Contractor> {

    private List<Contractor> contractorList;
    private Context context;

    TextView txtNameLabel;
    TextView txtManagerLabel;
    TextView txtAddressLabel;

    TextView txtCode;
    TextView txtName;
    TextView txtManager;
    TextView txtAddress;
    TextView txtDate;

    public ContractorAdapter(Context context, int resource, int textViewResourceId, List<Contractor> objects) {
        super(context, resource, textViewResourceId, objects);
        this.contractorList = objects;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View cell = convertView;
        if(cell==null){
            cell = inflater.inflate(R.layout.contractor_row, parent,false);
        }
        init(cell);
        cell.setTag(contractorList.get(position).getContractorId());

        txtCode.setText(Texts.ChangeToUnicode(Integer.toString(contractorList.get(position).getContractorId())));

        txtName.setText(contractorList.get(position).getOwName());
        txtManager.setText(contractorList.get(position).getTitle());
        txtAddress.setText(contractorList.get(position).getOwMobile());

        /*Calendar calendar = Calendar.getInstance();
        calendar.setTime(contractorList.get(position).getDateActivity());
        txtDate.setText(Texts.ChangeToUnicode(CalendarTool.SiminehCalendar.getIranianDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))));*/


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            if (position % 2 == 0)
                cell.setBackground(context.getResources().getDrawable(R.drawable.list_selector1));
            else
                cell.setBackground(context.getResources().getDrawable(R.drawable.list_selector2));
        }
        else{
            if (position % 2 == 0)
                cell.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.list_selector1));
            else
                cell.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.list_selector2));
        }
        return cell;
    }

    private void init(View cell){
        //txtNameLabel = (TextView) cell.findViewById(R.id.txtNameLabel);
        //txtManagerLabel = (TextView) cell.findViewById(R.id.txtManagerLabel);
        //txtAddressLabel = (TextView) cell.findViewById(R.id.txtAddressLabel);

        txtCode = (TextView) cell.findViewById(R.id.txtID);
        txtName = (TextView) cell.findViewById(R.id.txtName);
        //txtManager = (TextView) cell.findViewById(R.id.txtManager);
        txtAddress = (TextView) cell.findViewById(R.id.txtAddress);
        txtDate = (TextView) cell.findViewById(R.id.txtDate);

        /*txtNameLabel.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtManagerLabel.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtAddressLabel.setTypeface(Texts.getFont(Fonts.YEKAN));

        txtCode.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtName.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtManager.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtAddress.setTypeface(Texts.getFont(Fonts.YEKAN));
        txtDate.setTypeface(Texts.getFont(Fonts.YEKAN));*/
    }
}
