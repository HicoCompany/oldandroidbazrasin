package com.mojiapps.Shahrdari.adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mojiapps.Shahrdari.R;
import com.mojiapps.Shahrdari.database.ViolationMember;
import com.mojiapps.Shahrdari.enums.Fonts;
import com.mojiapps.Shahrdari.models.StaffCarViolation;
import com.mojiapps.Shahrdari.models.Violation;
import com.mojiapps.Shahrdari.utils.CalendarTool;
import com.mojiapps.Shahrdari.utils.DateUtils;
import com.mojiapps.Shahrdari.utils.Texts;
import com.mojiapps.Shahrdari.utils.Utils;

import java.util.Calendar;
import java.util.List;

/**
 * Created by mojtaba on 10/10/14.
 */
public class ViolationMemberAdapter extends ArrayAdapter<StaffCarViolation> {

    private List<StaffCarViolation> violationList;
    private Context context;

    ImageView imgViolation;
    TextView txtCode;
    TextView txtOwnerName;
    TextView txtAddress;
    TextView txtViolationType;
    TextView txtDate;
    TextView Description;
    View violationActDescriptionStatus;

    public ViolationMemberAdapter(Context context, int resource, int textViewResourceId, List<StaffCarViolation> objects) {
        super(context, resource, textViewResourceId, objects);
        this.violationList = objects;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View cell = convertView;
        if(cell==null){
            cell = inflater.inflate(R.layout.violation_member_row, parent,false);
        }
        init(cell);
        cell.setTag(violationList.get(position).getStaffCarViolationId());

        txtCode.setText(violationList.get(position).getStaffCar().getStaffCarId().toString());
        if (violationList.get(position).getStaffCar() != null)
             txtOwnerName.setText(violationList.get(position).getStaffCar().getOwName());
        txtAddress.setText(violationList.get(position).getAddress());

        String violations = "";
        for (Violation violation : violationList.get(position).getViolationList()) {
            violations += violation.getTitle()+" - ";
        }
        violations = violations.substring(0, violations.length()-3);
        txtViolationType.setText(violations);

        txtDate.setText(DateUtils.MiladiToPersianDateTime(violationList.get(position).getRegisterDate()));

        Description.setText(violationList.get(position).getDescription());

        if (violationList.get(position).getImage1() != null)
            if (violationList.get(position).getImage1().length() > 0)
                imgViolation.setImageBitmap(Utils.GetBitmapFromBase64(violationList.get(position).getImage1()));

        if (violationList.get(position).getViolationActDescription() != null)
            if (violationList.get(position).getViolationActDescription().length() > 0)
                violationActDescriptionStatus.setBackgroundColor(getContext().getResources().getColor(R.color.colorAccent));

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            if (position % 2 == 0)
                cell.setBackground(context.getResources().getDrawable(R.drawable.list_selector1));
            else
                cell.setBackground(context.getResources().getDrawable(R.drawable.list_selector2));
        }
        else{
            if (position % 2 == 0)
                cell.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.list_selector1));
            else
                cell.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.list_selector2));
        }*/
        return cell;
    }

    private void init(View cell){
        imgViolation = (ImageView) cell.findViewById(R.id.imgViolation);
        txtCode = (TextView) cell.findViewById(R.id.txtID);
        txtOwnerName = (TextView) cell.findViewById(R.id.txtOwner);
        txtAddress = (TextView) cell.findViewById(R.id.txtAddress);
        txtViolationType = (TextView) cell.findViewById(R.id.txtViolationType);
        txtDate = (TextView) cell.findViewById(R.id.txtDate);
        Description = (TextView) cell.findViewById(R.id.txtDescription);
        violationActDescriptionStatus = (View) cell.findViewById(R.id.violation_act_description_status);
    }

}
