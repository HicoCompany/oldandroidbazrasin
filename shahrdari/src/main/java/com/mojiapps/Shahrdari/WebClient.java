package com.mojiapps.Shahrdari;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class WebClient {

    public static int NETWORK_NOT_CONNECTED = 0;
    public static int NETWORK_CONNECTED_WIFI = 1;
    public static int NETWORK_CONNECTED_MOBILE = 2;

    // TODO:Must be replaced by below
    public static boolean isNetworkAvailable() {
        return getConnectivityStatus() != NETWORK_NOT_CONNECTED;
    }

    public static int getConnectivityStatus() {
        ConnectivityManager connectivityManager = (ConnectivityManager) ShahrdariApp.getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return NETWORK_CONNECTED_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return NETWORK_CONNECTED_MOBILE;
        }
        return NETWORK_NOT_CONNECTED;
    }
}
